<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Iso2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iso2', 'Iso2:') !!}
    {!! Form::text('iso2', null, ['class' => 'form-control']) !!}
</div>

<!-- Iso3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iso3', 'Iso3:') !!}
    {!! Form::text('iso3', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_code', 'Phone Code:') !!}
    {!! Form::text('phone_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Region Field -->
<div class="form-group col-sm-6">
    {!! Form::label('region', 'Region:') !!}
    {!! Form::text('region', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency', 'Currency:') !!}
    {!! Form::text('currency', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_code', 'Country Code:') !!}
    {!! Form::number('country_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nicename', 'Nice name:') !!}
    {!! Form::text('nicename', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}<br>
    {!! Form::radio('is_active', 1,true).'Active' !!}
    {!! Form::radio('is_active', 0,false).'Inactive' !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('countries.index') !!}" class="btn btn-default">Cancel</a>
</div>
