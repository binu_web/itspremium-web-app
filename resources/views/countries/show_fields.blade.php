<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $country->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $country->name !!}</p>
</div>

<!-- Iso2 Field -->
<div class="form-group">
    {!! Form::label('iso2', 'Iso2:') !!}
    <p>{!! $country->iso2 !!}</p>
</div>

<!-- Iso3 Field -->
<div class="form-group">
    {!! Form::label('iso3', 'Iso3:') !!}
    <p>{!! $country->iso3 !!}</p>
</div>

<!-- Phone Code Field -->
<div class="form-group">
    {!! Form::label('phone_code', 'Phone Code:') !!}
    <p>{!! $country->phone_code !!}</p>
</div>

<!-- Region Field -->
<div class="form-group">
    {!! Form::label('region', 'Region:') !!}
    <p>{!! $country->region !!}</p>
</div>

<!-- Latlng Field -->
<div class="form-group">
    {!! Form::label('latlng', 'Latlng:') !!}
    <p>{!! $country->latlng !!}</p>
</div>

<!-- Currency Field -->
<div class="form-group">
    {!! Form::label('currency', 'Currency:') !!}
    <p>{!! $country->currency !!}</p>
</div>

<!-- Country Code Field -->
<div class="form-group">
    {!! Form::label('country_code', 'Country Code:') !!}
    <p>{!! $country->country_code !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $country->deleted_at !!}</p>
</div>

