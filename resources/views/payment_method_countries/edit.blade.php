@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Payment Method Country
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($paymentMethodCountry, ['route' => ['paymentMethodCountries.update', $paymentMethodCountry->id], 'method' => 'patch']) !!}

                    @include('payment_method_countries.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection