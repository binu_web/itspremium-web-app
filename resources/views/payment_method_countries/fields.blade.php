<!-- Gateway Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gateway_id', 'Gateway Id:') !!}
    {!! Form::number('gateway_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Id Field -->


<div class="form-group col-sm-6">
    {!! Form::label('country_id', 'Country Id:') !!}
    {!! Form::number('country_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::number('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::number('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('paymentMethodCountries.index') !!}" class="btn btn-default">Cancel</a>
</div>
