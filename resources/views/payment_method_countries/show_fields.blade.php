<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
$gateway_id = $paymentMethodCountry->gateway_id;
$country = DB::table ('payment_method_countries')
        ->join ('payment_methods' , 'payment_method_countries.gateway_id' , '=' , 'payment_methods.id')
        ->join ('countries' , 'countries.id' , '=' , 'payment_method_countries.country_id')
        ->join ('admin_users' , 'admin_users.id' , '=' , 'payment_method_countries.updated_by')
        ->where ('payment_method_countries.gateway_id' , '=' , $gateway_id)
        ->select ('payment_method_countries.*' , 'payment_methods.*' , 'admin_users.*' , 'countries.*')
        ->get ();
?>
<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $paymentMethodCountry->id !!}</p>
</div>

<!-- Gateway Id Field -->
<div class="form-group">
    {!! Form::label('gateway_id', 'Gateway Name:') !!}
    <p>
        <?php
        echo $country[0]->gateway_name;
        ?>
    </p>
</div>

<!-- Country Id Field -->


<div class="form-group">
    {!! Form::label('country_id', 'Countries:') !!}
    <p>
        <?php
        $i = 1;
        $count = count ($country);
        foreach ($country as $key => $val) {
            if ($i != 3) {
                $comma = ", ";
            } else {
                $comma = "";

            }
            echo $val->name . $comma;
            $i++;
        }
        ?>
    </p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $paymentMethodCountry->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $paymentMethodCountry->updated_at !!}</p>
</div>

<!-- Created By Field -->
{{--<div class="form-group">--}}
{{--{!! Form::label('created_by', 'Created By:') !!}--}}
{{--<p> {{$country[0]->first_name.' '.$country[0]->last_name}}--}}

{{--{!! $paymentMethodCountry->created_by !!}</p>--}}
{{--</div>--}}

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{$country[0]->first_name.' '.$country[0]->last_name}}
    </p>
</div>

<!-- Deleted At Field -->
{{--<div class="form-group">--}}
{{--{!! Form::label('deleted_at', 'Deleted At:') !!}--}}
{{--<p>{!! $paymentMethodCountry->deleted_at !!}</p>--}}
{{--</div>--}}

