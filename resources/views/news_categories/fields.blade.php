<!-- Name Field -->
<div class="col-sm-12">
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    <div class="form-group col-sm-6">
        {!! Form::label('image', 'Image:') !!}
        {!! Form::file('image',['class' => 'form-control']) !!}
    </div>
</div>

<!-- Is active-->
<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}&nbsp;&nbsp;
    {!! Form::radio('is_active', 1,true).'Active' !!}&nbsp;&nbsp;&nbsp;
    {!! Form::radio('is_active', 0,false).'Inactive' !!}

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('newsCategories.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>
