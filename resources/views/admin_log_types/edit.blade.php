@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Admin Log Type
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($adminLogType, ['route' => ['adminLogTypes.update', $adminLogType->id], 'method' => 'patch']) !!}

                    @include('admin_log_types.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection