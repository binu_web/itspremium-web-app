<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $adminUserLog->id !!}</p>
</div>

<!-- Log Type Field -->
<div class="form-group">
    {!! Form::label('log_type', 'Log Type:') !!}
    <p>{!! $adminUserLog->log_type !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $adminUserLog->user_id !!}</p>
</div>

<!-- Action Field -->
<div class="form-group">
    {!! Form::label('action', 'Action:') !!}
    <p>{!! $adminUserLog->action !!}</p>
</div>

<!-- Ip Field -->
<div class="form-group">
    {!! Form::label('ip', 'Ip:') !!}
    <p>{!! $adminUserLog->ip !!}</p>
</div>

<!-- Latlong Field -->
<div class="form-group">
    {!! Form::label('latlong', 'Latlong:') !!}
    <p>{!! $adminUserLog->latlong !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $adminUserLog->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $adminUserLog->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $adminUserLog->deleted_at !!}</p>
</div>

