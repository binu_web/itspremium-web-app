<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $serviceMetaData->id !!}</p>
</div>

<!-- Meta Key Id Field -->
<div class="form-group">
    {!! Form::label('meta_key_id', 'Meta Key Id:') !!}
    <p>{!! $serviceMetaData->meta_key_id !!}</p>
</div>

<!-- Booked Id Field -->
<div class="form-group">
    {!! Form::label('booked_id', 'Booked Id:') !!}
    <p>{!! $serviceMetaData->booked_id !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $serviceMetaData->value !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $serviceMetaData->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $serviceMetaData->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $serviceMetaData->deleted_at !!}</p>
</div>

