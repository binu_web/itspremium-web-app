<!-- Meta Key Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meta_key_id', 'Meta Key Id:') !!}
    {!! Form::number('meta_key_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Booked Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('booked_id', 'Booked Id:') !!}
    {!! Form::number('booked_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::text('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('serviceMetaDatas.index') !!}" class="btn btn-default">Cancel</a>
</div>
