@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Service Meta Data
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($serviceMetaData, ['route' => ['serviceMetaDatas.update', $serviceMetaData->id], 'method' => 'patch']) !!}

                    @include('service_meta_datas.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection