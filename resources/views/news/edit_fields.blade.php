@section('css')
    <link rel="stylesheet" href="{{ asset("resources/assets/css/bootstrap-datetimepicker.min.css") }}"/>
@stop

<!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category', 'Category:') !!}
    {!! Form::select('category', $newsCategories, null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    @php
        $news = json_decode($news);
    @endphp
    <input type="hidden" name="image_old" id="image_old" value="<?=$news->image?>">

    {!! Form::label('image', 'Image:') !!}

    @if( strpos($news->image, "http") !== FALSE )
        <div class="catimage">
            <img width="100px" src="{{$news->image}}">
            <i class="ion-close-round" style="margin-left: 20px; font-size: 18px; cursor:pointer;"></i>
        </div><br>
    @else
        <div class="catimage">
            <img width="100px" src="{{asset("resources/assets/uploads/images")."/".$news->image}}">
            <i class="ion-close-round" style="margin-left: 20px; font-size: 18px; cursor:pointer;"></i>
        </div><br>
    @endif
    {!! Form::file('image', ['class' => 'form-control']) !!}
</div>

<!-- Source Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('source_link', 'Source Link:') !!}
    {!! Form::text('source_link', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Publish Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('publish_date', 'Publish Date:') !!}
    {!! Form::text('publish_date', null, ['class' => 'form-control']) !!}
</div>

    <!-- Publish Date Field -->

    <div class="form-group col-sm-6 country_selectbox">
        {!! Form::label('country', ' Country:') !!}&nbsp;&nbsp;
        {{Form::select('service_countries',$country,$selectedCountryArr,array('multiple'=>'multiple','class'=>'form-control selectpicker','id'=>'service_countries','name'=>'country[]'))}}

    </div>
<!-- is active Field -->
    <div class="form-group col-sm-12 radio_isactive ">
        {!! Form::label('is_active', ' Is Active:') !!}&nbsp;&nbsp;
        {!! Form::radio('is_active', 1,true).'Active' !!}&nbsp;&nbsp;&nbsp;
        {!! Form::radio('is_active', 0,false).'Inactive' !!}
    </div>


    <!-- Country Field -->

<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_trending', ' Trending:') !!}&nbsp;&nbsp;
    {!! Form::radio('is_trending', 1,true).'Active' !!}&nbsp;&nbsp;&nbsp;
    {!! Form::radio('is_trending', 0,false).'Inactive' !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('news.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script type="text/javascript" src="{{asset("resources/assets/js/moment.js")}}"></script>
    <script type="text/javascript" src="{{asset("resources/assets/js/bootstrap-datetimepicker.min.js")}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.catimage i').click(function () {
                var conf = confirm("Are you sure, you want to delete the image?");
                if (conf) {
                    $(this).closest('.catimage').empty();
                    $('#image_old').val("");
                }
            });

            $('#publish_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        });
    </script>
@stop