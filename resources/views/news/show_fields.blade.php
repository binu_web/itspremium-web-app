<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $news->id !!}</p>
</div>

<!-- Category Field -->
<div class="form-group">
    {!! Form::label('category', 'Category:') !!}
    <p>{!! $news->category_name !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $news->title !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>
        @php
            if( strpos($news->image,"http") !== FALSE )
                echo '<img src="'.$news->image.'" width="300px">';
            else
                echo '<img src="'.asset("resources/assets/uploads/images/").'/'.$news->image.'" width="300px">';
        @endphp
    </p>
</div>

<!-- Source Link Field -->
<div class="form-group">
    {!! Form::label('source_link', 'Source Link:') !!}
    <p>{!! $news->source_link !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $news->description !!}</p>
</div>

<!-- Publish Date Field -->
<div class="form-group">
    {!! Form::label('publish_date', 'Publish Date:') !!}
    <p>{!! $news->publish_date !!}</p>
</div>
<!-- Country Field -->
<div class="form-group">
    {!! Form::label('Country', 'Country:') !!}
    <p>{!! $country!!}</p>
</div>
<!-- Is Trending Field -->
<div class="form-group">
    {!! Form::label('is_trending', 'Is Trending:') !!}
    <p>
        @if($news->is_trending)
            Yes
        @else
            No
        @endif
    </p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    <p>
        @if($news->is_active)
            Active
        @else
            Inactive
        @endif
    </p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $news->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $news->updated_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $news->created_by_name !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $news->updated_by_name !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $news->deleted_at !!}</p>
</div>