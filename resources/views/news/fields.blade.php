@section('css')
    <link rel="stylesheet" href="{{ asset("resources/assets/css/bootstrap-datetimepicker.min.css") }}"/>
@stop

<!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category', 'Category:') !!}
    {!! Form::select('category', $newsCategories, null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', ['class' => 'form-control']) !!}
</div>

<!-- Source Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('source_link', 'Source Link:') !!}
    {!! Form::text('source_link', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Publish Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('publish_date', 'Publish Date:') !!}
    {!! Form::text('publish_date', null, ['class' => 'form-control']) !!}
</div>
    <!-- Country Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('Country', 'Country:') !!}
        {{Form::select('country',$countriesArr,null,array('multiple'=>'multiple','class'=>'form-control selectpicker','id'=>'country','name'=>'country[]'))}}

    </div>
<!-- is active Field -->
<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}&nbsp;&nbsp;
    {!! Form::radio('is_active', 1,true).'Active' !!}&nbsp;&nbsp;&nbsp;
    {!! Form::radio('is_active', 0,false).'Inactive' !!}
</div>


<!-- is trending Field -->
<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_trending', ' Trending:') !!}&nbsp;&nbsp;
    {!! Form::radio('is_trending', 1,true).'Active' !!}&nbsp;&nbsp;&nbsp;
    {!! Form::radio('is_trending', 0,false).'Inactive' !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('news.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script type="text/javascript" src="{{asset("resources/assets/js/moment.js")}}"></script>
    <script type="text/javascript" src="{{asset("resources/assets/js/bootstrap-datetimepicker.min.js")}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.catimage i').click(function () {
                var conf = confirm("Are you sure, you want to delete the image?");
                if (conf) {
                    $(this).closest('.catimage').empty();
                    $('#image_old').val("");
                }
            });

            $('#publish_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        });
    </script>
@stop