@section('css')
    @include('layouts.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%']) !!}

@section('scripts')
    @include('layouts.datatables_js')
    {!! $dataTable->scripts() !!}
@endsection

<div id="news-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">News</h4>
            </div>
            <div class="modal-body">
                <table class="ctable">
                    <tr>
                        <td>Title</td>
                        <td><label id="news-title"></label></td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td><label id="news-desc"></label></td>
                    </tr>
                    <tr>
                        <td>Image</td>
                        <td>
                            <p class="news-image"><img id="newImageId" src="" width="300px"></p>
                            <input type="hidden" name="image" id="news-image" value="">
                        </td>
                    </tr>
                    <tr>
                        <td>Source</td>
                        <td>
                            <p class="news-source"><a id="news-source" href="#" target="_blank"></a></p>
                            <input type="hidden" name="source_link" id="source-link" value="">
                        </td>
                    </tr>
                    <tr>
                        <td>Category</td>
                        <td>
                            <label id="news-category"></label>

                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>

    </div>
</div>
<script type="text/javascript" src="{{asset("resources/assets/js/moment.js")}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="{{asset("resources/assets/js/bootstrap-datetimepicker.min.js")}}"></script>

<script>

    function test(id) {
        $('#news-popup').modal('show');
        var URL = "{{ url('news/') }}";
        var csrf_token = "{{csrf_token()}}";
        $.ajax({
            url: URL + '/select_news_by_id',
            type: 'POST',
            DataType: 'JSON',
            data: {_token: csrf_token, id: id},
            success: function (data) {
                if (data.status == "success") {
                    var dd = data.data.data;
//                    $('#news-popup').modal('hide');
                    console.log(dd.category_name);
                    var src = dd.image;
                    if (src.indexOf("http") != -1) {
                        $('#newImageId').attr('src', src);
                    } else {
                        var path = 'resources/assets/uploads/images/' + src;

                        $('#newImageId').attr('src', path);
                    }
                    $('#news-title').html(dd.title);
                    $('#news-desc').html(dd.description);
                    $('#news-source').attr('href', dd.source_link).html(dd.source_link);
                    $('#news-category').html(dd.category_name);
                }
                else
                    alert("Technical Error! Please try again later");
            }
        });
    }


</script>