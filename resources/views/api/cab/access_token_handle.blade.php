<html>
<head>

    <script>
        var hash_param = window.location.hash;
        hash_param = hash_param.replace('#', '&');
        var url = window.location.href.split('#')[0];
        var url_split = url.split('?');
        var url_param = '?check=1' + ((1 in url_split) ? '&' + url_split[1] : '');
        window.location.href = "{{ route('api.cab.access_token_handle') }}" + url_param + hash_param;
    </script>
</head>

<body>
<h3> Your request is being handled. </h3>
</body>
</html>