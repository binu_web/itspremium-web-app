<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $location->id !!}</p>
</div>

<!-- Location Field -->
<div class="form-group">
    {!! Form::label('location', 'Location:') !!}
    <p>{!! $location->location !!}</p>
</div>

<!-- latitude Field -->
<div class="form-group">
    {!! Form::label('latitude', 'Latitude:') !!}
    <p>{!! $location->latitude !!}</p>
</div>

<!-- longitude Field -->
<div class="form-group">
    {!! Form::label('longitude', 'Longitude:') !!}
    <p>{!! $location->longitude !!}</p>
</div>

<!-- distance Field -->
<div class="form-group">
    {!! Form::label('distance', 'Cover Distance:') !!}
    <p>{!! $location->distance !!}</p>
</div>

<!-- Display Services -->
<div class="form-group">
    {!! Form::label(' Services', ' Services:') !!}
    <p>
        {{$services}}


    </p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    @if($location->is_active==1)
        <p class="label label-primary">{!! 'Yes' !!}</p>
    @else
        <p class="label label-danger">{!! 'No' !!}</p>
    @endif
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $location->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $location->updated_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{$created_user}}</p>
</div>

<!-- Updated By Field -->

<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{$updated_user}}</p>
</div>

<!-- Deleted At Field -->
{{--<div class="form-group">--}}
{{--{!! Form::label('deleted_at', 'Deleted At:') !!}--}}
{{--<p>{!! $location->deleted_at !!}</p>--}}
{{--</div>--}}

