<!-- Location Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location', 'Location:') !!}
    {!! Form::text('location', null, ['class' => ' form-control ','id'=>'pac-input','placeholder'=>'Enter a location','onblur'=>'reload()']) !!}
    <div id="map"></div>
</div>

<!-- distance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('distance', 'Service Distance:') !!}
    {!! Form::number('distance', null, ['class' => 'form-control']).'sqkm' !!}
</div>

<!-- Service Field -->

<div class="form-group col-sm-12 col-lg-12 country_selectbox">
    {!! Form::label('service', 'Service Category :') !!}
    {{Form::select('service',$service,$selected_service,array('multiple'=>'multiple','class'=>'form-control selectpicker','id'=>'service','name'=>'service[]'))}}
</div>
{!! Form::hidden('latitude', null, ['class' => 'form-control','id'=>'lat']) !!}
{!! Form::hidden('longitude', null, ['class' => 'form-control','id'=>'lon']) !!}
<!-- Created By Field -->
{{--<div class="form-group col-sm-6">--}}
{{--{!! Form::label('created_by', 'Created By:') !!}--}}
{{--{!! Form::number('created_by', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- Updated By Field -->
{{--<div class="form-group col-sm-6">--}}
{{--{!! Form::label('updated_by', 'Updated By:') !!}--}}
{{--{!! Form::number('updated_by', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- is_active Field -->
<div class="form-group col-sm-6 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}
    {!! Form::radio('is_active', 1,true).'Active' !!}
    {!! Form::radio('is_active', 0,false).'Inactive' !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('locations.index') !!}" class="btn btn-default">Cancel</a>
</div>
