<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_id', 'Device Id:') !!}
    {!! Form::text('device_id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Regid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('regId', 'Regid:') !!}
    {!! Form::text('regId', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_name', 'Device Name:') !!}
    {!! Form::text('device_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Token Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_token', 'Device Token:') !!}
    {!! Form::text('device_token', null, ['class' => 'form-control']) !!}
</div>

<!-- Builder Version Field -->
<div class="form-group col-sm-6">
    {!! Form::label('builder_version', 'Builder Version:') !!}
    {!! Form::text('builder_version', null, ['class' => 'form-control']) !!}
</div>

<!-- Os Version Field -->
<div class="form-group col-sm-6">
    {!! Form::label('os_version', 'Os Version:') !!}
    {!! Form::text('os_version', null, ['class' => 'form-control']) !!}
</div>

<!-- Screen Width Field -->
<div class="form-group col-sm-6">
    {!! Form::label('screen_width', 'Screen Width:') !!}
    {!! Form::number('screen_width', null, ['class' => 'form-control']) !!}
</div>

<!-- Screen Height Field -->
<div class="form-group col-sm-6">
    {!! Form::label('screen_height', 'Screen Height:') !!}
    {!! Form::number('screen_height', null, ['class' => 'form-control']) !!}
</div>

<!-- Ip Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ip_address', 'Ip Address:') !!}
    {!! Form::text('ip_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Fcm Token Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('fcm_token', 'Fcm Token:') !!}
    {!! Form::textarea('fcm_token', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('devices.index') !!}" class="btn btn-default">Cancel</a>
</div>
