<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $device->id !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $device->type !!}</p>
</div>

<!-- Device Id Field -->
<div class="form-group">
    {!! Form::label('device_id', 'Device Id:') !!}
    <p>{!! $device->device_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $device->user_id !!}</p>
</div>

<!-- Regid Field -->
<div class="form-group">
    {!! Form::label('regId', 'Regid:') !!}
    <p>{!! $device->regId !!}</p>
</div>

<!-- Device Name Field -->
<div class="form-group">
    {!! Form::label('device_name', 'Device Name:') !!}
    <p>{!! $device->device_name !!}</p>
</div>

<!-- Device Token Field -->
<div class="form-group">
    {!! Form::label('device_token', 'Device Token:') !!}
    <p>{!! $device->device_token !!}</p>
</div>

<!-- Builder Version Field -->
<div class="form-group">
    {!! Form::label('builder_version', 'Builder Version:') !!}
    <p>{!! $device->builder_version !!}</p>
</div>

<!-- Os Version Field -->
<div class="form-group">
    {!! Form::label('os_version', 'Os Version:') !!}
    <p>{!! $device->os_version !!}</p>
</div>

<!-- Screen Width Field -->
<div class="form-group">
    {!! Form::label('screen_width', 'Screen Width:') !!}
    <p>{!! $device->screen_width !!}</p>
</div>

<!-- Screen Height Field -->
<div class="form-group">
    {!! Form::label('screen_height', 'Screen Height:') !!}
    <p>{!! $device->screen_height !!}</p>
</div>

<!-- Ip Address Field -->
<div class="form-group">
    {!! Form::label('ip_address', 'Ip Address:') !!}
    <p>{!! $device->ip_address !!}</p>
</div>

<!-- Fcm Token Field -->
<div class="form-group">
    {!! Form::label('fcm_token', 'Fcm Token:') !!}
    <p>{!! $device->fcm_token !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $device->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $device->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $device->deleted_at !!}</p>
</div>

