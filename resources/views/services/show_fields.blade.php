<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $service->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $service->name !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    @if(!empty($service->image))
        <div class="catimage">
            <img width="100px" src="{{asset("resources/assets/uploads/images")."/".$service->image}}">
        </div><br>
    @endif</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    @if($service->is_active==1)
        <br><p class="label label-primary">{!! 'Yes' !!}</p>
    @else
        <br><p class="label label-danger">{!! 'No' !!}</p>
    @endif
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $service->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $service->updated_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    @foreach($admin_users as $key => $value)
        @if($key == $service->created_by)
            <p>{!! $value !!}</p>
        @endif
    @endforeach
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    @foreach($admin_users as $key => $value)
        @if($key == $service->created_by)
            <p>{!! $value !!}</p>
        @endif
    @endforeach
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $service->deleted_at !!}</p>
</div>

