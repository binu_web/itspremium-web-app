<!-- Name Field -->
<div class="col-sm-12">
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>
<!-- Image Field -->
<div class="col-sm-12">
    <div class="form-group col-sm-6">
        @php
            $newsCategory = json_decode($service);
        @endphp
        <input type="hidden" name="image_old" id="image_old" value="<?=$service->image?>">

        {!! Form::label('image', 'Image:') !!}
        @if(!empty($service->image))
            <div class="catimage">
                <img width="100px" src="{{asset("resources/assets/uploads/images")."/".$service->image}}">
                <i class="ion-close-round" style="margin-left: 20px; font-size: 18px; cursor:pointer;"></i>
            </div><br>
        @endif
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}&nbsp;&nbsp;
    {!! Form::radio('is_active', 1,true).'Active' !!}&nbsp;&nbsp;&nbsp;
    {!! Form::radio('is_active', 0,false).'Inactive' !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('services.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.catimage i').click(function () {
                var conf = confirm("Are you sure, you want to delete the image?");
                if (conf) {
                    $(this).closest('.catimage').empty();
                    $('#image_old').val("");
                }
            });
        });
    </script>
@stop