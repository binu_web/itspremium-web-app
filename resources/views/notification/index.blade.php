@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Notification
        </h1>
    </section>
    <div class="content">
        <p class="alert alert-success hide" id="successAlert">Push notifications successfully sent !</p>
        <p class="alert alert-danger hide" id="alertError">Push notifications successfully sent !</p>
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'notification.store' ,'id'=>'frm-notification']) !!}

                    @include('notification.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('public/js/notification/notification.js') }}"></script>
@endsection