<!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Device Type:') !!}
    {!! Form::select('type', [''=>'All','ANDROID'=>'Android','IOS'=>'iOS'],null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','data-validation'=> "required"]) !!}
</div>


<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description') !!}
    <small>(maximum 140 characters)</small>
    {!! Form::textarea('description', null, ['class' => 'form-control',  'rows'=>3,'data-validation'=> "required length", 'data-validation-length'=>"max140"]) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Send Notification', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('news.index') !!}" class="btn btn-default">Cancel</a>
</div>
