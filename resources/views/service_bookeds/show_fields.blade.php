<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $serviceBooked->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    @foreach($users as $key => $value)
        @if($key == $serviceBooked->user_id)
            <p>{!! $value !!}</p>
        @endif
    @endforeach
</div>

<!-- Service Id Field -->
<div class="form-group">
    {!! Form::label('service_id', 'Service Id:') !!}
    @foreach($categories as $key => $value)
        @if($key == $serviceBooked->service_id)
            <p>{!! $value !!}</p>
        @endif
    @endforeach
</div>

<!-- Service Api Id Field -->
<div class="form-group">
    {!! Form::label('service_api_id', 'Service Api Id:') !!}
    @foreach($service as $key => $value)
        @if($key == $serviceBooked->service_api_id)
            <p>{!! $value !!}</p>
        @endif
    @endforeach
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $serviceBooked->status !!}</p>
</div>
@foreach ($booking_data as $key => $value)
    <div class="form-group">
        {!! Form::label($key, $key.':') !!}
        <p>{!! $value !!}</p>
    </div>
@endforeach

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $serviceBooked->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $serviceBooked->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $serviceBooked->deleted_at !!}</p>
</div>

