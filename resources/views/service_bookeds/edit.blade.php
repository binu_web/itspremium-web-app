@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Service Booked
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($serviceBooked, ['route' => ['serviceBookeds.update', $serviceBooked->id], 'method' => 'patch']) !!}

                    @include('service_bookeds.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection