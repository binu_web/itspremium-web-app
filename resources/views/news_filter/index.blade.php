@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>News Filter</h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        Filter By Source:
                        @php
                            $news_source = json_decode($news_source);
                        @endphp

                        <select name="news_source" id="news_source" class="form-control"
                                style="width: 300px; display: inline-block;">
                            <option value="">Select Source</option>
                            @foreach($news_source as $list)
                                @if($source==$list->source_slug)
                                    <option value="{{$list->source_slug}}" selected>{{$list->source}}</option>
                                @else
                                    <option value="{{$list->source_slug}}">{{$list->source}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="news-articles">
                    @foreach($news as $article)
                        <div class="row">
                            <div class="col-md-3 news-image">
                                <img src="{{$article->urlToImage}}">
                            </div>
                            <div class="col-md-9">
                                <h3>{{$article->title}}</h3>
                                <p>{{$article->description}}</p>
                                <div class="news-obj hidden" class="hidden">{{ json_encode($article) }}</div>
                                <button type="button" class="btn btn-primary news-selected">Add News</button>
                            </div>
                        </div>
                    @endforeach

                    @if( empty($source) )
                        <p>Please select the source to load news.</p>
                    @endif

                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="news-popup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add News</h4>
                </div>
                {{ Form::open(array('url' => 'newsFilter/add_news', 'id'=>"news-form")) }}
                    <div class="modal-body">
                        <table class="ctable">
                            <tr>
                                <td>Title </td>
                                <td><input type="text" id="news-title" name="title" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Description </td>
                                <td><textarea id="news-desc" name="description" class="form-control"></textarea></td>
                            </tr>
                            <tr>
                                <td>Image </td>
                                <td>
                                    <p class="news-image"><img src="" width="150px"></p>
                                    <input type="hidden" name="image" id="news-image" value="">
                                </td>
                            </tr>
                            <tr>
                                <td>Source </td>
                                <td>
                                    <p class="news-source"><a href="#" >{{ $source }}</a></p>
                                    <input type="hidden" name="source_link" id="source-link" value="">
                                </td>
                            </tr>
                            <tr>
                                <td>Category </td>
                                <td>{!! Form::select('category', $news_category, null, ['class' => 'form-control', 'id' => 'category']) !!}</td>
                            </tr>
                            <tr>
                                <td>Category </td>
                                <td>
                                    {{Form::select('country',$countriesArr,null,array('multiple'=>'multiple','class'=>'form-control selectpicker','id'=>'country','name'=>'country[]','required'))}}

                                </td>
                            </tr>
                            <tr>
                                <td>Publish Date </td>
                                <td>{!! Form::text('publish_date', null, ['class' => 'form-control', 'id' => 'publish_date']) !!}</td>
                            </tr>
                            <tr>
                                <td>Trending </td>
                                <td>{!! Form::checkbox('is_trending', 1, false, ['id'=>'is_trending']) !!}</td>
                            </tr>
                            <tr>
                                <td>Visibility </td>
                                <td>{!! Form::checkbox('is_active', 1, true, ['id'=>'is_active']) !!}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Add</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                {{ Form::close() }}
            </div>

        </div>
    </div>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset("resources/assets/css/bootstrap-datetimepicker.min.css") }}"/>
@stop

@section('scripts')
    <script type="text/javascript" src="{{asset("resources/assets/js/moment.js")}}"></script>
    <script type="text/javascript" src="{{asset("resources/assets/js/bootstrap-datetimepicker.min.js")}}"></script>
    <script type="text/javascript" src="{{asset("resources/assets/js/jquery.validate.min.js")}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var URL = "{{ url('newsFilter/') }}";
            $('#news_source').change(function () {
                var selected = $(this).val();
                window.location.href = URL + "?source=" + selected;
            });

            $('#publish_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });

            $('#category').prepend('<option value="">Select Category</option>').val("");

            $('.news-selected').click(function () {

                var row = $(this).closest('.row');
                var newsobj = $.parseJSON(row.find('.news-obj').html());

                $('#news-articles .row').removeClass('active');
                row.addClass('active');

                $('#news-popup').find('#news-title').val(newsobj.title);
                $('#news-popup').find('#news-desc').html(newsobj.description);
                $('#news-popup').find('.news-image img').attr('src', newsobj.urlToImage);
                $('#news-popup').find('#news-image').val(newsobj.urlToImage);
                $('#news-popup').find('#publish_date').val("");
                $('#news-popup').find('#source-link').val(newsobj.url);
                $('#news-popup').find('.news-source a').attr('href', newsobj.url);

                $('#news-popup').modal('show');

            });

            // form validation
            $("#news-form").validate({
                rules: {
                    title: "required",
                    description: "required",
                    publish_date: "required",
                    category: "required",
                    country:"required"
                },
                submitHandler: function (form) {
                    var formData = $("#news-form").serialize();

                    $.ajax({
                        url: URL + '/add_news',
                        type: 'POST',
                        data: formData,
                        success: function (msg) {
                            if (msg == "success") {
                                $('#news-popup').modal('hide');
                                $('.news-articles .row.active').remove();
                            }
                            else
                                alert("Technical Error! Please try again later");
                        }
                    });
                }
            });
        });
    </script>
@stop

