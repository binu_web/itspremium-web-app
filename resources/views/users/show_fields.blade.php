<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $users->id !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $users->email !!}</p>
</div>

<!-- Mobile Field -->
<div class="form-group">
    {!! Form::label('mobile', 'Mobile:') !!}
    <p>{!! $users->mobile !!}</p>
</div>

{{--<!-- Password Field -->--}}
{{--<div class="form-group">--}}
{{--{!! Form::label('password', 'Password:') !!}--}}
{{--<p>{!! $users->password !!}</p>--}}
{{--</div>--}}

<!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{!! $users->first_name !!}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    <p>{!! $users->last_name !!}</p>
</div>

{{--<!-- Profile Pic Field -->--}}
{{--<div class="form-group">--}}
{{--{!! Form::label('profile_pic', 'Profile Pic:') !!}--}}
{{--<p>{!! $users->profile_pic !!}</p>--}}
{{--</div>--}}

<!-- Country Field -->
<div class="form-group">
    {!! Form::label('country', 'Country:') !!}
    @foreach($countries as $key => $value)
        @if($key == $users->country)
            <p>{!! $value !!}</p>
        @endif
    @endforeach
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    @if($users->is_active==1)
        <br><p class="label label-primary">{!! 'Yes' !!}</p>
    @else
        <br><p class="label label-danger">{!! 'No' !!}</p>
    @endif
</div>