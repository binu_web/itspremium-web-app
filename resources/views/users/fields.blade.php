<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mobile', 'Mobile:') !!}
    {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>

{{--<!-- Profile Pic Field -->--}}
{{--<div class="form-group col-sm-6">--}}
{{--{!! Form::label('profile_pic', 'Profile Pic:') !!}--}}
{{--{!! Form::file('profile_pic', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country', 'Country:') !!}
    {{ Form::select('country', $countries,  isset($user)? $user->country->pluck('id')->toArray() : null ,['class'=>'form-control select','data-placeholder'=>'Select country', 'data-fv-blank' => 'true']) }}
</div>
<!-- is active Field -->
<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}<br>
    {!! Form::radio('is_active', 1,true).'Active' !!}
    {!! Form::radio('is_active', 0,false).'Inactive' !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>
</div>
