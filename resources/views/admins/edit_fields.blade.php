<!-- User Type Field -->
{{--<div class="form-group col-sm-12">--}}
{{--{!! Form::label('user_type', 'User Type:') !!}--}}
{{--{!! Form::number('user_type', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- First Name Field -->
<div class="form-group col-sm-12">

    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', old('first_name') , ['class' => 'form-control']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>
<!-- Location Field -->
<div class="form-group col-sm-12">
    {!! Form::label('location', 'Location:') !!}
    {!! Form::text('location', null, ['class' => 'form-control']) !!}
</div>

<!-- Mobile Field -->
<div class="form-group col-sm-12">
    {!! Form::label('mobile', 'Mobile:') !!}
    {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
</div>
<!-- Email Field -->
<div class="form-group col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-12">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Job Role Field -->
<div class="form-group col-sm-12 col-lg-12 country_selectbox">
    {!! Form::label('Privilege', 'Privilege:') !!}
    {!! Form::text('job_role', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12 col-lg-12 country_selectbox">
    {!! Form::label('Privilege', 'Privilege:') !!}
    {{Form::select('user_type',$userType,$selectedAdminType,array('class'=>'form-control','id'=>'user_type','name'=>'user_type'))}}
</div>
<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}<br>
    {!! Form::radio('is_active', 1,true).'Active' !!}
    {!! Form::radio('is_active', 0,false).'Inactive' !!}
</div>
<!-- Activation Code Field -->
{{--<div class="form-group col-sm-6">--}}
{{--{!! Form::label('activation_code', 'Activation Code:') !!}--}}
{{--{!! Form::text('activation_code', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- Remember Token Field -->
{{--<div class="form-group col-sm-6">--}}
{{--{!! Form::label('remember_token', 'Remember Token:') !!}--}}
{{--{!! Form::text('remember_token', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- Created By Field -->
{{--<div class="form-group col-sm-6">--}}
{{--{!! Form::label('created_by', 'Created By:') !!}--}}
{{--{!! Form::number('created_by', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- Updated By Field -->
{{--<div class="form-group col-sm-6">--}}
{{--{!! Form::label('updated_by', 'Updated By:') !!}--}}
{{--{!! Form::number('updated_by', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admins.index') !!}" class="btn btn-default">Cancel</a>
</div>
