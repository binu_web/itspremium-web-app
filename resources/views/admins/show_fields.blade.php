<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $admin->id !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $admin->email !!}</p>
</div>

<!-- Password Field -->
{{--<div class="form-group">--}}
{{--{!! Form::label('password', 'Password:') !!}--}}
{{--<p>{!! $admin->password !!}</p>--}}
{{--</div>--}}

<!-- User Type Field -->
<div class="form-group">
    {!! Form::label('user_type', 'User Type:') !!}

    <p>
        {{ ucfirst($admin->adminUserType->type) }}
    </p>
</div>
<!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{!! $admin->first_name !!}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    <p>{!! $admin->last_name !!}</p>
</div>

<!-- Mobile Field -->
<div class="form-group">
    {!! Form::label('mobile', 'Mobile:') !!}
    <p>{!! $admin->mobile !!}</p>
</div>

<!-- Job Role Field -->
<div class="form-group">
    {!! Form::label('job_role', 'Job Role:') !!}
    <p>{!! $admin->job_role !!}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    @if($admin->is_active==1)
        <p class="label label-primary">{!! 'Yes' !!}</p>
    @else
        <p class="label label-danger">{!! 'No' !!}</p>
    @endif
</div>
<!-- Activation Code Field -->
{{--<div class="form-group">--}}
{{--{!! Form::label('activation_code', 'Activation Code:') !!}--}}
{{--<p>{!! $admin->activation_code !!}</p>--}}
{{--</div>--}}

{{--<!-- Remember Token Field -->--}}
{{--<div class="form-group">--}}
{{--{!! Form::label('remember_token', 'Remember Token:') !!}--}}
{{--<p>{!! $admin->remember_token !!}</p>--}}
{{--</div>--}}

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $admin->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $admin->updated_at !!}</p>
</div>

<!-- Created By Field -->
{{--<div class="form-group">--}}
{{--{!! Form::label('created_by', 'Created By:') !!}--}}
{{--<p>{!! $admin->created_by !!}</p>--}}
{{--</div>--}}

<!-- Updated By Field -->
{{--<div class="form-group">--}}
{{--{!! Form::label('updated_by', 'Updated By:') !!}--}}
{{--<p>{!! $admin->updated_by !!}</p>--}}
{{--</div>--}}

<!-- Deleted At Field -->
{{--<div class="form-group">--}}
{{--{!! Form::label('deleted_at', 'Deleted At:') !!}--}}
{{--<p>{!! $admin->deleted_at !!}</p>--}}
{{--</div>--}}

