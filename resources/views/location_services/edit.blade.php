@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Location Service
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($locationService, ['route' => ['locationServices.update', $locationService->id], 'method' => 'patch']) !!}

                    @include('location_services.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection