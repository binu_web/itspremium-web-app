@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Edit wallet details
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($wallet, ['route' => ['user_refund.update', $wallet->id], 'method' => 'patch', 'files' => true]) !!}

                    @include('user_refund.edit_fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection