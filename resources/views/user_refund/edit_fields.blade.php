@section('css')
    <link rel="stylesheet" href="{{ asset("resources/assets/css/bootstrap-datetimepicker.min.css") }}"/>
@stop

<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Email:') !!}
    {!! Form::select('user_id',$user, null, ['class' => 'form-control']) !!}
</div>
<!-- Provider Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('balance', 'Provider Name:') !!}
    {!! Form::text('balance', null, ['class' => 'form-control']) !!}
</div>
<!-- Publish Date Field -->
<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}&nbsp;&nbsp;
    {!! Form::radio('is_active', 1,true).'Active' !!}&nbsp;&nbsp;&nbsp;
    {!! Form::radio('is_active', 0,false).'Inactive' !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('serviceApis.index') !!}" class="btn btn-default">Cancel</a>
</div>