<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $wallet->id !!}</p>
</div>

<div class="form-group">
    {!! Form::label('first_name', 'User first name:') !!}
    <p>{!! $user->first_name !!}</p>
</div>

<div class="form-group">
    {!! Form::label('last_name', 'User last name:') !!}
    <p>{!! $user->last_name !!}</p>
</div>

<div class="form-group">
    {!! Form::label('mobile', 'User mobile:') !!}
    <p>{!! $user->mobile !!}</p>
</div>

<div class="form-group">
    {!! Form::label('email', 'User email:') !!}
    <p>{!! $user->email !!}</p>
</div>
<div class="form-group">
    {!! Form::label('balance', 'Wallet balance:') !!}
    <p style="color: green"><b>{!! $wallet->balance !!}</b></p>
</div>
<?php if(isset($walletLog)){
?>
<h2>User Wallet Log </h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th>Service</th>
        <th>Amount</th>
        <th>Transaction type</th>
        <th>Transaction method</th>
        <th>Created at</th>
        <th>Updated at</th>
        {{--<th>Actions</th>--}}
    </tr>
    </thead>
    <tbody>
    @foreach($walletLog as $logs)
        <tr>
            @foreach($service_api as $key => $value)
                @if($key == $logs['service_id'])
                    <td>{!! $value !!}</td>
                @endif
            @endforeach

            <td>{!! $logs['balance'] !!}</td>
            <td>{!! $logs['transaction_type'] !!}</td>
            <td>{!! $logs['transaction_method'] !!}</td>
            <td>{!! $logs['created_at'] !!}</td>
            <td>{!! $logs['updated_at'] !!}</td>
            {{--<td><a href="{{ route('user_refund/wallet_log_edit').'/'.$logs['id'] }}" class='btn btn-warning btn-xs'>--}}
                    {{--<p>Edit</p>--}}
                {{--</a>--}}
                {{--<a href="{{ route('user_refund/wallet_log_delete').'/'.$logs['id'] }}" class='btn btn-danger btn-xs'>--}}
                    {{--<p>Delete</p>--}}
                {{--</a>--}}
            {{--</td>--}}

        </tr>
    @endforeach


    </tbody>
</table>
<?php
} ?>


