<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $serviceApi->id !!}</p>
</div>

<!-- Provider Name Field -->
<div class="form-group">
    {!! Form::label('provider_name', 'Provider Name:') !!}
    <p>{!! $serviceApi->provider_name !!}</p>
</div>

<!-- Service Category Field -->
<div class="form-group">
    {!! Form::label('service_category', 'Service Category:') !!}
    @foreach($serviceCategories as $key => $value)
        @if($key == $serviceApi->service_category)
            <p>{!! $value !!}</p>
        @endif
    @endforeach
</div>

<!-- Api Key Field -->
<div class="form-group">
    {!! Form::label('api_key', 'Api Key:') !!}
    <p>{!! $serviceApi->api_key !!}</p>
</div>

<!-- Api Url Field -->
<div class="form-group">
    {!! Form::label('api_url', 'Api Url:') !!}
    <p>{!! $serviceApi->api_url !!}</p>
</div>

<!-- Api Token Field -->
<div class="form-group">
    {!! Form::label('api_token', 'Api Token:') !!}
    <p>{!! $serviceApi->api_token !!}</p>
</div>

<!-- Api Metadata Field -->
<div class="form-group">
    {!! Form::label('api_metadata', 'Api Metadata:') !!}
    <p>{!! $serviceApi->api_metadata !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    @if(!empty($serviceApi->image))
        <div class="catimage">
            <img width="100px" src="{{asset("resources/assets/uploads/images")."/".$serviceApi->image}}">
        </div><br>
    @endif</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    @if($serviceApi->is_active)
        <p>Active</p>
    @else
        <p>In Active</p>
    @endif
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $serviceApi->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $serviceApi->updated_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    @foreach($admin_users as $key => $value)
        @if($key == $serviceApi->created_by)
            <p>{!! $value !!}</p>
        @endif
    @endforeach
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    @foreach($admin_users as $key => $value)
        @if($key == $serviceApi->created_by)
            <p>{!! $value !!}</p>
        @endif
    @endforeach</div>

