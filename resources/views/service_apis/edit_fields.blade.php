@section('css')
    <link rel="stylesheet" href="{{ asset("resources/assets/css/bootstrap-datetimepicker.min.css") }}"/>
@stop
<!-- Provider Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('provider_name', 'Provider Name:') !!}
    {!! Form::text('provider_name', null, ['class' => 'form-control']) !!}
</div>
<!-- Service Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_category', 'Service Category:') !!}
    {!! Form::select('service_category',$serviceCategories, null, ['class' => 'form-control']) !!}
</div>
<!-- Api Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('api_key', 'Api Key:') !!}
    {!! Form::text('api_key', null, ['class' => 'form-control']) !!}
</div>

<!-- Api Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('api_url', 'Api Url:') !!}
    {!! Form::text('api_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Api Token Field -->
<div class="form-group col-sm-6">
    {!! Form::label('api_token', 'Api Token:') !!}
    {!! Form::text('api_token', null, ['class' => 'form-control']) !!}
</div>

<!-- Api Metadata Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('api_metadata', 'Api Metadata:') !!}
    {!! Form::textarea('api_metadata', null, ['class' => 'form-control']) !!}
</div>
<!-- Image Field -->
<div class="col-sm-12">
    <div class="form-group col-sm-6">
        @php
        $newsCategory = json_decode($serviceApi);
        @endphp
        <input type="hidden" name="image_old" id="image_old" value="<?=$serviceApi->image?>">

        {!! Form::label('image', 'Image:') !!}
        @if(!empty($serviceApi->image))
            <div class="catimage">
                <img width="100px" src="{{asset("resources/assets/uploads/images")."/".$serviceApi->image}}">
                <i class="ion-close-round" style="margin-left: 20px; font-size: 18px; cursor:pointer;"></i>
            </div><br>
        @endif
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
    </div>
</div>
<!-- Publish Date Field -->
<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}&nbsp;&nbsp;
    {!! Form::radio('is_active', 1,true).'Active' !!}&nbsp;&nbsp;&nbsp;
    {!! Form::radio('is_active', 0,false).'Inactive' !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('serviceApis.index') !!}" class="btn btn-default">Cancel</a>
</div>