@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Service Api
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($serviceApi, ['route' => ['serviceApis.update', $serviceApi->id], 'method' => 'patch', 'files' => true]) !!}

                    @include('service_apis.edit_fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection