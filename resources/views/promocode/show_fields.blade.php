<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $promocode->id !!}</p>
</div>
<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('promo_code', 'Promo Code :') !!}
    <p>{!! $promocode->promo_code !!}</p>
</div>
<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('description', 'Description :') !!}
    <p>{!! $promocode->description !!}</p>
</div>
<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('no_of_maximum_use', 'Maximum number of uses :') !!}
    <p>{!! $promocode->no_of_maximum_use !!}</p>
</div>
<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('discount_type', 'Discount Type :') !!}
        @if($promocode->discount_type == 1)
            <p>{!! 'Percentage' !!}</p>
        @else
        <p>{!! 'Fixed Amount' !!}</p>
        @endif
</div>
<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('discount', 'Discount :') !!}
    <p>{!! $promocode->discount !!}</p>
</div>
<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('valid_from', 'Valid From :') !!}
    <p>{!! $promocode->valid_from !!}</p>
</div>
<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('valid_to', 'Valid to :') !!}
    <p>{!! $promocode->valid_to !!}</p>
</div>
<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $promocode->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $promocode->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $promocode->deleted_at !!}</p>
</div>

