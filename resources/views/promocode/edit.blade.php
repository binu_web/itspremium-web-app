@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Edit Promo code
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($promocode, ['route' => ['promocode.update', $promocode->id], 'method' => 'patch']) !!}

                    @include('promocode.edit_fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection