<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('promo_code', 'Promo Code*:') !!}
    {!! Form::text('promo_code', null, ['class' => 'form-control']) !!}
</div>
<div class="clear"></div>
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('validation_rules', 'Valication Rules:') !!}
    {!! Form::textarea('validation_rules', null, ['class' => 'form-control']) !!}
</div>
<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_of_maximum_use', 'Maximum number of uses:') !!}
    {!! Form::number('no_of_maximum_use', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('no_of_times_per_user', 'No of Times per user:') !!}
    {!! Form::number('no_of_times_per_user', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('no_of_times_per_user_per_service', 'No of Times per user Per service:') !!}
    {!! Form::number('no_of_times_per_user_per_service', null, ['class' => 'form-control']) !!}
    <p class="help-text"></p>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('deduction_type', 'Discount Type:') !!}
    {!! Form::select('deduction_type', ['1' => 'Cash Back', '2' => 'Direct deduction'], '1',['class' => 'form-control'])!!}
</div>
<div class="clear"></div>
<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount_type', 'Discount Type:') !!}
    {!! Form::select('discount_type', ['1' => 'Percentage', '2' => 'Fixed Amount'], '1',['class' => 'form-control'])!!}
</div>
<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount', 'Discount:') !!}
    {!! Form::number('discount', null, ['class' => 'form-control']) !!}
</div>
<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valid_from', 'Valid From:') !!}
    {!! Form::date('valid_from', null, ['class' => 'form-control']) !!}
</div>
<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valid_to', 'Valid To:') !!}
    {!! Form::date('valid_to', null, ['class' => 'form-control']) !!}
</div>
<!-- is active Field -->
<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}&nbsp;&nbsp;
    {!! Form::radio('is_active', 1,true).'Active' !!}&nbsp;&nbsp;&nbsp;
    {!! Form::radio('is_active', 0,false).'Inactive' !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('promocode.index') !!}" class="btn btn-default">Cancel</a>
</div>
