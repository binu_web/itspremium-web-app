<!-- Gateway Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gateway_name', 'Gateway Name:') !!}
    {!! Form::text('gateway_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Api Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('api_key', 'Api Key:') !!}
    {!! Form::text('api_key', null, ['class' => 'form-control']) !!}
</div>

<!-- Api secret  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('api_secret', 'Api Secret:') !!}
    {!! Form::text('api_secret', null, ['class' => 'form-control']) !!}
</div>

<!-- Api Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('api_url', 'Api Service Url:') !!}
    {!! Form::text('api_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Api Metadata Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('redirect_url', 'Redirect Url:') !!}
    {!! Form::text('redirect_url', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12 country_selectbox">
    {!! Form::label('service_countries', 'Service Countries:') !!}
    {{Form::select('service_countries',$countriesArr,$selectedCountryArr,array('multiple'=>'multiple','class'=>'form-control selectpicker','id'=>'service_countries','name'=>'service_countries[]'))}}
</div>

<!-- Created By Field -->
{{--<div class="form-group col-sm-6">--}}
{{--{!! Form::label('api_metadata', 'api_metadata:') !!}--}}
{{--{!! Form::textarea('api_metadata', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- is_active Field -->
<div class="form-group col-sm-6 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}
    {!! Form::radio('is_active', 1,true).'Active' !!}
    {!! Form::radio('is_active', 0,false).'Inactive' !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('paymentMethods.index') !!}" class="btn btn-default">Cancel</a>
</div>
<style>

    /*.form-group.country_selectbox .btn-group,.form-group.country_selectbox .btn-group button, .form-group.country_selectbox .btn-group .multiselect-container{*/
    /*width: 100%;*/
    /*}*/
    /*.radio_isactive input[type=radio]{*/
    /*margin-left: 25px;*/
    /*margin-right: 10px;*/
    /*vertical-align: top;*/
    /*}*/

</style>