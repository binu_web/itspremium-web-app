<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $paymentMethod->id !!}</p>
</div>

<!-- Gateway Name Field -->
<div class="form-group">
    {!! Form::label('gateway_name', 'Gateway Name:') !!}
    <p>{!! $paymentMethod->gateway_name !!}</p>
</div>

<!-- Display Countries -->
<div class="form-group">
    {!! Form::label('Countries Servicing', 'Countries Servicing:') !!}
    <p>
        @foreach($paymentMethod->paymentMethodCountries()->get() as $country)
            {{ ucfirst($country->country->name).',  ' }}
        @endforeach

    </p>
</div>

<!-- Api Key Field -->
<div class="form-group">
    {!! Form::label('api_key', 'Api Key:') !!}
    <p>{!! $paymentMethod->api_key !!}</p>
</div>

<!-- Api Url Field -->
<div class="form-group">
    {!! Form::label('api_url', 'Api Url:') !!}
    <p>{!! $paymentMethod->api_url !!}</p>
</div>

<!-- Api Metadata Field -->
<div class="form-group">
    {!! Form::label('api_metadata', 'Api Metadata:') !!}
    <p>{!! $paymentMethod->api_metadata !!}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    @if($paymentMethod->is_active==1)
        <p class="label label-primary">{!! 'Yes' !!}</p>
    @else
        <p class="label label-danger">{!! 'No' !!}</p>
    @endif
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $paymentMethod->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $paymentMethod->updated_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{$created_user->first_name}}</p>
</div>

<!-- Updated By Field -->

<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{$updated_user->first_name}}</p>
</div>

<!-- Deleted At Field -->
{{--<div class="form-group">--}}
{{--{!! Form::label('deleted_at', 'Deleted At:') !!}--}}
{{--<p>{!! $paymentMethod->deleted_at !!}</p>--}}
{{--</div>--}}

