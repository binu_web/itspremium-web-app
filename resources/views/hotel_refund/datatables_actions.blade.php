{{--{!! Form::open(['route' => ['hotel_refund.destroy', $id], 'method' => 'delete']) !!}--}}
<div class='btn-group'>
    <a href="{{ route('hotel_refund.show', $id) }}" class='btn btn-info btn-xs'>
        <p>View</p>
    </a>
    <a href="{{ route('full_refund').'/'.$id }}" class='btn btn-danger btn-xs'>
        <p>Full Refund</p>
    </a>
    <button type="button" id="open_amount_popup" data-id= "{{$id}}" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#yourModal">Partial Refund</button>

    {{--{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [--}}
        {{--'type' => 'submit',--}}
        {{--'class' => 'btn btn-danger btn-xs',--}}
        {{--'onclick' => "return confirm('Are you sure?')"--}}
    {{--]) !!}--}}
</div>
{{--{!! Form::close() !!}--}}
<script type="text/javascript" src="{{ URL::asset('resources/assets/js/partial_refund.js') }}"></script>

