@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Hotel Refund Requests</h1>
        {{--<h1 class="pull-right">--}}
            {{--<a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"--}}
               {{--href="{!! route('hotel_refund.create') !!}">Add New</a>--}}
        {{--</h1>--}}
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('hotel_refund.table')
            </div>
        </div>
    </div>

    <div class="modal fade" id="yourModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Partial Amount</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="POST" action="hotel_refund/partial_refund"role="form">
                        <input type="hidden" class="form-control" id="booking_id" name="booking_id" value=""/>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="p_amount" >Amount to be pay</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="p_amount" name="p_amount" placeholder="Amount"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Proceed</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

