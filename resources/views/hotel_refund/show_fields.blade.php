<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $booking->id !!}</p>
</div>

<div class="form-group">
    {!! Form::label('hotel_name', 'Hotel name:') !!}
    <p>{!! $metadata['hotel_name'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('stay_start_date', 'Stay start date:') !!}
    <p>{!! $metadata['stay_start_date'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('stay_end_date', 'Stay end date:') !!}
    <p>{!! $metadata['stay_end_date'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('address_line', 'Address:') !!}
    <p>{!! $metadata['address_line'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('city_name', 'City:') !!}
    <p>{!! $metadata['city_name'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('state', 'State:') !!}
    <p>{!! $metadata['state'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('net_hotel_amount_before_tax', 'Hotel amount before Tax:') !!}
    <p>{!! $metadata['net_hotel_amount_before_tax'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('hotel_tax_amount', 'Hotel tax:') !!}
    <p>{!! $metadata['hotel_tax_amount'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('hotel_final_amount_with_tax', 'Amount with tzx:') !!}
    <p>{!! $metadata['hotel_final_amount_with_tax'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('service_charge', 'Hotel tax:') !!}
    <p>{!! $metadata['service_charge'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('tax_for_service_charge', 'Tax of service charge:') !!}
    <p>{!! $metadata['tax_for_service_charge'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('net_amount_to_pay', 'Net amount to pay:') !!}
    <h4><b>{!! $metadata['net_amount_to_pay'] !!}</b></h4>
</div>

<div class="form-group">
    {!! Form::label('affiliate_commission_amount', 'Affiliate Commission:') !!}
    <p>{!! $metadata['affiliate_commission_amount'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('final_booking_unique_id', 'Final booking unique id:') !!}
    <p>{!! $metadata['final_booking_unique_id'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('cancel_unique_id', 'Cancel unique id:') !!}
    <p>{!! $metadata['cancel_unique_id'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('customer_phone_number', 'Customer phone number:') !!}
    <p>{!! $metadata['customer_phone_number'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('correlation_id', 'Correlation id:') !!}
    <p>{!! $metadata['correlation_id'] !!}</p>
</div>

<div class="form-group">
    {!! Form::label('cancel_refund_amount', 'Cancel refund amount:') !!}
    <h4><b>{!! $metadata['cancel_refund_amount'] !!}</b></h4>
</div>

<div class="form-group">
    <a  href="{{ route('full_refund').'/'.$booking->id }}" class='btn btn-danger btn-xs'>
        <p>Full Refund</p>
    </a>
</div>



