<!-- Service Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_id', 'Service Id:') !!}
    {!! Form::number('service_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Meta Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meta_key', 'Meta Key:') !!}
    {!! Form::text('meta_key', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('serviceMetaKeys.index') !!}" class="btn btn-default">Cancel</a>
</div>
