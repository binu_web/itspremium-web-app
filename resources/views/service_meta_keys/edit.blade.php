@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Service Meta Key
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($serviceMetaKey, ['route' => ['serviceMetaKeys.update', $serviceMetaKey->id], 'method' => 'patch']) !!}

                    @include('service_meta_keys.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection