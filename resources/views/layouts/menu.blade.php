<li class="treeview">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
        <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
    </ul>
</li>


<li class="treeview">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>User</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('users*') ? 'active' : '' }}">
            <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
        </li>

        {{--<li class="{{ Request::is('userLogs*') ? 'active' : '' }}">--}}
        {{--<a href="{!! route('userLogs.index') !!}"><i class="fa fa-edit"></i><span>UserLogs</span></a>--}}
        {{--</li>--}}

        <li class="{{ Request::is('admins*') ? 'active' : '' }}">
            <a href="{!! route('admins.index') !!}"><i class="fa fa-edit"></i><span>Admins</span></a>
        </li>

        {{--<li class="{{ Request::is('adminUserTypes*') ? 'active' : '' }}">--}}
        {{--<a href="{!! route('adminUserTypes.index') !!}"><i class="fa fa-edit"></i><span>AdminUserTypes</span></a>--}}
        {{--</li>--}}

        {{--<li class="{{ Request::is('adminUserLogs*') ? 'active' : '' }}">--}}
        {{--<a href="{!! route('adminUserLogs.index') !!}"><i class="fa fa-edit"></i><span>AdminUserLogs</span></a>--}}
        {{--</li>--}}

        {{--<li class="{{ Request::is('adminLogTypes*') ? 'active' : '' }}">--}}
        {{--<a href="{!! route('adminLogTypes.index') !!}"><i class="fa fa-edit"></i><span>AdminLogTypes</span></a>--}}
        {{--</li>--}}
    </ul>
</li>


<li class="{{ Request::is('countries*') ? 'active' : '' }}">
    <a href="{!! route('countries.index') !!}"><i class="fa fa-edit"></i><span>Countries</span></a>
</li>

<li class="treeview">
    <a href="{!! route('locations.index') !!}">
        <i class="fa fa-edit"></i> <span>Location</span>
        <span class="pull-right-container">
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            </span>
    </a>
    {{--<ul class="treeview-menu" >--}}
    {{--<li class="{{ Request::is('locations*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('locations.index') !!}"><i class="fa fa-edit"></i><span>Locations</span></a>--}}
    {{--</li>--}}

    {{--<li class="{{ Request::is('locationServices*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('locationServices.index') !!}"><i class="fa fa-edit"></i><span>LocationServices</span></a>--}}
    {{--</li>--}}
    {{--</ul>--}}
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>News</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('news*') ? 'active' : '' }}">
            <a href="{!! route('news.index') !!}"><i class="fa fa-edit"></i><span>News</span></a>
        </li>

        <li class="{{ Request::is('newsCategories*') ? 'active' : '' }}">
            <a href="{!! route('newsCategories.index') !!}"><i class="fa fa-edit"></i><span>News Categories</span></a>
        </li>

        <li class="{{ Request::is('newsSources*') ? 'active' : '' }}">
            <a href="{!! route('newsSources.index') !!}"><i class="fa fa-edit"></i><span>News Sources</span></a>
        </li>

        <li class="{{ Request::is('newsFilter*') ? 'active' : '' }}">
            <a href="{!! route('newsFilter.index') !!}"><i class="fa fa-edit"></i><span>News Filter</span></a>
        </li>

    </ul>
</li>

<li class="treeview">
    <a href="{!! route('paymentMethods.index') !!}">
        <i class="fa fa-edit"></i> <span>Payment Methods</span>
        {{--<span class="pull-right-container">--}}
        {{--<i class="fa fa-angle-left pull-right"></i>--}}
        {{--</span>--}}
    </a>
    {{--<ul class="treeview-menu" >--}}
    {{--<li class="{{ Request::is('paymentMethodCountries*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('paymentMethodCountries.index') !!}"><i class="fa fa-edit"></i><span>PaymentMethodCountries</span></a>--}}
    {{--</li>--}}

    {{--<li class="{{ Request::is('paymentMethods*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('paymentMethods.index') !!}"><i class="fa fa-edit"></i><span>PaymentMethods</span></a>--}}
    {{--</li>--}}
    {{--</ul>--}}
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Service API</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('serviceApis*') ? 'active' : '' }}">
            <a href="{!! route('serviceApis.index') !!}"><i class="fa fa-edit"></i><span>Service Apis</span></a>
        </li>

        {{--<li class="{{ Request::is('serviceMetaDatas*') ? 'active' : '' }}">--}}
        {{--<a href="{!! route('serviceMetaDatas.index') !!}"><i class="fa fa-edit"></i><span>ServiceMetaDatas</span></a>--}}
        {{--</li>--}}

        {{--<li class="{{ Request::is('serviceMetaKeys*') ? 'active' : '' }}">--}}
        {{--<a href="{!! route('serviceMetaKeys.index') !!}"><i class="fa fa-edit"></i><span>ServiceMetaKeys</span></a>--}}
        {{--</li>--}}

        <li class="{{ Request::is('services*') ? 'active' : '' }}">
            <a href="{!! route('services.index') !!}"><i class="fa fa-edit"></i><span>Services Categories</span></a>
        </li>

        {{--<li class="{{ Request::is('serviceBookeds*') ? 'active' : '' }}">--}}
        {{--<a href="{!! route('serviceBookeds.index') !!}"><i class="fa fa-edit"></i><span>ServiceBookeds</span></a>--}}
        {{--</li>--}}
    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Reports</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('serviceBookeds*') ? 'active' : '' }}">
            <a href="{!! route('serviceBookeds.index') !!}"><i class="fa fa-edit"></i><span>Booking Reports</span></a>
        </li>
    </ul>
    <ul class="treeview-menu">
        <li class="{{ Request::is('feedback*') ? 'active' : '' }}">
            <a href="{!! route('feedback.index') !!}"><i class="fa fa-edit"></i><span>Feedback Reports</span></a>
        </li>
    </ul>
</li>


<li class="{{ Request::is('transactions*') ? 'active' : '' }}">
    <a href="{!! route('transactions.index') !!}"><i class="fa fa-edit"></i><span>Transactions</span></a>
</li>

<li class="{{ Request::is('notification*') ? 'active' : '' }}">
    <a href="{!! route('notification.index') !!}"><i class="fa fa-bell"></i><span>Push Notification</span></a>
</li>

<li class="{{ Request::is('promocode*') ? 'active' : '' }}">
    <a href="#">
        <i class="fa fa-qrcode"></i> <span>Promo code management</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('promocode*') ? 'active' : '' }}">
            <a href="{!! route('promocode.index') !!}"><i class="fa fa-qrcode"></i><span>Promo code</span></a>
        </li>
    </ul>
    <ul class="treeview-menu">
        <li class="{{ Request::is('promocode_service_map*') ? 'active' : '' }}">
            <a href="{!! route('promocode_service_map.index') !!}"><i class="fa fa-qrcode"></i><span>Assign promo code</span></a>
        </li>
    </ul>

</li>

<li class="{{ Request::is('promocode*') ? 'active' : '' }}">
    <a href="#">
        <i class="fa fa-money"></i> <span>Refund management</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('hotel_refund*') ? 'active' : '' }}">
            <a href="{!! route('hotel_refund.index') !!}"><i class="fa fa-money"></i><span>Hotels</span></a>
        </li>
    </ul>
    <ul class="treeview-menu">
        <li class="{{ Request::is('hotel_refund*') ? 'active' : '' }}">
            <a href="{!! route('hotel_refund.index') !!}"><i class="fa fa-money"></i><span>Flights</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('user_refund*') ? 'active' : '' }}">
    <a href="{!! route('user_refund.index') !!}"><i class="fa fa-edit"></i><span>User refund management</span></a>
</li>



<li class="{{ Request::is('devices*') ? 'active' : '' }}">
    <a href="{!! route('devices.index') !!}"><i class="fa fa-edit"></i><span>Devices</span></a>
</li>

