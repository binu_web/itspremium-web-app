@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Show Assigned promocode
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('promocode_service_map.show_fields')
                    <a href="{!! route('promocode_service_map.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
