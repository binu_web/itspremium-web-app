@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Assign promo code to service
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'promocode_service_map.store']) !!}

                    @include('promocode_service_map.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
