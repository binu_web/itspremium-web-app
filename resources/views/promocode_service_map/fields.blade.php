<!-- User Id Field -->
<!-- Service Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('promocode_id', 'Promo Code:') !!}
    {!! Form::select('promocode_id',$promocodes, null, ['class' => 'form-control']) !!}
</div>

<!-- Service Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_api_id', 'Service:') !!}
    {!! Form::select('service_api_id[]',$services, null, ['multiple'=>'multiple','class' => 'form-control']) !!}
</div>

<!-- is active Field -->
<div class="form-group col-sm-12 radio_isactive ">
    {!! Form::label('is_active', ' Is Active:') !!}&nbsp;&nbsp;
    {!! Form::radio('is_active', 1,true).'Active' !!}&nbsp;&nbsp;&nbsp;
    {!! Form::radio('is_active', 0,false).'Inactive' !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('promocode_service_map.index') !!}" class="btn btn-default">Cancel</a>
</div>
