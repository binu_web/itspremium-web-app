@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Edit Promo code service map
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($promocode_service_map, ['route' => ['promocode_service_map.update', $promocode_service_map->id], 'method' => 'patch']) !!}

                    @include('promocode_service_map.edit_fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection