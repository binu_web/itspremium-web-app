<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $promocode_service_map->id !!}</p>
</div>
<!-- Service Category Field -->
<div class="form-group">
    {!! Form::label('promocode_id', 'Promocode:') !!}
    @foreach($promocodes as $key => $value)
        @if($key == $promocode_service_map->promocode_id)
            <p>{!! $value !!}</p>
        @endif
    @endforeach
</div>
<!-- Service Category Field -->
<div class="form-group">
    {!! Form::label('service_api_id', 'Service:') !!}
    @foreach($services as $key => $value)
        @if($key == $promocode_service_map->service_api_id)
            <p>{!! $value !!}</p>
        @endif
    @endforeach
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $promocode_service_map->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $promocode_service_map->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $promocode_service_map->deleted_at !!}</p>
</div>

