<!-- Source Field -->
<div class="col-sm-12">

    <div class="form-group col-sm-6">
        <label>Select Source</label>
        <select class="form-control" id="drop_source">
        </select>
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('source', 'Source:') !!}
        {!! Form::text('source', null, ['class' => 'form-control','id'=>'source','readonly']) !!}
    </div>
</div>

<!-- Source Slug Field -->
<div class="col-sm-12">
    <div class="form-group col-sm-6">
        {!! Form::label('source_slug', 'Source Slug:') !!}
        {!! Form::text('source_slug', null, ['class' => 'form-control','id'=>'source_slug','readonly']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('newsSources.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>

<?php
$sourceCat = $sourceCategory;
$sour = $source;
?>
@section('scripts')
    <script type="text/javascript" src="{{asset("resources/assets/js/moment.js")}}"></script>
    <script type="text/javascript" src="{{asset("resources/assets/js/bootstrap-datetimepicker.min.js")}}"></script>
    <script type="text/javascript" src="{{asset("resources/assets/js/jquery.validate.min.js")}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            source()
        });
        function source() {
            var drop = '';
            $sour =<?php echo json_encode ($sour); ?>;
            $sourceCa = <?php echo json_encode ($sourceCat); ?>;

            drop += '<option>--Select--</option>';
            $.each($sour, function (i, ApiValue) {
                $c = 0;
                $.each($sourceCa, function (j, tblValue) {
                    if (tblValue.source_slug == ApiValue.id) {
                        $c++;
                        console.log(tblValue.source_slug);
                        console.log(ApiValue.id);
                    }
                });
                if ($c == 0) {
                    drop += '<option value="' + ApiValue.id + '">' + ApiValue.name + '</option>';
                }
                $c = 0;
            });
            $('#drop_source').html(drop);
        }
        $('#drop_source').change(function () {
            if ($("#drop_source option:selected").text() != '--Select--') {
                var txt = $("#drop_source option:selected").text();
                var val = $("#drop_source option:selected").val();
            }
            $('#source').val(txt);
            $('#source_slug').val(val);

        });
    </script>
@stop
