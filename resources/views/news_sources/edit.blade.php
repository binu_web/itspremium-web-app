@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            News Sources
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($newsSource, ['route' => ['newsSources.update', $newsSource->id], 'method' => 'patch', 'files' => true]) !!}

                    @include('news_sources.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection