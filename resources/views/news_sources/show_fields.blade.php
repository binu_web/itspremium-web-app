<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $newsSource->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('source', 'Source:') !!}
    <p>{!! $newsSource->source !!}</p>
</div>


<div class="form-group">
    {!! Form::label('source_slug', 'Source Slug:') !!}
    <p>{!! $newsSource->source_slug !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $newsSource->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $newsSource->updated_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $newsSource->created_by_name !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $newsSource->updated_by_name !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $newsSource->deleted_at !!}</p>
</div>

