<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $transaction->id !!}</p>
</div>

<!-- Booked Id Field -->
<div class="form-group">
    {!! Form::label('booked_id', 'Booked Id:') !!}
    <p>{!! $transaction->booked_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $transaction->user_id !!}</p>
</div>

<!-- Gateway Id Field -->
<div class="form-group">
    {!! Form::label('gateway_id', 'Gateway Id:') !!}
    <p>{!! $transaction->gateway_id !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $transaction->amount !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $transaction->status !!}</p>
</div>

<!-- Status Msg Field -->
<div class="form-group">
    {!! Form::label('status_msg', 'Status Msg:') !!}
    <p>{!! $transaction->status_msg !!}</p>
</div>

<!-- Refund Status Field -->
<div class="form-group">
    {!! Form::label('refund_status', 'Refund Status:') !!}
    <p>{!! $transaction->refund_status !!}</p>
</div>

<!-- Refund Msg Field -->
<div class="form-group">
    {!! Form::label('refund_msg', 'Refund Msg:') !!}
    <p>{!! $transaction->refund_msg !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $transaction->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $transaction->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $transaction->deleted_at !!}</p>
</div>

