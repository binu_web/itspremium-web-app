<!-- Booked Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('booked_id', 'Booked Id:') !!}
    {!! Form::number('booked_id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Gateway Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gateway_id', 'Gateway Id:') !!}
    {!! Form::number('gateway_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::number('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Msg Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('status_msg', 'Status Msg:') !!}
    {!! Form::textarea('status_msg', null, ['class' => 'form-control']) !!}
</div>

<!-- Refund Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('refund_status', 'Refund Status:') !!}
    {!! Form::text('refund_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Refund Msg Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('refund_msg', 'Refund Msg:') !!}
    {!! Form::textarea('refund_msg', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('transactions.index') !!}" class="btn btn-default">Cancel</a>
</div>
