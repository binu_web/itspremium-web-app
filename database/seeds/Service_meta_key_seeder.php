<?php

use Illuminate\Database\Seeder;

class Service_meta_key_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
            DB::table ('service_meta_key')->truncate ();


            $data = array(
                array('service_id' => 2 , 'meta_key' => 'status' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'product_id' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'destination_latitude' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'destination_longitude' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'driver' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'pickup_latitude' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'pickup_longitude' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'request_id' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'eta' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'location' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'vehicle' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'shared' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'driver_phone_number' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'driver_rating' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'driver_picture_url' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'driver_name' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'driver_sms_number' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'bearing' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'vehicle_make' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'vehicle_picture_url' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'vehicle_model' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'vehicle_license_plate' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,

				array('service_id' => 2 , 'meta_key' => 'event_id' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'event_time' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'event_type' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'meta_user_id' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'meta_resource_id' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'meta_status' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'resource_href' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,

				array('service_id' => 1 , 'meta_key' => 'booking_id' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'crn' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'driver_name' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'driver_number' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'cab_type' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'cab_number' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'car_model' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'car_color' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'eta' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'driver_lat' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'driver_lng' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'share_ride_url' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,

				array('service_id' => 1 , 'meta_key' => 'request_type' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'pickup_lat' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'pickup_lng' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'drop_lat' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'drop_lng' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'ola_money_balance' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'trip_info_amount' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'trip_info_payable_amount' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'trip_info_distance_value' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'trip_info_distance_unit' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'trip_info_trip_time_value' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'trip_info_trip_time_unit' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'trip_info_wait_time_value' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'trip_info_wait_time_unit' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'trip_info_discount' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'trip_info_advance' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'trip_info_mode_of_advance' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 1 , 'meta_key' => 'booking_status' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,

				array('service_id' => 1 , 'meta_key' => 'cancellation_reason' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,

				array('service_id' => 1 , 'meta_key' => 'message' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,

				array('service_id' => 2 , 'meta_key' => 'driver_lat' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 2 , 'meta_key' => 'driver_lng' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,


				array('service_id' => 4 , 'meta_key' => 'booking_id' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'display_id' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'diner_name' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'restaurant_name' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'restaurant_address' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'dining_date_time' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'people' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'special_request' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'offer_text' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'city_name' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'area_name' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'locality_name' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,

				array('service_id' => 4 , 'meta_key' => 'diner_email' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'diner_phone' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'booking_date' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'booking_time' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'rest_id' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'male' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'order_id' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,

				array('service_id' => 4 , 'meta_key' => 'special_requirement' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'offer' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'no_of_people' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'no_of_kids' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'dining_date' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'booking_status' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'booking_added_date' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'table_allocation_status' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,

				array('service_id' => 4 , 'meta_key' => 'b_id' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'affiliate_id' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'disp_id' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'offer_id' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'status' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('service_id' => 4 , 'meta_key' => 'url' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,

				//Hotels - Yatra Hotels Starts:
				['service_id' => 5, 'meta_key' => 'hotel_code', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'no_of_rooms', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'guest_details', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'rate_plan_code', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'room_type_code', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'hotel_name', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'area_name', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'address_line', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'city_name', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'state', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'country_name', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'hotel_contact_numbers', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'stay_start_date', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'stay_end_date', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'hotel_room_base_amount_before_tax', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],

				['service_id' => 5, 'meta_key' => 'total_additional_guest_amount', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'hotel_discount_amount', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'net_hotel_amount_before_tax', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],

				['service_id' => 5, 'meta_key' => 'hotel_tax_amount', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'hotel_tax_name', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'hotel_final_amount_with_tax', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],

				['service_id' => 5, 'meta_key' => 'service_charge', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'service_charge_percetage', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'tax_for_service_charge', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'service_charge_tax_percentage', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'service_tax_name', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],

				['service_id' => 5, 'meta_key' => 'promo_id', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'discount_percentage', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'discount_amount', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'net_amount_to_pay', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'affiliate_commission_amount', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'affiliate_commission_percentage', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'affiliate_commission_hotel_tax_included', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],


				['service_id' => 5, 'meta_key' => 'customer_special_instruction', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'customer_name_prefix', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'customer_first_name', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'customer_middle_name', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'customer_surname', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'customer_phone_number', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'customer_phone_type', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'customer_email_id', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'gaurantee_type', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'payment_status', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'payment_referrance_id', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'provisional_booking_uniqueid_type', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'provisional_booking_unique_id', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'final_booking_uniqueid_type', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'final_booking_unique_id', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'booking_status', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'cancel_penalty_info_text', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],

				['service_id' => 5, 'meta_key' => 'correlation_id', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'cancel_policy_text', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'cancel_date', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'cancel_type', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'cancel_refund_amount', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'cancel_currency_code', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],

				['service_id' => 5, 'meta_key' => 'cancel_confirm_text', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'cancel_unique_id', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],

				['service_id' => 5, 'meta_key' => 'refund_unique_id', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'refund_entity', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'refund_amount', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'refund_currency', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 5, 'meta_key' => 'refund_created_at', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				

				//Yatra Hotel ends
				//new seeder in Uber:
				['service_id' => 2, 'meta_key' => 'fare_amount', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 2, 'meta_key' => 'currency_code', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 2, 'meta_key' => 'distance_unit', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 2, 'meta_key' => 'duration_estimate', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 2, 'meta_key' => 'distance_estimate', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],

				//new seeder in ola:

				['service_id' => 1, 'meta_key' => 'min_fare', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 1, 'meta_key' => 'max_fare', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 1, 'meta_key' => 'distance', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 1, 'meta_key' => 'duration', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 1, 'meta_key' => 'currency', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],

				//new seeder in Uber:
				['service_id' => 2, 'meta_key' => 'receipt_subtotal', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 2, 'meta_key' => 'receipt_total_charged', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 2, 'meta_key' => 'receipt_total_owed', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 2, 'meta_key' => 'receipt_total_fare', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 2, 'meta_key' => 'receipt_travelled_duration', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],
				['service_id' => 2, 'meta_key' => 'receipt_travelled_distance', 'created_at' => date ('Y-m-d H:i:s'), 'updated_at' => date ('Y-m-d H:i:s')],




			);
            DB::table ('service_meta_key')->insert ($data);

			DB::statement ('SET FOREIGN_KEY_CHECKS=1;');

        }
    }

}
