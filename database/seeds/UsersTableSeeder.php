<?php
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run ()
	{
		DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
		DB::table ('users')->truncate ();

		DB::statement ('SET FOREIGN_KEY_CHECKS=1;');

		//1
		$data = array(
			'email' => 'admin@admin.com' ,
			'mobile' => '985555555555' ,
			'password' => \Illuminate\Support\Facades\Hash::make ('password') ,
			'first_name' => 'Admin' ,
			'last_name' => 'Admin' ,
			'profile_pic' => ' ' ,
			'country' => null ,
			'is_active' => '1' ,
			'activation_token' => ' ' ,
			'created_at' => date ('Y-m-d H:i:s') ,
			'updated_at' => date ('Y-m-d H:i:s')

		);


		$user = \App\Models\User::insert ($data);
	}
}
