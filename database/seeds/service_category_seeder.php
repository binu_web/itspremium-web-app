<?php

use Illuminate\Database\Seeder;

class service_category_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
            DB::table ('services')->truncate ();


            $data = array(
                array('name' => 'Cabs' , 'image' => '1492671263.jpg' , 'is_active' => '1' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
                array('name' => 'Restaurants' , 'image' => '1492671263.jpg' , 'is_active' => '1' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')),
                array('name' => 'Hotels' , 'image' => '1492671263.jpg' , 'is_active' => '1' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')),



            );
            DB::table ('services')->insert ($data);
            DB::statement ('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
