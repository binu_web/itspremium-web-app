<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class YatraHotelDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	ini_set('memory_limit', '2048M');
        DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
        DB::table ('yatra_hotel_details')->truncate ();
        DB::statement ('SET FOREIGN_KEY_CHECKS=1;');

        $file = 'storage/app/yatra_hotel/1300001176.xls';
        $rows = Excel::load($file)->get()->toArray();

        $aHotelData = [];
        foreach($rows as $key => $aVal) {
        	$aData 		 = [];
        	$aData['VendorId']   		= $aVal['vendorid'];
        	$aData['VendorName']   		= $aVal['vendorname'];
        	$aData['HotelClass']  		= $aVal['hotelclass'];
        	$aData['Location']   		= $aVal['location'];
        	$aData['City']   			= $aVal['city'];
        	$aData['Country']   		= $aVal['country'];
        	$aData['Address1']  		= $aVal['address1'];
        	$aData['Address2']   		= $aVal['address2'];
        	$aData['Area']   			= $aVal['area'];
        	$aData['Address']   		= $aVal['address'];
        	$aData['HotelOverview']     = $aVal['hoteloverview'];
        	$aData['ReviewRating']   	= $aVal['reviewrating'];
        	$aData['ReviewCount']   	= $aVal['reviewcount'];
        	$aData['Latitude']   		= $aVal['latitude'];
        	$aData['Longitude']   				= $aVal['longitude'];
        	$aData['DefaultCheckInTime']   		= $aVal['defaultcheckintime'];
        	$aData['DefaultCheckOutTime']   	= $aVal['defaultcheckouttime'];
        	$aData['Hotel_Star']   				= $aVal['hotel_star'];
        	$aData['HotelGroupID']   			= $aVal['hotelgroupid'];
        	$aData['HotelGroupName']   			= $aVal['hotelgroupname'];
        	$aData['ImagePath']   				= $aVal['imagepath'];
        	$aData['HotelSearchKey']   			= $aVal['hotelsearchkey'];
        	$aData['Area_Seo_Id']   			= $aVal['area_seo_id'];
        	$aData['SecondaryAreaIds']   		= $aVal['secondaryareaids'];
        	$aData['SecondaryAreaName']   		= $aVal['secondaryareaname'];
        	$aData['NoOfFloors']   				= $aVal['nooffloors'];
        	$aData['NoOfRooms']   				= $aVal['noofrooms'];
        	$aData['State']   					= $aVal['state'];
        	$aData['PinCode']   				= $aVal['pincode'];
        	$aData['ThemeList']   				= $aVal['themelist'];
        	$aData['CategoryList']   			= $aVal['categorylist'];
        	$aData['CityZone']   				= $aVal['city_zone'];
        	$aData['WeekDay_Rank']   			= $aVal['weekday_rank'];
        	$aData['WeekEnd_Rank']   			= $aVal['weekend_rank'];

        	$aHotelData[] = $aData;
        }

        DB::table ('yatra_hotel_details')->insert ($aHotelData);
        echo 'Migration completed';
        
        

        

    }
}
