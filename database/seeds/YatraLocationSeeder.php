<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use App\Services\XmlToJson;

class YatraLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
        DB::table ('yatra_locations')->truncate ();
        DB::statement ('SET FOREIGN_KEY_CHECKS=1;');

        $cityXmlFile    = 'storage/app/yatra_hotel/city.xml';
        $areaXmlFile    = 'storage/app/yatra_hotel/area.xml';

        $cityXml 		= file_get_contents($cityXmlFile);
        $areaXml 		= file_get_contents($areaXmlFile);

        $aCityXmlData 		= XmlToJson::get($cityXml);

        $aAreaXmlData       = XmlToJson::get($areaXml);

        //city:

        $aCity 				= [];
        $aCities 			= $aCityXmlData['Cities']['City'];

        foreach($aCities as $key => $city) {
        	$_aTemp 			= [];
        	$_aTemp['name'] 	= $city;
        	$_aTemp['type'] 	= 1;
        	$_aTemp['area_id']  = null;
        	$aCity[]			= $_aTemp;
        }

       # print_r($aCity[0]); die;

        DB::table ('yatra_locations')->insert($aCity);
        echo 'city data seeded';

        $aAreaData 		   = $aAreaXmlData['Areas']['Area'];
        $aArea 			   = [];
        foreach($aAreaData as $key => $aVal) {
        	$_aTemp = [];
        	$_aTemp['name'] 	= $aVal['AreaName'].", ".$aVal['CityName'];
        	$_aTemp['type'] 	= 2;
        	$_aTemp['area_id']  = $aVal['AreaId'];
        	$aArea[]			= $_aTemp;
        }

        DB::table('yatra_locations')->insert($aArea);
        echo "Area data seeded";
       
    }
}
