<?php

use Illuminate\Database\Seeder;

class service_log_event_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
        DB::table ('service_log_event_type')->truncate ();


        $data = array(
            array('service_api_id' => 5 , 'event' => 'city search' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
            array('service_api_id' => 5 , 'event' => 'hotel details search' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
            array('service_api_id' => 5 , 'event' => 'provisional booking' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
            array('service_api_id' => 5 , 'event' => 'final booking' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
            array('service_api_id' => 5 , 'event' => 'process cancel' ,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
            array('service_api_id' => 5 , 'event' => 'confirm cancel' , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
        );

        DB::table ('service_log_event_type')->insert ($data);

        DB::statement ('SET FOREIGN_KEY_CHECKS=1;');

    }
}
