<?php

use Illuminate\Database\Seeder;

class service_api_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
            DB::table ('service_apis')->truncate ();


            $data = array(
                array('provider_name' => 'Ola' , 'service_category' => '1' ,'api_key'=>'bkdfhjbs782434','api_url'=>'https://developers.olacabs.com/','api_token'=>'894nflksndfkdmsf','api_metadata'=>'sdhfsdf','api_image' => 'ola.png' , 'is_active' => '1' ,'created_by'=>1,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
                array('provider_name' => 'Uber' , 'service_category' => '1' ,'api_key'=>'bkdfhjbs782434','api_url'=>'https://developer.uber.com/','api_token'=>'894nflksndfkdmsf','api_metadata'=>'sdhfsdf','api_image' => 'uber.png' , 'is_active' => '1' ,'created_by'=>1,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
                array('provider_name' => 'Zomato' , 'service_category' => '2' ,'api_key'=>'bkdfhjbs782434','api_url'=>'https://developers.zomato.com/','api_token'=>'894nflksndfkdmsf','api_metadata'=>'sdhfsdf','api_image' => 'zomato.png' , 'is_active' => '1' ,'created_by'=>1,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
                array('provider_name' => 'Dineout' , 'service_category' => '2' ,'api_key'=>'bkdfhjbs782434','api_url'=>'http://api.dineoutdeals.in/apidoc/','api_token'=>'DO231909','api_metadata'=>'ksdfjds','api_image' => 'dineout.png' , 'is_active' => '1' ,'created_by'=>1,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
                array('provider_name' => 'Yatra' , 'service_category' => '3' ,'api_key'=>'bkdfhjbs782434','api_url'=>'http://api.dineoutdeals.in/apidoc/','api_token'=>'DO231909','api_metadata'=>'ksdfjds','api_image' => 'dineout.png' , 'is_active' => '1' ,'created_by'=>1,'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,



            );
            DB::table ('service_apis')->insert ($data);
            DB::statement ('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
