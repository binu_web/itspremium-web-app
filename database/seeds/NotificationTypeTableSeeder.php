<?php

use Illuminate\Database\Seeder;

class NotificationTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
        DB::table ('notification_type')->truncate ();
        DB::statement ('SET FOREIGN_KEY_CHECKS=1;');

        $data = array(
            array('type' => 'custom' , 'name' => 'booking' , 'is_active' => 1, 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
            array('type' => 'service' , 'name' => 'cancel' , 'is_active' => 1, 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
            array('type' => 'custom' , 'name' => 'cancel' , 'is_active' => 1, 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
            array('type' => 'service' , 'name' => 'booking' , 'is_active' => 1, 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
            array('type' => 'service' , 'name' => 'refund' , 'is_active' => 1, 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
        );
        DB::table ('notification_type')->insert ($data);
    }
}
