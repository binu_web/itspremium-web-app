<?php

use Illuminate\Database\Seeder;

class cabDetailsLog extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run ()
	{
		{
			DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
			DB::table ('cab_details_log')->truncate ();

			DB::statement ('SET FOREIGN_KEY_CHECKS=1;');
			$data = array(
				array('user_id' => 1 , 'location_type' => 'PICKUP' , 'location' => 'Koratty Infopark, Koratty, Kerala, Indien' , 'latitude' => '10.2682105' , 'longitude' => '76.3543222' , 'is_favorite' => 1 , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('user_id' => 1 , 'location_type' => 'PICKUP' , 'location' => 'Kaloor, Kochi, Kerala, Indien' , 'latitude' => '9.9970903' , 'longitude' => '76.302815' , 'is_favorite' => 0 , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('user_id' => 1 , 'location_type' => 'DROPOFF' , 'location' => 'Thrikkakara, Ernakulam, Kerala, Indien' , 'latitude' => '10.0327153' , 'longitude' => '76.331889' , 'is_favorite' => 1 , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('user_id' => 1 , 'location_type' => 'PICKUP' , 'location' => 'Thrippunithura, Kochi, Kerala, Indien' , 'latitude' => '9.9486828' , 'longitude' => '76.3463624' , 'is_favorite' => 1 , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('user_id' => 1 , 'location_type' => 'DROPOFF' , 'location' => 'Fort Kochi, Kochi, Kerala, Indien' , 'latitude' => '9.9657787' , 'longitude' => '76.3080794' , 'is_favorite' => 1 , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('user_id' => 1 , 'location_type' => 'DROPOFF' , 'location' => 'Marine Drive, Kochi, Kerala, Indien' , 'latitude' => '9.9825798' , 'longitude' => '76.2754275' , 'is_favorite' => 1 , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('user_id' => 1 , 'location_type' => 'PICKUP' , 'location' => 'Thrippunithura, Kochi, Kerala, Indien' , 'latitude' => '9.9486828' , 'longitude' => '76.3463624' , 'is_favorite' => 1 , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('user_id' => 1 , 'location_type' => 'PICKUP' , 'location' => 'Koratty Infopark, Koratty, Kerala, Indien' , 'latitude' => '10.2682105' , 'longitude' => '76.3543222' , 'is_favorite' => 1 , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('user_id' => 1 , 'location_type' => 'DROPOFF' , 'location' => 'Kaloor, Kochi, Kerala, Indien' , 'latitude' => '9.9970903' , 'longitude' => '76.302815' , 'is_favorite' => 0 , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
				array('user_id' => 1 , 'location_type' => 'PICKUP' , 'location' => 'Muringoor, Muringur Vadakkummuri, Kerala, Indien' , 'latitude' => '10.2834371' , 'longitude' => '76.3420206' , 'is_favorite' => 1 , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
			);
			DB::table ('cab_details_log')->insert ($data);
		}
	}
}
