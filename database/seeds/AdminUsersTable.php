<?php

use Illuminate\Database\Seeder;

class AdminUsersTable extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run ()
	{

		DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
		DB::table ('admin_users')->truncate ();
		DB::statement ('SET FOREIGN_KEY_CHECKS=1;');

		$data = array(
			'email' => 'admin@admin.com' ,
			'mobile' => '985555555555' ,
			'password' => \Illuminate\Support\Facades\Hash::make ('password') ,
			'user_type' => 1 ,
			'first_name' => 'Admin' ,
			'last_name' => 'Admin' ,
			'job_role' => 'Administrator' ,
			'is_active' => '1' ,
			'created_at' => date ('Y-m-d H:i:s') ,
			'updated_at' => date ('Y-m-d H:i:s')
		);

		DB::table ('admin_users')->insert ($data);
	}
}
