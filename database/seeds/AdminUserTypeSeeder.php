<?php

use Illuminate\Database\Seeder;

class AdminUserTypeSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run ()
	{
		DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
		DB::table ('admin_user_type')->truncate ();

		DB::statement ('SET FOREIGN_KEY_CHECKS=1;');
		$data = array(
			array('type' => 'Super Admin' , 'is_active' => 1 , 'created_by' => null , 'updated_by' => null , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
			array('type' => 'Service Admin' , 'is_active' => 1 , 'created_by' => null , 'updated_by' => null , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
			array('type' => 'User Admin' , 'is_active' => 1 , 'created_by' => null , 'updated_by' => null , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,
			array('type' => 'Report Admin' , 'is_active' => 1 , 'created_by' => null , 'updated_by' => null , 'created_at' => date ('Y-m-d H:i:s') , 'updated_at' => date ('Y-m-d H:i:s')) ,

		);
		$adminUserType = \App\Models\AdminUserType::insert ($data);
	}
}
	