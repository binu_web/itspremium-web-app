<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblNews extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('news' , function (Blueprint $table) {
			$table->increments ('id');
			$table->integer ('category')->unsigned ()->nullable ();
			$table->foreign ('category')->references ('id')->on ('news_categories');
			$table->string ('title')->nullable ();
			$table->string ('image')->nullable ();
			$table->string ('source_link')->nullable ();
			$table->text ('description')->nullable ();
			$table->date ('publish_date')->nullable ();
			$table->boolean ('is_trending')->default (1);
			$table->boolean ('is_active')->default (1);
			$table->timestamps ();
			$table->integer ('created_by')->unsigned ()->nullable ();
			$table->foreign ('created_by')->references ('id')->on ('admin_users');
			$table->integer ('updated_by')->unsigned ()->nullable ();
			$table->foreign ('updated_by')->references ('id')->on ('admin_users');
			$table->softDeletes ();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('news');
	}
}
