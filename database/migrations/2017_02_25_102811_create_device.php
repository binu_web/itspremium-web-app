<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevice extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ("devices" , function (Blueprint $table) {
			$table->increments ('id');
			$table->enum ('type' , ['ANDROID' , 'IOS']);
			$table->string ('device_id' , 255)->nullable ();
			$table->integer ('user_id')->unsigned ()->nullable ();
			$table->foreign ('user_id')->references ('id')->on ('users');
			$table->string ('regId' , 255)->nullable ();
			$table->string ('device_name' , 255)->nullable ();
			$table->string ('device_token' , 255)->nullable ();
			$table->string ('builder_version' , 255)->nullable ();
			$table->string ('os_version' , 255)->nullable ();
			$table->integer ('screen_width')->unsigned ()->nullable ();
			$table->integer ('screen_height')->unsigned ()->nullable ();
			$table->string ('ip_address' , 255)->nullable ();
			$table->text ('fcm_token')->nullable ();

			$table->timestamps ();
			$table->softDeletes ();
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('devices');
	}
}
