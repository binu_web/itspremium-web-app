<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserForOtpVerification extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::table ('users' , function (Blueprint $table) {
			$table->string ('otp_token' , 50)->nullable ();;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		//        Schema::drop('users');

		Schema::table ('users' , function (Blueprint $table) {
			$table->dropColumn ('otp_token');


		});
	}
}
