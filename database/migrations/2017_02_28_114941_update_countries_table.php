<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCountriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::table ('countries' , function (Blueprint $table) {
			$table->string ('nicename' , 255)->nullable ();
			$table->boolean ('is_active')->default (1);
			$table->timestamps ();
			$table->string ('name' , 255)->nullable ()->change ();
			$table->string ('deleted_at' , 10)->nullable ()->change ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::table ('countries' , function ($table) {
			$table->dropColumn ('nicename');
		});
	}
}
