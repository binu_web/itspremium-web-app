<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceEventTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('service_log_event_type' , function (Blueprint $table) {
            $table->increments ('id');
            $table->Integer ('service_api_id')->unsigned ();
            $table->foreign ('service_api_id')->references ('id')->on ('service_apis');
            $table->string ('event' , 100)->nullable ();
            $table->timestamps ();
            $table->integer ('created_by')->unsigned ()->nullable ();
            $table->foreign ('created_by')->references ('id')->on ('admin_users');
            $table->integer ('updated_by')->unsigned ()->nullable ();
            $table->foreign ('updated_by')->references ('id')->on ('admin_users');
            $table->softDeletes ();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('service_log_event_type');

    }
}
