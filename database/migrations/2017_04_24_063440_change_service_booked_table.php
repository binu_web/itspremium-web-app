<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeServiceBookedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table ('services_booked' , function ($table) {
            $table->string ('booking_key',255)->nullable ()->after ('service_api_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table ('services_booked' , function ($table) {
            $table->dropColumn ('booking_key');
        });
    }
}
