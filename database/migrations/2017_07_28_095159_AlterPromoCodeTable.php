<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPromoCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promo_code', function (Blueprint $table) {
            $table->tinyInteger('deduction_type')->default(1)->comment('1: cash back to wallet 2: direct deducton')->after('no_of_maximum_use');
            $table->integer('no_of_times_per_user')->nullable()->after('deduction_type');
            $table->integer('no_of_times_per_user_per_service')->nullable()->after('no_of_times_per_user');
            $table->text('validation_rules')->nullable()->after('description');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promo_code', function (Blueprint $table) {
            $table->dropColumn('validation_rules');
            $table->dropColumn('deduction_type');
            $table->dropColumn('no_of_times_per_user');
            $table->dropColumn('no_of_times_per_user_per_service');
        });
    }
}
