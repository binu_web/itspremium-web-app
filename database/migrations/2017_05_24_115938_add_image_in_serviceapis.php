<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageInServiceapis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table ('service_apis' , function ($table) {
            $table->string ('api_image',255)->nullable ()->after ('api_metadata');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table ('service_apis' , function ($table) {
            $table->dropColumn ('api_image');
        });
    }
}
