<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymentMethods extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::table ('payment_methods' , function (Blueprint $table) {
			$table->string ('api_secret' , 255);
			$table->string ('redirect_url' , 255);


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		//        Schema::drop('payment_methods');

		Schema::table ('payment_methods' , function (Blueprint $table) {
			$table->dropColumn ('api_secret');
			$table->dropColumn ('redirect_url');


		});

	}
}
