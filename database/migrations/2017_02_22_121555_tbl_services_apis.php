<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblServicesApis extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('service_apis' , function (Blueprint $table) {
			$table->increments ('id');
			$table->string ('provider_name' , 255)->nullable ();
			$table->integer ('service_category')->unsigned ()->nullable ();
			$table->foreign ('service_category')->references ('id')->on ('services');
			$table->string ('api_key' , 255)->nullable ();
			$table->string ('api_url' , 255)->nullable ();
			$table->string ('api_token' , 255)->nullable ();
			$table->text ('api_metadata')->nullable ();
			$table->boolean ('is_active')->default (1);
			$table->integer ('created_by')->unsigned ()->nullable ();
			$table->foreign ('created_by')->references ('id')->on ('admin_users');
			$table->integer ('updated_by')->unsigned ()->nullable ();
			$table->foreign ('updated_by')->references ('id')->on ('admin_users');
			$table->timestamps ();
			$table->softDeletes ();


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('service_apis');
	}
}
