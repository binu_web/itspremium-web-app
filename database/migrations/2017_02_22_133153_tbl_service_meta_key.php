<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblServiceMetaKey extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('service_meta_key' , function (Blueprint $table) {
			$table->increments ('id');
			$table->Integer ('service_id')->unsigned ();
			$table->foreign ('service_id')->references ('id')->on ('services');
			$table->string ('meta_key' , 100)->nullable ();
			$table->timestamps ();
			$table->integer ('created_by')->unsigned ()->nullable ();
			$table->foreign ('created_by')->references ('id')->on ('admin_users');
			$table->integer ('updated_by')->unsigned ()->nullable ();
			$table->foreign ('updated_by')->references ('id')->on ('admin_users');
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('service_meta_key');
	}
}
