<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblAdminLogType extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('admin_log_type' , function (Blueprint $table) {
			$table->increments ('id');
			$table->string ('name')->nullable ();
			$table->timestamps ();
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('admin_log_type');
	}
}
