<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletsLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_log', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer ('wallet_id')->unsigned ();
            $table->foreign ('wallet_id')->references ('id')->on ('wallet');
            $table->Integer ('service_id')->unsigned ();
            $table->foreign ('service_id')->references ('id')->on ('service_apis');
            $table->float('balance', 8, 2);
            $table->enum ('transaction_type' , ['CREDIT' , 'DEBIT']);
            $table->string ('transaction_method' , 30)->nullable ()->comment('for which transactions like book,cancel,refund');
            $table->timestamps();
            $table->softDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wallet_log');
    }
}
