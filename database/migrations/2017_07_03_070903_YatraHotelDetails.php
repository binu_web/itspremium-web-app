<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class YatraHotelDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yatra_hotel_details', function (Blueprint $table) {
            /*
            * We keeps the excel headings exactly as field names. Only reqd fields are creating
            */
            $table->string('VendorId');
            $table->primary('VendorId');
            $table->string('VendorName', 300)->nullable();
            $table->string('HotelClass', 50)->nullable();
            $table->string('Location', 300)->nullable();
            $table->string('City', 300)->nullable();
            $table->string('Address1', 300)->nullable();
            $table->string('Address2', 300)->nullable();
            $table->text('HotelOverview')->nullable();
            $table->string('ReviewRating', 10)->nullable();
            $table->string('ReviewCount', 10)->nullable();
            $table->string('Latitude', 20)->nullable();
            $table->string('Longitude', 20)->nullable();
            $table->string('DefaultCheckInTime', 25)->nullable();
            $table->string('DefaultCheckOutTime', 25)->nullable();
            $table->string('Hotel_Star', 10)->nullable();
            $table->string('HotelGroupName', 300)->nullable();
            $table->string('ImagePath', 400)->nullable();
            $table->string('NoOfFloors', 10)->nullable();
            $table->string('NoOfRooms', 10)->nullable();
            $table->string('State', 300)->nullable();
            $table->string('PinCode', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('yatra_hotel_details');
    }
}
