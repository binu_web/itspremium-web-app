<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblNewsBookMark extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('news_bookmark' , function ($table) {
            $table->increments ('id');
            $table->integer ('user_id')->unsigned ();
            $table->foreign ('user_id')->references ('id')->on ('users');
            $table->integer ('news_id')->unsigned ();
            $table->foreign ('news_id')->references ('id')->on ('news');
            $table->boolean('is_saved')->default(0);
            $table->timestamps ();
            $table->softDeletes ();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('news_bookmark');
    }
}
