<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblTransactions extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('transactions' , function (Blueprint $table) {
			$table->increments ('id');
			$table->Integer ('booked_id')->unsigned ();
			$table->foreign ('booked_id')->references ('id')->on ('services_booked');
			$table->Integer ('user_id')->unsigned ();
			$table->foreign ('user_id')->references ('id')->on ('users');
			$table->Integer ('gateway_id')->unsigned ()->nullable ();
			$table->float ('amount' , 10 , 3)->default ('0.00');
			$table->enum ('status' , ['PENDING' , 'INPROGRESS' , 'SUCCESS' , 'CANCELLED']);
			$table->text ('status_msg')->nullable ();
			$table->enum ('refund_status' , ['NULL' , 'INPROGRESS' , 'SUCCESS' , 'FAILURE']);
			$table->text ('refund_msg')->nullable ();
			$table->timestamps ();
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('transactions');
	}
}
