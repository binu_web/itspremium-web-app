<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblNotificationType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('notification_type' , function ($table) {
            $table->increments ('id');
            $table->string('type');
            $table->string('name');
            $table->boolean('is_active')->default (1);
            $table->timestamps ();
            $table->softDeletes ();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('notification_type');
    }
}
