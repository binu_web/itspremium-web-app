<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurentLogDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('restaurant_log_details' , function ($table) {
            $table->increments ('id');
            $table->integer ('user_id')->unsigned ()->nullable ();
            $table->foreign ('user_id')->references ('id')->on ('users');
            $table->string ('location')->nullable ();
            $table->string ('latitude')->nullable ();
            $table->string ('longitude')->nullable ();
            $table->boolean ('is_favorite')->default (0);
            $table->timestamps ();
            $table->softDeletes ();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop ('restaurant_log_details');
    }
}
