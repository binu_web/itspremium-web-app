<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblNewsCategories extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('news_categories' , function (Blueprint $table) {
			$table->increments ('id');
			$table->string ('name')->nullable ();
			$table->string ('image')->nullable ();
			$table->boolean ('is_active')->default (1);
			$table->timestamps ();
			$table->Integer ('created_by')->unsigned ()->nullable ();
			$table->foreign ('created_by')->references ('id')->on ('admin_users');
			$table->Integer ('updated_by')->unsigned ()->nullable ();
			$table->foreign ('updated_by')->references ('id')->on ('admin_users');
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('news_categories');
	}
}
