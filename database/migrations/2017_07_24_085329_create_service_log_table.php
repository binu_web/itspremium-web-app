<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('service_log' , function (Blueprint $table) {
            $table->increments ('id');
            $table->Integer ('user_id')->unsigned ();
            $table->foreign ('user_id')->references ('id')->on ('users');
            $table->Integer ('service_id')->unsigned ();
            $table->foreign ('service_id')->references ('id')->on ('services');
            $table->Integer ('service_api_id')->unsigned ();
            $table->foreign ('service_api_id')->references ('id')->on ('service_apis');
            $table->Integer ('event_type_id')->unsigned ();
            $table->foreign ('event_type_id')->references ('id')->on ('service_log_event_type');
            $table->enum ('http_type' , ['REQUEST' , 'RESPONSE']);
            $table->enum ('data_type' , ['JSON' , 'XML']);
            $table->longText('data')->nullable ();
            $table->timestamps ();
            $table->softDeletes ();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('service_log');
    }
}
