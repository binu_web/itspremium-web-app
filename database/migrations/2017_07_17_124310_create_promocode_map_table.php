<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocodeMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('promocode_service_map' , function (Blueprint $table) {

            $table->increments ('id');
            $table->integer ('promocode_id')->unsigned ()->nullable ();
            $table->foreign ('promocode_id')->references ('id')->on ('promo_code');
            $table->integer ('service_api_id')->unsigned ()->nullable ();
            $table->foreign ('service_api_id')->references ('id')->on ('service_apis');
            $table->boolean ('is_active')->default (1);
            $table->integer ('created_by')->unsigned ()->nullable ();
            $table->foreign ('created_by')->references ('id')->on ('admin_users');
            $table->integer ('updated_by')->unsigned ()->nullable ();
            $table->foreign ('updated_by')->references ('id')->on ('admin_users');
            $table->timestamps ();
            $table->softDeletes ();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('promocode_service_map');
    }
}
