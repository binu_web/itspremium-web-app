<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblAdminUsers extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('admin_users' , function (Blueprint $table) {
			$table->increments ('id');
			$table->string ('email' , 255);
			$table->string ('password' , 255)->nullable ();
			$table->integer ('user_type')->unsigned ()->nullable ();
			$table->foreign ('user_type')->references ('id')->on ('admin_user_type');
			$table->string ('first_name')->nullable ();
			$table->string ('last_name')->nullable ();
			$table->string ('mobile')->nullable ();
			$table->string ('job_role')->nullable ();
			$table->boolean ('is_active')->default (1);
			$table->string ('activation_code' , 100)->nullable ();
			$table->string ('remember_token' , 255)->nullable ();
			$table->timestamps ();
			$table->integer ('created_by')->nullable ();
			$table->integer ('updated_by')->nullable ();
			$table->softDeletes ();

		});
//        Schema::table('admin_users', function($table) {
//            $table->foreign('user_type')->references('id')->on('admin_user_type')->onDelete('cascade');
//        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('admin_users');
	}
}
