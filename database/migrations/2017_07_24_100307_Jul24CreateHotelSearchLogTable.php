<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Jul24CreateHotelSearchLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_search_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned ();
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('search_log')->nullable();
            $table->tinyInteger('is_favourite')->nullable()->comment('1: yes');
            $table->timestamps();
            $table->softDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hotel_search_log');
    }
}
