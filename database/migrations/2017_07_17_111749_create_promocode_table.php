<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_code', function (Blueprint $table) {
            $table->increments('id');
            $table->string('promo_code', 50)->nullable();
            $table->tinyInteger('promo_type')->default(1)->comment('1: promocode');
            $table->tinyInteger('discount_type')->default(1)->comment('1: percentage , 2 : fixed');
            $table->decimal('discount', 10,2)->nullable();
            $table->date('valid_from')->nullable();
            $table->date('valid_to')->nullable();
            $table->integer('no_of_maximum_use')->nullable();
            $table->string('description', 150)->nullable();
            $table->timestamps();
            $table->integer ('created_by')->unsigned ()->nullable ();
            $table->foreign ('created_by')->references ('id')->on ('admin_users');
            $table->integer ('updated_by')->unsigned ()->nullable ();
            $table->foreign ('updated_by')->references ('id')->on ('admin_users');
            $table->softDeletes ();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promo_code');
    }
}
