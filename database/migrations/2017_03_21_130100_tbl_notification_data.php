<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblNotificationData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('notification' , function ($table) {
            $table->increments ('id');
            $table->integer ('user_id')->unsigned ();
            $table->foreign ('user_id')->references ('id')->on ('users');
            $table->integer ('service_api_id')->unsigned ();
            $table->foreign ('service_api_id')->references ('id')->on ('service_apis');
            $table->integer ('notification_type_id')->unsigned ();
            $table->foreign ('notification_type_id')->references ('id')->on ('notification_type');
            $table->text('notification_data');
            $table->boolean('is_active')->default (1);
            $table->timestamps ();
            $table->softDeletes ();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('notification');
    }
}
