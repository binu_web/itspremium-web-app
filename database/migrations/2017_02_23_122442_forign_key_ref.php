<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForignKeyRef extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::table ('transactions' , function ($table) {
			$table->foreign ('gateway_id')->references ('id')->on ('payment_methods');
		});
		Schema::table ('users' , function ($table) {
			$table->foreign ('country')->references ('id')->on ('countries');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		//
	}
}
