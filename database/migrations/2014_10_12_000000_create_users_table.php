<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('users' , function (Blueprint $table) {
			$table->increments ('id');
			$table->string ('email' , 255);
			$table->string ('mobile');
			$table->string ('password' , 255)->nullable ();
			$table->string ('first_name')->nullable ();
			$table->string ('last_name')->nullable ();
			$table->string ('profile_pic' , 255)->nullable ();
			$table->integer ('country')->unsigned ()->nullable ();
			$table->boolean ('is_active')->default (1);
			$table->string ('activation_token' , 255)->nullable ();
			$table->timestamps ();
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('users');
	}
}
