<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblServiceMetaData extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('service_meta_data' , function (Blueprint $table) {
			$table->increments ('id');
			$table->Integer ('meta_key_id')->unsigned ();
			$table->foreign ('meta_key_id')->references ('id')->on ('service_meta_key');
			$table->Integer ('booked_id')->unsigned ();
			$table->foreign ('booked_id')->references ('id')->on ('services_booked');
			$table->string ('value' , 255)->nullable ();
			$table->timestamps ();
			$table->integer ('created_by')->unsigned ()->nullable ();
			$table->foreign ('created_by')->references ('id')->on ('admin_users');
			$table->integer ('updated_by')->unsigned ()->nullable ();
			$table->foreign ('updated_by')->references ('id')->on ('admin_users');
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('service_meta_data');
	}
}
