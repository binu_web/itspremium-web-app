<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table ('transactions' , function (Blueprint $table) {
            $table->double('refund_amount' , 10,3)->after ('status_msg');
            $table->tinyInteger('refund_type')->after ('refund_status')->comment('1=> FULL REFUND, 2 => PARTIAL REFUND');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table ('transactions' , function ($table) {
            $table->dropColumn ('refund_amount');
            $table->dropColumn ('refund_type');
        });
    }
}
