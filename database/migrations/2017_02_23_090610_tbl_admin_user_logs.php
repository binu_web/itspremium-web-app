<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblAdminUserLogs extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('admin_user_logs' , function (Blueprint $table) {
			$table->increments ('id');
			$table->integer ('log_type')->unsigned ()->nullable ();
			$table->foreign ('log_type')->references ('id')->on ('admin_log_type');
			$table->integer ('user_id')->unsigned ()->nullable ();
			$table->foreign ('user_id')->references ('id')->on ('admin_users');
			$table->enum ('action' , ['ADD' , 'MODIFY' , 'DELETE']);
			$table->string ('ip')->nullable ();
			$table->string ('latitude')->nullable ();
			$table->string ('longitude')->nullable ();
			$table->timestamps ();
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('admin_user_logs');
	}
}
