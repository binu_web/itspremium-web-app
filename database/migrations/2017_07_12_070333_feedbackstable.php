<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Feedbackstable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('feedback' , function (Blueprint $table) {
            $table->increments ('id');
            $table->Integer ('user_id')->unsigned ();
            $table->foreign ('user_id')->references ('id')->on ('users');
            $table->longText('feedback');
            $table->timestamps ();
            $table->integer ('created_by')->nullable ();
            $table->integer ('updated_by')->nullable ();
            $table->softDeletes ();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ('feedback');
    }
}
