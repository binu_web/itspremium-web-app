<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblNewsSources extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ("news_sources" , function (Blueprint $table) {
			$table->increments ('id');
			$table->string ('source' , 255)->nullable ();
			$table->string ('source_slug' , 255)->nullable ();
			$table->integer ('created_by')->unsigned ()->nullable ();
			$table->foreign ('created_by')->references ('id')->on ('admin_users');
			$table->integer ('updated_by')->unsigned ()->nullable ();
			$table->foreign ('updated_by')->references ('id')->on ('admin_users');
			$table->timestamps ();
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('news_sources');
	}
}
