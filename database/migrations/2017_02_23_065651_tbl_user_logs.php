<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblUserLogs extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('user_logs' , function (Blueprint $table) {
			$table->increments ('id');
			$table->Integer ('log_type')->unsigned ();
			$table->foreign ('log_type')->references ('id')->on ('log_type');
			$table->Integer ('user_id')->unsigned ()->nullable ();
			$table->foreign ('user_id')->references ('id')->on ('users');
			$table->Integer ('service_id')->unsigned ()->nullable ();
			$table->foreign ('service_id')->references ('id')->on ('services_booked');
			$table->string ('ip')->nullable ();
			$table->string ('latitude')->nullable ();
			$table->string ('longitude')->nullable ();
			$table->timestamps ();
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('user_logs');
	}
}
