<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPaymentMethods extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('payment_methods' , function (Blueprint $table) {
			$table->increments ('id');
			$table->string ('gateway_name');
			$table->string ('api_key' , 255)->nullable ();
			$table->string ('api_url' , 255)->nullable ();
			$table->text ('api_metadata')->nullable ();
			$table->boolean ('is_active')->default (1);
			$table->timestamps ();
			$table->integer ('created_by')->unsigned ()->nullable ();
			$table->foreign ('created_by')->references ('id')->on ('admin_users');
			$table->integer ('updated_by')->unsigned ()->nullable ();
			$table->foreign ('updated_by')->references ('id')->on ('admin_users');
			$table->softDeletes ();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('payment_methods');
	}
}
