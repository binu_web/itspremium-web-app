<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCabLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::table ('cab_details_log' , function (Blueprint $table) {
			$table->dropColumn ('is_favorite');
		});
		Schema::table ('cab_details_log' , function (Blueprint $table) {
			$table->boolean ('is_favorite_pickup')->default (0);
		});
		Schema::table ('cab_details_log' , function (Blueprint $table) {
			$table->boolean ('is_favorite_drop_off')->default (0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		//
	}
}
