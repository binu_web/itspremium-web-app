<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApiUserAccessTokenTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		//
		Schema::create ('user_access_tokens' , function ($table) {
			$table->increments ('id');
			$table->integer ('user_id')->unsigned ();
			$table->integer ('service_api_id')->unsigned ()->nullable ();
			$table->text ('access_token')->nullable ();
			$table->boolean ('is_active')->default (1);
			$table->integer ('token_expiry')->nullable ();
			$table->timestamps ();
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		//
		Schema::drop ('user_access_tokens');
	}
}
