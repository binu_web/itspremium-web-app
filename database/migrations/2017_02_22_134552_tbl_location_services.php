<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblLocationServices extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('location_services' , function (Blueprint $table) {
			$table->increments ('id');
			$table->Integer ('service')->unsigned ();
			$table->foreign ('service')->references ('id')->on ('service_apis');
			$table->Integer ('location')->unsigned ();
			$table->foreign ('location')->references ('id')->on ('locations');
			$table->boolean ('is_active')->default (1);
			$table->timestamps ();
			$table->integer ('created_by')->unsigned ()->nullable ();
			$table->foreign ('created_by')->references ('id')->on ('admin_users');
			$table->integer ('updated_by')->unsigned ()->nullable ();
			$table->foreign ('updated_by')->references ('id')->on ('admin_users');
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('location_services');
	}
}
