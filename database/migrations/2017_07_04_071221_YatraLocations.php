<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class YatraLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yatra_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 300);
            $table->tinyInteger('type')->default(1)->comment('1: city 2: area');
            $table->string('area_id', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('yatra_locations');
    }
}
