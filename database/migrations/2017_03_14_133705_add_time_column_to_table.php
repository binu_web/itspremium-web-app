<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeColumnToTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		//
		Schema::table ('cab_details_log' , function ($table) {
			$table->datetime ('ride_time')->default (DB::raw ('CURRENT_TIMESTAMP'));
			$table->enum ('ride_type' , ['RIDE_NOW' , 'RIDE_LATER'])->default ('RIDE_NOW');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		//
		Schema::table ('cab_details_log' , function ($table) {
			$table->dropColumns (['ride_time' , 'ride_type']);
		});
	}
}
