<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnumValuesToStatusOfServiceBooked extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('services_booked', 'status'))
        {
            Schema::table('services_booked', function($table) {
                $table->dropColumn('status');
            });
        }
        DB::statement("ALTER TABLE services_booked ADD COLUMN status ENUM('IN PROGRESS', 'PROCESSING', 'BOOKED','CANCELLED','REJECTED','COMPLETED')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services_booked', function($table) {
            $table->dropColumn('status');
        });
    }
}
