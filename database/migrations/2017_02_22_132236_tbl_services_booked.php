<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblServicesBooked extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('services_booked' , function (Blueprint $table) {
			$table->increments ('id');
			$table->Integer ('user_id')->unsigned ();
			$table->foreign ('user_id')->references ('id')->on ('users');
			$table->Integer ('service_id')->unsigned ();
			$table->foreign ('service_id')->references ('id')->on ('services');
			$table->Integer ('service_api_id')->unsigned ();
			$table->foreign ('service_api_id')->references ('id')->on ('service_apis');
			$table->enum ('status' , ['BOOKED' , 'CANCELLED' , 'PENDING',]);
			$table->string ('ip' , 30)->nullable ();
			$table->string ('latitude' , 50)->nullable ();
			$table->string ('longitude' , 50)->nullable ();
			$table->timestamps ();
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('services_booked');
	}
}
