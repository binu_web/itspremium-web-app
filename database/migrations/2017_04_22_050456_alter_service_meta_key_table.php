<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServiceMetaKeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::table ('service_meta_key' , function (Blueprint $table) {

            $table->dropForeign(['service_id']);
            $table->foreign ('service_id')->references ('id')->on ('service_apis');
            $table->integer('created_by')->nullable()->unsigned()->change();
            $table->integer('updated_by')->nullable()->unsigned()->change();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
