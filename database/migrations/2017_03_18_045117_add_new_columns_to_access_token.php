<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToAccessToken extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		//
		Schema::table ('user_access_tokens' , function ($table) {
			$table->text ('refresh_token')->nullable ()->after ('access_token');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		//
		Schema::table ('user_access_tokens' , function ($table) {
			$table->dropColumn ('refresh_token');
		});
	}
}
