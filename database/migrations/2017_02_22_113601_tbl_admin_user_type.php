<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblAdminUserType extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('admin_user_type' , function (Blueprint $table) {
			$table->increments ('id');
			$table->string ('type' , 100)->nullable ();
			$table->boolean ('is_active')->default (1);
			$table->integer ('created_by')->nullable ();
			$table->integer ('updated_by')->nullable ();
			$table->timestamps ();
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('admin_user_type');
	}
}
