<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblCountries extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('countries' , function (Blueprint $table) {
			$table->increments ('id');
			$table->string ('name' , 100)->nullable ();
			$table->string ('iso2' , 10)->nullable ();
			$table->string ('iso3' , 10)->nullable ();
			$table->string ('phone_code' , 10)->nullable ();
			$table->string ('region' , 50)->nullable ();
			$table->string ('latitude' , 50)->nullable ();
			$table->string ('longitude' , 50)->nullable ();
			$table->string ('currency' , 10)->nullable ();
			$table->Integer ('country_code')->length (5)->nullable ();
			$table->softDeletes ();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('countries');
	}
}
