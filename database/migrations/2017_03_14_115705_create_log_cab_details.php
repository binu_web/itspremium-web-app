<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogCabDetails extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		//
		Schema::create ('cab_details_log' , function ($table) {
			$table->increments ('id');
			$table->integer ('user_id')->unsigned ()->nullable ();
			$table->foreign ('user_id')->references ('id')->on ('users');
			$table->string ('pickup_location' , 255);
			$table->string ('pickup_latitude' , 255);
			$table->string ('pickup_longitude' , 255);
			$table->string ('dropoff_location' , 255);
			$table->string ('dropoff_latitude' , 255);
			$table->string ('dropoff_longitude' , 255);
			$table->boolean ('is_favorite')->default (0);
			$table->timestamps ();
			$table->softDeletes ();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */

	public function down ()
	{
		//
		Schema::drop ('cab_details_log');
	}
}
