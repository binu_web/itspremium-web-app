<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomFiledToServiceBookedTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		//
		DB::statement ("ALTER TABLE services_booked CHANGE status status ENUM('IN PROGRESS','PROCESSING','BOOKED','CANCELLED','REJECTED','COMPLETED','ARRIVING')");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		//
		DB::statement ("ALTER TABLE services_booked CHANGE status status ENUM('IN PROGRESS','PROCESSING','BOOKED','CANCELLED','REJECTED','COMPLETED')");
	}
}
