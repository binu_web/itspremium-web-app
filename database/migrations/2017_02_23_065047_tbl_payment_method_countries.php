<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPaymentMethodCountries extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create ('payment_method_countries' , function (Blueprint $table) {
			$table->increments ('id');
			$table->Integer ('gateway_id')->unsigned ();
			$table->foreign ('gateway_id')->references ('id')->on ('payment_methods');
			$table->Integer ('country_id')->unsigned ();
			$table->foreign ('country_id')->references ('id')->on ('countries');
			$table->timestamps ();
			$table->integer ('created_by')->unsigned ()->nullable ();
			$table->foreign ('created_by')->references ('id')->on ('admin_users');
			$table->integer ('updated_by')->unsigned ()->nullable ();
			$table->foreign ('updated_by')->references ('id')->on ('admin_users');
			$table->softDeletes ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::drop ('payment_method_countries');
	}
}
