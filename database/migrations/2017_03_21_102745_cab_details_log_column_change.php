<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CabDetailsLogColumnChange extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		//
		Schema::table ('cab_details_log' , function ($table) {
			$table->dropColumn (['pickup_location' ,
				'pickup_latitude' ,
				'pickup_longitude' ,
				'dropoff_location' ,
				'dropoff_latitude' ,
				'dropoff_longitude' ,
				'is_favorite_pickup' ,
				'is_favorite_drop_off']);


		});

		Schema::table ('cab_details_log' , function ($table) {
			$table->enum ('location_type' , ['PICKUP' , 'DROPOFF'])->default ('PICKUP');
			$table->string ('location')->nullable ();
			$table->string ('latitude')->nullable ();
			$table->string ('longitude')->nullable ();
			$table->boolean ('is_favorite')->default (0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		//
	}
}
