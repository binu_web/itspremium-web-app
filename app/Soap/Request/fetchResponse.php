<?php

namespace App\Soap\Request;

class fetchResponse
{
    /**
     * @var string
     */
    protected $RequestedCurrency;

    /**
     * @var string
     */
    protected $SortOrder;

    /**
     * @var string
     */
    protected $CityName;

    /**
     * @var string
     */
    protected $CountryName;

    /**
     * GetConversionAmount constructor.
     *
     * @param string $CurrencyFrom
     * @param string $CurrencyTo
     * @param string $RateDate
     * @param string $Amount
     */
    public function __construct($RequestedCurrency, $SortOrder, $CityName, $CountryName)
    {
        $this->RequestedCurrency = $RequestedCurrency;
        $this->SortOrder   = $SortOrder;
        $this->CityName     = $CityName;
        $this->CountryName   = $CountryName;
    }


    /**
     * @return string
     */
    public function getRequestedCurrency()
    {
        return $this->RequestedCurrency;
    }

    /**
     * @return string
     */
    public function getSortOrder()
    {
        return $this->SortOrder;
    }

    /**
     * @return string
     */
    public function getCityName()
    {
        return $this->CityName;
    }

    /**
     * @return string
     */
    public function getCountryName()
    {
        return $this->CountryName;
    }
}

?>