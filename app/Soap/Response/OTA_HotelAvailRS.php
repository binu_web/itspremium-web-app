<?php

namespace App\Soap\Response;

class OTA_HotelAvailRS
{
    /**
     * @var string
     */
    protected $GetHotelSearchResult;

    /**
     * GetConversionAmountResponse constructor.
     *
     * @param string
     */
    public function __construct($GetHotelSearchResult)
    {
        $this->GetHotelSearchResult = $GetHotelSearchResult;
    }

    /**
     * @return string
     */
    public function GetHotelSearchResult()
    {
        return $this->GetHotelSearchResult;
    }
}