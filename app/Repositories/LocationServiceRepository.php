<?php

namespace App\Repositories;

use App\Models\LocationService;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LocationServiceRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'service' ,
		'location' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return LocationService::class;
	}

	public function insert (array $data , $id)
	{
		$AuthId = Auth::id ();
		foreach ($data['service'] as $value) {
			$LocationService = new LocationService();
			$LocationService->is_active = $data['is_active'];
			$LocationService->service = $value;
			$LocationService->location = $id;
			$LocationService->updated_by = $AuthId;
			$LocationService->created_by = $AuthId;
			$s_status = $LocationService->save ();
		}
		$ins_id = $LocationService->id;
		return $ins_id;
	}

	public function update (array $data , $id)
	{
		$auth_id = Auth::id ();
		DB::table ('location_services')->where ('location' , '=' , $id)->delete ();
		foreach ($data['service'] as $value) {
			$LocationService = new LocationService();
			$LocationService->is_active = $data['is_active'];
			$LocationService->service = $value;
			$LocationService->location = $id;
			$LocationService->updated_by = $auth_id;
			$LocationService->created_by = $auth_id;
			$s_status = $LocationService->save ();
		}

	}

	public function delete ($id)
	{
		DB::table ('location_services')->where ('location' , '=' , $id)->delete ();
	}
}
