<?php

namespace App\Repositories;

use App\Models\NewsSource;
use InfyOm\Generator\Common\BaseRepository;

class NewsSourceRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'source' ,
		'source_slug' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return NewsSource::class;
	}

	public function findWithoutFail ($id , $columns = ['*'])
	{
		return \App\Models\NewsSource::where ('news_sources.id' , '=' , $id)
			->select ('news_sources.*' , 'cu.first_name as created_by_name' , 'uu.first_name as updated_by_name')
			->leftJoin ('admin_users as cu' , 'cu.id' , '=' , 'news_sources.created_by')
			->leftJoin ('admin_users as uu' , 'uu.id' , '=' , 'news_sources.updated_by')
			->first ();
	}

}
