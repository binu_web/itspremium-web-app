<?php

namespace App\Repositories;

use App\Models\User;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\Hash;

class UsersRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'email' ,
		'mobile' ,
		'password' ,
		'first_name' ,
		'last_name' ,
		'profile_pic' ,
		'country' ,
		'is_active' ,
		'activation_token'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return User::class;
	}
	/**
	 * Check the user exists or not
	 *
	 * @param $email
	 *
	 * @author Midhun
	 * @return bool
	 */
	public function Check_email_exists ($email)
	{
		if (User::where ('email' , $email)->first ()) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Insert user deatils in registration
	 *
	 * @param $data
	 *
	 * @author Midhun
	 * @return User
	 */

	public function regUser ($data)
	{
		$email = $data['email'];
		$password = $data['password'];
		$mobile_number = $data['mobile'];
		$f_name = $data['first_name'];
		$l_name = $data['last_name'];
		$country = $data['country'];
		$is_active = $data['is_active'];


		//Add basic student details
		$user = new User();
		$user->email = $email;
		$user->mobile = $mobile_number;
		$user->password = Hash::make ($password);
		$user->first_name = $f_name;
		$user->last_name = $l_name;
		$user->country = $country;
		$user->is_active = $is_active;
		$user->save ();

		return $user;
	}

	/**
	 * Update user
	 *
	 * @param $data
	 *
	 * @author Midhun
	 */

	public function updateuser ($data , $id)
	{

		if (isset($data['password'])) {

			$password = $data['password'];
		}
		$email = $data['email'];
		$mobile_number = $data['mobile'];
		$f_name = $data['first_name'];
		$l_name = $data['last_name'];
		$country = $data['country'];
		$is_active = $data['is_active'];

		$user = User::find ($id);
		$user->email = $email;
		$user->first_name = $f_name;
		$user->last_name = $l_name;
		$user->mobile = $mobile_number;
		if (!empty($password))
			$user->password = Hash::make ($password);
		$user->country = $country;
		$user->is_active = $is_active;
		return $status = $user->save ();

	}

	public function deactivate ($id)
	{

		$user = User::find ($id);
		$user->deleted_at = date ('Y-m-d H:i:s');
		return $status = $user->save ();

	}
}
