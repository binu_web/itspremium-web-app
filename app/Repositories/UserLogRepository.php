<?php

namespace App\Repositories;

use App\Models\UserLog;
use InfyOm\Generator\Common\BaseRepository;

class UserLogRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'log_type' ,
		'user_id' ,
		'service_id' ,
		'ip' ,
		'latlong'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return UserLog::class;
	}
}
