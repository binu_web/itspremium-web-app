<?php

namespace App\Repositories;

use App\Models\AdminUserLog;
use InfyOm\Generator\Common\BaseRepository;

class AdminUserLogRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'log_type' ,
		'user_id' ,
		'action' ,
		'ip' ,
		'latlong'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return AdminUserLog::class;
	}
}
