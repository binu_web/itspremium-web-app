<?php

namespace App\Repositories;

use App\Models\AdminUserType;
use InfyOm\Generator\Common\BaseRepository;

class AdminUserTypeRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'type' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return AdminUserType::class;
	}
}
