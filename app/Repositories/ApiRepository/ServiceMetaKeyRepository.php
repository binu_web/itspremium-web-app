<?php


namespace App\Repositories\ApiRepository;

use App\Repositories\ApiRepository\DeviceRepository;

use App\Models\CabLog;
use App\Models\ServiceBooked;
use App\Models\Device;
use App\Models\ServiceMetaData;
use App\Models\ServiceMetaKey;
use App\Services\PushNotification;
use App\Models\NotificationType;
use Illuminate\Support\Facades\Log;
use Response;
use DB;
use App\Services\Yatra\HotelBookingProcess;
use App\Models\Transaction;
use App\Services\NotificationService;

use Exception;
use App\Models\ServiceApi;


class ServiceMetaKeyRepository
{
	public $serviceMetaKey;

	CONST OUR_STATUS_FOR_UBER = array('in_progress' => 'IN PROGRESS' , 'processing' => 'PROCESSING' , 'accepted' => 'BOOKED' , 'arriving' => 'ARRIVING' , 'driver_canceled' => 'REJECTED' , 'rider_canceled' => 'CANCELLED' , 'completed' => 'COMPLETED');
	CONST OUR_STATUS_FOR_OLA = array('CLIENT_LOCATED' => 'IN PROGRESS' , 'IN_PROGRESS' => 'BOOKED' , 'CANCELLED' => 'CANCELLED' , 'BOOKING_COMPLETED' => 'COMPLETED');
	CONST OUR_STATUS_FOR_DINEOUT = array('Confirmed' => 'BOOKED' , 'Cancelled' => 'CANCELLED' , 'Denied' => 'REJECTED' , 'New' => 'PROCESSING');
	CONST OUR_STATUS_FOR_YATRAHOTEL = array('Cancelled' => 'CANCELLED');


	/**
	 * Constuct Function
	 *
	 * @author Binoop V
	 */
	public function __construct ()
	{
		$this->serviceBooked = new ServiceBooked();
		$this->serviceMetaKey = new ServiceMetaKey();
		$this->serviceMetaData = new ServiceMetaData();
		$this->deviceRepository = new DeviceRepository();
	}

	/**
	 * @param $API_SERVICE_ID
	 * @param $booking_id
	 * @param $meta_key
	 * @param $data
	 *
	 * @author Midhun
	 *         Add the booking details from service
	 *
	 */

	public function addOladBookDetails ($API_SERVICE_CATEGORY_ID , $API_SERVICE_ID , $user_id , $data , $aOtherParam)
	{

		try {

			//change data to array
			$myarray = (array)$data;

			$result = ['booking_id' => $myarray['booking_id'] ,
				'crn' => $myarray['crn'] ,
				'driver_name' => $myarray['driver_name'] ,
				'driver_number' => $myarray['driver_number'] ,
				'cab_type' => $myarray['cab_type'] ,
				'cab_number' => $myarray['cab_number'] ,
				'car_model' => $myarray['car_model'] ,
				'car_color' => $myarray['car_color'] ,
				'eta' => $myarray['eta'] ,
				'driver_lat' => $myarray['driver_lat'] ,
				'driver_lng' => $myarray['driver_lng'] ,
				'share_ride_url' => $myarray['share_ride_url'] ,
				'min_fare' => $aOtherParam['min_fare'] ,
				'max_fare' => $aOtherParam['max_fare'] ,
				'distance' => $aOtherParam['distance'] ,
				'duration' => $aOtherParam['duration'] ,
				'currency' => $aOtherParam['currency'] ,

				'request_type' => null ,
				'pickup_lat' => $aOtherParam['pickup_lat'] ,
				'pickup_lng' => $aOtherParam['pickup_lng'] ,
				'drop_lat' => $aOtherParam['drop_lat'] ,
				'drop_lng' => $aOtherParam['drop_lng'] ,
				'ola_money_balance' => null ,
				'trip_info_amount' => null ,
				'trip_info_payable_amount' => null ,
				'trip_info_distance_value' => null ,
				'trip_info_distance_unit' => null ,
				'trip_info_trip_time_value' => null ,
				'trip_info_trip_time_unit' => null ,
				'trip_info_wait_time_value' => null ,
				'trip_info_wait_time_unit' => null ,
				'trip_info_discount' => null ,
				'trip_info_advance' => null ,
				'trip_info_mode_of_advance' => null ,
				'booking_status' => null ,
			];
			// Creating random booking key
			$booking_key = booking_key_generator ();

			$ser_booked = $this->serviceBooked;
			$ser_booked->user_id = $user_id;
			$ser_booked->service_id = $API_SERVICE_CATEGORY_ID;
			$ser_booked->service_api_id = $API_SERVICE_ID;
			$ser_booked->booking_key = $booking_key;
			$ser_booked->status = 'PROCESSING';
			$ser_booked->service_data = serialize ($myarray);
			$ser_booked->save ();

			$book_for_id = $ser_booked->id;

			// Insert details into service meta data based on the
			foreach ($result as $key => $value) {

				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , '=' , $API_SERVICE_ID)->first ();
				$meta_key_id = $meta_key_id['id'];

				$ser_meta_data = new ServiceMetaData();
				$ser_meta_data->meta_key_id = $meta_key_id;
				$ser_meta_data->booked_id = $book_for_id;
				$ser_meta_data->value = $value;
				//                $ser_meta_data->created_by = $user_id;
				$ser_meta_data->save ();
			}
			// return booking details
			$return = ['booking_id' => $book_for_id , 'booking_key' => $booking_key];

			return $return;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}

	/**
	 * @param $booking_id
	 *
	 * @author Midhun
	 *         Get ola booking_id from service meta data table
	 */
	public function getOlaBookingdetails ($booking_id , $API_SERVICE_ID)
	{

		try {
			$booking_details = ServiceMetaData::select ('value')
				->join ('service_meta_key' , 'service_meta_data.meta_key_id' , '=' , 'service_meta_key.id')
				->where ('service_meta_key.meta_key' , '=' , 'booking_id')
				->where ('service_meta_key.service_id' , '=' , $API_SERVICE_ID)
				->where ('service_meta_data.booked_id' , '=' , $booking_id)->first ();

			return $booking_details['value'];
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);

			// dd ($e->getMessage ());
			return false;
		}
	}

	public function addUberBookDetails ($API_SERVICE_CATEGORY_ID , $API_SERVICE_ID , $user_id , $data , $aOtherParam)
	{

		try {
			// Changes data to array
			$myarray = (array)$data;
			#print_r($myarray);
			// customize the array for itzpremium purpose.
			$result = ['status' => $myarray['status'] ,
				'product_id' => $myarray['product_id'] ,
				'destination_latitude' => $myarray['destination']->latitude ,
				'destination_longitude' => $myarray['destination']->longitude ,
				'driver' => $myarray['driver'] ,
				'pickup_latitude' => $myarray['pickup']->latitude ,
				'pickup_longitude' => $myarray['pickup']->longitude ,
				'request_id' => $myarray['request_id'] ,
				'eta' => $myarray['eta'] ,
				'location' => $myarray['location'] ,
				'vehicle' => $myarray['vehicle'] ,
				'shared' => $myarray['shared'] ,
				'fare_amount' 	=> $aOtherParam['fare_amount'],
				'currency_code' => $aOtherParam['currency_code'],
				'distance_estimate' => $aOtherParam['distance_estimate'],
				'duration_estimate' => $aOtherParam['duration_estimate'],
				'distance_unit'		=> $aOtherParam['distance_unit'],
				'receipt_subtotal'			=> null,
				'receipt_total_charged'		=> null,
				'receipt_total_owed'		=> null,
				'receipt_total_fare'			=> null,
				'receipt_travelled_duration' 	=> null,
				'receipt_travelled_distance'	=> null,
				
				'driver_phone_number' 	=> null,
				'driver_rating' 		=> null,
				'driver_picture_url' 	=> null,
				'driver_name' 			=> null,
				'driver_sms_number' 	=> null,
				'driver_lat' 			=> null,
				'driver_lng'	 		=> null,
				'bearing' 				=> null,
				'vehicle_make' 			=> null,
				'vehicle_picture_url' 	=> null,
				'vehicle_model' 		=> null,
				'vehicle_license_plate' => null,

			];

			// Creating random booking key
			$booking_key = booking_key_generator ();

			// Insert details into services_booked table
			$ser_booked = $this->serviceBooked;
			$ser_booked->user_id = $user_id;
			$ser_booked->service_id = $API_SERVICE_CATEGORY_ID;
			$ser_booked->service_api_id = $API_SERVICE_ID;
			$ser_booked->booking_key = $booking_key;
			$ser_booked->status = 'PROCESSING';
			$ser_booked->service_data = serialize ($myarray);
			$ser_booked->save ();

			// Save the last insert id (Booking id)
			$book_for_id = $ser_booked->id;

			// Insert details into service meta data based on the
			foreach ($result as $key => $value) {

				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , '=' , $API_SERVICE_ID)->first ();
				$meta_key_id = $meta_key_id['id'];

				$ser_meta_data = new ServiceMetaData();
				$ser_meta_data->meta_key_id = $meta_key_id;
				$ser_meta_data->booked_id = $book_for_id;
				$ser_meta_data->value = $value;
				//                $ser_meta_data->created_by = $user_id;
				$ser_meta_data->save ();
			}
			// return booking details
			$return = ['booking_id' => $book_for_id , 'booking_key' => $booking_key];

			return $return;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}
	}

	/**
	 * @param $data
	 * @param $booking_id
	 *
	 * @author Midhun
	 *         Update ola booking details into service_meta_data and service_booked table
	 */

	public function UpdateolaBookDetails ($data , $booking_id , $service_id)
	{

		try {
			// Changes data to array
			$myarray = (array)$data;

			// collecting service booking details
			$booking_details = ServiceBooked::where ('id' , '=' , $booking_id)->first ();

			// ensure booking is already exist or not
			if (empty($booking_details)) {
				return false;
			}


			// Update booking details in service booked table
			$update = ServiceBooked::where ('id' , $booking_id)->update (['service_data' => serialize ($myarray)]);

			// Customize the result
			$result = ['request_type' => $myarray['request_type'] ,
				'pickup_lat' => $myarray['pickup_lat'] ,
				'pickup_lng' => $myarray['pickup_lng'] ,
				'drop_lat' => $myarray['drop_lat'] ,
				'drop_lng' => $myarray['drop_lng'] ,
				'ola_money_balance' => $myarray['ola_money_balance'] ,
				'trip_info_amount' => $myarray['trip_info']->amount ,
				'trip_info_payable_amount' => $myarray['trip_info']->payable_amount ,
				'trip_info_distance_value' => $myarray['trip_info']->distance->value ,
				'trip_info_distance_unit' => $myarray['trip_info']->distance->unit ,
				'trip_info_trip_time_value' => $myarray['trip_info']->trip_time->value ,
				'trip_info_trip_time_unit' => $myarray['trip_info']->trip_time->unit ,
				'trip_info_wait_time_value' => $myarray['trip_info']->wait_time->value ,
				'trip_info_wait_time_unit' => $myarray['trip_info']->wait_time->unit ,
				'trip_info_discount' => $myarray['trip_info']->discount ,
				'trip_info_advance' => $myarray['trip_info']->advance ,
				'trip_info_mode_of_advance' => $myarray['trip_info']->mode_of_advance ,
				'booking_status' => $myarray['booking_status'] ,
			];

			// Update the service meta data
			foreach ($result as $key => $value) {

				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , $service_id)->first ();
				$meta_key_id = $meta_key_id['id'];

				$update = ServiceMetaData::where ('meta_key_id' , $meta_key_id)
					->where ('booked_id' , $booking_id)
					->update (['value' => $value]);
			}

			// Collect the details to return.
			$return_key = ['driver_name' , 'driver_number' , 'cab_number' , 'car_model' , 'car_color' , 'eta' , 'driver_lat' , 'driver_lng' , 'pickup_lat' , 'pickup_lng' , 'drop_lat' , 'drop_lng' , 'min_fare' , 'max_fare'];

			$return_array = array();
			foreach ($return_key as $keys) {
				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $keys)->where ('service_id' , $service_id)->first ();
				$meta_key_id = $meta_key_id['id'];

				$meta_data = ServiceMetaData::select ('value')->where ('meta_key_id' , '=' , $meta_key_id)
					->where ('booked_id' , '=' , $booking_id)
					->first ();
				$return_array[$keys] = $meta_data['value'];
			}

			// get service name of service api
			$services = ServiceApi::where ('deleted_at' , NULL)->where ('id' , $booking_details['service_api_id'])->first ();
			$service_name = strtolower ($services->provider_name);


			// Change the return araay for same result as ola
			$new_return_array = ['driver_name' => $return_array['driver_name'] ,
				'driver_number' => $return_array['driver_number'] ,
				'cab_number' => $return_array['cab_number'] ,
				'car_model' => $return_array['car_model'] ,
				'car_color' => $return_array['car_color'] ,
				'eta' => $return_array['eta'] ,
				'driver_lat' => $return_array['driver_lat'] ,
				'driver_lng' => $return_array['driver_lng'] ,
				'pickup_lat' => $return_array['pickup_lat'] ,
				'pickup_lng' => $return_array['pickup_lng'] ,
				'drop_lat' => $return_array['drop_lat'] ,
				'drop_lng' => $return_array['drop_lng'] ,
				'price' => $return_array['min_fare'] . '-' . $return_array['max_fare'] ,
				'status' => $booking_details['status'] ,
				'service_name' => $service_name ,
			];


			return $new_return_array;

		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}


	/**
	 * Update Uber Details
	 *
	 * @param $data
	 * @param $booking_id
	 * @param $service_id
	 *
	 * @return array|bool
	 */
	public function UpdateUberBookDetails ($data , $booking_id , $service_id)
	{
		try {
			// Changes data to array
			$myarray = (array)$data;

			// collecting service booking details
			$booking_details = ServiceBooked::where ('id' , '=' , $booking_id)->first ();

			// ensure booking is already exist or not
			if (empty($booking_details)) {
				return false;
			}

			// Update booking details in service booked table
			$update = ServiceBooked::where ('id' , $booking_id)->update (['service_data' => serialize ($myarray)]);

			// Customize the result
			$result = ['status' => $myarray['status'] ,
				'driver_phone_number' => $myarray['driver']->phone_number ,
				'driver_rating' => $myarray['driver']->rating ,
				'driver_picture_url' => $myarray['driver']->picture_url ,
				'driver_name' => $myarray['driver']->name ,
				'driver_sms_number' => $myarray['driver']->sms_number ,
				'eta' => $myarray['pickup']->eta ,
				'driver_lat' => $myarray['location']->latitude ,
				'driver_lng' => $myarray['location']->longitude ,
				'bearing' => $myarray['location']->bearing ,

				'vehicle_make' => $myarray['vehicle']->make ,
				'vehicle_picture_url' => $myarray['vehicle']->picture_url ,
				'vehicle_model' => $myarray['vehicle']->model ,
				'vehicle_license_plate' => $myarray['vehicle']->license_plate ,
				'shared' => $myarray['shared'] ,
			];

			// Update the service meta data
			foreach ($result as $key => $value) {

				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , $service_id)->first ();
				$meta_key_id = $meta_key_id['id'];

				$update = ServiceMetaData::where ('meta_key_id' , $meta_key_id)
					->where ('booked_id' , $booking_id)
					->update (['value' => $value]);
			}

			// Collect the details to return.
			$return_key = ['driver_name' , 'driver_phone_number' , 'vehicle_license_plate' , 'vehicle_make' , 'vehicle_model' , 'eta' , 'driver_lat' , 'driver_lng' , 'pickup_latitude' , 'pickup_longitude' , 'destination_latitude' , 'destination_longitude' , 'fare_amount'];

			$return_array = array();
			foreach ($return_key as $keys) {
				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $keys)->where ('service_id' , $service_id)->first ();
				$meta_key_id = $meta_key_id['id'];

				$meta_data = ServiceMetaData::select ('value')->where ('meta_key_id' , '=' , $meta_key_id)
					->where ('booked_id' , '=' , $booking_id)
					->first ();
				$return_array[$keys] = $meta_data['value'];
			}

			// get service name of service api
			$services = ServiceApi::where ('deleted_at' , NULL)->where ('id' , $booking_details['service_api_id'])->first ();
			$service_name = strtolower ($services->provider_name);

			// Change the return araay for same result as ola
			$new_return_array = ['driver_name' => $return_array['driver_name'] ,
				'driver_number' => $return_array['driver_phone_number'] ,
				'cab_number' => $return_array['vehicle_license_plate'] ,
				'car_model' => $return_array['vehicle_make'] . ' ' . $return_array['vehicle_model'] ,
				'car_color' => null ,
				'eta' => $return_array['eta'] ,
				'driver_lat' => $return_array['driver_lat'] ,
				'driver_lng' => $return_array['driver_lng'] ,
				'pickup_lat' => $return_array['pickup_latitude'] ,
				'pickup_lng' => $return_array['pickup_longitude'] ,
				'drop_lat' => $return_array['destination_latitude'] ,
				'drop_lng' => $return_array['destination_longitude'] ,
				'price' => $return_array['fare_amount'] ,
				'status' => $booking_details['status'] ,
				'service_name' => $service_name ,
			];

			return $new_return_array;

		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}

	/**
	 * @param $service_id
	 * @param $booking_id
	 *
	 * @return return database operation based on webhoook for OLA
	 */


	public function updateStatusOfBookingForOla ($service_id , $result)
	{
		$booked_id_of_service_booked = ServiceMetaData
			::join ('service_meta_key' , 'service_meta_key.id' , '=' , 'service_meta_data.meta_key_id')
			->select ('service_meta_data.booked_id')
			->where ('service_meta_key.meta_key' , 'booking_id')
			->where ('service_meta_key.service_id' , $service_id)
			->where ('service_meta_data.value' , $result['booking_id'])
			->getQuery ()// Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
			->get ();

		if ($booked_id_of_service_booked->count () == 0) {
			return Response::json (['status' => 'error' , 'msg' => 'booking not found'] , 404);
		}


		$booked_id = $booked_id_of_service_booked[0]->booked_id;

		$get_our_status = SELF::OUR_STATUS_FOR_OLA[$result['booking_status']];

		DB::beginTransaction ();
		try {
			ServiceBooked::where ('id' , '=' , $booked_id)->update (array('status' => $get_our_status));
			foreach ($result as $key => $value) {
				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->first ();
				$meta_key_id = $meta_key_id['id'];
				$update = ServiceMetaData::where ('meta_key_id' , $meta_key_id)
					->where ('booked_id' , $booked_id)
					->update (['value' => $value]);
			}
		}
		catch (\Exception $e) {
			DB::rollBack ();
			throw $e;
		}
		DB::commit ();
		/*
		 * sending notification to particular user.
		 */

		$title = 'Ride Status';
		$description = 'Your Ola ride is ' . strtolower ($get_our_status);
		$customData = ['booking_id' => $booked_id , 'service' => SERVICE_OLA , 'status' => $get_our_status];

		$oNotificationService = new NotificationService;

		$aNotificationParams = [];
		$aNotificationParams['booking_id'] = $booked_id;
		$aNotificationParams['title'] = $title;
		$aNotificationParams['desc'] = $description;
		$aNotificationParams['custom_data'] = $customData;

		$oNotificationService->sendNotification ($aNotificationParams);
		return Response::json (['status' => 'success' , 'msg' => 'booking status updated'] , 200);
	}


	/**
	 * @param $service_id
	 * @param $booking_id
	 *
	 * @return return database operation based on webhoook for UBER
	 */
	public function updateStatusOfBookingForUber ($service_id , $result)
	{

		$booked_id_of_service_booked = ServiceMetaData
			::join ('service_meta_key' , 'service_meta_key.id' , '=' , 'service_meta_data.meta_key_id')
			->select ('service_meta_data.booked_id')
			->where ('service_meta_key.meta_key' , 'request_id')
			->where ('service_meta_key.service_id' , $service_id)
			->where ('service_meta_data.value' , $result['meta_resource_id'])
			->getQuery ()// Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
			->get ();


		if ($booked_id_of_service_booked->count () == 0) {
			return Response::json (['status' => 'error' , 'msg' => 'booking not found'] , 404);
		}


		$booked_id = $booked_id_of_service_booked[0]->booked_id;

		$statusArray = SELF::OUR_STATUS_FOR_UBER;
		if (!isset($statusArray[$result['status']])) {
			Log::alert ($result);
			return Response::json (['status' => 'error' , 'msg' => 'booking not found'] , 404);
		}

		$get_our_status = SELF::OUR_STATUS_FOR_UBER[$result['status']];

		DB::beginTransaction ();

		try {
			ServiceBooked::where ('id' , '=' , $booked_id)->update (array('status' => $get_our_status));
			foreach ($result as $key => $value) {
				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->first ();
				$meta_key_id = $meta_key_id['id'];
				$update = ServiceMetaData::where ('meta_key_id' , $meta_key_id)
					->where ('booked_id' , $booked_id)
					->update (['value' => $value]);
			}
		}
		catch (\Exception $e) {
			DB::rollBack ();
			throw $e;
		}
		DB::commit ();

		/*
		 * sending notification to particular user.
		 */

		$title = 'Ride Status';
		$description = 'Your Uber ride is ' . strtolower ($get_our_status);
		$customData = [];
		$customData = ['booking_id' => $booked_id , 'service' => SERVICE_UBER , 'status' => $get_our_status];

		$oNotificationService = new NotificationService;

		$aNotificationParams = [];
		$aNotificationParams['booking_id'] = $booked_id;
		$aNotificationParams['title'] = $title;
		$aNotificationParams['desc'] = $description;
		$aNotificationParams['custom_data'] = $customData;

		$oNotificationService->sendNotification ($aNotificationParams);

		return Response::json (['status' => 'success' , 'msg' => 'booking status updated'] , 200);
	}

	/**
	 * @param $API_SERVICE_CATEGORY_ID
	 * @param $API_SERVICE_ID
	 * @param $user_id
	 * @param $data
	 *
	 * @author Midhun
	 *         add ride later details in booking table
	 */
	public function addOlaRideLater ($API_SERVICE_CATEGORY_ID , $API_SERVICE_ID , $user_id , $data)
	{

		try {
			//change data to array
			$myarray = (array)$data;
			$result = ['status' => $myarray['status'] ,
				'booking_id' => $myarray['booking_id'] ,
				'message' => $myarray['message'] ,
			];

			// Creating random booking key
			$booking_key = booking_key_generator ();

			$ser_booked = $this->serviceBooked;
			$ser_booked->user_id = $user_id;
			$ser_booked->service_id = $API_SERVICE_CATEGORY_ID;
			$ser_booked->service_api_id = $API_SERVICE_ID;
			$ser_booked->status = 'BOOKED';
			$ser_booked->service_data = serialize ($myarray);
			$ser_booked->save ();

			$book_for_id = $ser_booked->id;

			// Insert details into service meta data based on the
			foreach ($result as $key => $value) {

				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , '=' , $API_SERVICE_ID)->first ();
				$meta_key_id = $meta_key_id['id'];

				$ser_meta_data = new ServiceMetaData();
				$ser_meta_data->meta_key_id = $meta_key_id;
				$ser_meta_data->booked_id = $book_for_id;
				$ser_meta_data->value = $value;
				//                $ser_meta_data->created_by = $user_id;
				$ser_meta_data->save ();
			}
			// return booking details
			$return = ['booking_id' => $book_for_id , 'booking_key' => $booking_key];

			return $return;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}
	}

	/**
	 * Update the cancel
	 *
	 * @param $booking_id
	 * @param $service_id
	 *
	 * @author Midhun
	 *
	 */
	public function UpdateUberCancel ($booking_id , $service_id)
	{

		try {
			// collecting service booking details
			$booking_details = ServiceBooked::where ('id' , '=' , $booking_id)->first ();

			// ensure booking is already exist or not
			if (empty($booking_details)) {
				return false;
			}

			// select service meta key of status details
			$meta_key_details = ServiceMetaKey::where ('meta_key' , '=' , 'status')->where ('service_id' , '=' , $service_id)->first ();

			// Update booking details in service booked table
			$servcice_update = ServiceBooked::where ('id' , $booking_id)->update (['status' => 'CANCELLED']);

			// Update booking details in service meta data table
			$update = ServiceMetaData::where ('booked_id' , $booking_id)->where ('meta_key_id' , $meta_key_details['id'])->update (['value' => 'CANCELLED']);

			return $update;

		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}

	/**
	 * @param $API_SERVICE_CATEGORY_ID
	 * @param $API_SERVICE_ID
	 * @param $user_id
	 * @param $data
	 *
	 * @author Midhun
	 *         Add dineout booking details into sever
	 *
	 */

	public function addDineoutBookDetails ($API_SERVICE_CATEGORY_ID , $API_SERVICE_ID , $user_id , $resp_data , $req_data)
	{
		try {
			//change data to array
			$myarray = (array)$resp_data->output_params;

			$result = ['booking_id' => $myarray['booking_id'] ,
				'display_id' => $myarray['display_id'] ,
				'diner_name' => $myarray['diner_name'] ,
				'restaurant_name' => $myarray['restaurant_name'] ,
				'restaurant_address' => $myarray['restaurant_address'] ,
				'dining_date_time' => $myarray['dining_date_time'] ,
				'people' => $myarray['people'] ,
				'special_request' => $myarray['special_request'] ,
				'offer_text' => $myarray['offer_text'] ,
				'city_name' => $myarray['city_name'] ,
				'area_name' => $myarray['area_name'] ,
				'locality_name' => $myarray['locality_name'] ,

				'diner_email' => $req_data['email'] ,
				'diner_phone' => $req_data['phone'] ,
				'booking_date' => $req_data['booking_date'] ,
				'booking_time' => $req_data['booking_time'] ,
				'rest_id' => $req_data['rest_id'] ,
				'male' => $req_data['male'] ,

				'order_id' => null ,
				'special_requirement' => null ,
				'offer' => null ,
				'no_of_people' => null ,
				'no_of_kids' => null ,
				'dining_date' => null ,
				'booking_status' => null ,
				'booking_added_date' => null ,
				'table_allocation_status' => null ,
				'b_id' => null ,
				'affiliate_id' => null ,
				'disp_id' => null ,
				'offer_id' => null ,
				'status' => null ,
				'url' => null ,

			];

			// Creating random booking key
			$booking_key = booking_key_generator ();

			$ser_booked = $this->serviceBooked;
			$ser_booked->user_id = $user_id;
			$ser_booked->service_id = $API_SERVICE_CATEGORY_ID;
			$ser_booked->service_api_id = $API_SERVICE_ID;
			$ser_booked->booking_key = $booking_key;
			$ser_booked->status = 'PROCESSING';
			$ser_booked->service_data = serialize (array_merge ($myarray , $req_data));
			$ser_booked->save ();

			$book_for_id = $ser_booked->id;

			// Insert details into service meta data based on the
			foreach ($result as $key => $value) {

				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , '=' , $API_SERVICE_ID)->first ();
				$meta_key_id = $meta_key_id['id'];

				$ser_meta_data = new ServiceMetaData();
				$ser_meta_data->meta_key_id = $meta_key_id;
				$ser_meta_data->booked_id = $book_for_id;
				$ser_meta_data->value = $value;
				//                $ser_meta_data->created_by = $user_id;
				$ser_meta_data->save ();
			}

			//notification related works
			$title = 'Your Booking Generated';
			$description = 'Hi Customer, Booking ID: ' . $booking_key . 'Restaurant Name: ' . $myarray['restaurant_name'] . ' Restaurant Address: ' . $myarray['restaurant_address'] . ' People: ' . $myarray['people'];

			$customData = ['booking_id' => $book_for_id , 'service' => 'dineout' , 'status' => strtolower ('PROCESSING')];
			$oNotificationService = new NotificationService;
			$aNotificationParams = [];
			$aNotificationParams['booking_id'] = $book_for_id;
			$aNotificationParams['title'] = $title;
			$aNotificationParams['desc'] = $description;
			$aNotificationParams['custom_data'] = $customData;

			$oNotificationService->sendNotification ($aNotificationParams);

			// return booking details
			$return = ['booking_id' => $book_for_id , 'booking_key' => $booking_key , 'diner_name' => $myarray['diner_name'] ,
				'dining_date_time' => $myarray['dining_date_time'] , 'male' => $req_data['male'] , 'female' => ($myarray['people'] - $req_data['male']) ,
				'restaurant_name' => $myarray['restaurant_name'] , 'restaurant_address' => $myarray['restaurant_address'] , 'offer_text' => $myarray['offer_text']];

			return $return;


		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}
	}

	public function updateDineoutBookDetails ($data , $booking_id , $service_id)
	{
		try {

			//change data to array
			$myarray = (array)$data->output_param->data[0];

			$status = SELF::OUR_STATUS_FOR_DINEOUT[$myarray['booking_status']];
			// TODO : Add more cases based on the return cases

			$update = ServiceBooked::where ('id' , $booking_id)->update (['status' => $status]);

			$result = ['booking_id' => $myarray['booking_id'] ,
				'diner_name' => $myarray['diner_name'] ,
				'diner_phone' => $myarray['diner_phone'] ,
				'special_requirement' => $myarray['special_requirement'] ,
				'offer' => $myarray['offer'] ,
				'no_of_people' => $myarray['no_of_people'] ,
				'no_of_kids' => $myarray['no_of_kids'] ,
				'dining_date' => $myarray['dining_date'] ,
				'booking_status' => $myarray['booking_status'] ,
				'booking_added_date' => $myarray['booking_added_date'] ,
				'table_allocation_status' => $myarray['table_allocation_status'] ,
				'restaurant_name' => $myarray['restaurant_name'] ,
				'city_name' => $myarray['city_name'] ,
				'area_name' => $myarray['area_name'] ,
				'locality_name' => $myarray['locality_name'] ,
			];

			// Update the service meta data
			foreach ($result as $key => $value) {

				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , $service_id)->first ();
				$meta_key_id = $meta_key_id['id'];

				$update = ServiceMetaData::where ('meta_key_id' , $meta_key_id)
					->where ('booked_id' , $booking_id)
					->update (['value' => $value]);
			}

			// Collect the details to return.
			$return_key = ['restaurant_name' , 'restaurant_address' , 'people' , 'dining_date_time' , 'diner_name' , 'diner_email' , 'diner_phone'];

			$return_array = array();
			foreach ($return_key as $keys) {
				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $keys)->where ('service_id' , $service_id)->first ();
				$meta_key_id = $meta_key_id['id'];

				$meta_data = ServiceMetaData::select ('value')->where ('meta_key_id' , '=' , $meta_key_id)
					->where ('booked_id' , '=' , $booking_id)
					->first ();
				$return_array[$keys] = $meta_data['value'];
			}

			return $return_array;

		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}

	/**
	 * @param $booking_id
	 * @param $API_SERVICE_ID
	 *
	 * @author Midhun
	 * @return bool
	 *         get dineout booking details including booking id and diner phone.
	 */
	public function getdineoutBookingdetails ($booking_id , $API_SERVICE_ID)
	{
		try {
			// Collect the details to return.
			$return_key = ['display_id' , 'diner_phone'];

			$return_array = array();
			foreach ($return_key as $keys) {
				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $keys)->where ('service_id' , $API_SERVICE_ID)->first ();
				$meta_key_id = $meta_key_id['id'];

				$meta_data = ServiceMetaData::select ('value')->where ('meta_key_id' , '=' , $meta_key_id)
					->where ('booked_id' , '=' , $booking_id)
					->first ();
				$return_array[$keys] = $meta_data['value'];
			}
			return $return_array;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}

	public function cancelDineoutBooking ($booking_id , $service_id)
	{

		try {


			// collecting service booking details
			$booking_details = ServiceBooked::where ('id' , '=' , $booking_id)->first ();

			// ensure booking is already exist or not
			if (empty($booking_details)) {
				return false;
			}

			//insert notification for booking
			$notification_data = ['title' => 'Your Booking Cancelled' ,
				'text' => 'Hi Customer, your booking towards this Booking ID: ' . $booking_details['booking_key'] . ' is cancelled'];
			$notification_data = json_encode ($notification_data);
			$notificaion_type_id = NotificationType::select ('id')->where ('type' , '=' , 'service')->where ('name' , '=' , 'cancel')->first ();
			$notify = insert_notification ($booking_details['user_id'] , $service_id , $notificaion_type_id->id , $notification_data);


			// Update booking details in service booked table
			$servcice_update = ServiceBooked::where ('id' , $booking_id)->update (['status' => 'CANCELLED']);


			return $servcice_update;

		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}


	/**
	 * @param $service_id
	 * @param $booking_id
	 *
	 * @return return database operation based on webhoook for OLA
	 */


	public function updateStatusOfBookingForDineout ($service_id , $result)
	{
		$booked_id_of_service_booked = ServiceMetaData::join ('service_meta_key' , 'service_meta_key.id' , '=' , 'service_meta_data.meta_key_id')
			->select ('service_meta_data.booked_id')
			->where ('service_meta_key.meta_key' , 'booking_id')
			->where ('service_meta_key.service_id' , $service_id)
			->where ('service_meta_data.value' , $result['booking_id'])
			->getQuery ()// Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
			->first ();

		if (!$booked_id_of_service_booked) {
			return Response::json (['status' => 'error' , 'msg' => 'booking not found'] , 404);
		}
		$booked_id = $booked_id_of_service_booked->booked_id;
		$get_our_status = SELF::OUR_STATUS_FOR_DINEOUT[$result['booking_status']];

		DB::beginTransaction ();
		try {
			ServiceBooked::where ('id' , '=' , $booked_id)->update (array('status' => $get_our_status));
			foreach ($result as $key => $value) {
				$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->first ();
				$meta_key_id = $meta_key_id['id'];
				$update = ServiceMetaData::where ('meta_key_id' , $meta_key_id)
					->where ('booked_id' , $booked_id)
					->update (['value' => $value]);
			}
		}
		catch (\Exception $e) {
			DB::rollBack ();
			throw $e;
		}
		DB::commit ();

		/*
		 * sending notification to particular user.
		 */


		$title = 'Booking Status';
		$description = 'Your booking status changed to ' . $result['booking_status'];
		$customData = [];
		$customData['status'] = $result['booking_status'];

		$oNotificationService = new NotificationService;

		$aNotificationParams = [];
		$aNotificationParams['booking_id'] = $booked_id;
		$aNotificationParams['title'] = $title;
		$aNotificationParams['desc'] = $description;
		$aNotificationParams['custom_data'] = $customData;

		$oNotificationService->sendNotification ($aNotificationParams);

		return Response::json (['status' => 'success' , 'msg' => 'booking status updated'] , 200);
	}


	public function addYatraHotelBookingDetails ($aRequestData)
	{
		try {
			// Creating random booking key
			$booking_key = booking_key_generator ();
			$oServiceBooked = $this->serviceBooked;
			$oServiceBooked->user_id = $aRequestData['userId'];
			$oServiceBooked->service_id = $aRequestData['serviceId'];
			$oServiceBooked->service_api_id = $aRequestData['serviceApiId'];
			$oServiceBooked->booking_key = $booking_key;
			$oServiceBooked->ip = isset($aRequestData['ip']) ? $aRequestData['ip'] : null;
			$oServiceBooked->latitude = isset($aRequestData['latitude']) ? $aRequestData['latitude'] : null;
			$oServiceBooked->longitude = isset($aRequestData['longitude']) ? $aRequestData['longitude'] : null;
			$oServiceBooked->created_at = date ('Y-m-d H:i:s');
			$oServiceBooked->service_data = serialize ($aRequestData);
			$oServiceBooked->status = 'PROCESSING';
			$oServiceBooked->save ();

			$bookingId = $oServiceBooked->id;


			$aMetaDataArray = [
				'hotel_code' => $aRequestData['HotelCode'] ,
				'no_of_rooms' => $aRequestData['nofUnits'] ,
				'guest_details' => json_encode ($aRequestData['guestCount']) ,
				'rate_plan_code' => $aRequestData['ratePlanCode'] ,
				'room_type_code' => $aRequestData['roomTypeCode'] ,
				'hotel_name' => null ,
				'area_name' => null ,
				'address_line' => null ,
				'city_name' => null ,
				'state' => null ,
				'country_name' => null ,
				'hotel_contact_numbers' => null ,
				'stay_start_date' => $aRequestData['StayDateRangeStart'] ,
				'stay_end_date' => $aRequestData['StayDateRangeEnd'] ,
				'hotel_room_base_amount_before_tax' => $aRequestData['rateWithOutAdditionalGuestAmount'] ,
				'total_additional_guest_amount' => $aRequestData['additionalGuestAmount'] ,
				'hotel_discount_amount' => $aRequestData['hotelDiscountAmount'] ,
				'net_hotel_amount_before_tax' => $aRequestData['AmountBeforeTax'] ,
				'hotel_tax_amount' => $aRequestData['taxAmount'] ,
				'hotel_tax_name' => $aRequestData['hotelTaxName'] ,
				'hotel_final_amount_with_tax' => $aRequestData['AmountBeforeTax'] + $aRequestData['taxAmount'] ,
				'service_charge' => $aRequestData['serviceCharge'] ,
				'service_charge_percetage' => $aRequestData['serviceChargePercentage'] ,
				'tax_for_service_charge' => $aRequestData['serviceChargeTax'] ,
				'service_charge_tax_percentage' => $aRequestData['serviceChargeTaxPercentage'] ,
				'service_tax_name' => $aRequestData['serviceTaxName'] ,
				'promo_id' => $aRequestData['promoId'] ,
				'discount_percentage' => $aRequestData['discountPercentage'] ,
				'discount_amount' => $aRequestData['discountAmount'] ,
				'net_amount_to_pay' => $aRequestData['netAmountToPay'] ,
				'customer_special_instruction' => $aRequestData['userComments'] ,
				'customer_name_prefix' => $aRequestData['customerDetails']['prefix'] ,
				'customer_first_name' => $aRequestData['customerDetails']['firstName'] ,
				'customer_middle_name' => $aRequestData['customerDetails']['middileName'] ,
				'customer_surname' => $aRequestData['customerDetails']['surname'] ,
				'customer_phone_number' => $aRequestData['customerDetails']['phoneNumber'] ,
				'customer_phone_type' => $aRequestData['customerDetails']['PhoneTechType'] ,
				'customer_email_id' => $aRequestData['customerDetails']['emailId'] ,
				'gaurantee_type' => $aRequestData['guaranteeType'] ,
				'payment_status' => $aRequestData['netAmountToPay'] > 0 ? 'PENDING' : 'SUCCESS' ,
				'payment_referrance_id' => null ,
				'provisional_booking_uniqueid_type' => $aRequestData['uniqueIdType'] ,
				'provisional_booking_unique_id' => $aRequestData['provisionalBookingId'] ,
				'final_booking_uniqueid_type' => null ,
				'final_booking_unique_id' => null ,
				'booking_status' => 'PROCESSING' ,
				'cancel_penalty_info_text' => null ,
				'affiliate_commission_amount' => null ,
				'affiliate_commission_percentage' => null ,
				'affiliate_commission_hotel_tax_included' => null ,

				'correlation_id' => null ,
				'cancel_policy_text' => null ,
				'cancel_date' => null ,
				'cancel_type' => null ,
				'cancel_refund_amount' => null ,
				'cancel_currency_code' => null ,

				'cancel_confirm_text' => null ,
				'cancel_unique_id' => null ,

				'refund_unique_id' => null ,
				'refund_entity' => null ,
				'refund_amount' => null ,
				'refund_currency' => null ,
				'refund_created_at' => null ,


			];

			$aServiceMetaData = [];
			foreach ($aMetaDataArray as $key => $value) {
				$oMetaKey = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , '=' , $aRequestData['serviceApiId'])->first ();
				#echo $aRequestData['serviceId']; die;
				$metakeyId = $oMetaKey['id'];

				$oServiceMetaData = new ServiceMetaData();
				$aServiceMetaData[] = [
					'meta_key_id' => $metakeyId ,
					'booked_id' => $bookingId ,
					'value' => $value
				];

			}

			if ($aServiceMetaData)
				ServiceMetaData::insert ($aServiceMetaData);

			return [
				'booking_id' => $bookingId
			];

		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}


	/*
	 * update booking status as Booked
	 *
	 *
	 */

	public function updateYatraBookingAsConfirmed ($aData)
	{
		try {
			$bookingId = $aData['bookingId'];
			$aMetaKeyValues = $aData['metaKeyValue'];
			$serviceApiId = $aData['serviceApiId'];
			$paymentReferenceId = $aMetaKeyValues['payment_referrance_id'];


			//update service booked table status:
			$oServiceBooked = ServiceBooked::find ($bookingId);
			$oServiceBooked->status = 'BOOKED';
			$oServiceBooked->save ();
			$userId = $oServiceBooked->user_id;


			foreach ($aMetaKeyValues as $key => $value) {
				$oMetaKey = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , '=' , $serviceApiId)->first ();
				#echo $aRequestData['serviceId']; die;
				$metakeyId = $oMetaKey['id'];

				ServiceMetaData::where ('meta_key_id' , '=' , $metakeyId)
					->where ('booked_id' , '=' , $bookingId)
					->update (['value' => $value]);
			}


			////////////
			$aServiceMetaData = ServiceMetaData::where ('booked_id' , '=' , $bookingId)
				->select (['value' , 'service_meta_data.meta_key_id' , 'service_meta_key.meta_key'])
				->join ('service_meta_key' , 'service_meta_key.id' , '=' , 'service_meta_data.meta_key_id')
				->get ()->toArray ();

			$oHotelBookingProcess = new HotelBookingProcess;

			$aFormattedData = $oHotelBookingProcess->formatMetaKeyValues ($aServiceMetaData);
			$netAmountToPay = $aFormattedData['net_amount_to_pay'];
			$hotelName = $aFormattedData['hotel_name'];
			$stayStartDate = $aFormattedData['stay_start_date'];
			$stayEndDate = $aFormattedData['stay_end_date'];
			$customerFirstName = $aFormattedData['customer_first_name'];
			$yatraBookingId = $aFormattedData['final_booking_unique_id'];
			$hotelAddress = $aFormattedData['address_line'] . ", " . $aFormattedData['city_name'];


			//entry to transaction table:


			$oTransaction = new Transaction;
			$oTransaction->booked_id = $bookingId;
			$oTransaction->user_id = $userId;
			$oTransaction->gateway_id = $paymentReferenceId;
			$oTransaction->amount = $netAmountToPay;
			$oTransaction->status = 'SUCCESS';
			$oTransaction->save ();

			$notificationData = "Dear " . $customerFirstName . ", Your booking has been successfully completed. This is your booking Id: " . $yatraBookingId . ". Please see the
            details below- Hotel :" . $hotelName . ", " . $hotelAddress . ". Check in date: " . $stayStartDate . ", Checkout date: " . $stayEndDate . ".";

			$aNotificationParams = [];
			$aNotificationParams['booking_id'] = $bookingId;
			$aNotificationParams['title'] = 'Your Booking Generated';
			$aNotificationParams['desc'] = $notificationData;
			$aNotificationParams['custom_data'] = [];


			$oNotificationService = new NotificationService;
			$oNotificationService->sendNotification ($aNotificationParams);

			//entry  to notification table

			die('its done!');


		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			// dd ($e->getMessage ());
			return false;
		}

	}

	/*
   *
   *
   *
   *
   */
	public function getValidYatraProvisionalBookingId ($aWhere)
	{
		$serviceId = $aWhere['service_id'];
		$serviceApiId = $aWhere['service_api_id'];
		$bookingId = $aWhere['booking_id'];
		$provisionalBookingId = null;
		$oMetakey = ServiceMetaKey::where ('service_id' , '=' , $serviceApiId)
			->where ('meta_key' , '=' , 'provisional_booking_unique_id')
			->get ()->first ();
		$metaKeyId = $oMetakey->id;

		$oServiceMetaData = ServiceMetaData::where ('meta_key_id' , '=' , $metaKeyId)
			->where ('booked_id' , '=' , $bookingId)
			->select ('value')
			->get ()->first ();

		$_provisionalBookingId = isset($oServiceMetaData->value) ? $oServiceMetaData->value : null;

		#echo $_provisionalBookingId; die;
		if ($_provisionalBookingId) {
			$oServiceBooked = ServiceBooked::find ($bookingId);

			if (isset($oServiceBooked->status) && $oServiceBooked->status == 'PROCESSING')
				$provisionalBookingId = $_provisionalBookingId;
		}

		return $provisionalBookingId;

	}


	/*
	* get booking status
	*
	*
	*/

	public function getYatraBookingStatus ($bookingId , $loggedUserId)
	{
		$oBooking = ServiceBooked::find ($bookingId);
		$aReturn = [];

		$aError = [];


		if (!isset($oBooking->id) || (isset($oBooking->id) && $oBooking->user_id != $loggedUserId)) {
			$aError[] = 'Invalid booking Id';
		} else {
			$bookingKey = $oBooking->booking_key;
			$status = ucwords ($oBooking->status);
			$userId = $oBooking->user_id;

			$aServiceMetaData = ServiceMetaData::where ('booked_id' , '=' , $bookingId)
				->select (['value' , 'service_meta_data.meta_key_id' , 'service_meta_key.meta_key'])
				->join ('service_meta_key' , 'service_meta_key.id' , '=' , 'service_meta_data.meta_key_id')
				->get ()->toArray ();


			$aReturn['service_booked'] = $oBooking->toArray ();
			$aReturn['meta_data'] = $aServiceMetaData;


		}

		return [
			'sError' => implode (',' , $aError) ,
			'aData' => $aReturn ,

		];


	}

	/**
	 * @param $owhere
	 *
	 * @author Midhun
	 *         get booking details of cancel for process cancel
	 */
	public function getYatraBookingDetails ($aWhere)
	{
		$serviceId = $aWhere['service_id'];
		$serviceApiId = $aWhere['service_api_id'];
		$bookingId = $aWhere['booking_id'];


		// Collect the details to return.
		$return_key = ['provisional_booking_unique_id' , 'final_booking_unique_id' , 'customer_surname' , 'customer_email_id' , 'stay_start_date' , 'stay_end_date'];

		$return_array = array();
		foreach ($return_key as $keys) {
			$meta_key_id = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $keys)->where ('service_id' , $serviceApiId)->first ();
			$meta_key_id = $meta_key_id['id'];

			$meta_data = ServiceMetaData::select ('value')->where ('meta_key_id' , '=' , $meta_key_id)
				->where ('booked_id' , '=' , $bookingId)
				->first ();
			$return_array[$keys] = $meta_data['value'];
		}

		return $return_array;

	}

	/*
 * update initialize cancel status
 *
 *
 */

	public function updateYatraBookingAsCancelled ($aData)
	{

		try {
			$bookingId = $aData['bookingId'];
			$aMetaKeyValues = $aData['metaKeyValue'];
			$serviceApiId = $aData['serviceApiId'];
//            $status       = $aData['status'];

			foreach ($aMetaKeyValues as $key => $value) {

				$oMetaKey = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , '=' , $serviceApiId)->first ();
				$metakeyId = $oMetaKey['id'];

				ServiceMetaData::where ('meta_key_id' , '=' , $metakeyId)
					->where ('booked_id' , '=' , $bookingId)
					->update (['value' => $value]);
			}

		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}

	/**
	 * @param $aData
	 *
	 * @author Midhun
	 * @return bool
	 *
	 * update confirm cancel details
	 */

	public function updateYatraCancelAsConfirmed ($aData)
	{
		try {
			$bookingId = $aData['bookingId'];
			$aMetaKeyValues = $aData['metaKeyValue'];
			$serviceApiId = $aData['serviceApiId'];


			$status = SELF::OUR_STATUS_FOR_YATRAHOTEL[$aData['status']];
			//update service booked table status:
			$oServiceBooked = ServiceBooked::find ($bookingId);
			$oServiceBooked->status = $status;
			$oServiceBooked->save ();

			foreach ($aMetaKeyValues as $key => $value) {
				$oMetaKey = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , '=' , $serviceApiId)->first ();
				#echo $aRequestData['serviceId']; die;
				$metakeyId = $oMetaKey['id'];

				ServiceMetaData::where ('meta_key_id' , '=' , $metakeyId)
					->where ('booked_id' , '=' , $bookingId)
					->update (['value' => $value]);
			}
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}
	}

	/**
	 * @param $aData
	 *
	 * @author Midhun
	 * @return bool
	 *
	 * Update refund details in full amount
	 */

	public function updateYatraBookingAsRefunded ($aData)
	{
		try {
			$bookingId = $aData['bookingId'];
			$aMetaKeyValues = $aData['metaKeyValue'];
			$serviceApiId = $aData['serviceApiId'];
			$refundMessage = $aData['message'];


			$status = SELF::OUR_STATUS_FOR_YATRAHOTEL[$aData['status']];
			//update service booked table status:
			$oServiceBooked = ServiceBooked::find ($bookingId);
			$oServiceBooked->status = $status;
			$oServiceBooked->save ();

			foreach ($aMetaKeyValues as $key => $value) {
				$oMetaKey = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , '=' , $serviceApiId)->first ();
				#echo $aRequestData['serviceId']; die;
				$metakeyId = $oMetaKey['id'];

				ServiceMetaData::where ('meta_key_id' , '=' , $metakeyId)
					->where ('booked_id' , '=' , $bookingId)
					->update (['value' => $value]);
			}

			//update the refund status in transactions table

			// Update booking details in service booked table
			$updteTransaction = Transaction::where ('booked_id' , $bookingId)->update (['refund_status' => $status , 'refund_msg' => $refundMessage]);

			return true;


		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}
	}


	/*
	* get booking history - it will return last 20 results
	*
	*
	*
	*/

	public function getBookingHistory ($userId , $serviceId , $serviceApiId)
	{
		/*$aBookingHistory = ServiceBooked::where('user_id', '=', $userId)
										->where('services_booked.service_id', '=', $serviceId)
										->where('services_booked.service_api_id', '=', $serviceApiId)

										->join('service_meta_key', 'service_meta_key.service_id', '=', 'services_booked.service_api_id')
										->join('service_meta_data',  function ($join) {
											$join->on('service_meta_data.booked_id','=','services_booked.id');
											$join->on('service_meta_data.meta_key_id', '=','service_meta_key.id');
										})

										->select(['services_booked.service_id','services_booked.service_api_id','services_booked.created_at','services_booked.status as booking_status','services_booked.id as booking_id',
											'service_meta_key.meta_key','service_meta_data.value'
											])
										->orderBy('services_booked.created_at', 'DESC')
										->get()->toArray();*/

		$aBookingHistory = ServiceBooked::where ('user_id' , '=' , $userId)
			->where ('services_booked.service_id' , '=' , $serviceId)
			->where ('services_booked.service_api_id' , '=' , $serviceApiId)
			->orderBy ('services_booked.created_at' , 'DESC')
			->limit (20)
			->get ()->toArray ();
		$aReturn = [];

		$oHotelBookingProcess = new HotelBookingProcess();

		foreach ($aBookingHistory as $key => $aValue) {
			$_bookingId = $aValue['id'];
			$_createdAt = formatDate ('Y-m-d H:i:s' , 'd-m-Y h:i A' , $aValue['created_at']);
			$_status = ucwords (strtolower ($aValue['status']));

			$_aBooking = [];
			$_aBooking['booking_id'] = $_bookingId;
			$_aBooking['created_at'] = $_createdAt;
			$_aBooking['status'] = $_status;

			$aServiceMetaData = ServiceMetaData::where ('booked_id' , '=' , $_bookingId)
				->select (['value' , 'service_meta_data.meta_key_id' , 'service_meta_key.meta_key'])
				->join ('service_meta_key' , 'service_meta_key.id' , '=' , 'service_meta_data.meta_key_id')
				->get ()->toArray ();


			$_aReturn = $oHotelBookingProcess->formatMetaKeyValues ($aServiceMetaData);


			$_aBooking = array_merge ($_aBooking , $_aReturn);
			$aReturn[] = $_aBooking;
			//echo 'gggggg';
			#dd($_aBooking);

		}


		return $aReturn;
	}

	/*
	*
	*
	*
	*
	*/

	public function getMetaKeyValues ($serviceApiId , $bookingId , $aMetaKeys)
	{
		$aReturn = [];

		$aMetaData = ServiceMetaKey::where ('service_id' , '=' , $serviceApiId)
			->whereIn ('meta_key' , $aMetaKeys)
			->select ('service_meta_key.id as key_id' , 'service_meta_key.meta_key' , 'service_meta_data.value')
			->join ('service_meta_data' , function ($join) use ($bookingId) {
				$join->on ('service_meta_data.meta_key_id' , '=' , 'service_meta_key.id');
				$join->where ('service_meta_data.booked_id' , '=' , $bookingId);
			})
			->get ()->toArray ();

		foreach ($aMetaData as $key => $aVal) {
			$_keyName = $aVal['meta_key'];
			$_value = $aVal['value'];

			$aReturn[$_keyName] = $_value;
		}

		return $aReturn;
	}


	public function updateMetaDataValues ($serviceApiId , $bookingId , $aData)
	{
		try {
			foreach ($aData as $key => $value) {
				$oMetaKey = ServiceMetaKey::select ('id')->where ('meta_key' , '=' , $key)->where ('service_id' , '=' , $serviceApiId)->first ();
				$metakeyId = isset($oMetaKey['id']) ? $oMetaKey['id'] : null;

				ServiceMetaData::where ('meta_key_id' , '=' , $metakeyId)
					->where ('booked_id' , '=' , $bookingId)
					->update (['value' => $value]);


			}
		}
		catch (Exception $e) {
			//echo $e->getMessage(); //nothing happens here!!
			//die;
		}

	}


}
