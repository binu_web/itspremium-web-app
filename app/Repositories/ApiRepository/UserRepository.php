<?php

namespace App\Repositories\ApiRepository;

use App\Models\ServiceApi;
use App\Models\User;
use App\Models\UserApiAccessToken;
use App\Services\SMSApi;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\Models\ServiceBooked;
use App\Models\ServiceMetaData;
use App\Models\ServiceMetaKey;
use App\Models\Feedback;
use App\Models\Notification;
use Exception;

//use Illuminate\Support\Facades\Notification;


class UserRepository
{

	public $user;

	/**
	 * Constuct Function
	 *
	 * @author Binoop V
	 */
	public function __construct ()
	{
		$this->user = new User();
	}

	/**
	 * Check if the user is available in the system.
	 *
	 * @param $email
	 * @param $password
	 *
	 * @author Binoop V
	 * @return mixed
	 */
	public function authenicateUser ($email , $password)
	{
		$user = User::where ('email' , $email)->first ();

		if (is_null ($user))
			return false;

		if (Hash::check ($password , $user->password))
			return $user;
		else
			return false;


	}


	/**
	 * Check if the user is available in the system with Specified Social Fields.
	 *
	 * @param $column      - Column to Search (facebook_id || google_id)
	 * @param $credentials (value)
	 *
	 * @author Binoop V
	 * @return mixed
	 */
	public function serachUserThroughSocial ($column , $credientials)
	{
		$user = User::where ($column , $credientials)->first ();

		if (is_null ($user))
			return false;

		return $user;
	}


	/**
	 * Check if the user is available in the system with Specified Social Fields.
	 *
	 * @param $column      - Column to Search (facebook_id || google_id)
	 * @param $credentials (value)
	 *
	 * @author Binoop V
	 * @return mixed
	 */
	public function authenicateUserThroughSocial ($email , $column , $credientials)
	{
		$user = User::where ('email' , $email)->first ();

		if (is_null ($user))
			return false;


		// Check user if email is valid
		if ($user->$column != $credientials) {
			$user->$column = $credientials;
			$user->update ();
		}


		return $user;
	}


	/**
	 * Check the user with email exists or not
	 *
	 * @param $email
	 *
	 * @author Midhun
	 * @return bool
	 */
	public function check_email_exists ($email)
	{

		if (User::where ('email' , $email)->first ()) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Check the user with phone exists or not
	 *
	 * @param $email
	 *
	 * @author Midhun
	 * @return bool
	 */
	public function check_mobile_exists ($mobile)
	{

		if (User::where ('mobile' , $mobile)->first ()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Insert user deatils in registration
	 *
	 * @param $data
	 *
	 * @author Midhun
	 * @return User
	 */

	public function regUser ($data)
	{

		$email = $data['email'];
		$password = $data['password'];
		$mobile_number = $data['mobile_number'];
		$f_name = $data['f_name'];
		$l_name = $data['l_name'];
		$fb_id = isset($data['fb_id']) ? $data['fb_id'] : null;
		$google_id = isset($data['google_id']) ? $data['google_id'] : null;


		//Add basic student details
		$this->user->email = $email;
		$this->user->mobile = $mobile_number;
		$this->user->password = Hash::make ($password);;
		$this->user->first_name = $f_name;
		$this->user->last_name = $l_name;
		$this->user->country = $data['country'];
		$this->user->facebook_id = $fb_id;
		$this->user->google_id = $google_id;

		try {
			$status = $this->user->save ();
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}


		return $status ? $this->user : false;

	}

	/**
	 *  Create otp token and send to user with message
	 *
	 * @param $mobile_number
	 *
	 * @author Midhun
	 * @return string
	 *
	 */
	public function createOtpVerification ($mobile_number)
	{


		$strlength = 6;
		$nums = "0123456789";
		$otp = '';
		$charLen = strlen ($nums);

		for ($i = 0; $i < $strlength; $i++) {
			$otp .= $nums[rand (0 , $charLen - 1)];
		}

		try {
			$status = $this->user->where ('mobile' , $mobile_number)
				->update (['otp_token' => $otp]);
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

		// TODO: We have to send the OTP to mobile with text message
		Log::info ('OTP FOR MOBILE NUMBER ' . $mobile_number . ' is ' . $otp);


		// SMS API
		$smsAPI = new SMSApi();
		$isSMSSent = $smsAPI->send (Config::get ('messagelist.REGISTERATION') . " " . $otp , $mobile_number);

		return $otp;
	}

	/**
	 * Verify the otp number with mobile number and confirm
	 *
	 * @param $mobile_number
	 * @param $otp_token
	 *
	 * @author Midhun
	 */
	public function verifyToken ($mobile_number , $otp_token)
	{
		try {
			$users = $this->user->where ('mobile' , $mobile_number)->first ();
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}
		if ($users->otp_token == $otp_token) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Obtain User Mobile Details
	 */
	public function getMobileNumber ($email)
	{
		try {
			$users = $this->user->select ('mobile')->where ('email' , $email)->first ();
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

		return $users;
	}

	/**
	 * Update Password For the User
	 */
	public function updatePassword ($email , $password)
	{
		try {
			$users = $this->user->where ('email' , $email)->update (['password' => Hash::make ($password)]);
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

		return $users;

	}

	/**
	 * @param $user_id
	 *
	 * @author Midhun
	 *         collect the service details of
	 */
	public function getUserServices ($user_id)
	{
		try {
			$account_details = ServiceApi::select ('service_apis.id as id' , 'service_apis.provider_name as service_name' , 'service_apis.api_image as image' , 'services.name as service_cat' , 'user_access_tokens.access_token')
				->leftjoin ('services' , 'service_apis.service_category' , '=' , 'services.id')
				->leftJoin ('user_access_tokens' , 'service_apis.id' , '=' , DB::raw ('user_access_tokens.service_api_id AND user_access_tokens.is_active = 1 AND user_access_tokens.deleted_at IS NULL AND user_access_tokens.user_id =' . $user_id))
				->where ('service_apis.is_active' , '=' , 1)
				->where ('service_apis.deleted_at' , '=' , NULL)
				->get ()->toArray ();

			$temp = [];
			foreach ($account_details as $accounts) {

				if ($accounts['access_token'] != null) {
					$accounts['connect'] = true;
				} else {
					$accounts['connect'] = false;
				}
				$temp[] = $accounts;
			}

			return $temp;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
//			dd ($e->getMessage ());
			return false;
		}
	}

	/**
	 * @param $user_id
	 *
	 * @author Midhun
	 * @return bool
	 *         Get all bookings of user
	 */

	Public function getUserBookings ($user_id)
	{

		try {
			$details = ServiceBooked::select ('services_booked.*' , 'service_apis.provider_name as service_name' , 'service_apis.api_image as service_image' , 'services.name as service_categoryname' , 'services.image as category_image')
				->join ('service_apis' , 'service_apis.id' , '=' , 'services_booked.service_api_id')
				->join ('services' , 'services.id' , '=' , 'services_booked.service_id')
				->orderBy ('services_booked.id' , 'desc')
				->where ('user_id' , $user_id)->get ()->toArray ();

			return $details;

		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
//			dd ($e->getMessage ());
			return false;
		}

	}

	/**
	 * @param $user_id
	 * @param $booking_id
	 *
	 * @author Midhun
	 *         Get the booking details of a particular booking
	 */
	public function getUserBookingDetails ($booking_id)
	{
		try {

			$serviceBooked = ServiceBooked::where ('id' , $booking_id)->get ()->toArray ();
			$details = DB::table ('service_meta_key')
				->join ('service_meta_data' , 'service_meta_key.id' , '=' , 'service_meta_data.meta_key_id')
				->select ('meta_key' , 'VALUE')
				->where ('booked_id' , $booking_id)
				->get ()->toarray ();
			$booking_data = array();

			foreach ($details as $detail) {
				if ($detail->VALUE == null)
					$d_value = 'NO DATA';
				else
					$d_value = $detail->VALUE;
				$booking_data[$detail->meta_key] = $d_value;
			}

			$response = array_merge ($booking_data , $serviceBooked);

			return $response;

		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
//			dd ($e->getMessage ());
			return false;
		}
	}

	/**
	 * @param $feedback
	 * @param $user_id
	 *
	 * @author Midhun
	 *         Save the user feedback in feedback table
	 */
	public function postUserFeedback ($feedback , $user_id)
	{
		try {
			$ser_booked = new Feedback();
			$ser_booked->user_id = $user_id;
			$ser_booked->feedback = $feedback;
			$ser_booked->save ();

			return true;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
//			dd ($e->getMessage ());
			return false;
		}
	}

	public function getUserProfile ($user_id)
	{
		try {

			$profile = User::where ('id' , $user_id)->get ()->toArray ();
			return $profile;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
//			dd ($e->getMessage ());
			return false;
		}
	}

	public function getUserNotifications ($user_id)
	{
		try {
			$details = DB::table ('notification')
				->select ('notification.*' , 'notification.id as notification_id' , 'service_apis.id' , 'service_apis.provider_name' , 'service_apis.api_image' , 'services.*' , 'service_apis.service_category as service_category_id' , 'services.image as category_image' , 'services.name as category_name')
				->join ('service_apis' , 'notification.service_api_id' , '=' , 'service_apis.id')
				->join ('services' , 'service_apis.service_category' , '=' , 'services.id')
				->where ('notification.user_id' , $user_id)
				->where ('notification.deleted_at' , null)
				->orderBy ('notification.id' , 'desc')
				->get ()->toarray ();

			$notifications = array();
			foreach ($details as $notify) {
				$data = json_decode ($notify->notification_data);
				$notifications[] = [
					'notification_id' => $notify->notification_id ,
					'user_id' => $notify->user_id ,
					'service_api_id' => $notify->service_api_id ,
					'notification_type_id' => $notify->notification_type_id ,
					'is_active' => $notify->is_active ,
					'created_at' => $notify->created_at ,
					'updated_at' => $notify->updated_at ,
					'provider_name' => $notify->provider_name ,
					'api_image' => $notify->api_image ,
					'service_category_id' => $notify->service_category_id ,
					'category_name' => $notify->category_name ,
					'category_image' => $notify->category_image ,
					'notifications_data' => $data ,

				];
			}

			return $notifications;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
//			dd ($e->getMessage ());
			return false;
		}
	}

	public function deleteUserNotifications ($notification_id)
	{
		try {
			$update = Notification::where ('id' , $notification_id)
				->update (['deleted_at' => date ('Y-m-d H:i:s')]);
			return true;

		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
//			dd ($e->getMessage ());
			return false;
		}
	}


	/*
	*
	*
	*
	*/

	public function getNotificationDetails($bookingId)
	{
		
		$oServiceBooked = ServiceBooked::find($bookingId);
		$serviceApiId   = $oServiceBooked->service_api_id;
		$bookingKey 	= $oServiceBooked->booking_key;

		$aReturn 	   = [
			'booking_id' 	 => $bookingId,
			'service_api_id' => $serviceApiId,
			'booking_key'	 => $bookingKey

		];
		$aMetakeys 	   = [];
		$aMetaValues   = [];
		if($bookingId) {
			switch($serviceApiId) {
				case "1":
				//get ola related fields 
				$aMetakeys = ['driver_name','driver_number','cab_type','cab_number','car_model','eta','driver_lat','driver_lng','trip_info_payable_amount','trip_info_discount',
				'trip_info_wait_time_unit','trip_info_wait_time_value','trip_info_advance','trip_info_distance_unit','booking_status'];
				break;

				case "2": //uber
				$aMetakeys = ['driver_name','driver_sms_number','vehicle_model','vehicle_make','driver_lat','driver_lng','eta','pickup_latitude','pickup_longitude','destination_latitude','destination_longitude'];
				break;

				case "3": //Zomato - this case not exists
				$aMetakeys 	= [];
				break;

				case "4":
				$aMetakeys  = ['booking_id','display_id','diner_name','restaurant_name','restaurant_address','dining_date_time','people','special_request','booking_date','booking_time','order_id','no_of_people','no_of_kids','dining_date','booking_status','table_allocation_status'];
				break;

				case "5":
				$aMetakeys = ['hotel_name','area_name','address_line','city_name','state','hotel_contact_numbers','stay_start_date','stay_end_date','hotel_final_amount_with_tax',
				'service_charge','tax_for_service_charge'];
				break;


				default:
				$aMetakeys = [];
				break;

			}
		}

		if($aMetakeys) {
			$aMetaValues 		  = $this->getMetaKeyValues($serviceApiId, $bookingId, $aMetakeys);
			$aReturn['meta_data'] = $aMetaValues;
		}

		return $aReturn;
		
	}

	/*
	*
	*
	*
	*/

	public function getMetaKeyValues($serviceApiId, $bookingId, $aKeyNames)
	{


		$aReturn = [];

		$aMetaData = ServiceMetaKey::where('service_id', '=', $serviceApiId)
									->whereIn('meta_key', $aKeyNames)
									->select('service_meta_key.id as key_id', 'service_meta_key.meta_key', 'service_meta_data.value')
									->join('service_meta_data', function ($join) use ($bookingId) {
										$join->on('service_meta_data.meta_key_id', '=', 'service_meta_key.id');
										$join->where('service_meta_data.booked_id', '=', $bookingId);
									})
									->get()->toArray();

		return $aMetaData;
	}

    /*
     * To disconnect a service of a user
     *
     */
    public function disconnectService($serviceApiId, $userId)
    {
        $oUser          = User::find($userId);
        if(!$oUser)
            throw new Exception('User not exists', 400);
        $oAccessToken   = $oUser->userAccessTokenbyservice($serviceApiId);
        $oAccessToken->update(['access_token' => null, 'refresh_token' => null, 'token_expiry' => null, 'is_active' => 0]);
    }
}


