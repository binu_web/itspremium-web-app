<?php


namespace App\Repositories\ApiRepository;


use App\Models\CabLog;
use App\Models\ServiceBooked;
use App\Models\ServiceApi;
use DB;

class CabLogRepository
{
	public $cabLog;

	CONST OUR_STATUS_FOR_CAB = array('ARRIVING','IN PROGRESS','PROCESSING','BOOKED');


	/**
	 * Constuct Function
	 *
	 * @author Binoop V
	 */
	public function __construct ()
	{
		$this->cabLog = new CabLog();
	}

	/**
	 * @return bool
	 * cabLog Pickup & drop off
	 * @frijo
	 */
	public function getCabLog ($user_id)
	{

		try {

			$cabLog = $this->cabLog->where ('deleted_at' , null)
				->where ('user_id' , $user_id)
				->orderBy ('location_type' , 'DESC')
				->orderBy ('location' , 'ASC')
				->orderBy ('is_favorite' , 'DESC')->get ();

			if (count ($cabLog) == 0)
				return [];

			$data = [];
			$location = '';
			$location_type = $cabLog[0]->location_type;
			foreach ($cabLog as $value) {

				if ($location_type == $value->location_type && $location != $value->location) {
					$data[strtolower ($value->location_type)][] = ['id' => $value->id ,
						'location' => $value->location ,
						'latitude' => $value->latitude ,
						'is_favorite' => $value->is_favorite ,
					];
				}

				$location = $value->location;
				$location_type = $value->location_type;
			}

			return $data;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}
	}

	/**
	 * @param $user_id
	 *
	 * @return mixed
	 * update user cab log
	 * @frijo
	 */
	public function setCabLogFavorite ($location_id , $favorite)
	{
		try {


			$result = $this->cabLog
				->where ('id' , $location_id)
				->where ('deleted_at' , null)
				->update (['is_favorite' => $favorite]);
			return $result;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}
	}

	/**
<<<<<<< HEAD
	 * @param $user_id
	 * @author Midhun
	 * get the whole list of booking history of cabs based on user
	 */

	public function getCabHistory($user_id){

		try {
			$details = ServiceBooked::select('services_booked.*','service_apis.provider_name as service_name')
			->join('service_apis', 'service_apis.id', '=', 'services_booked.service_api_id')
			->where('user_id',$user_id)->get()->toArray();

			return $details;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}

	/**
	 * @param $booking_id
	 * @author Midhun
	 *  Get booking details of history
	 */

	public function getHistoryDetails($booking_id){

		try {
			$serviceBooked = ServiceBooked::where('id',$booking_id)->get()->toArray();
			$details = DB::table('service_meta_key')
				->join('service_meta_data', 'service_meta_key.id', '=', 'service_meta_data.meta_key_id')
				->select('meta_key', 'VALUE')
				->where('booked_id',$booking_id)
				->get()->toarray();
			$booking_data = array();

			foreach($details as $detail){
				if($detail->VALUE == null)
					$d_value = 'NO DATA';
				else
					$d_value = $detail->VALUE;
				$booking_data[$detail->meta_key] =  $d_value;
			}

			$response = array_merge($booking_data, $serviceBooked);

			return $response;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}


	}
	 /** Insert user log details at the time of cab search
	 * @param $data
	 * @param $user_id
	 * @author Midhun
	 */

	public function Insert_log_details($request,$user_id){

		try{
		//Checks drop details exist
		if (checkArrayIsEmpty ([$request['drop_lat'] , $request['drop_lng'] , $request['category']])) {
			// Checks the details already exist
			$details = CabLog::select('id')
				->where('user_id','=',$user_id)->where('location_type','=','PICKUP')
				->where('latitude','=',$request['pickup_lat'])->where('longitude','=',$request['pickup_lng'])
				->first();
			// If row exist update the date
			if($details!==null){
				$status = CabLog::where('user_id','=',$user_id)->where('location_type','=','PICKUP')
					->where('latitude','=',$request['pickup_lat'])->where('longitude','=',$request['pickup_lng'])
					->update(['updated_at' => date ('Y-m-d H:i:s')]);
			}
			// add the details into row
			else{
				$insert_log = new CabLog();
				$insert_log->user_id = $user_id;
				$insert_log->ride_time = $request['ride_time'];
				$insert_log->ride_type = 'RIDE_NOW';
				$insert_log->location_type = 'PICKUP';
				$insert_log->location = $request['pickup_loc'];
				$insert_log->latitude = $request['pickup_lat'];
				$insert_log->longitude = $request['pickup_lng'];
				$status = $insert_log->save();

			}
		}
		else{

			// Checks the details already exist
			$details = CabLog::select('id')
				->where('user_id','=',$user_id)->where('location_type','=','PICKUP')
				->where('latitude','=',$request['pickup_lat'])->where('longitude','=',$request['pickup_lng'])
				->first();
				// If row exist update the date
						if($details!==null){
							$status = CabLog::where('user_id','=',$user_id)->where('location_type','=','PICKUP')
								->where('latitude','=',$request['pickup_lat'])->where('longitude','=',$request['pickup_lng'])
								->update(['updated_at' => date ('Y-m-d H:i:s')]);
						}
						// add the details into row
						else{
							$insert_log = new CabLog();
							$insert_log->user_id = $user_id;
							$insert_log->ride_time = $request['ride_time'];
							$insert_log->ride_type = 'RIDE_NOW';
							$insert_log->location_type = 'PICKUP';
							$insert_log->location = $request['pickup_loc'];
							$insert_log->latitude = $request['pickup_lat'];
							$insert_log->longitude = $request['pickup_lng'];
							$status = $insert_log->save();

						}

			// Checks the details already exist
			$dropdetails = CabLog::select('id')
				->where('user_id','=',$user_id)->where('location_type','=','DROPOFF')
				->where('latitude','=',$request['drop_lat'])->where('longitude','=',$request['drop_lng'])
				->first();
			// If row exist update the date
						if($dropdetails!==null){
							$dropstatus = CabLog::where('user_id','=',$user_id)->where('location_type','=','DROPOFF')
								->where('latitude','=',$request['drop_lat'])->where('longitude','=',$request['drop_lng'])
								->update(['updated_at' => date ('Y-m-d H:i:s')]);
						}
						// add the details into row
						else{
							$insert_log = new CabLog();
							$insert_log->user_id = $user_id;
							$insert_log->ride_time = $request['ride_time'];
							$insert_log->ride_type = 'RIDE_NOW';
							$insert_log->location_type = 'DROPOFF';
							$insert_log->location = $request['drop_loc'];
							$insert_log->latitude = $request['drop_lat'];
							$insert_log->longitude = $request['drop_lng'];
							$dropstatus = $insert_log->save();

						}
			}
			return true;

		}catch(\Exception $e){
			sqlFailureLog($e);
			return false;

		}

	}

	/**
	 * @param $request
	 * @author Midhun
	 * get on going ride details of cabs
	 */

	public function getOngoingRideDetails($request)
	{

		try{
				// Checks the details already exist
				$details = ServiceBooked::select()
					->where('service_id','=',$request['service_category_id'])
					->where('user_id','=',$request['user_id'])
					->whereIn('status', SELF::OUR_STATUS_FOR_CAB)
					->orderBy('id','DESC')
					->first();

			    $return = [];
				if($details){

					$service_name = ServiceApi::where ('id' , $details->service_api_id)->select('provider_name')->first();

					$details['service_name'] = strtolower ($service_name->provider_name);

					$return = [

						'booking_id' => $details->id,
						'service_id' => $details->service_id,
						'service_api_id' => $details->service_api_id,
						'user_id' => $details->user_id,
						'booking_key' => $details->booking_key,
						'status' =>$details->status,
						'service_name' => $details->service_name,
					];

				}

				return $return;

		}catch(\Exception $e){
		sqlFailureLog($e);
		return ['error' => TRUE , 'message' => $e->getMessage () , 'code' => $e->getCode ()];
		}
	}

}