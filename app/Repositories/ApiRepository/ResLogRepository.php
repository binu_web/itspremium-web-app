<?php


namespace App\Repositories\ApiRepository;


use App\Models\RestaurantLog;
use App\Models\ServiceBooked;
use DB;

class ResLogRepository
{
    public $ResLog;

    /**
     * Constuct Function
     *
     * @author Binoop V
     */
    public function __construct ()
    {
        $this->ResLog = new RestaurantLog();
    }


    /** Insert user log details at the time of retuarant search
     * @param $data
     * @param $user_id
     * @author Midhun
     */

    public function Insert_log_details($request,$user_id){

        try{

            $request = [
                'lat' => $request->get ('lat') ,
                'long' => $request->get ('long') ,
                'location' => $request->get ('location') ,
            ];


               // Checks the details already exist
                $details = RestaurantLog::select('id')
                    ->where('user_id','=',$user_id)
                    ->where('latitude','=',$request['lat'])->where('longitude','=',$request['long'])
                    ->first();
                // If row exist update the date
                if($details!==null){
                    $status = RestaurantLog::where('user_id','=',$user_id)
                        ->where('latitude','=',$request['lat'])->where('longitude','=',$request['long'])
                        ->update(['updated_at' => date ('Y-m-d H:i:s')]);
                }
                // add the details into row
                else{
                    $insert_log = new RestaurantLog();
                    $insert_log->user_id = $user_id;
                    $insert_log->location = $request['location'];
                    $insert_log->latitude = $request['lat'];
                    $insert_log->longitude = $request['long'];
                    $status = $insert_log->save();

                }
                return true;

        }catch(\Exception $e){
            sqlFailureLog($e);
            return false;

        }

    }

    /**
    <<<<<<< HEAD
     * @param $user_id
     * @author Midhun
     * get the whole list of booking history of retaurant based on user
     */

    public function getResLogHistory($user_id){

        try {
            $details = RestaurantLog::select()
                ->where('user_id',$user_id)
                ->whereNotNull('location')
                ->orderBy('is_favorite',1)
                ->get()
                ->toArray();

            return $details;
        }
        catch (\Exception $e) {
            sqlFailureLog ($e);
            return false;
        }

    }

    /**
     * @param $user_id
     *
     * @return mixed
     * update user res log
     * @frijo
     */
    public function setResLogFavorite ($location_id , $favorite)
    {
        try {
            $result = RestaurantLog::where ('id' , $location_id)
                ->where ('deleted_at' , null)
                ->update (['is_favorite' => $favorite]);
            return $result;
        }
        catch (\Exception $e) {
            sqlFailureLog ($e);
            return false;
        }
    }

    /**
     * @param $user_id
     * @author Midhun
     * Collect the favourite locations of user
     */
    public function getResFavoriteLocations($user_id)
    {
        try {
            $details = RestaurantLog::select()
                ->where('user_id',$user_id)->where('is_favorite',1)->get()->toArray();

            return $details;
        }
        catch (\Exception $e) {
            sqlFailureLog ($e);
            return false;
        }

    }







}