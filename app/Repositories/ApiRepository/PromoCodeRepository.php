<?php 
/*
*
*
*
*
*/

namespace App\Repositories\ApiRepository;

use App\Models\PromoCode;
use App\Models\PromocodeServiceMap;
use DB;



class PromoCodeRepository
{

	public $oPromoCode;
	public function __construct()
	{
		$this->oPromoCode = new PromoCode;
	}


	public function getAllPromoCodes()
	{
		#dd(get_class_methods())
		$aPromoCode = PromoCode::whereRaw('CURDATE() >= valid_from AND CURDATE() <= valid_to')->get()->toArray();
		$aReturn    = [];

		foreach($aPromoCode as $key => $aVal) {
			#dd($aVal);
			#echo $aVal['valid_from']; die;
			
			$_validFrom = formatDate('Y-m-d h:i:s', 'd-m-Y', $aVal['valid_from']);
			$_validTo   = formatDate('Y-m-d h:i:s', 'd-m-Y', $aVal['valid_to']);

			$aReturn[] 			 = [
				'promo_code'	=> $aVal['promo_code'],
				'discount_type' => $aVal['discount_type'],
				'discount'		=> $aVal['discount'],
				'valid_from_date'    => $_validFrom,
				'valid_to_date'		=> $_validTo,
				'no_of_maximum_use' => $aVal['no_of_maximum_use'],
				'description'		=> $aVal['description'],
				'validation_rules' => $aVal['validation_rules'],


			];
			#$aReturn
		}

		return $aReturn;
	}


	public function getServicePromoCodes($serviceId)
	{
		$aPromoCode = PromoCode::join('promocode_service_map', 'promocode_service_map.promocode_id', '=', 'promo_code.id')
								->where('service_api_id', '=', $serviceId)
								->whereRaw('CURDATE() >= valid_from AND CURDATE() <= valid_to')
								->select([
									'promo_code.promo_code','promo_code.discount_type','promo_code.discount','promo_code.no_of_maximum_use',
									'promo_code.description','promo_code.validation_rules',
									DB::Raw('DATE_FORMAT(promo_code.valid_from, "%d-%m-%Y %h:%i %p") as valid_from_date,
										DATE_FORMAT(promo_code.valid_to,"%d-%m-%Y %h:%i %p") as valid_to_date

										')

									])
								->get()->toArray();

		return $aPromoCode;
	}

	/*
	* validate promo code
	*
	*
	*/

	public function validatePromoCode($promoCode, $serviceId, $serviceApiId)
	{
		$aError = [];
		$oPromoCode 	= PromoCode::where('promo_code', '=', $promoCode)->get()->first();

		$promoId 		= isset($oPromoCode->id) ? $oPromoCode->id : null;
		$aError 		= [];
		$aData 			= [];





		if($promoId) {
			$discount 	  = $oPromoCode->discount;
			$discountType = $oPromoCode->discount_type;
			$validFrom 	  = $oPromoCode->valid_from;
			$validTo 	  = $oPromoCode->valid_to;
			$maxNoOfUse   = $oPromoCode->no_of_maximum_use;
			$noOfTimesPerUser = $oPromoCode->no_of_times_per_user;
			$noOfTimesPerUserPerService = $oPromoCode->no_of_times_per_user_per_service;

			//check the promo code is mapped to any service:

			$aPromoService  = PromocodeServiceMap::where('promocode_id','=', $promoId)->get()->toArray();
			if($aPromoService) {
				//check the serviceApi id is mapped with promo code:
				$aMappedDet = PromocodeServiceMap::where('promocode_id','=', $promoId)
												 ->where('service_api_id', '=', $serviceApiId)
												 ->where('is_active', '=', 1)
												 ->get()->first();

				if(!isset($aMappedDet->id))
					$aError[] = 'Promocode not mapped to the applied service';


			}

			if(!$aError) {	

				$curDate 	= date('Y-m-d');
				

				if(!($curDate >= $validFrom) || ($validTo && $curDate >  $validTo))
					$aError[] = 'Promo code has expired';

				//get total no of times used, per user, per user per service:
				#$totalUsedNum 	= 


				/////









			}







		}
		else
			$aError[] = 'Invalid promo code';


	}
}
