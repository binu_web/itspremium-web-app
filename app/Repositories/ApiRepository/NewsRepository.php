<?php

namespace App\Repositories\ApiRepository;

use App\Models\News;
use App\Models\NewsBookmark;
use App\Models\NewsLog;
use DB;

class NewsRepository{

    public $news,$newsBookmark,$newsLog;

    /**
     * construct fucntion
     */
    public function __construct(NewsLog $newsLog)
    {
        $this->news = new News();
        $this->newsBookmark=new NewsBookmark();
        $this->newsLog=$newsLog;
    }

    /**
     * List all news with category
     * @author Midhun
     *
     */
    public function getNews(){

        try {
            $news = $this->news->select('id','title','image','source_link','category','description','publish_date')
                ->with('newsCategory')
                ->where('is_active',1)
                ->where('deleted_at',null)
                ->orderBy('created_at', 'desc')
                ->get()->toarray();

        }
        catch (\Exception $e) {
            sqlFailureLog ($e);
            return false;
        }

        return $news;

    }

    /**
     * find news based on category
     * @param $category
     * @author Midhun
     */
    public function getNewsBasedCategory($category){

        try {
            $news = $this->news->select('id','title','image','source_link','category','description','publish_date')
                ->with('newsCategory')
                ->where('is_active',1)
                ->where('category',$category)
                ->where('deleted_at',null)
                ->orderBy('created_at', 'desc')
                ->get()->toarray();

       }
        catch (\Exception $e) {
            sqlFailureLog ($e);
            return false;
        }

        return $news;
    }

    /**
     * Get single news
     * @param $id
     * @author Midhun
     * @return bool
     */

    public function getNews_id($id){
        try {
            $news = $this->news->select('id','title','image','source_link','category','description','publish_date')
                ->with('newsCategory')
                ->where('is_active',1)
                ->where('id',$id)
                ->where('deleted_at',null)
                ->orderBy('created_at', 'desc')
                ->get()->toarray();

        }
        catch (\Exception $e) {
            sqlFailureLog ($e);
            return false;
        }

        return $news;
    }

    /**
     * Set news
     * @param news_id,user_id
     * @auther Frijo
     */
    public function setNews($news_id,$user_id)
    {
        try {

            $this->newsBookmark->news_id = $news_id;
            $this->newsBookmark->user_id = $user_id;
            $this->newsBookmark->is_saved = 1;
            $this->newsBookmark->created_at = date('Y-m-d H:i:s');
            $this->newsBookmark->updated_at = date('Y-m-d H:i:s');
            $s_status = $this->newsBookmark->save();
            return $s_status;

        } catch (\Exception $e) {
            sqlFailureLog($e);
            return false;
        }
    }

    /**
     * Set news By Id
     * @param news_id,user_id
     * @auther Frijo
     */
    public function setNewsById(array $data,$user_id)
    {
        try {
            $this->newsBookmark = $this->newsBookmark->find($data['news_book_id']);
            $this->newsBookmark->news_id = $data['news_id'];
            $this->newsBookmark->user_id = $user_id;
            $this->newsBookmark->is_saved = $data['is_saved'];
            $this->newsBookmark->created_at = date('Y-m-d H:i:s');
            $this->newsBookmark->updated_at = date('Y-m-d H:i:s');
            $s_status = $this->newsBookmark->save();
            return $s_status;

        } catch (\Exception $e) {
            sqlFailureLog($e);
            return false;
        }
    }

    /**
     * Get news by user id
     * @param user id
     * @author Frijo
     */
    public function getNewsById($user_id){
        try {
            $news = $this->newsBookmark->where('news_bookmark.user_id',$user_id)
                ->where('news_bookmark.is_saved',1)
                ->join('news','news.id','=','news_bookmark.news_id')
                ->select('news.*')
                ->where('news_bookmark.deleted_at',null)
                ->get()->toArray();
            return $news;
        } catch (\Exception $e) {
            sqlFailureLog($e);
            return false;
        }
    }
    /**
     * news log
     *
     */
    public function setNewsLog(array $data,$user_id){
        $newsLogCount=$this->newsLog->where('news_id','=',$data['news_id'])->where('user_id',$user_id)->count();
        if($newsLogCount==0){
            $this->newsLog->news_id = $data['news_id'];
            $this->newsLog->user_id = $user_id;
            $this->newsLog->source_link = $data['source_link'];
            $this->newsLog->created_at = date('Y-m-d H:i:s');
            $this->newsLog->updated_at = date('Y-m-d H:i:s');
            $s_status = $this->newsLog->save();
            return $s_status;
        }else{
            return true;
        }
    }

}