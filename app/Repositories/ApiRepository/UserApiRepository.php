<?php

namespace App\Repositories\ApiRepository;

use App\Models\User;
use App\Models\ServiceMetaData;
use App\Models\ServiceMetaKey;
use App\Models\UserApiAccessToken;
use Illuminate\Support\Facades\Hash;
use DB;

class UserApiRepository
{

	public $userApi;

	/**
	 * Constuct Function
	 *
	 * @author Binoop V
	 */
	public function __construct ()
	{
		$this->userApi = new UserApiAccessToken();
		$this->usertableApi = new User();
        $this->serviceMetaData=new ServiceMetaData();
        $this->serviceMetaKey=new ServiceMetaKey();
	}

	/**
	 * Check if Token Exists
	 *
	 * @param $user_id
	 * @param $service_id
	 *
	 * @author Binoop V
	 */
	public function checkTokenExists ($user_id = 0 , $service_api_id = 0)
	{
		if (empty($user_id))
			return false;

		$result = $this->userApi->byUser ($user_id)->byServiceApi ($service_api_id)->first ();

		return is_null ($result) ? false : $result->id;
	}

	/**
	 * Update Token Based on the Token Id
	 *
	 * @param array $updateSet
	 * @param       $token_id
	 *
	 * @author Binoop V
	 * @return mixed
	 */
	public function updateToken ($updateSet = [] , $token_id)
	{
		try {
			$status = $this->userApi->where ('id' , $token_id)->update ($updateSet);
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}


		return $status;

	}


	/**
	 * Create New User Service API Access Token Entry
	 *
	 * @param array $dataSet
	 *
	 * @author Binoop V
	 * @return mixed
	 */
	public function createToken ($dataSet = [])
	{
		$this->userApi->access_token = $dataSet['access_token'];
		$this->userApi->service_api_id = $dataSet['service_api_id'];
		$this->userApi->user_id = $dataSet['user_id'];
		$this->userApi->token_expiry = $dataSet['token_expiry'];

		if (isset($dataSet['refresh_token'])) {
			$this->userApi->refresh_token = $dataSet['refresh_token'];

		}


		return $this->userApi->save ();

	}
    public static function authenicateUserForJwt ($email , $id)
    {
        $user = User::where ('email' , $email)->where('id',$id)->first ();

        if ($user)
            return true;
        else
            return false;
    }



}