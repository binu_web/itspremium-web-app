<?php

namespace App\Repositories\ApiRepository;

use App\Models\NewsCategory;


class NewsCategoryRepository
{

	public $newscategory;

	/**
	 * Constuct Function
	 *
	 * @author Binoop V
	 */
	public function __construct ()
	{
		$this->newscategory = new NewsCategory();
	}

	/**
	 * @author Midhun
	 * Get all active news category
	 */
	public function getCategory ()
	{

		try {
			$news_category = $this->newscategory->where ('is_active' , 1)->where ('deleted_at' , null)->get ()->toArray ();
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

		return $news_category;
	}

}