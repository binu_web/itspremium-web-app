<?php

namespace App\Repositories\ApiRepository;

use App\Models\Notification;
use App\Models\NotificationType;
use Illuminate\Support\Facades\Hash;


class NotificationRepository
{

	public $notification,$notificationType;

	/**
	 * Constuct Function
	 *
	 * @author frijo
	 */
	public function __construct (Notification $notification,NotificationType $notificationType)
	{
		$this->notification = $notification;
		$this->notificationType=$notificationType;
	}

	/**
	 * @param array $data
	 * @param array $data2
	 * Set notification data
	 * @Author Frijo
	 * @return mixed
	 */
	public function setNotificatin(array $data,array $data2){
		$this->notification->user_id=$data['user_id'];
		$this->notification->service_api_id=$data['service_api_id'];
		$this->notification->notification_type_id=$data['notification_type_id'];
		$this->notification->notification_data=serialize($data2);
		$this->notification->is_active=1;
		$this->notification->created_at=date('Y-m-d H:i:s');
		$this->notification->updated_at=date('Y-m-d H:i:s');
		$result=$this->notification->save();

		return $result;

	}

	/**
	 * @param $userId
	 * List Notification
	 * @Author Frijo
	 */
	public function listNotificatin($userId){
		$result=$this->notification->where('user_id',$userId)
				->select('notification_type.name','notification_type.type','notification.*')
				->leftJoin('notification_type','notification_type.id','=','notification.notification_type_id')->get();
		$data = [];
		foreach($result as $val){
			$notification_data=unserialize($val['notification_data']);
			$data[]=(is_null($val) || empty($val)) ? [] : ['id' => $val->id, 'user_id' => $val->user_id,'service_api_id' => $val->service_api_id,'name' => $val->name, 'type' => $val->type,'notification_data'=>$notification_data];
		}

		return $data;

	}
}