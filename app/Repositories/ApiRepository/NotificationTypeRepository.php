<?php

namespace App\Repositories\ApiRepository;

use App\Models\Notification;
use App\Models\NotificationType;
use Illuminate\Support\Facades\Hash;


class NotificationTypeRepository
{

	public $notification,$notificationType;

	/**
	 * Constuct Function
	 *
	 * @author frijo
	 */
	public function __construct ()
	{
		$this->notification = new Notification();
		$this->notificationType=new NotificationType();
	}

}