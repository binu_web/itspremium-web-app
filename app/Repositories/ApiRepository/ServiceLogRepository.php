<?php


namespace App\Repositories\ApiRepository;

use App\Repositories\ApiRepository\DeviceRepository;

use App\Models\ServiceLog;
use Response;
use DB;

class ServiceLogRepository
{

    /**
     * Constuct Function
     *
     * @author Binoop V
     */
    public function __construct ()
    {
        $this->serviceLog = new ServiceLog();
    }

    public function addServiceLog($adata)
    {

        try{
        $ser_meta_data = new ServiceLog();

        foreach($adata as $key => $value)
        {
            $ser_meta_data->$key = $value;
        }

        $ser_meta_data->save();

        return true;

        }
        catch (\Exception $e) {
            sqlFailureLog ($e);
            return false;
        }

    }

}