<?php
namespace App\Repositories\ApiRepository;

use App\Models\HotelSearchLog;
use App\Models\YatraLocation;

class HotelSearchLogRepository 
{

	private $oHotelSerachLog;
	public function __construct()
	{
		$this->oHotelSerachLog = new HotelSearchLog;
	}

	/*
	*
	*
	*
	*/

	public function insertLog($request, $userId)
	{	
		$locationId   = $request->get('location_id');
		$stayDateFrom = $request->get('stay_date_from');
		$stayDateTo   = $request->get('stay_date_to');
		$roomDetails  = $request->get('room_details');
		$sortOrder 	  = $request->get('sort_order');
		$hotelCode 	  = $request->get('hotel_code');
		$hotelName    = $request->get('hotel_name');
		$locationType = $request->get('location_type');
	
		$aData 		  = [
			'location_id' 	 => $locationId,
			'stay_date_from' => $stayDateFrom,
			'stay_date_to'   => $stayDateTo,
			'room_details'   => json_decode($roomDetails, true),
			'sort_order' 	 => $sortOrder,
			'hotel_code'	 => $hotelCode,
			'hotel_name'	 => $hotelName,
			'location_type'  => $locationType,
		];


		$sData 				= json_encode($aData);

		
		//entry to hotel search log:
		$oSearch 					= new HotelSearchLog;
		$oSearch->user_id    	 	= $userId;
		$oSearch->search_log 		= $sData;
		$oSearch->save();


	}


	/*
	*
	*
	*
	*
	*/

	public function getLog($userId)
	{

		$aLog = HotelSearchLog::where('user_id', '=', $userId)
							   ->orderBy('created_at', 'DESC')
							   ->limit(10)
							   ->get()->toArray();


		$aReturn = [];

		foreach($aLog as $key => $aVal) {
			$aSeachLog      = json_decode($aVal['search_log'], true);
			if($aSeachLog) {
				$userId 		= $aVal['user_id'];
				$locationId 	= $aVal['location_id'];
			}
			
			$isFavourite 	= $aVal['is_favourite'] ? $aVal['is_favourite'] : 0;
			$id 			= $aVal['id'];
			$oLog 			= HotelSearchLog::find($id);
			$oLocation 		= $oLog->location()->get()->first();
			
			$aReturn[] 		= [
				'location_id' 	=> $locationId,
				'location_name' => $oLocation->name,
				'location_type' => $oLocation->type,
				'is_favourite'  => $isFavourite,
			];

		}

		$aReturn = [];

		
			

		return $aReturn;
	}

	/*
	* set as favourite
	*
	*
	*
	*/

	public function setasFavourite($flag, $locationId, $userId)
	{	
		if($flag == 1){
			$isFavourite = 1;
		}
		else {
			$isFavourite = 0;
		}
		$update = HotelSearchLog::where('location_id', '=', $locationId)
							     ->update(['is_favourite' => $isFavourite]);

		return $update;
	}


	/*
	* get last search
	*
	*
	*
	*/

	public function getLastSearch($userId)
	{

		$oData = HotelSearchLog::where('user_id', '=', $userId)
								->orderBy('created_at', 'DESC')
								->get()->first();

		$aSearchLog  = [];

		if(isset($oData->id) && $oData->id) {
			$searchLog  = $oData->search_log;
			$aSearchLog = json_decode($searchLog, true);
			$isFavorite = $oData->is_favourite ? $oData->is_favourite : '';
			$aSearchLog['is_favorite'] = $isFavorite;

		}

		return $aSearchLog;
	}
	
	






}