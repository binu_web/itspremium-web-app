<?php

namespace App\Repositories\ApiRepository;

use App\Models\Device;
use Illuminate\Support\Facades\Hash;


class DeviceRepository
{

	public $device;

	// Defining Model For Device Models
	public function __construct ()
	{
		$this->device = new Device();
	}

	/**
	 * Insert device details in
	 *
	 * @param $data
	 * @param $user_id
	 *
	 * @author Midhun
	 */

	public function createDevice ($data)
	{
		$this->device->device_id = $data['device_id'];
		$this->device->device_name = $data['device_name'];
		$this->device->device_token = $data['device_token'];
		$this->device->fcm_token = $data['fcm_token'];
		$this->device->type = $data['type'];


		// Save User
		try {
			$status = $this->device->save ();
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;

		}

		return $status ? $this->device->id : false;

	}

	/**
	 * Check if Device Is Registered or Not
	 *
	 * @param $device_id
	 * @param $fcm_token
	 *
	 * @author Binoop V
	 * @return Device Obj
	 */
	public function checkIfRegistered ($device_id , $fcm_token)
	{
		$device = $this->device->byDevice ($device_id)->first ();
		return $device;
	}

	/**
	 * Update FCM token for a device
	 *
	 * @param $id
	 * @param $fcm_token
	 *
	 * @author Binoop V
	 * @return mixed
	 */
	public function updateFCMToken ($id , $fcm_token)
	{
		$device = $this->device->find ($id);
		$device->fcm_token = $fcm_token;

		return $device->save ();
	}

	/**
	 * Check / Update if User device is Updated or not
	 *
	 * @param $device_id
	 * @param $user_id
	 *
	 * @author Binoop V
	 * @return bool
	 */
	public function checkAndUpdateDevice ($device_id , $user_id)
	{
		$device = $this->device->find ($device_id);

		// Update Device for user
		if (!is_null ($device) && ($device->user_id != $user_id)) {
			$device->user_id = $user_id;
			return $device->save ();
		}
		return true;
	}


	/**
	 * Get User Device Tokens Based on Service Select by User
	 *
	 * @param $booked_id
	 *
	 * @return bool
	 */
	public function getUserFCMTokens ($booked_id)
	{
		if (!empty($booked_id))
			$tokens = $this->device->join ('services_booked' , 'services_booked.user_id' , '=' , 'devices.user_id')
				->select ('devices.fcm_token')
				->where ('services_booked.id' , $booked_id)
				->get ()
				->pluck ('fcm_token');
		else
			return false;

		return $tokens->toArray ();
	}
}
