<?php


namespace App\Repositories\ApiRepository;


use App\Models\UserApiAccessToken;
use DB;

class UserAccessTokenRepository{

    public $userAccessToken;

    /**
     * Contruct Function
     *
     */

    public function __construct(){
        $this->userAccessToken = new UserApiAccessToken();

    }

    /**
     * get service token based on service api
     * @author Midhun
     */
    public function getAccestoken($user_id){

        try{


            $get_access_token = $this->userAccessToken->select('access_token')
                                ->where('user_id',$user_id)
                                ->where('service_api_id',1)
                                ->where('deleted_at', null)
                                ->first()->toarray();

            //TODO : Confirm the service api id from any other calls

            return $get_access_token['access_token'];

        }
        catch(\Exception $e){
            sqlFailureLog($e);
            return false;
        }

    }

}