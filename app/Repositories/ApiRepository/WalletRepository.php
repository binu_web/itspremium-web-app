<?php

namespace App\Repositories\ApiRepository;

use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\WalletLog;
use Exception;
use App\Models\ServiceBooked;
use App\Models\ServiceMetaData;
use App\Services\Yatra\HotelBookingProcess;
use App\Services\NotificationService;


class WalletRepository
{

	public $wallet;

	// Defining Model For Device Models
	public function __construct ()
	{
		$this->wallet = new Wallet();
		$this->wallet_log = new WalletLog();
	}

	/** start user account  */

	public function insertUserToWallet ($userId)
	{

		try {
			//Add basic student details
			$this->wallet->user_id = $userId;
			$this->wallet->balance = 0;
			$status = $this->wallet->save ();

			return $status;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}

	/**
	 * @param $request
	 *
	 * @author Midhun
	 *
	 * Add amount in wallet
	 */
	public function addAmountInWallet ($request)
	{
		try {

			$select = Wallet::where ('user_id' , '=' , $request['user_id'])->first ()->toArray ();

			if (is_array ($select) && isset($select['balance'])) {
				$balance = $select['balance'];
			} else {
				throw new Exception('No wallet is created for this user' , 400);
			}

			$update = Wallet::where ('user_id' , '=' , $request['user_id'])
				->update (['balance' => $balance + $request['amount']]);

			$current_balance = $balance + $request['amount'];

			//Add basic student details
			$this->wallet_log->wallet_id = $select['id'];
			$this->wallet_log->service_id = $request['service_id'];
			$this->wallet_log->balance = $request['amount'];
			$this->wallet_log->transaction_type = $request['transaction_type'];
			$this->wallet_log->transaction_method = $request['transaction_method'];
			$updateLog = $this->wallet_log->save ();


			if ($request['full_payment'] = 1) {

				$status = 'REFUNDED';
				$transcation_status = 1;
			} else {
				$status = 'PARTIALLY REFUNDED';
				$transcation_status = 2;
			}
			// TODO : Uncomment the below codes once finish the new migrate for refund
			//update service booked table status:
//			$oServiceBooked = ServiceBooked::find ($request['booking_id']);
//			$oServiceBooked->status = $status;
//			$oServiceBooked->save ();

			// notification for hotel cancel starts
			$aServiceMetaData = ServiceMetaData::where ('booked_id' , '=' , $request['booking_id'])
				->select (['value' , 'service_meta_data.meta_key_id' , 'service_meta_key.meta_key'])
				->join ('service_meta_key' , 'service_meta_key.id' , '=' , 'service_meta_data.meta_key_id')
				->get ()->toArray ();

			$oHotelBookingProcess = new HotelBookingProcess;
			$aFormattedData = $oHotelBookingProcess->formatMetaKeyValues ($aServiceMetaData);

			$customerFirstName = $aFormattedData['customer_first_name'];
			$yatraBookingId = $aFormattedData['final_booking_unique_id'];

			$notificationData = "Dear " . $customerFirstName . ", Your wallet is credited with Rs : " . $request['amount'] . " on " . date ('Y-m-d H:i:s') . ". Your wallet balance is " . $current_balance . ".";

			$aNotificationParams = [];
			$aNotificationParams['booking_id'] = $request['booking_id'];
			$aNotificationParams['title'] = 'Your refund is credited';
			$aNotificationParams['desc'] = $notificationData;
			$aNotificationParams['custom_data'] = [];
			$aNotificationParams['service'] = 'refund';

			$oNotificationService = new NotificationService;
			$oNotificationService->sendNotification ($aNotificationParams);

			$update = Transaction::where ('booked_id' , '=' , $request['booking_id'])
				->update (['refund_amount' => $request['amount'] , 'refund_status' => 'SUCCESS' , 'refund_type' => $transcation_status , 'refund_msg' => 'Refund processed successfully']);

			return true;

		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}

	}

	/**
	 * @param $user_id
	 *
	 * @author Midhun
	 *         Get wallet details of logged user
	 */

	public function getWalletDetails ($user_id)
	{
		try {

			$details = Wallet::where ('user_id' , '=' , $user_id)->first ()->toArray ();

			return $details;
		}
		catch (\Exception $e) {
			sqlFailureLog ($e);
			return false;
		}
	}


}


