<?php

namespace App\Repositories;

use App\Models\ServiceApi;
use InfyOm\Generator\Common\BaseRepository;

class ServiceApiRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'provider_name' ,
		'service_category' ,
		'api_key' ,
		'api_url' ,
		'api_token' ,
		'api_metadata' ,
		'api_image' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return ServiceApi::class;
	}
}
