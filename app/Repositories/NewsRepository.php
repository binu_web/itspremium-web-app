<?php

namespace App\Repositories;

use App\Models\News;
use InfyOm\Generator\Common\BaseRepository;
use Auth;
use App\Models\NewsCountry;


class NewsRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'category' ,
		'title' ,
		'image' ,
		'source_link' ,
		'description' ,
		'publish_date' ,
		'is_trending' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return News::class;
    }
    public $newsCountry,$news;

    public function __construct(NewsCountry $NewsCountry,News $news){
        $this->newsCountry = $NewsCountry;
        $this->news=$news;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        return \App\Models\News::where('news.id', '=', $id)
            ->select('news.*', 'nc.name as category_name', 'cu.first_name as created_by_name', 'uu.first_name as updated_by_name')
            ->leftJoin('news_categories as nc', 'nc.id', '=', 'news.category')
            ->leftJoin('admin_users as cu', 'cu.id', '=', 'news.created_by')
            ->leftJoin('admin_users as uu', 'uu.id', '=', 'news.updated_by')
            ->first();
    }

    public function selectNewsbyId($id)
    {
        return \App\Models\News::where('news.id', '=', $id)
            ->select('news.*', 'nc.name as category_name', 'cu.first_name as created_by_name', 'uu.first_name as updated_by_name')
            ->leftJoin('news_categories as nc', 'nc.id', '=', 'news.category')
            ->leftJoin('admin_users as cu', 'cu.id', '=', 'news.created_by')
            ->leftJoin('admin_users as uu', 'uu.id', '=', 'news.updated_by')
            ->first();
    }

    /**
     *news insertion
     *
     * @author Frijo
     */
    public function createNews(array $data)
    {

        $userId = Auth::id();
        $country = $data['country'];
        $this->news->category = $data['category'];
        $this->news->title = $data['title'];
        $this->news->source_link = $data['source_link'];
        $this->news->description = $data['description'];
        $this->news->publish_date = $data['publish_date'];
        $this->news->image = $data['image'];
        $this->news->is_active = $data['is_active'];
        $this->news->is_trending = $data['is_trending'];
        $this->news->created_by = $data['created_by'];
        $this->news->updated_by = $data['updated_by'];
        if ($this->news->save()) {
            $newsId = $this->news->id;
            //query Binding
            foreach ($country as $value) {
                $query_data[] =
                    array(
                        'country_id'=>$value,
                        'news_id'=>$newsId,
                        'created_by'=>$userId,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=> date('Y-m-d H:i:s'));
            }
            // insert Query
            $this->newsCountry->insert($query_data);
        }
    }

    /**
     * @param array $data
     * @param $id
     * update news & country
     */
    public function updateNews(array $data,$id){
        $userId = Auth::id();
        $country = $data['country'];
        $this->news = News::find($id);
        $this->news->category = $data['category'];
        $this->news->title = $data['title'];
        $this->news->source_link = $data['source_link'];
        $this->news->description = $data['description'];
        $this->news->publish_date = $data['publish_date'];
        $this->news->image = $data['image'];
        $this->news->is_active = $data['is_active'];
        $this->news->is_trending = $data['is_trending'];
        $this->news->updated_by = $data['updated_by'];
        if ($this->news->save()) {
            //removing old country
            $this->newsCountry->where('news_id', $id)->forceDelete();
            //Query Binding
            foreach ($country as $value) {
                $query_data[] =
                    array(
                        'country_id'=>$value,
                        'news_id'=>$id,
                        'created_by'=>$userId,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=> date('Y-m-d H:i:s'));
            }
            // insert country Query
            $this->newsCountry->insert($query_data);
        }
    }
    /**
     * delete news and Country
     *
     */
    public function deleteNews($id){
        $this->news->where('id', $id)->delete();
        $this->newsCountry->where('news_id',$id)->delete();

    }

}
