<?php

namespace App\Repositories;

use App\Models\PromoCode;
use InfyOm\Generator\Common\BaseRepository;

class PromoCodeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'promo_code' ,
        'deleted_at' ,
        'created_by' ,
        'updated_by'
    ];

    /**
     * Configure the Model
     **/
    public function model ()
    {
        return PromoCode::class;
    }
}
