<?php

namespace App\Repositories;

use App\Models\PaymentMethodCountry;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentMethodCountryRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'gateway_id' ,
		'country_id' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return PaymentMethodCountry::class;
	}

	public function insert (array $data , $id)
	{
		$auth_id = Auth::id ();
		$payment_country_arr = $data['service_countries'];
		foreach ($payment_country_arr as $value) {
			$paymentcountry = new PaymentMethodCountry();
			$paymentcountry->updated_by = $auth_id;
			$paymentcountry->created_by = $auth_id;
			$paymentcountry->gateway_id = $id;
			$paymentcountry->country_id = $value;
			$s_status = $paymentcountry->save ();
		}

	}

	public function update (array $data , $id)
	{
//        dd($data);
		$auth_id = Auth::id ();
		$payment_country_arr = $data['service_countries'];
		DB::table ('payment_method_countries')->where ('gateway_id' , '=' , $id)->delete ();
		foreach ($payment_country_arr as $value) {
			$paymentcountry = new PaymentMethodCountry();
			$paymentcountry->updated_by = $auth_id;
			$paymentcountry->created_by = $auth_id;
			$paymentcountry->gateway_id = $id;
			$paymentcountry->country_id = $value;
			$s_status = $paymentcountry->save ();
		}

	}

	public function delete ($id)
	{
		DB::table ('payment_method_countries')->where ('gateway_id' , '=' , $id)->delete ();
	}

}
