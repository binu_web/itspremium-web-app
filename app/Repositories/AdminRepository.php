<?php

namespace App\Repositories;

use App\Models\Admin;
use InfyOm\Generator\Common\BaseRepository;

class AdminRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'email' ,
		'password' ,
		'user_type' ,
		'first_name' ,
		'last_name' ,
		'mobile' ,
		'job_role' ,
		'is_active' ,
		'activation_code' ,
		'remember_token' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return Admin::class;
	}

	public function getEmailCount ($email)
	{
		return Admin::where ('email' , '=' , $email)->count ();
	}

	public function updateAdminUser (array $data , $id)
	{

	}
}
