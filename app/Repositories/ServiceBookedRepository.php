<?php

namespace App\Repositories;

use App\Models\ServiceBooked;
use InfyOm\Generator\Common\BaseRepository;

class ServiceBookedRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'user_id' ,
		'service_id' ,
		'service_api_id' ,
		'status' ,
		'ip' ,
		'latlong'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return ServiceBooked::class;
	}
}
