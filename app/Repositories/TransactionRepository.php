<?php

namespace App\Repositories;

use App\Models\Transaction;
use InfyOm\Generator\Common\BaseRepository;

class TransactionRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'booked_id' ,
		'user_id' ,
		'gateway_id' ,
		'amount' ,
		'status' ,
		'status_msg' ,
		'refund_status' ,
		'refund_msg'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return Transaction::class;
	}
}
