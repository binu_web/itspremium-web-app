<?php

namespace App\Repositories;

use App\Models\Device;
use InfyOm\Generator\Common\BaseRepository;

class DevicesRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'id' ,
		'type' ,
		'device_id' ,
		'user_id' ,
		'regId' ,
		'device_name' ,
		'device_token' ,
		'builder_version' ,
		'os_version' ,
		'screen_width' ,
		'screen_height' ,
		'ip_address'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return Device::class;
	}
}
