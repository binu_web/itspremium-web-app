<?php

namespace App\Repositories;

use App\Models\NewsCategory;
use InfyOm\Generator\Common\BaseRepository;

class NewsCategoryRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'name' ,
		'image' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return NewsCategory::class;
	}

	public function findWithoutFail ($id , $columns = ['*'])
	{
		return \App\Models\NewsCategory::where ('news_categories.id' , '=' , $id)
			->select ('news_categories.id' , 'news_categories.name' , 'news_categories.image' , 'news_categories.is_active' , 'news_categories.created_at' , 'news_categories.updated_at' , 'news_categories.deleted_at' , 'cu.first_name as created_by_name' , 'uu.first_name as updated_by_name')
			->leftJoin ('admin_users as cu' , 'cu.id' , '=' , 'news_categories.created_by')
			->leftJoin ('admin_users as uu' , 'uu.id' , '=' , 'news_categories.updated_by')
			->first ();
	}

}
