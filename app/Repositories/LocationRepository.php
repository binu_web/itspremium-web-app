<?php

namespace App\Repositories;

use App\Models\Location;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LocationRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'location' ,
		'lat_long' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return Location::class;
	}

	public function create (array $data)
	{
//        dd($data);
		$id = Auth::id ();
		$location = new Location();
		$location->is_active = $data['is_active'];
		$location->updated_by = $id;
		$location->created_by = $id;
		$location->distance = $data['distance'];
		$location->longitude = $data['longitude'];
		$location->latitude = $data['latitude'];
		$location->location = $data['location'];
		$s_status = $location->save ();
		$ins_id = $location->id;
		return $ins_id;
	}

	public function updateLocation ($data , $id)
	{


		$auth_id = Auth::id ();
		$location = Location::find ($id);
		$location->is_active = $data['is_active'];
		$location->updated_by = $auth_id;
		$location->distance = $data['distance'];
		$location->longitude = $data['longitude'];
		$location->latitude = $data['latitude'];
		$location->location = $data['location'];
		return $status = $location->save ();

	}
}
