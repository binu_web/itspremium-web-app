<?php

namespace App\Repositories;

use App\Models\Country;
use InfyOm\Generator\Common\BaseRepository;

class CountryRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'name' ,
		'iso2' ,
		'iso3' ,
		'phone_code' ,
		'region' ,
		'latlng' ,
		'currency' ,
		'country_code'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return Country::class;
	}

	/**
	 * Add country
	 *
	 * @param $data
	 *
	 * @author Midhun
	 *
	 */

	public function add_country ($data)
	{

		$name = $data['name'];
		$iso2 = $data['iso2'];
		$iso3 = $data['iso3'];
		$phone_code = $data['phone_code'];
		$region = $data['region'];
		$latitude = $data['latitude'];
		$longitude = $data['longitude'];
		$currency = $data['currency'];
		$country_code = $data['country_code'];
		$nicename = $data['nicename'];

		$country = new Country();
		$country->name = $name;
		$country->iso2 = $iso2;
		$country->iso3 = $iso3;
		$country->phone_code = $phone_code;
		$country->region = $region;
		$country->latitude = $latitude;
		$country->longitude = $longitude;
		$country->currency = $currency;
		$country->country_code = $country_code;
		$country->nicename = $nicename;
		$country->is_active = $data['is_active'];
		$country->created_at = date ('Y-m-d H:i:s');
		return $status = $country->save ();
	}

	/**
	 * Update country
	 *
	 * @param $data
	 * @param $id
	 *
	 * @author Midhun
	 * @return mixed
	 */

	public function update_country ($data , $id)
	{

		$name = $data['name'];
		$iso2 = $data['iso2'];
		$iso3 = $data['iso3'];
		$phone_code = $data['phone_code'];
		$region = $data['region'];
		$latitude = $data['latitude'];
		$longitude = $data['longitude'];
		$currency = $data['currency'];
		$country_code = $data['country_code'];

		$country = Country::find ($id);
		$country->name = $name;
		$country->iso2 = $iso2;
		$country->iso3 = $iso3;
		$country->phone_code = $phone_code;
		$country->region = $region;
		$country->latitude = $latitude;
		$country->longitude = $longitude;
		$country->currency = $currency;
		$country->country_code = $country_code;
		$country->is_active = $data['is_active'];
		$country->updated_at = date ('Y-m-d H:i:s');
		return $status = $country->save ();

	}
}
