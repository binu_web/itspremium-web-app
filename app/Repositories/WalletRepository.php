<?php

namespace App\Repositories;

use App\Models\Wallet;
use InfyOm\Generator\Common\BaseRepository;

class WalletRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'balance' ,
        'deleted_at' ,
        'created_at' ,
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model ()
    {
        return Wallet::class;
    }
}
