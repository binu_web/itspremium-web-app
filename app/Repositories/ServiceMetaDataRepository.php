<?php

namespace App\Repositories;

use App\Models\ServiceMetaData;
use InfyOm\Generator\Common\BaseRepository;

class ServiceMetaDataRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'meta_key_id' ,
		'booked_id' ,
		'value'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return ServiceMetaData::class;
	}
}
