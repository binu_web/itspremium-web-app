<?php

namespace App\Repositories;

use App\Models\Device;
use InfyOm\Generator\Common\BaseRepository;

class DeviceRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'type' ,
		'device_id' ,
		'user_id' ,
		'regId' ,
		'device_name' ,
		'device_token' ,
		'builder_version' ,
		'os_version' ,
		'screen_width' ,
		'screen_height' ,
		'ip_address' ,
		'fcm_token'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return Device::class;
	}


	// Defining Model For Device Models
	public function __construct ()
	{
		$this->device = new Device();
	}


	/**
	 * Get All Devices
	 *
	 * @param null $type
	 *
	 * @author Binoop V
	 * @return mixed
	 */
	public function getDeviceFCMTokens ($type = '')
	{

		if (!empty($type))
			$devices = $this->device->byType ($type)->pluck ('fcm_token');
		else
			$devices = $this->device->pluck ('fcm_token');

		return $devices->toArray ();
	}
}
