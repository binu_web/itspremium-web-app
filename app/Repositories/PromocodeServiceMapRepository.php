<?php

namespace App\Repositories;

use App\Models\PromocodeServiceMap;
use InfyOm\Generator\Common\BaseRepository;

class PromoCodeServiceMapRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'promocode_id' ,
        'service_api_id' ,
        'deleted_at' ,
        'created_by' ,
        'updated_by'
    ];

    /**
     * Configure the Model
     **/
    public function model ()
    {
        return PromocodeServiceMap::class;
    }
}
