<?php

namespace App\Repositories;

use App\Models\ServiceMetaKey;
use InfyOm\Generator\Common\BaseRepository;

class ServiceMetaKeyRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'service_id' ,
		'meta_key'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return ServiceMetaKey::class;
	}
}
