<?php

namespace App\Repositories;

use App\Models\PaymentMethod;
use App\Models\PaymentMethodCountry;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentMethodRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'gateway_name' ,
		'api_key' ,
		'api_url' ,
		'api_metadata' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return PaymentMethod::class;
	}

	public function create (array $data)
	{

		$id = Auth::id ();
		$api_metadata = array('gateway_name' => $data['gateway_name'] , 'api_secret' => $data['api_secret'] , 'api_key' => $data['api_key'] , 'api_url' => $data['api_url']);
		$payment = new PaymentMethod();
		$payment->is_active = $data['is_active'];
		$payment->api_metadata = serialize ($api_metadata);
		$payment->created_by = $id;
		$payment->updated_by = $id;
		$payment->redirect_url = $data['redirect_url'];
		$payment->api_url = $data['api_url'];
		$payment->api_secret = $data['api_secret'];
		$payment->gateway_name = $data['gateway_name'];
		$payment->api_key = $data['api_key'];

		$s_status = $payment->save ();
		$ins_id = $payment->id;
		return $ins_id;
	}

	/**
	 * Payment update
	 */
	public function update (array $data , $id)
	{

		$api_metadata = array('gateway_name' => $data['gateway_name'] , 'api_secret' => $data['api_secret'] , 'api_key' => $data['api_key'] , 'api_url' => $data['api_url']);
		$payment = PaymentMethod::find ($id);
		$payment->is_active = $data['is_active'];
		$payment->api_metadata = serialize ($api_metadata);
		$payment->updated_by = $data['updated_by'];
		$payment->redirect_url = $data['redirect_url'];
		$payment->api_url = $data['api_url'];
		$payment->api_secret = $data['api_secret'];
		$payment->gateway_name = $data['gateway_name'];
		$payment->api_key = $data['api_key'];
		$s_status = $payment->save ();
		$ins_id = $payment->id;
		return $ins_id;
	}

	/**
	 * @param $id
	 */

	public function delete ($id)
	{
		DB::table ('payment_methods')->where ('id' , '=' , $id)->delete ();
	}


}
