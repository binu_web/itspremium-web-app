<?php

namespace App\Repositories;

use App\Models\AdminLogType;
use InfyOm\Generator\Common\BaseRepository;

class AdminLogTypeRepository extends BaseRepository
{
	/**
	 * @var array
	 */
	protected $fieldSearchable = [
		'name'
	];

	/**
	 * Configure the Model
	 **/
	public function model ()
	{
		return AdminLogType::class;
	}
}
