<?php

use ServiceTo\ValidateEmail;
use App\Models\Notification;

DEFINE ('URL_VALUE_DELIMITER' , '|');
DEFINE ('SERVICE_UBER' , 'uber');
DEFINE ('SERVICE_OLA' , 'ola');
DEFINE ('SERVICE_DINEOUT' , 'dineout');


/**
 * Define all common helper functions that can be used in the applications here
 *
 * @author : Binoop V
 */


/**
 * Check if the array values are not empty
 *
 * @param array $array      : The array to be checked
 * @param array $exceptions : array keys which are to be excluded from the validation
 *
 * @author Binoop V
 * @return bool
 */

function checkArrayIsEmpty ($array = [] , $exceptions = [])
{
	$isEmpty = false;
	foreach ($array as $key => $value) {

		if (in_array ($key , $exceptions))
			continue;

		if (empty($value) && $value != 0 || is_null ($value))
			$isEmpty = true;
	}

	return $isEmpty;
}

/**
 * Check arary has empty elements
 *
 * @param array $array
 *
 * @return bool
 */
function arrayHasEmptyElement (Array $array)
{
	foreach ($array as $item)
		if (empty($item))
			return true;

	return false;
}

/**
 * Check email format for every email
 *
 * @param $email_id
 *
 * @return bool
 */

function checkEmailFormat ($email_id)
{
	$validateemail = new ValidateEmail;
	if (!filter_var ($email_id , FILTER_VALIDATE_EMAIL))
		return false;
	return true;

//	try {
//		if (!$validateemail->test ($email_id))
//			return false;
//		else
//			return true;
//	}
//	catch (\ServiceTo\ValidateEmailException $vee) {
//		return false;
//	}

}


function validate_mobilenumber ($mobile_number)
{

	if (preg_match ('/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/' , $mobile_number)) {
		return true;
	} else {
		return false;
	}
}

/**
 * Log SQL Events Incase of failures
 *
 * @author Binoop V
 */
function sqlFailureLog ($e)
{
	\Illuminate\Support\Facades\Log::warning ('SQL ERROR : ' . $e->getMessage ());
}


/**
 * Log  Http Events Incase of failures
 *
 * @author Binoop V
 */
function httpFailureLog ($e)
{

	$error = is_object ($e) ? $e->getMessage () : $e;


	// Mail Function
//	if (is_object ($e) && property_exists ($e , 'serviceError')) {
//		\Illuminate\Support\Facades\Mail::raw ($e , function ($message) use ($e) {
//			$message->to (env ('INFO_USER' , 'binu@webandcrafts.com'));
//			$message->from (env ('INFO_USER' , 'binu@webandcrafts.com') , 'Primium');
//			$message->subject ('Service API Error IN ' . $e->serviceError);
//		});
//	}


	\Illuminate\Support\Facades\Log::warning ('GUZZLE HTTP ERROR : ' . $error);

}


/**
 * Log Guzzle Http Events Incase of failures
 *
 * @author Binoop V
 */
function guzzleHttpFailureLog ($e)
{

	\Illuminate\Support\Facades\Log::warning ('GUZZLE HTTP ERROR : ' . $e->getMessage ());
	\Illuminate\Support\Facades\Log::emergency($e);
}


/**
 * Handle Exception Response Status Code
 */
function processHttpFailure (Exception $e)
{
	$code = $e->getCode ();
	// Handle Cases
	switch ($code) {
		case 400: {
			// Resource is not identified
			$message = "invalid request parameter !";
			break;
		}
		case 404: {
			// Resource is not identified
			$message = "Requested url is not found !";
			break;
		}
		case 401 : {
			$message = "You are not authorized to access the requested resource";
			break;
		}
		case 403 : {
			$message = "you are forbidden to accesses the requested resource";
			break;
		}
		default: {
			// Default Case to be handled.
			$message = "Issue related to connecting to network";
			$code = 500;
			break;
		}
	}

	return [$message , $code];
}


/**
 * Properites to be checked if exits for object Class Eg : $data->first_property->second_property->third_property
 *
 *
 * @param string $service
 * @param string $object
 * @param array  $propertiesArray (List property in sequential order) Eg
 *                                ['first_property','second_property','third_property']
 */
function checkResponseObjectProperties ($service = "" , $objectClass = "" , $propertiesArray = [])
{
	$status = true;
	foreach ($propertiesArray as $property) {
		if (property_exists ($objectClass , $property)) {
			$objectClass = $objectClass->$property;
		} else {
			$stackTrace = stackTrace ();
			Log::critical ("API RESPONSE ERROR FOR SERVICE :" . $service . ": No property '" . $property . "' exists. \n" . $stackTrace);
			$status = false;

			// TODO : Create an alert system to detect error
			break;
		}
	}

	return $status;
}


/**
 * Custom Php debug Stack Trace for logging
 *
 */
function stackTrace ()
{
	$debugParam = debug_backtrace ();
	$stackResponse = "Stack Trace : \n";
	$i = 0;
	foreach ($debugParam as $param) {
		if (!isset($param['file']))
			continue;
		$stackResponse .= "#" . $i . "  " . $param['file'] . " (" . $param['line'] . ")" . "\n";
		$i++;
	}

	return $stackResponse;
}

/**
 * Booking key generator for service booked table
 */

function booking_key_generator ()
{
	$key = '';
	$length = 12;
	$keys = array_merge (range (0 , 9) , range ('A' , 'Z'));

	for ($i = 0; $i < $length; $i++) {
		$key .= $keys[array_rand ($keys)];
	}
	return $key;
}

/**
 * @param $algo
 * @param $input
 * @param $secret_key
 *
 * @author Midhun
 *         Genearates the hashkey based on the specified algorithm,
 */
function HMAC_hashkey_generator ($algo , $input , $secret_key)
{

	$output = hash_hmac ($algo , $input , $secret_key);

	return $output;
}

/**
 * @param $sc
 * @param $input
 *
 * @author Midhun
 *         Split the string with given character
 */
function string_split ($delimiter , $input)
{

	$pieces = explode ("$delimiter" , $input);

	return $pieces;
}

/**
 * @param $request
 *
 * @author Midhun
 *         Create input string from array
 */
function input_string_generator ($request)
{
	if (is_array ($request)) {

		$output = implode (',' , array_map (
			function ($v , $k) {
				return '"' . $k . '":"' . $v . '"';
			} ,
			$request ,
			array_keys ($request)
		));

		return '{' . $output . '}';
	}
}

// insert service
function insert_notification ($user_id , $API_SERVICE_ID , $notificaion_type_id , $notification_data)
{
	try {
		$ser_booked = new Notification();
		$ser_booked->user_id = $user_id;
		$ser_booked->service_api_id = $API_SERVICE_ID;
		$ser_booked->notification_type_id = $notificaion_type_id;
		$ser_booked->notification_data = $notification_data;
		$ser_booked->save ();

		return true;

	}
	catch (\Exception $e) {
		sqlFailureLog ($e);
		dd ($e->getMessage ());
		return false;
	}
}


/*
* To format date
* $inputFormat : such as 'd/m/Y', $outputFormat as 'Y-m-d'
* @author Harinarayanan
*/
function formatDate ($inputFormat , $outputFormat , $dateString)
{
	$formattedDate = null;
	if ($dateString) {
		$datetime = \DateTime::createFromFormat ($inputFormat , $dateString);
		if ($datetime)
			$formattedDate = $datetime->format ($outputFormat);
	}
	return $formattedDate;
}

/*
* Create Random Password
* @author Harinarayanan
*/

function createRandomPassword ()
{
	$strLength = 5;
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen ($characters);
	$randomString = '';
	for ($i = 0; $i < $strLength; $i++) {
		$randomString .= $characters[rand (0 , $charactersLength - 1)];
	}

	return $randomString;
}

/*
* Get day of the date
* @author Harinarayanan
*/
function getDayOfDate ($date)
{
	$day = date ('D' , strtotime ($date));
	return $day;
}

/*
* Comvert String to Time
*
* @author Harinarayanan 
*/
function convertStringToTime ($timeString)
{
	return date ("G:i" , strtotime ($timeString));
}


/*
* returns the array is associated or numeric indexed
* @return true if associative array 
* @return false if sequential array
* @author Harinarayanan
*/
function checkIsAssocArray (array $arr)
{
	if (array() === $arr) return false;
	return array_keys ($arr) !== range (0 , count ($arr) - 1);
}


