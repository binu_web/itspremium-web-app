<?php 
namespace App\APIResponses\Hotels\Yatra;

/**
 * Format Yatra Hotel search API response to Client
 * author : Harinarayanan T
 * Date:  7/5/2017
 *
 */

use App\Models\YatraHotelDetail;
use App\Services\UtilityService;
use App\Services\XmlToJson;

class HotelSearchResult
{
	private $aResult;
	private $aAPIResponse;
	private $aRoomType;
	private $aRatePlan;
	private $aTaxCodes = [19 => 'GST'];
	

	public function __construct($response)
	{
		$this->aAPIResponse = $response;
	}


	public function getResponse()
	{
		/*
			Response output structure:
			hotel_details [
					[
						hotel_name 
						hotel_code
						images [],
						rating
						no_of_reviews
						address_line
						address_line2
						total_rate   (it will get from the first room plan data)
						rooms_left  : based on the any one room type 
						latitude
						longitude  
						amenities : []
						check_in_date
						check_out_date
						no_of_rooms
						no_of_guests
						room_types : [
							[room_name
							room_id
							price 
							image
							max_guest
							available_service
							cancellation_policy
							]
							....
						]
					]
					..

				]
	

	*/
		/*echo 'getting the table data<br>';
		echo time();
		echo '<br>';
	    $hotelCode = "00087912";
	    $oHotel    = YatraHotelDetail::find($hotelCode);
	    echo $oHotel->VendorName; 
	    echo time();
	    die;
	    dd($oHotel);*/
		
		$this->aResult = [];

		/*echo '<pre>'; 
		print_r($this->aAPIResponse); die;*/
		//dd($this->aAPIResponse);
		$correlationId 			= isset($this->aAPIResponse['@CorrelationID']) ? $this->aAPIResponse['@CorrelationID']: null;


		$this->aAPIResponse     = array_get($this->aAPIResponse, 'RoomStays');
		
		//manage the bad format of response:
		if(checkIsAssocArray($this->aAPIResponse['RoomStay'])) {
			$this->aAPIResponse['RoomStay'] = [$this->aAPIResponse['RoomStay']];
		}

		#dd($this->aAPIResponse);
		$aRoomStay 			= $this->aAPIResponse['RoomStay'];

		/*$testXml = ' <RoomRate RoomID="0000010831" RatePlanCode="0000923136">
                            <Rates>
                                <Rate EffectiveDate="2017-07-24T00:00:00.000+05:30">
                                    <Base AmountBeforeTax="12999.0">
                                        <Taxes Amount="5459.72">
                                            <Tax Code="19" Amount="5459.72"/>
                                        </Taxes>
                                    </Base>
                                    <AdditionalGuestAmounts>
                                        <AdditionalGuestAmount RPH="1" AgeQualifyingCode="10">
                                            <Amount AmountBeforeTax="6500.0"/>
                                        </AdditionalGuestAmount>
                                    </AdditionalGuestAmounts>
                                    <TPA_Extensions>
                                        <Rate BaseAdultOccupancy="2" BaseChildOccupancy="0" Bookable="true" RatePlanCode="0000923136" RoomTypeCode="0000010831"/>
                                    </TPA_Extensions>
                                </Rate>
                            </Rates>
                            <Total CurrencyCode="INR"/>
                            <GuestCounts>
                                <GuestCount AgeQualifyingCode="10" Count="2"/>
                                <GuestCount AgeQualifyingCode="8" Count="0"/>
                            </GuestCounts>
                        </RoomRate>';
             
              $aXml = XmlToJson::get($testXml);
              dd($aXml);*/
		//dd($aRoomStay);
		//dd($aRoomStay);
		foreach($aRoomStay as $key => $aVal) {
			
			$aBasicPropertyInfo = $aVal['BasicPropertyInfo'];
			$hotelCode 			= $aBasicPropertyInfo['@HotelCode'];
			$currencyCode 		= $aBasicPropertyInfo['@CurrencyCode'];
			#echo $hotelCode."--";
			$oHotelDetails 		= YatraHotelDetail::find($hotelCode);
		
			$vendorId 					= isset($oHotelDetails->VendorId) ? $oHotelDetails->VendorId : null;
			
			
			$hotelName 					= isset($oHotelDetails->VendorName) ? $oHotelDetails->VendorName : '';
			$hotelClass 				= isset($oHotelDetails->HotelClass) ? $oHotelDetails->HotelClass : '';
			$location 					= isset($oHotelDetails->Location) ? $oHotelDetails->Location 	 : '';
			$city 						= isset($oHotelDetails->City) ? $oHotelDetails->City : '';
			$address1 				    = isset($oHotelDetails->Address1) ? $oHotelDetails->Address1 : '';
			$address2 					= isset($oHotelDetails->Address2) ? $oHotelDetails->Address2 : '';
			$hotelReview 				= isset($oHotelDetails->HotelOverview) ? $oHotelDetails->HotelOverview : '';
			$reviewRating 				= isset($oHotelDetails->ReviewRating) ? $oHotelDetails->ReviewRating : '';
			$reviewCount  				= isset($oHotelDetails->ReviewCount) ? $oHotelDetails->ReviewCount : '';
			$latitude 				    = isset($oHotelDetails->Latitude) ? $oHotelDetails->Latitude : '';
			$longitude 				    = isset($oHotelDetails->Longitude) ? $oHotelDetails->Longitude : '';
			$defaultCheckInTime 		= isset($oHotelDetails->DefaultCheckInTime) ? $oHotelDetails->DefaultCheckInTime : '';
			$defaultCheckOutTime 		= isset($oHotelDetails->DefaultCheckOutTime) ? $oHotelDetails->DefaultCheckOutTime : '';
			$hotelStar 				    = isset($oHotelDetails->Hotel_Star) ? $oHotelDetails->Hotel_Star : '';
			$hotelGroupName 			= isset($oHotelDetails->HotelGroupName) ? $oHotelDetails->HotelGroupName : '';
			$imagePath 				    = isset($oHotelDetails->ImagePath) ? $oHotelDetails->ImagePath : '';
			$noOfFloors 				= isset($oHotelDetails->NoOfFloors) ? $oHotelDetails->NoOfFloors : '';
			$noOfRooms 				    = isset($oHotelDetails->NoOfRooms) ? $oHotelDetails->NoOfRooms : '';
			$state 				        = isset($oHotelDetails->State) ? $oHotelDetails->State : '';
			$pincode 				    = isset($oHotelDetails->PinCode) ? $oHotelDetails->PinCode : '';

			$aHotelDet 					= [];
			$this->aRoomType 			= [];
			$this->aRatePlan 			= [];




			if($vendorId) {
				//start preparing hotel data:
				
				$aHotelDet['hotel_name']  = $hotelName;
				$aHotelDet['hotel_code']  = (string)$vendorId;
				$aHotelDet['images'] 	  = [$imagePath];
				$aHotelDet['rating'] 	  = $reviewRating;
				$aHotelDet['no_of_reviews'] = $reviewCount;
				$aHotelDet['address_line']  = $address1;
				$aHotelDet['location']      = $location;
				$aHotelDet['city']			= $city;
				$aHotelDet['latitude']	    = $latitude;
				$aHotelDet['longitude']		= $longitude;
				$aHotelDet['default_check_in_time']  = formatDate('H:i:s', 'h:i A', $defaultCheckInTime);
				$aHotelDet['default_check_out_time'] = formatDate('H:i:s', 'h:i A', $defaultCheckOutTime);
				$aHotelDet['star_rating']            = $hotelStar;
				$aHotelDet['group_name']			 = $hotelGroupName;
				$aHotelDet['no_of_floors']			 = $noOfFloors;
				$aHotelDet['no_of_rooms']			 = $noOfRooms;
				$aHotelDet['state']					 = $state;
				$aHotelDet['pincode']				 = $pincode;
				$aHotelDet['correlation_id']  		 = $correlationId;
 					
				
				$aAmenities 						 = array_get($aVal, 'TPA_Extensions.HotelBasicInformation.Amenities.PropertyAmenities');
				$aAmenities 						 = $aAmenities ? $aAmenities : [];
				
				$aHotelDet['amenities']				 = $aAmenities;



				$aRoomTypes 						 = array_get($aVal, 'RoomTypes.RoomType');
				$aRatePlans 						 = array_get($aVal, 'RatePlans.RatePlan');
				$aRoomRates 						 = array_get($aVal, 'RoomRates.RoomRate');

				$aRooms 				= [];
				$aRoomTypes 			= $aRoomTypes ? $aRoomTypes : [];
				$aRatePlans 			= $aRatePlans ? $aRatePlans : [];
				$aRoomRates 			= $aRoomRates ? $aRoomRates : [];

				if($aRoomTypes && checkIsAssocArray($aRoomTypes)){
					$aRoomTypes 		= [$aRoomTypes];
				}
				if($aRatePlans && checkIsAssocArray($aRatePlans)) {
					$aRatePlans 		= [$aRatePlans];
				}
				if($aRoomRates && checkIsAssocArray($aRoomRates)) {
					$aRoomRates 		= [$aRoomRates];
				}
				$this->aRoomType 		= $aRoomTypes;
				$this->aRatePlan 		= $aRatePlans;

				#dd($aRoomRates);

				foreach($aRoomRates as $key => $aRoomRate) {

					$ratePlanCode 	  = $aRoomRate['@RatePlanCode'];
					$roomTypeCode 	  = $aRoomRate['@RoomID'];
					$uniqueId 		  = $roomTypeCode."#".$ratePlanCode;

					$_aRoomType 	  = $this->getRoomType($roomTypeCode);
					$_aRatePlan 	  = $this->getRatePlan($ratePlanCode);

					/// Room  rate and tax
					$_aRoomRate 				= array_get($aRoomRate, 'Rates.Rate');
					$_aPriceDetails 			= isset($_aRoomRate['Base']) ? $_aRoomRate['Base'] : [];
					$_curAmountBeforeTax 		= isset($_aPriceDetails['@AmountBeforeTax']) ? $_aPriceDetails['@AmountBeforeTax'] : 0;
					$_aTax 						= isset($_aPriceDetails['Taxes']) ? $_aPriceDetails['Taxes'] : [];
					$_curTaxAmount 				= isset($_aTax['@Amount']) ? $_aTax['@Amount'] : 0;

					
					$_aAddtionalGuestAmount 	= array_get($_aRoomRate, 'AdditionalGuestAmounts.AdditionalGuestAmount');
					$_aAddtionalGuestAmount 	= $_aAddtionalGuestAmount ? $_aAddtionalGuestAmount : [];

					//maximum allowed guest details:

					$aMaxAllowedGuest 		    = array_get($_aRoomType, 'TPA_Extensions.RoomType');
					$aMaxAllowedGuest 			= $aMaxAllowedGuest ? $aMaxAllowedGuest : [];

					$_maxAdultCount 			= isset($aMaxAllowedGuest['@maxAdult']) ? $aMaxAllowedGuest['@maxAdult'] : null;
					$_maxChildCount 			= isset($aMaxAllowedGuest['@maxChild']) ? $aMaxAllowedGuest['@maxChild'] : null;
					$_maxGuestCount 			= isset($aMaxAllowedGuest['@maxGuest']) ? $aMaxAllowedGuest['@maxGuest'] : null;
					$_minChildAge 				= isset($aMaxAllowedGuest['@minChildAge']) ? $aMaxAllowedGuest['@minChildAge'] : null;

					

                    
					$_aDiscount 				= array_get($_aRoomRate, 'Discount');
					$_aDiscount 				= $_aDiscount ? $_aDiscount : [];
					#dd($aXml);
	

					$_baseDiscountAmount 			= 0;
					$_AddionalGuestDiscAmount 		= 0;
					if($_aDiscount && checkIsAssocArray($_aDiscount)){
						$_aDiscount = [$_aDiscount];
					} 

					foreach($_aDiscount as $dkey => $aDisc) {
						$_discType 					= isset($aDisc['@AppliesTo']) ? $aDisc['@AppliesTo'] : null;
						$_discAmountBeforeTax 		= isset($aDisc['@AmountBeforeTax']) ? $aDisc['@AmountBeforeTax'] : null;
						if($_discType == 'Base')
							$_baseDiscountAmount = $_baseDiscountAmount + $_discAmountBeforeTax;
						else if($_discType == 'AdditionalGuestAmount')
							$_AddionalGuestDiscAmount = $_AddionalGuestDiscAmount + $_discAmountBeforeTax;
					}
					
					$_totalYatraDiscount = $_baseDiscountAmount + $_AddionalGuestDiscAmount;


					$_additionalGuestAmount 	= 0;
					if($_aAddtionalGuestAmount && checkIsAssocArray($_aAddtionalGuestAmount)) {
						$_aAddtionalGuestAmount = [$_aAddtionalGuestAmount];
					}
					#echo 'gggggg';
					#dd($_aAddtionalGuestAmount);
					if($_aAddtionalGuestAmount) {
						foreach($_aAddtionalGuestAmount as $key => $aVal) {
							$_aAdditionalAmount 		= isset($aVal['Amount']) ? $aVal['Amount'] : [];
							$_adAmountBeforeTax 		= isset($_aAdditionalAmount['@AmountBeforeTax']) ? $_aAdditionalAmount['@AmountBeforeTax'] : 0;
							$_additionalGuestAmount 	= $_additionalGuestAmount + $_adAmountBeforeTax;
						}
					}
					#echo $_additionalGuestAmount;die;
					///
					
					if(!isset($aRooms[$uniqueId])) {
						$_aRoomBasicInfo = [];
						$_aRoomBasicInfo['room_type_code'] 	= $roomTypeCode;
						$_aRoomBasicInfo['rate_plan_code'] 	= $ratePlanCode;
						$_aRoomBasicInfo['room_name']		= $_aRatePlan['@RatePlanName'];
						$_aRoomBasicInfo['available_quantity']  = $_aRatePlan['@AvailableQuantity'];
						$aRoomImages					        = $_aRoomType['RoomDescription']['Image'];
						$_aRoomBasicInfo['room_images'] 	    = $aRoomImages;
						$_cancellationPolicy 				    = isset($_aRatePlan['CancelPenalties']['CancelPenalty']['PenaltyDescription']['Text'])  ? $_aRatePlan['CancelPenalties']['CancelPenalty']['PenaltyDescription']['Text'] : [];
						$_aRoomBasicInfo['cancellation_policy'] = $_cancellationPolicy;
						$aIncludedServices 						= array_get($_aRatePlan, 'RatePlanInclusions.RatePlanInclusionDesciption.Text');
						$aIncludedServices 					    = $aIncludedServices ? $aIncludedServices : [];
						$_aRoomBasicInfo['inclusions']			= $aIncludedServices;
						$_aOccupancy 						    = $_aRoomType['Occupancy'];
						/*foreach($_aOccupancy  as $key => $aOcc) {
							$_maxOccupancy = $aOcc['@MaxOccupancy'] ? $aOcc['@MaxOccupancy'] : 0;
							$_ageCode 	   = $aOcc['@AgeQualifyingCode'];
							$_guestText    = 'Adults';
							if($_ageCode == 8)
								$_guestText = 'Children';


							$_aRoomBasicInfo['max_guests'][] = ["text" => $_guestText, "max_count" => $_maxOccupancy];
						}
*/
						$_aRoomBasicInfo['hotel_total_amount']			= $_curAmountBeforeTax + $_additionalGuestAmount;
						$_aRoomBasicInfo['hotel_total_discount']  		= $_baseDiscountAmount + $_AddionalGuestDiscAmount;
						$_aRoomBasicInfo['net_yatra_amount_before_tax'] = $_curAmountBeforeTax + $_additionalGuestAmount - ($_baseDiscountAmount + $_AddionalGuestDiscAmount);
						$_aRoomBasicInfo['base_room_rate_before_tax'] 	= $_curAmountBeforeTax;
						$_aRoomBasicInfo['additional_guest_amount_before_tax'] = $_additionalGuestAmount;
						$_aRoomBasicInfo['room_tax_amount'] 	 		= $_curTaxAmount;
						$_taxCode 								 = isset($_aTax['Tax']['@Code']) ? $_aTax['Tax']['@Code'] : null;
						$_aRoomBasicInfo['tax_name']			 = isset($this->aTaxCodes[$_taxCode]) ? $this->aTaxCodes[$_taxCode] : '';
						
						$_aRoomBasicInfo['max_adult_count'] 		= $_maxAdultCount ;
						$_aRoomBasicInfo['max_child_count'] 		= $_maxChildCount;
						$_aRoomBasicInfo['max_guest_count'] 		= $_maxGuestCount;
						$_aRoomBasicInfo['min_child_age'] 			= $_minChildAge;
						$aRooms[$uniqueId] 						    = $_aRoomBasicInfo;
					}
					else {
						//price and tax management
						$aRooms[$uniqueId]['hotel_total_amount']			  = $aRooms[$uniqueId]['hotel_total_amount'] + $_curAmountBeforeTax + $_additionalGuestAmount;
						$aRooms[$uniqueId]['hotel_total_discount']  		  = $aRooms[$uniqueId]['hotel_total_discount'] + $_baseDiscountAmount + $_AddionalGuestDiscAmount;
						$aRooms[$uniqueId]['net_yatra_amount_before_tax'] 	  = $aRooms[$uniqueId]['net_yatra_amount_before_tax'] + ($_curAmountBeforeTax + $_additionalGuestAmount - ($_baseDiscountAmount + $_AddionalGuestDiscAmount));
						$aRooms[$uniqueId]['base_room_rate_before_tax']   			= $aRooms[$uniqueId]['base_room_rate_before_tax'] + $_curAmountBeforeTax;
						$aRooms[$uniqueId]['additional_guest_amount_before_tax'] 	= $aRooms[$uniqueId]['additional_guest_amount_before_tax'] + $_additionalGuestAmount;
						$aRooms[$uniqueId]['room_tax_amount']             			= round($aRooms[$uniqueId]['room_tax_amount'] + $_curTaxAmount, 2);

						
					}

					//$aRooms[$uniqueId]['tax_details'][]			 = $_aTax;


				}


				$aRooms 					= array_values($aRooms);
				$_defaultRoomDet 			= [];
				$_defaultRoomRate 			= '';
				$_defaultAvailablRooms 	    = '';
				if($aRooms) {
					$_defaultRoomRate	 	= $aRooms[0]['net_yatra_amount_before_tax'];
					$_defaultAvailablRooms 	= $aRooms[0]['available_quantity'];
				}

				$_defaultRoomDet['room_rate'] 				= $_defaultRoomRate;
				$_defaultRoomDet['no_of_rooms_available']   = $_defaultAvailablRooms;
				$aHotelDet['default_room_details']  		= $_defaultRoomDet;
				$aHotelDet['rooms']							= $aRooms;


				#dd($aHotelDet['rooms']);
				$this->aResult[] 			= $aHotelDet;
				//dd($this->aResult);
	 			#die('ffff');
			}

			
		 	

		}

		return $this->aResult;


	}

	public function getRoomType($roomTypeCode)
	{
		$aReturn = [];
		
		foreach($this->aRoomType as $key => $aVal) {
			if($aVal['@RoomTypeCode'] == $roomTypeCode) {
				$aReturn = $aVal;
				break;
			}
		}

		return $aReturn;
	}

	public function getRatePlan($ratePlanCode)
	{
		$aReturn = [];
		foreach($this->aRatePlan as $key => $aVal) {
			if($aVal['@RatePlanCode'] == $ratePlanCode) {
				$aReturn = $aVal;
				break;
			}
		}

		return $aReturn;
	}


	
}
