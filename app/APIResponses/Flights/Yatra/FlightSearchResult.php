<?php 
namespace App\APIResponses\Flights\Yatra;

/**
 * Format Yatra Flight search API response to Client
 * author : Harinarayanan T
 * Date:  7/5/2017
 *
 */
use Exception;

class FlightSearchResult
{

	/*
	*
	*
	*/
	public function formatSearchResult($aResponse)
	{
		$aData = array_get($aResponse, 'data.resultData.0');
		$aData = $aData ? $aData : [];
		#echo json_encode($aData); die;
		$aAirLines = array_get($aData, 'fltSchedule.airlineNames');
		$searchId  = isset($aData['fltSchedule']['scid']) ? $aData['fltSchedule']['scid'] : null;
		
		if(!$searchId) 
			throw new Exception ('Search Id not exists', 404);

		#dd($aData['fltSchedule']['scid']);
		$aFlightSchedule = array_get($aData, 'fltSchedule');
		$aFlightSchedule = $aFlightSchedule ? $aFlightSchedule : [];
		dd($aFlightSchedule);
		foreach($aFlightSchedule as $key => $aFlightData) {
			$uniqueId = $key;
			dd($aFlightData[0]);
		}

		/*echo 'air lines';
		die('out');*/
		dd($aAirLines);

	}
}