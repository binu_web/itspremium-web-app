<?php

namespace App\DataTables;

use App\Models\Admin;
use Form;
use Yajra\Datatables\Services\DataTable;

class AdminDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		return $this->datatables
			->eloquent ($this->query ())
			->editColumn ('is_active' , function ($data) {

				return "<span class='label label-" . (($data->is_active) ? 'success' : 'danger') . "''>" . (($data->is_active) ? 'Yes' : 'No') . "</span>";

			})
			->addColumn ('action' , 'admins.datatables_actions')
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$admins = Admin::query ();

		return $this->applyScopes ($admins);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [

//            'password' => ['name' => 'password', 'data' => 'password'],
//            'user_type' => ['name' => 'user_type', 'data' => 'user_type'],
			'first_name' => ['name' => 'first_name' , 'data' => 'first_name'] ,
			'last_name' => ['name' => 'last_name' , 'data' => 'last_name'] ,
//            'location' => ['name' => 'location', 'data' => 'location'],
			'email' => ['name' => 'email' , 'data' => 'email'] ,
			'mobile' => ['name' => 'mobile' , 'data' => 'mobile'] ,
//            'job_role' => ['name' => 'job_role', 'data' => 'job_role'],
			'is_active' => ['name' => 'is_active' , 'data' => 'is_active'] ,
//            'activation_code' => ['name' => 'activation_code', 'data' => 'activation_code'],
//            'remember_token' => ['name' => 'remember_token', 'data' => 'remember_token'],
//            'created_by' => ['name' => 'created_by', 'data' => 'created_by'],
//            'updated_by' => ['name' => 'updated_by', 'data' => 'updated_by']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'admins';
	}
}
