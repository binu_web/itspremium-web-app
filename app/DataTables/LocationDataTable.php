<?php

namespace App\DataTables;

use App\Models\Location;
use Form;
use Yajra\Datatables\Services\DataTable;

class LocationDataTable extends DataTable
{


	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{

		return $this->datatables
			->eloquent ($this->query ())
//            ->eloquent($query)
			->addColumn ('action' , 'locations.datatables_actions')
			->editColumn ('is_active' , function ($row) {
				return "<span class='label label-" . (($row->is_active) ? 'success' : 'danger') . "''>" . (($row->is_active) ? 'Yes' : 'No') . "</span>";
			})
			->editColumn ('created_by' , function ($row) {
				$category = [];
				foreach ($row->locationService as $val) {
					$category[$val->serviceApis->serviceCategory->name][] = $val->serviceApis->provider_name;
				}
				$service = '';
				foreach ($category as $key => $value) {
					$service = '<span style="font-weight: bold;text-transform: capitalize">' . $key . '</span>' . ' : ' . implode (', ' , $value);
					$data[] = '<br>' . $service;
				}
				return $data;
			})
			->make (true);
	}


	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$locations = Location::query ();

		return $this->applyScopes ($locations);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
//                    'print',
//                    'reset',
//                    'reload',
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'Location' => ['name' => 'location' , 'data' => 'location'] ,
			'Service Category' => ['name' => 'created_by' , 'data' => 'created_by'] ,
			'Latitude' => ['name' => 'latitude' , 'data' => 'latitude'] ,
			'Longitude' => ['name' => 'longitude' , 'data' => 'longitude'] ,
			'Cover Distance' => ['name' => 'distance' , 'data' => 'distance'] ,
			'Status' => ['name' => 'is_active' , 'data' => 'is_active'] ,
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'locations';
	}
}
