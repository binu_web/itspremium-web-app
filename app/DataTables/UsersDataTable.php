<?php

namespace App\DataTables;

use App\Models\User;
use Form;
use Yajra\Datatables\Services\DataTable;

class UsersDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		$query = $this->query ()->select ('users.*' , 'c.name')
			->leftJoin ('countries as c' , 'c.id' , '=' , 'users.country')
			->where ('users.deleted_at' , '=' , null);

		return $this->datatables
			->eloquent ($query)
//                ->eloquent($this->query())
			->editColumn ('is_active' , function ($data) {

				return "<span class='label label-" . (($data->is_active) ? 'success' : 'danger') . "''>" . (($data->is_active) ? 'Yes' : 'No') . "</span>";

			})
			->editColumn ('country' , function ($row) {
				return $row->name;
			})
			->addColumn ('action' , 'users.datatables_actions')
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$users = User::query ();

		return $this->applyScopes ($users);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{

		return [
			'email' => ['name' => 'email' , 'data' => 'email'] ,
			'mobile' => ['name' => 'mobile' , 'data' => 'mobile'] ,
			'first_name' => ['name' => 'first_name' , 'data' => 'first_name'] ,
			'last_name' => ['name' => 'last_name' , 'data' => 'last_name'] ,
//            'profile_pic' => ['name' => 'profile_pic', 'data' => 'profile_pic'],
			'country' => ['name' => 'country' , 'data' => 'country'] ,
			'is_active' => ['name' => 'is_active' , 'data' => 'is_active'] ,
//            'activation_token' => ['name' => 'activation_token', 'data' => 'activation_token']

		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'users';
	}
}
