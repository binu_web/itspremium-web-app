<?php

namespace App\DataTables;

use App\Models\Feedback;
use Form;
use Yajra\Datatables\Services\DataTable;

class FeedbackDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax ()
    {
        $query = $this->query ()->select ('feedback.*' , 'u.first_name' )
            ->leftJoin ('users as u' , 'u.id' , '=' , 'feedback.user_id')
            ->where ('feedback.deleted_at' , '=' , null)
            ->orderBy('id','DESC');



        return $this->datatables
            ->eloquent ($query)
            ->editColumn ('user_id' , function ($row) {
                return $row->first_name;
            })
            ->addColumn ('action' , 'feedback.datatables_actions')
            ->make (true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query ()
    {
        $feedback = Feedback::query ();

        return $this->applyScopes ($feedback);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html ()
    {
        return $this->builder ()
            ->columns ($this->getColumns ())
            ->addAction (['width' => '10%'])
            ->ajax ('')
            ->parameters ([
                'dom' => 'Bfrtip' ,
                'scrollX' => false ,
                'buttons' => [
                    [
                        'extend' => 'collection' ,
                        'text' => '<i class="fa fa-download"></i> Export' ,
                        'buttons' => [
                            'csv' ,
                            'excel' ,
                            'pdf' ,
                        ] ,
                    ] ,
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns ()
    {
        return [
            'user_id' => ['name' => 'user_id' , 'data' => 'user_id'] ,
            'feedback' => ['name' => 'feedback' , 'data' => 'feedback'] ,

            'created_at' => ['name' => 'created_at' , 'data' => 'created_at'] ,
            'updated_at' => ['name' => 'updated_at' , 'data' => 'updated_at'] ,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename ()
    {
        return 'feedback';
    }
}
