<?php

namespace App\DataTables;

use App\Models\PaymentMethod;
use Form;
use Yajra\Datatables\Services\DataTable;

class PaymentMethodDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		return $this->datatables
			->eloquent ($this->query ())
			->editColumn ('is_active' , function ($data) {

				return "<span class='label label-" . (($data->is_active) ? 'success' : 'danger') . "''>" . (($data->is_active) ? 'Yes' : 'No') . "</span>";

			})
			->addColumn ('action' , 'payment_methods.datatables_actions')
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$paymentMethods = PaymentMethod::query ();

		return $this->applyScopes ($paymentMethods);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
//                    'print',
//                    'reset',
//                    'reload',
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'gateway_name' => ['name' => 'gateway_name' , 'data' => 'gateway_name'] ,
			'api_key' => ['name' => 'api_key' , 'data' => 'api_key'] ,
//            'api_url' => ['name' => 'api_url', 'data' => 'api_url'],
//            'api_metadata' => ['name' => 'api_metadata', 'data' => 'api_metadata'],
			'is_active' => ['name' => 'is_active' , 'data' => 'is_active'] ,
//            'created_by' => ['name' => 'created_by', 'data' => 'created_by'],
//            'updated_by' => ['name' => 'updated_by', 'data' => 'updated_by']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'paymentMethods';
	}
}
