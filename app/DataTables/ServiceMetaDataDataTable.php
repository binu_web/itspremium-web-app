<?php

namespace App\DataTables;

use App\Models\ServiceMetaData;
use Form;
use Yajra\Datatables\Services\DataTable;

class ServiceMetaDataDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		return $this->datatables
			->eloquent ($this->query ())
			->addColumn ('action' , 'service_meta_datas.datatables_actions')
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$serviceMetaDatas = ServiceMetaData::query ();

		return $this->applyScopes ($serviceMetaDatas);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
					'print' ,
					'reset' ,
					'reload' ,
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'meta_key_id' => ['name' => 'meta_key_id' , 'data' => 'meta_key_id'] ,
			'booked_id' => ['name' => 'booked_id' , 'data' => 'booked_id'] ,
			'value' => ['name' => 'value' , 'data' => 'value']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'serviceMetaDatas';
	}
}
