<?php

namespace App\DataTables;

use App\Models\ServiceBooked;
use Form;
use Yajra\Datatables\Services\DataTable;

class HotelRefundTable extends DataTable
{

    const HOTEL_SERVICE_ID = 3;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax ()
    {
        $query = $this->query ()->select ('services_booked.*', 'u.first_name','u.last_name','u.email','s.provider_name')->leftJoin ('users as u' , 'u.id' , '=' , 'services_booked.user_id')->leftJoin ('service_apis as s' , 's.id' , '=' , 'services_booked.service_api_id')
            ->where ('services_booked.deleted_at' , '=' , null)
            ->where ('services_booked.service_id' , '=' , self::HOTEL_SERVICE_ID)
            ->where ('services_booked.status' , '=' , 'CANCELLED')
            ->orderBy('id','DESC');


        return $this->datatables
            ->eloquent ($query)
            ->addColumn ('action' , 'hotel_refund.datatables_actions')
            ->editColumn ('user_id' , function ($row) {
                return $row->first_name.' '.$row->last_name;
            })
            ->editColumn ('service_api_id' , function ($row) {
                return $row->provider_name;
            })
            ->editColumn ('created_by' , function ($row) {
                return $row->first_name;
            })
            ->make (true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query ()
    {
        $promocode = ServiceBooked::query ();

        return $this->applyScopes ($promocode);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html ()
    {
        return $this->builder ()
            ->columns ($this->getColumns ())
            ->addAction (['width' => '10%'])
            ->ajax ('')
            ->parameters ([
                'dom' => 'Bfrtip' ,
                'scrollX' => false ,
                'buttons' => [
                    [
                        'extend' => 'collection' ,
                        'text' => '<i class="fa fa-download"></i> Export' ,
                        'buttons' => [
                            'csv' ,
                            'excel' ,
                            'pdf' ,
                        ] ,
                    ] ,
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns ()
    {
        return [
            'booking_id' => ['name' => 'id' , 'data' => 'id'] ,
            'user_name' => ['name' => 'user_id' , 'data' => 'user_id'] ,
            'user_email' => ['name' => 'email' , 'data' => 'email'] ,

            'booking_key' => ['name' => 'booking_key' , 'data' => 'booking_key'] ,
            'service_api_name' => ['name' => 'service_api_id' , 'data' => 'service_api_id'] ,
            'status' => ['name' => 'status' , 'data' => 'status'] ,
//            'description' => ['name' => 'description' , 'data' => 'description'] ,
            'created_at' => ['name' => 'created_at' , 'data' => 'created_at'] ,
            'updated_at' => ['name' => 'updated_at' , 'data' => 'updated_at'] ,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename ()
    {
        return 'hotel_refund';
    }
}
