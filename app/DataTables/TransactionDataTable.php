<?php

namespace App\DataTables;

use App\Models\Transaction;
use Form;
use Yajra\Datatables\Services\DataTable;

class TransactionDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		return $this->datatables
			->eloquent ($this->query ())
			->addColumn ('action' , 'transactions.datatables_actions')
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$transactions = Transaction::query ();

		return $this->applyScopes ($transactions);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
					'print' ,
					'reset' ,
					'reload' ,
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'booked_id' => ['name' => 'booked_id' , 'data' => 'booked_id'] ,
			'user_id' => ['name' => 'user_id' , 'data' => 'user_id'] ,
			'gateway_id' => ['name' => 'gateway_id' , 'data' => 'gateway_id'] ,
			'amount' => ['name' => 'amount' , 'data' => 'amount'] ,
			'status' => ['name' => 'status' , 'data' => 'status'] ,
			'status_msg' => ['name' => 'status_msg' , 'data' => 'status_msg'] ,
			'refund_status' => ['name' => 'refund_status' , 'data' => 'refund_status'] ,
			'refund_msg' => ['name' => 'refund_msg' , 'data' => 'refund_msg']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'transactions';
	}
}
