<?php

namespace App\DataTables;

use App\Models\ServiceBooked;
use Form;
use Yajra\Datatables\Services\DataTable;

class ServiceBookedDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		$query = $this->query ()->select ('services_booked.*' , 'u.first_name' , 'c.provider_name as api_name','s.name as api_category_name')
			->leftJoin ('users as u' , 'u.id' , '=' , 'services_booked.user_id')
			->leftJoin ('service_apis as c' , 'c.id' , '=' , 'services_booked.service_api_id')
			->leftJoin ('services as s' , 's.id' , '=' , 'services_booked.service_id')
			->where ('services_booked.deleted_at' , '=' , null)
			->orderBy('id','DESC');



		return $this->datatables
			->eloquent ($query)
			->editColumn ('user_id' , function ($row) {
				return $row->first_name;
			})
			->editColumn ('service_api_id' , function ($row) {
				return $row->api_name;
			})
			->editColumn ('service_id' , function ($row) {
				return $row->api_category_name;
			})
			->addColumn ('action' , 'service_bookeds.datatables_actions')
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$serviceBookeds = ServiceBooked::query ();

		return $this->applyScopes ($serviceBookeds);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'booking_key' => ['name' => 'booking_key' , 'data' => 'booking_key'],
			'user_id' => ['name' => 'user_id' , 'data' => 'user_id'] ,
			'service_id' => ['name' => 'service_id' , 'data' => 'service_id'] ,
			'service_api_id' => ['name' => 'service_api_id' , 'data' => 'service_api_id'] ,
			'status' => ['name' => 'status' , 'data' => 'status'] ,
			'created_at' => ['name' => 'created_at' , 'data' => 'created_at'] ,
			'updated_at' => ['name' => 'updated_at' , 'data' => 'updated_at'] ,
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'services_booked';
	}
}
