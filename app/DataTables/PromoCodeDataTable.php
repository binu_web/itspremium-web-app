<?php

namespace App\DataTables;

use App\Models\PromoCode;
use Form;
use Yajra\Datatables\Services\DataTable;

class PromoCodeDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax ()
    {
        $query = $this->query ()->select ('promo_code.*', 'u.first_name')->leftJoin ('admin_users as u' , 'u.id' , '=' , 'promo_code.created_by')
            ->where ('promo_code.deleted_at' , '=' , null)
            ->orderBy('id','DESC');

        return $this->datatables
            ->eloquent ($query)
            ->addColumn ('action' , 'promocode.datatables_actions')
            ->editColumn ('is_active' , function ($row) {
                return "<span class='label label-" . (($row->is_active) ? 'success' : 'danger') . "''>" . (($row->is_active) ? 'Yes' : 'No') . "</span>";
            })
            ->editColumn ('created_by' , function ($row) {
                return $row->first_name;
            })
            ->make (true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query ()
    {
        $promocode = PromoCode::query ();

        return $this->applyScopes ($promocode);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html ()
    {
        return $this->builder ()
            ->columns ($this->getColumns ())
            ->addAction (['width' => '10%'])
            ->ajax ('')
            ->parameters ([
                'dom' => 'Bfrtip' ,
                'scrollX' => false ,
                'buttons' => [
                    [
                        'extend' => 'collection' ,
                        'text' => '<i class="fa fa-download"></i> Export' ,
                        'buttons' => [
                            'csv' ,
                            'excel' ,
                            'pdf' ,
                        ] ,
                    ] ,
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns ()
    {
        return [
            'promo_code' => ['name' => 'promo_code' , 'data' => 'promo_code'] ,
            'discount' => ['name' => 'discount' , 'data' => 'discount'] ,
            'discount_type' => ['name' => 'discount_type' , 'data' => 'discount_type'] ,
            'valid_from' => ['name' => 'valid_from' , 'data' => 'valid_from'] ,
            'valid_to' => ['name' => 'valid_to' , 'data' => 'valid_to'] ,
            'no_of_maximum_use' => ['name' => 'no_of_maximum_use' , 'data' => 'no_of_maximum_use'] ,
            'description' => ['name' => 'description' , 'data' => 'description'] ,
            'created_at' => ['name' => 'created_at' , 'data' => 'created_at'] ,
            'updated_at' => ['name' => 'updated_at' , 'data' => 'updated_at'] ,
            'validation_rules' => ['name' => 'validation_rules', 'data'=> 'validation_rules']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename ()
    {
        return 'promocode';
    }
}
