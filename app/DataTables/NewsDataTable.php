<?php

namespace App\DataTables;

use App\Models\News;
use Form;
use Yajra\Datatables\Services\DataTable;

class NewsDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		$query = $this->query ()->select ('news.*' , 'u.first_name' , 'c.name as category_name')
			->leftJoin ('admin_users as u' , 'u.id' , '=' , 'news.created_by')
			->leftJoin ('news_categories as c' , 'c.id' , '=' , 'news.category');

		return $this->datatables
			->eloquent ($query)
			->editColumn ('image' , function ($row) {
				if ($row->image != "") {
					if (strpos ($row->image , "http") !== FALSE)
						$src = $row->image;
					else
						$src = asset ('resources/assets/uploads/images/') . '/' . $row->image;
					return '<a href="#" onClick="test(' . $row->id . ');"><img class="news-image-popup" width="100" src="' . $src . '" /></a>';
				} else
					return "-";
			})
			->editColumn ('is_active' , function ($row) {
				if ($row->is_active)
					return "active";
				else
					return "inactive";
			})
			->editColumn ('created_by' , function ($row) {
				return $row->first_name;
			})
			->editColumn ('is_trending' , function ($row) {
				if ($row->is_trending)
					return "yes";
				else
					return "no";
			})
			->editColumn ('category' , function ($row) {
				return $row->category_name;
			})
			->addColumn ('action' , 'news.datatables_actions')
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$news = News::query ();

		return $this->applyScopes ($news);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'title' => ['name' => 'title' , 'data' => 'title'] ,
			'category' => ['name' => 'category' , 'data' => 'category'] ,
			'image' => ['name' => 'image' , 'data' => 'image'] ,
			'publish_date' => ['name' => 'publish_date' , 'data' => 'publish_date'] ,
			'is_trending' => ['name' => 'is_trending' , 'data' => 'is_trending'] ,
			'is_active' => ['name' => 'is_active' , 'data' => 'is_active'] ,
			'created_by' => ['name' => 'created_by' , 'data' => 'created_by'] ,
			'created_at' => ['name' => 'created_at' , 'data' => 'created_at'] ,
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'news';
	}
}
