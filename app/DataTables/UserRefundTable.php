<?php

namespace App\DataTables;

use App\Models\Wallet;
use Form;
use Yajra\Datatables\Services\DataTable;

class UserRefundTable extends DataTable
{

    const HOTEL_SERVICE_ID = 3;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax ()
    {
        $query = $this->query ()->select ('wallet.*', 'u.id as userId','u.email as userEmail','u.first_name as userFirstname','u.last_name as userLastname','u.mobile as userMobile')
            ->leftJoin ('users as u' , 'u.id' , '=' , 'wallet.user_id')
            ->where ('wallet.deleted_at' , '=' , null)
            ->where ('u.deleted_at' , '=' , null);
//            ->groupBy('users.id');


        return $this->datatables
            ->eloquent ($query)
            ->addColumn ('action' , 'user_refund.datatables_actions')
            ->editColumn ('user_name' , function ($row) {
                return $row->userFirstname.' '.$row->userLastname;
            })
//            ->editColumn ('service_api_id' , function ($row) {
//                return $row->provider_name;
//            })
//            ->editColumn ('created_by' , function ($row) {
//                return $row->first_name;
//            })
            ->make (true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query ()
    {
        $promocode = Wallet::query ();

        return $this->applyScopes ($promocode);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html ()
    {
        return $this->builder ()
            ->columns ($this->getColumns ())
            ->addAction (['width' => '10%'])
            ->ajax ('')
            ->parameters ([
                'dom' => 'Bfrtip' ,
                'scrollX' => false ,
                'buttons' => [
                    [
                        'extend' => 'collection' ,
                        'text' => '<i class="fa fa-download"></i> Export' ,
                        'buttons' => [
                            'csv' ,
                            'excel' ,
                            'pdf' ,
                        ] ,
                    ] ,
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns ()
    {
        return [
            'user_id' => ['name' => 'userId' , 'data' => 'userId'] ,
            'wallet_id' => ['name' => 'id' , 'data' => 'id'] ,
            'user_name' => ['name' => 'userFirstname' , 'data' => 'userFirstname'] ,
            'user_email' => ['name' => 'userEmail' , 'data' => 'userEmail'] ,
            'user_phone_no' => ['name' => 'userMobile' , 'data' => 'userMobile'] ,
            'wallet_balance' => ['name' => 'balance' , 'data' => 'balance'] ,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename ()
    {
        return 'user_refund';
    }
}
