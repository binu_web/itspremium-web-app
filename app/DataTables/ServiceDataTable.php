<?php

namespace App\DataTables;

use App\Models\Service;
use Form;
use Yajra\Datatables\Services\DataTable;

class ServiceDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		$query = $this->query ()->select ('services.*' , 'u.first_name')->leftJoin ('admin_users as u' , 'u.id' , '=' , 'services.created_by');

		return $this->datatables
			->eloquent ($query)
			->addColumn ('action' , 'services.datatables_actions')
			->editColumn ('image' , function ($row) {
				if ($row->image != "")
					return '<img width="100" src="' . asset ('resources/assets/uploads/images/') . '/' . $row->image . '" />';
				else
					return "-";
			})
			->editColumn ('is_active' , function ($row) {

				return "<span class='label label-" . (($row->is_active) ? 'success' : 'danger') . "''>" . (($row->is_active) ? 'Yes' : 'No') . "</span>";

			})
			->editColumn ('created_by' , function ($row) {
				return $row->first_name;
			})
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$services = Service::query ();

		return $this->applyScopes ($services);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [

					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'name' => ['name' => 'name' , 'data' => 'name'] ,
			'image' => ['name' => 'image' , 'data' => 'image'] ,
			'is_active' => ['name' => 'is_active' , 'data' => 'is_active'] ,
			'created_by' => ['name' => 'created_by' , 'data' => 'created_by'] ,
			'created At' => ['name' => 'created_at' , 'data' => 'created_at'] ,
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'services';
	}
}
