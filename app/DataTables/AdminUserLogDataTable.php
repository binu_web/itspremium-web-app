<?php

namespace App\DataTables;

use App\Models\AdminUserLog;
use Form;
use Yajra\Datatables\Services\DataTable;

class AdminUserLogDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		return $this->datatables
			->eloquent ($this->query ())
			->addColumn ('action' , 'admin_user_logs.datatables_actions')
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$adminUserLogs = AdminUserLog::query ();

		return $this->applyScopes ($adminUserLogs);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
					'print' ,
					'reset' ,
					'reload' ,
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'log_type' => ['name' => 'log_type' , 'data' => 'log_type'] ,
			'user_id' => ['name' => 'user_id' , 'data' => 'user_id'] ,
			'action' => ['name' => 'action' , 'data' => 'action'] ,
			'ip' => ['name' => 'ip' , 'data' => 'ip'] ,
			'latlong' => ['name' => 'latlong' , 'data' => 'latlong']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'adminUserLogs';
	}
}
