<?php

namespace App\DataTables;

use App\Models\AdminLogType;
use Form;
use Yajra\Datatables\Services\DataTable;

class AdminLogTypeDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		return $this->datatables
			->eloquent ($this->query ())
			->addColumn ('action' , 'admin_log_types.datatables_actions')
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$adminLogTypes = AdminLogType::query ();

		return $this->applyScopes ($adminLogTypes);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
					'print' ,
					'reset' ,
					'reload' ,
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'name' => ['name' => 'name' , 'data' => 'name']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'adminLogTypes';
	}
}
