<?php

namespace App\DataTables;

use App\Models\Device;
use Form;
use Yajra\Datatables\Services\DataTable;

class DeviceDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'devices.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $devices = Device::query();

        return $this->applyScopes($devices);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'type' => ['name' => 'type', 'data' => 'type'],
            'device_id' => ['name' => 'device_id', 'data' => 'device_id'],
            'user_id' => ['name' => 'user_id', 'data' => 'user_id'],
            'regId' => ['name' => 'regId', 'data' => 'regId'],
            'device_name' => ['name' => 'device_name', 'data' => 'device_name'],
            'device_token' => ['name' => 'device_token', 'data' => 'device_token'],
            'builder_version' => ['name' => 'builder_version', 'data' => 'builder_version'],
            'os_version' => ['name' => 'os_version', 'data' => 'os_version'],
            'screen_width' => ['name' => 'screen_width', 'data' => 'screen_width'],
            'screen_height' => ['name' => 'screen_height', 'data' => 'screen_height'],
            'ip_address' => ['name' => 'ip_address', 'data' => 'ip_address'],
            'fcm_token' => ['name' => 'fcm_token', 'data' => 'fcm_token']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'devices';
    }
}
