<?php

namespace App\DataTables;

use App\Models\PaymentMethodCountry;
use Form;
use Yajra\Datatables\Services\DataTable;

class PaymentMethodCountryDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		return $this->datatables
			->eloquent ($this->query ())
			->addColumn ('action' , 'payment_method_countries.datatables_actions')
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$paymentMethodCountries = PaymentMethodCountry::query ();

		return $this->applyScopes ($paymentMethodCountries);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
//                    'print',
//                    'reset',
//                    'reload',
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'gateway_id' => ['name' => 'gateway_id' , 'data' => 'gateway_id'] ,
//            'country_id' => ['name' => 'country_id', 'data' => 'country_id'],
			'created_by' => ['name' => 'created_by' , 'data' => 'created_by'] ,
			'updated_by' => ['name' => 'updated_by' , 'data' => 'updated_by']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'paymentMethodCountries';
	}
}
