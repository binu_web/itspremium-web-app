<?php

namespace App\DataTables;

use App\Models\PromocodeServiceMap;
use Form;
use Yajra\Datatables\Services\DataTable;

class PromocodeServiceMapDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax ()
    {
        $query = $this->query ()->select ('promocode_service_map.*', 'u.promo_code as promo_code','s.provider_name as service_name')
            ->leftJoin ('promo_code as u' , 'u.id' , '=' , 'promocode_service_map.promocode_id')
            ->leftJoin ('service_apis as s' , 's.id' , '=' , 'promocode_service_map.service_api_id')
            ->where ('promocode_service_map.deleted_at' , '=' , null)
            ->orderBy('id','DESC');

        return $this->datatables
            ->eloquent ($query)
            ->addColumn ('action' , 'promocode_service_map.datatables_actions')
            ->editColumn ('is_active' , function ($row) {
                return "<span class='label label-" . (($row->is_active) ? 'success' : 'danger') . "''>" . (($row->is_active) ? 'Yes' : 'No') . "</span>";
            })
            ->editColumn ('promocode_id' , function ($row) {
                return $row->promo_code;
            })
            ->editColumn ('service_api_id' , function ($row) {
                return $row->service_name;
            })
            ->editColumn ('created_by' , function ($row) {
                return $row->first_name;
            })
            ->make (true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query ()
    {
        $promocode = PromocodeServiceMap::query ();

        return $this->applyScopes ($promocode);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html ()
    {
        return $this->builder ()
            ->columns ($this->getColumns ())
            ->addAction (['width' => '10%'])
            ->ajax ('')
            ->parameters ([
                'dom' => 'Bfrtip' ,
                'scrollX' => false ,
                'buttons' => [
                    [
                        'extend' => 'collection' ,
                        'text' => '<i class="fa fa-download"></i> Export' ,
                        'buttons' => [
                            'csv' ,
                            'excel' ,
                            'pdf' ,
                        ] ,
                    ] ,
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns ()
    {
        return [
            'promocode_id' => ['name' => 'promocode_id' , 'data' => 'promocode_id'] ,
            'service_api_id' => ['name' => 'service_api_id' , 'data' => 'service_api_id'] ,
            'is_active' => ['name' => 'is_active' , 'data' => 'is_active'] ,
            'created_at' => ['name' => 'created_at' , 'data' => 'created_at'] ,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename ()
    {
        return 'promocode_service_map';
    }
}
