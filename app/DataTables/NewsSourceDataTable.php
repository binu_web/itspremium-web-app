<?php

namespace App\DataTables;

use App\Models\NewsSource;
use Form;
use Yajra\Datatables\Services\DataTable;

class NewsSourceDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		$query = $this->query ()->select ('news_sources.*' , 'u.first_name')->join ('admin_users as u' , 'u.id' , '=' , 'news_sources.created_by');;

		return $this->datatables
			->eloquent ($query)
			->addColumn ('action' , 'news_sources.datatables_actions')
			->editColumn ('created_by' , function ($row) {
				return $row->first_name;
			})
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$newsSources = NewsSource::query ();

		return $this->applyScopes ($newsSources);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'source' => ['name' => 'source' , 'data' => 'source'] ,
			'source_slug' => ['name' => 'source_slug' , 'data' => 'source_slug'] ,
			'created_by' => ['name' => 'created_by' , 'data' => 'created_by'] ,
			'created At' => ['name' => 'created_at' , 'data' => 'created_at'] ,
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'newsSources';
	}
}
