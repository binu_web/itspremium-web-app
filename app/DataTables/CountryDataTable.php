<?php

namespace App\DataTables;

use App\Models\Country;
use Form;
use Yajra\Datatables\Services\DataTable;

class CountryDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		return $this->datatables
			->eloquent ($this->query ())
			->addColumn ('action' , 'countries.datatables_actions')
			->editColumn ('is_active' , function ($data) {
				return "<span class='label label-" . (($data->is_active) ? 'success' : 'danger') . "''>" . (($data->is_active) ? 'Yes' : 'No') . "</span>";
			})
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$countries = Country::query ();

		return $this->applyScopes ($countries);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'name' => ['name' => 'name' , 'data' => 'name'] ,
			'iso2' => ['name' => 'iso2' , 'data' => 'iso2'] ,
			'iso3' => ['name' => 'iso3' , 'data' => 'iso3'] ,
			'phone_code' => ['name' => 'phone_code' , 'data' => 'phone_code'] ,
			'region' => ['name' => 'region' , 'data' => 'region'] ,
			'latitude' => ['name' => 'latitude' , 'data' => 'latitude'] ,
			'longitude' => ['name' => 'longitude' , 'data' => 'longitude'] ,
			'currency' => ['name' => 'currency' , 'data' => 'currency'] ,
			'country_code' => ['name' => 'country_code' , 'data' => 'country_code'] ,
			'nicename' => ['name' => 'nicename' , 'data' => 'nicename'] ,
			'is_active' => ['name' => 'is_active' , 'data' => 'is_active']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'countries';
	}
}
