<?php

namespace App\DataTables;

use App\Models\ServiceApi;
use Form;
use Yajra\Datatables\Services\DataTable;

class ServiceApiDataTable extends DataTable
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax ()
	{
		$query = $this->query ()->select ('service_apis.*' , 'u.first_name' , 'c.name as category_name')
			->leftJoin ('admin_users as u' , 'u.id' , '=' , 'service_apis.created_by')
			->leftJoin ('services as c' , 'c.id' , '=' , 'service_apis.service_category');

		return $this->datatables
			->eloquent ($query)
			->editColumn ('image' , function ($row) {
				if ($row->image != "")
					return '<img width="100" src="' . asset ('resources/assets/uploads/images/') . '/' . $row->image . '" />';
				else
					return "-";
			})
			->editColumn ('is_active' , function ($row) {

				return "<span class='label label-" . (($row->is_active) ? 'success' : 'danger') . "''>" . (($row->is_active) ? 'Yes' : 'No') . "</span>";

			})
			->editColumn ('created_by' , function ($row) {
				return $row->first_name;
			})
			->editColumn ('service_category' , function ($row) {
				return $row->category_name;
			})
			->addColumn ('action' , 'service_apis.datatables_actions')
			->make (true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query ()
	{
		$serviceApis = ServiceApi::query ();

		return $this->applyScopes ($serviceApis);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html ()
	{
		return $this->builder ()
			->columns ($this->getColumns ())
			->addAction (['width' => '10%'])
			->ajax ('')
			->parameters ([
				'dom' => 'Bfrtip' ,
				'scrollX' => false ,
				'buttons' => [
					[
						'extend' => 'collection' ,
						'text' => '<i class="fa fa-download"></i> Export' ,
						'buttons' => [
							'csv' ,
							'excel' ,
							'pdf' ,
						] ,
					] ,
					'colvis'
				]
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns ()
	{
		return [
			'provider_name' => ['name' => 'provider_name' , 'data' => 'provider_name'] ,
			'service_category' => ['name' => 'service_category' , 'data' => 'service_category'] ,
			'api_key' => ['name' => 'api_key' , 'data' => 'api_key'] ,
			'api_url' => ['name' => 'api_url' , 'data' => 'api_url'] ,
			'api_token' => ['name' => 'api_token' , 'data' => 'api_token'] ,
			'api_metadata' => ['name' => 'api_metadata' , 'data' => 'api_metadata'] ,
			'image' => ['name' => 'image' , 'data' => 'image'] ,
			'is_active' => ['name' => 'is_active' , 'data' => 'is_active'] ,
			'created_by' => ['name' => 'created_by' , 'data' => 'created_by'] ,
//            'updated_by' => ['name' => 'updated_by', 'data' => 'updated_by']
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename ()
	{
		return 'serviceApis';
	}
}
