<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Support\Facades\Cache;

class YatraLocation extends Model
{
	#protected $table = 'yatra_locations';

	public static function getData()
	{
		$aLocations = Cache::get('yatra_locations', []);
		if(!$aLocations) {
			$aLocations = YatraLocation::all()->toArray();
			Cache::forEver('yatra_locations', $aLocations);

		}

		return $aLocations;
	}

}