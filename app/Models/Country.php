<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Country
 *
 * @package App\Models
 * @version February 24, 2017, 11:44 am UTC
 */
class Country extends Model
{
	use SoftDeletes;

	public $table = 'countries';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];

	public $timestamps = false;


	public $fillable = [
		'name' ,
		'iso2' ,
		'iso3' ,
		'phone_code' ,
		'region' ,
		'latlng' ,
		'currency' ,
		'country_code'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'name' => 'string' ,
		'iso2' => 'string' ,
		'iso3' => 'string' ,
		'phone_code' => 'string' ,
		'region' => 'string' ,
		'latlng' => 'string' ,
		'currency' => 'string' ,
		'country_code' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function paymentMethodCountries ()
	{
		return $this->hasMany (\App\Models\PaymentMethodCountry::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function users ()
	{
		return $this->hasMany (\App\Models\User::class);
	}
}
