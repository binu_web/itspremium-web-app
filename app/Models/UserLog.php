<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserLog
 *
 * @package App\Models
 * @version February 24, 2017, 12:07 pm UTC
 */
class UserLog extends Model
{
	use SoftDeletes;

	public $table = 'user_logs';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'log_type' ,
		'user_id' ,
		'service_id' ,
		'ip' ,
		'latlong'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'log_type' => 'integer' ,
		'user_id' => 'integer' ,
		'service_id' => 'integer' ,
		'ip' => 'string' ,
		'latlong' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function logType ()
	{
		return $this->belongsTo (\App\Models\LogType::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function servicesBooked ()
	{
		return $this->belongsTo (\App\Models\ServicesBooked::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function user ()
	{
		return $this->belongsTo (\App\Models\User::class);
	}
}
