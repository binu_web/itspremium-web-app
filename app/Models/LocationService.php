<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LocationService
 *
 * @package App\Models
 * @version February 24, 2017, 11:48 am UTC
 */
class LocationService extends Model
{
	use SoftDeletes;

	public $table = 'location_services';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'service' ,
		'location' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'service' => 'integer' ,
		'location' => 'integer' ,
		'created_by' => 'integer' ,
		'updated_by' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function adminUser ()
	{
		return $this->belongsTo (\App\Models\Admin::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function location ()
	{
		return $this->belongsTo (\App\Models\Location::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function serviceApi ()
	{
		return $this->belongsTo (\App\Models\ServiceApi::class);
	}

	public function serviceApis ()
	{
		return $this->hasOne (\App\Models\ServiceApi::class , 'id' , 'service');
	}


}
