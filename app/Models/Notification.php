<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class News
 * @package App\Models
 * @version February 24, 2017, 11:50 am UTC
 */
class Notification extends Model
{
    use SoftDeletes;

    public $table = 'notification';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'service_api_id',
        'notification_type_id',
        'notification_data',
        'is_active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'service_api_id' => 'integer',
        'notification_type_id' => 'integer',
        'notification_data' => 'text',
        'is_active'=>'integer',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function notificationType ()
    {
        return $this->belongsTo (\App\Models\NotificationType::class );
    }


}
