<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Support\Facades\Cache;

class YatraHotelDetail extends Model
{
	public $primaryKey = 'VendorId';
	public $timestamps = false;
	public $incrementing = false; //very important- if our primary key is not auto incrementing then need to use this . otherwise VendorId consider as interger, it will omit the zeros 


	/*
	* To get all hotels
	*
	*
	*/
	public static function getAllHotels()
	{
		$aAllHotels = Cache::get('yatra_hotels', []);
		if(!$aAllHotels) {
			$aAllHotels = YatraHotelDetail::
										  select(['VendorId as hotel_code', 'VendorName as hotel_name','Location as location',
										  	'City as city','Address1 as address1','Address2 as address2'])
										  ->get()->all();
			Cache::forEver('yatra_hotels', $aAllHotels);
		}

		return $aAllHotels;
	}
	
}