<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserApiAccessToken extends Model
{
	//
	use softDeletes;
	public $table = 'user_access_tokens';


	/**
	 * Scope Query By User Id
	 *
	 * @param $query
	 * @param $device_id
	 *
	 * @author Binoop V
	 */
	public function scopeByUser ($query , $user_id)
	{
		return $query->where ('user_id' , $user_id);
	}


	/**
	 * Scope Query By Service Id
	 *
	 * @param $query
	 * @param $device_id
	 *
	 * @author Binoop V
	 */
	public function scopeByServiceApi ($query , $service_api_id)
	{
		return $query->where ('service_api_id' , $service_api_id);
	}
}
