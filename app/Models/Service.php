<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Service
 *
 * @package App\Models
 * @version February 24, 2017, 12:03 pm UTC
 */
class Service extends Model
{
	use SoftDeletes;

	public $table = 'services';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'name' ,
		'image' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'name' => 'string' ,
		'image' => 'string' ,
		'created_by' => 'integer' ,
		'updated_by' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function adminUser ()
	{
		return $this->belongsTo (\App\Models\AdminUser::class);
	}


//
//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
//     **/
//    public function adminUser()
//    {
//        return $this->belongsTo(\App\Models\AdminUser::class);
//    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function serviceApis ()
	{
		return $this->hasMany (\App\Models\ServiceApi::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function serviceMetaKeys ()
	{
		return $this->hasMany (\App\Models\ServiceMetaKey::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function servicesBookeds ()
	{
		return $this->hasMany (\App\Models\ServicesBooked::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function serviceApisList ()
	{
		return $this->hasMany (\App\Models\ServiceApi::class , 'service_category');
	}
}
