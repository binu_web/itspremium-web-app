<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CabLog
 *
 * @package App\Models
 * @version March 14, 2017, 12:07 pm UTC
 */
class RestaurantLog extends Model
{
    use SoftDeletes;

    public $table = 'restaurant_log_details';

    protected $dates = ['deleted_at'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user ()
    {
        return $this->belongsTo (\App\Models\User::class);
    }
}
