<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ServiceMetaData
 *
 * @package App\Models
 * @version February 24, 2017, 12:00 pm UTC
 */
class ServiceMetaData extends Model
{
	use SoftDeletes;

	public $table = 'service_meta_data';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'meta_key_id' ,
		'booked_id' ,
		'value'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'meta_key_id' => 'integer' ,
		'booked_id' => 'integer' ,
		'value' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function servicesBooked ()
	{
		return $this->belongsTo (\App\Models\ServicesBooked::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function serviceMetaKey ()
	{
		return $this->belongsTo (\App\Models\ServiceMetaKey::class);
	}
}
