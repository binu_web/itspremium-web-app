<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Transaction
 *
 * @package App\Models
 * @version February 24, 2017, 12:06 pm UTC
 */
class Transaction extends Model
{
	use SoftDeletes;

	public $table = 'transactions';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'booked_id' ,
		'user_id' ,
		'gateway_id' ,
		'amount' ,
		'status' ,
		'status_msg' ,
		'refund_status' ,
		'refund_msg'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'booked_id' => 'integer' ,
		'user_id' => 'integer' ,
		'gateway_id' => 'integer' ,
		'amount' => 'float' ,
		'status' => 'string' ,
		'status_msg' => 'string' ,
		'refund_status' => 'string' ,
		'refund_msg' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function servicesBooked ()
	{
		return $this->belongsTo (\App\Models\ServicesBooked::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function paymentMethod ()
	{
		return $this->belongsTo (\App\Models\PaymentMethod::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function user ()
	{
		return $this->belongsTo (\App\Models\User::class);
	}
}
