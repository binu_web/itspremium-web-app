<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class News
 *
 * @package App\Models
 * @version February 24, 2017, 11:50 am UTC
 */
class News extends Model
{
	use SoftDeletes;

	public $table = 'news';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'category' ,
		'title' ,
		'image' ,
		'source_link' ,
		'description' ,
		'publish_date' ,
		'is_trending' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'category' => 'integer' ,
		'title' => 'string' ,
		'image' => 'string' ,
		'source_link' => 'string' ,
		'description' => 'string' ,
		'publish_date' => 'date' ,
		'created_by' => 'integer' ,
		'updated_by' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function newsCategory ()
	{
		return $this->belongsTo (\App\Models\NewsCategory::class , 'category' , 'id');
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function adminUser ()
	{
		return $this->belongsTo (\App\Models\Admin::class);
	}


}
