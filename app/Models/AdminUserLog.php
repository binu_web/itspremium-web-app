<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AdminUserLog
 *
 * @package App\Models
 * @version February 24, 2017, 11:37 am UTC
 */
class AdminUserLog extends Model
{
	use SoftDeletes;

	public $table = 'admin_user_logs';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'log_type' ,
		'user_id' ,
		'action' ,
		'ip' ,
		'latlong'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'log_type' => 'integer' ,
		'user_id' => 'integer' ,
		'action' => 'string' ,
		'ip' => 'string' ,
		'latlong' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function adminLogType ()
	{
		return $this->belongsTo (\App\Models\AdminLogType::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function adminUser ()
	{
		return $this->belongsTo (\App\Models\Admin::class);
	}
}
