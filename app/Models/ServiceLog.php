<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ServiceMetaKey
 *
 * @package App\Models
 * @version February 24, 2017, 12:01 pm UTC
 */
class ServiceLog extends Model
{
    use SoftDeletes;

    public $table = 'service_log';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id' ,
        'service_id',
        'service_api_id' ,
        'event_type_id',
        'http_type' ,
        'data_type',
        'data'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer' ,
        'service_id' => 'integer' ,
        'service_api_id' => 'integer' ,
        'event_type_id' => 'integer' ,
        'http_type' => 'integer' ,
        'data_type' => 'integer' ,
        'event' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function service ()
    {
        return $this->belongsTo (\App\Models\ServiceLog::class);
    }

}
