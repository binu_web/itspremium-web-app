<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class NewsSource
 *
 * @package App\Models
 * @version February 24, 2017, 11:52 am UTC
 */
class NewsSource extends Model
{
	use SoftDeletes;

	public $table = 'news_sources';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	protected $dates = ['deleted_at'];

	public $fillable = [
		'source' ,
		'source_slug' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'source' => 'string' ,
		'source_slug' => 'string' ,
		'created_by' => 'integer' ,
		'updated_by' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function adminUser ()
	{
		return $this->belongsTo (\App\Models\Admin::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function news ()
	{
		return $this->hasMany (\App\Models\News::class);
	}

}
