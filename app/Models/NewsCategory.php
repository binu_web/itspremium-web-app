<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class NewsCategory
 *
 * @package App\Models
 * @version February 24, 2017, 11:52 am UTC
 */
class NewsCategory extends Model
{
	use SoftDeletes;

	public $table = 'news_categories';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'name' ,
		'image' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'name' => 'string' ,
		'image' => 'string' ,
		'created_by' => 'integer' ,
		'updated_by' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function adminUser ()
	{
		return $this->belongsTo (\App\Models\Admin::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function news ()
	{
		return $this->hasMany (\App\Models\News::class);
	}

}
