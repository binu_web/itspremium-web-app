<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ServiceBooked
 *
 * @package App\Models
 * @version February 24, 2017, 12:04 pm UTC
 */
class ServiceBooked extends Model
{
	use SoftDeletes;

	public $table = 'services_booked';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'user_id' ,
		'service_id' ,
		'service_api_id' ,
		'status' ,
		'ip' ,
		'latlong'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'user_id' => 'integer' ,
		'service_id' => 'integer' ,
		'service_api_id' => 'integer' ,
		'status' => 'string' ,
		'ip' => 'string' ,
		'latlong' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function serviceApi ()
	{
		return $this->belongsTo (\App\Models\ServiceApi::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function service ()
	{
		return $this->belongsTo (\App\Models\Service::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function user ()
	{
		return $this->belongsTo (\App\Models\User::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function serviceMetaData ()
	{
		return $this->hasMany (\App\Models\ServiceMetaDatum::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function transactions ()
	{
		return $this->hasMany (\App\Models\Transaction::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function userLogs ()
	{
		return $this->hasMany (\App\Models\UserLog::class);
	}
}
