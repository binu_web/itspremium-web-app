<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelSearchLog extends Model
{
	protected $table = 'hotel_search_log';

	protected $dates = ['deleted_at'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function user ()
	{
		return $this->belongsTo (\App\Models\User::class);
	}

	public function location()
	{
		return $this->belongsTo(\App\Models\YatraLocation::class);
	}
}