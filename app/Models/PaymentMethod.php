<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentMethod
 *
 * @package App\Models
 * @version February 24, 2017, 11:57 am UTC
 */
class PaymentMethod extends Model
{
	use SoftDeletes;

	public $table = 'payment_methods';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'gateway_name' ,
		'api_key' ,
		'api_url' ,
		'api_metadata' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'gateway_name' => 'string' ,
		'api_key' => 'string' ,
		'api_url' => 'string' ,
		'api_metadata' => 'string' ,
		'created_by' => 'integer' ,
		'updated_by' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function adminUser ()
	{
		return $this->belongsTo (\App\Models\AdminUser::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
//    public function adminUser()
//    {
//        return $this->belongsTo(\App\Models\AdminUser::class);
//    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function paymentMethodCountries ()
	{
		return $this->hasMany (\App\Models\PaymentMethodCountry::class , 'gateway_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function transactions ()
	{
		return $this->hasMany (\App\Models\Transaction::class);
	}
}
