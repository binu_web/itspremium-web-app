<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AdminLogType
 *
 * @package App\Models
 * @version February 24, 2017, 11:37 am UTC
 */
class AdminLogType extends Model
{
	use SoftDeletes;

	public $table = 'admin_log_type';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'name'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'name' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function adminUserLogs ()
	{
		return $this->hasMany (\App\Models\AdminUserLog::class);
	}
}
