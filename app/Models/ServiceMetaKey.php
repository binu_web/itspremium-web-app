<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ServiceMetaKey
 *
 * @package App\Models
 * @version February 24, 2017, 12:01 pm UTC
 */
class ServiceMetaKey extends Model
{
	use SoftDeletes;

	public $table = 'service_meta_key';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'service_id' ,
		'meta_key'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'service_id' => 'integer' ,
		'meta_key' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function service ()
	{
		return $this->belongsTo (\App\Models\Service::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function serviceMetaData ()
	{
		return $this->hasMany (\App\Models\ServiceMetaDatum::class);
	}
}
