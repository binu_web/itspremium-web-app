<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Location
 *
 * @package App\Models
 * @version February 24, 2017, 11:46 am UTC
 */
class Location extends Model
{
	use SoftDeletes;

	public $table = 'locations';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'location' ,
		'lat_long' ,
		'is_active' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'location' => 'string' ,
		'lat_long' => 'string' ,
		'created_by' => 'integer' ,
		'updated_by' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function adminUser ()
	{
		return $this->belongsTo (\App\Models\Admin::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function locationServices ()
	{
		return $this->hasMany (\App\Models\LocationService::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function locationService ()
	{
		return $this->hasMany (\App\Models\LocationService::class , 'location');
	}


	public function createdBy ()
	{
		return $this->hasOne (\App\Models\Admin::class , 'id' , 'created_by');
	}

	public function updatedBy ()
	{
		return $this->hasOne (\App\Models\Admin::class , 'id' , 'updated_by');
	}

}
