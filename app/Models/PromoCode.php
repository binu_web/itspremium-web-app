<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class News
 * @package App\Models
 * @version February 24, 2017, 11:50 am UTC
 */
class PromoCode extends Model
{
    use SoftDeletes;

    public $table = 'promo_code';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [

        'promo_code',
        'promo_type',
        'discount_type',
        'discount',
        'valid_from',
        'valid_to',
        'no_of_maximum_use',
        'description',
        'validation_rules',
        'deduction_type',
        'no_of_times_per_user',
        'no_of_times_per_user_per_service',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'promo_code' => 'string',
        'promo_type' => 'integer',
        'discount_type' => 'integer',
        'discount' => 'decimal',
        'valid_from' => 'datetime',
        'valid_to' => 'datetime',
        'description' => 'string',
        'validation_rules' => 'string',
        'deduction_type'    => 'integer',
        'no_of_times_per_user' => 'integer',
        'no_of_times_per_user_per_service' => 'integer'
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'promo_code' => 'unique:promo_code',
    ];

   /* public function rules()
    {
        return [
            'promo_code' => 'unique:promo_code'.$this->id,
        ];
    }
*/



}
