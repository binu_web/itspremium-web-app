<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Admin
 *
 * @package App\Models
 * @version February 24, 2017, 11:31 am UTC
 */
class Admin extends Model
{
	use SoftDeletes;

	public $table = 'admin_users';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'email' ,
		'password' ,
		'user_type' ,
		'first_name' ,
		'last_name' ,
		'mobile' ,
		'job_role' ,
		'is_active' ,
		'activation_code' ,
		'remember_token' ,
		'created_by' ,
		'updated_by'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer' ,
		'email' => 'string' ,
		'password' => 'string' ,
		'user_type' => 'integer' ,
		'first_name' => 'string' ,
		'last_name' => 'string' ,
		'mobile' => 'string' ,
		'job_role' => 'string' ,
		'activation_code' => 'string' ,
		'remember_token' => 'string' ,
		'created_by' => 'integer' ,
		'updated_by' => 'integer'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function adminUserType ()
	{
		return $this->belongsTo (\App\Models\AdminUserType::class , 'user_type');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function adminUserLogs ()
	{
		return $this->hasMany (\App\Models\AdminUserLog::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function locationServices ()
	{
		return $this->hasMany (\App\Models\LocationService::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function locations ()
	{
		return $this->hasMany (\App\Models\Location::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function news ()
	{
		return $this->hasMany (\App\Models\News::class);
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function newsCategories ()
	{
		return $this->hasMany (\App\Models\NewsCategory::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function paymentMethodCountries ()
	{
		return $this->hasMany (\App\Models\PaymentMethodCountry::class);
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function paymentMethods ()
	{
		return $this->hasMany (\App\Models\PaymentMethod::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function serviceApis ()
	{
		return $this->hasMany (\App\Models\ServiceApi::class);
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function services ()
	{
		return $this->hasMany (\App\Models\Service::class);
	}
}
