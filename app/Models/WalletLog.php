<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class News
 * @package App\Models
 * @version February 24, 2017, 11:50 am UTC
 */
class WalletLog extends Model
{
    use SoftDeletes;

    public $table = 'wallet_log';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [

        'wallet_id',
        'service_id',
        'balance',
        'transaction_type',
        'transaction_method',
        'deleted_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'balance' => 'float',
        'deleted_at' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];




}
