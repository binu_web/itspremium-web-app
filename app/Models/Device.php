<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Country
 *
 * @package App\Models
 * @version February 24, 2017, 11:44 am UTC
 */
class Device extends Model
{
	use SoftDeletes;

	public $table = 'devices';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';


	protected $dates = ['deleted_at'];


	public $fillable = [

		'id' ,
		'type' ,
		'device_id' ,
		'user_id' ,
		'regId' ,
		'device_name' ,
		'device_token' ,
		'builder_version' ,
		'os_version' ,
		'screen_width' ,
		'screen_height' ,
		'ip_address'

	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [

		'id' => 'integer' ,
		'type' => 'enum' ,
		'device_id' => 'string' ,
		'user_id' => 'integer' ,
		'regId' => 'string' ,
		'device_name' => 'string' ,
		'device_token' => 'string' ,
		'builder_version' => 'string' ,
		'os_version' => 'string' ,
		'screen_width' => 'integer' ,
		'screen_height' => 'integer' ,
		'ip_address' => 'string'

	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 **/
	public function users ()
	{
		return $this->hasMany (\App\Models\User::class);
	}


	/**
	 * Scope Query By Device Id
	 *
	 * @param $query
	 * @param $device_id
	 *
	 * @author Binoop V
	 */
	public function scopeByDevice ($query , $device_id)
	{
		return $query->where ('device_id' , $device_id);
	}

	/**
	 * Scope Query By Device Id
	 *
	 * @param $query
	 * @param $device_id
	 *
	 * @author Binoop V
	 */
	public function scopeByToken ($query , $fcm_token)
	{
		return $query->where ('fcm_token' , $fcm_token);
	}


	/**
	 * Scope Query By Device Type
	 *
	 * @param $query
	 * @param $type
	 *
	 * @author Binoop V
	 */
	public function scopeByType ($query , $type)
	{
		return $query->where ('type' , $type);
	}


}
