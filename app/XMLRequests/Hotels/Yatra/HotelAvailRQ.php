<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 6/28/2017
 * Time: 10:49 AM
 */

namespace App\XMLRequests\Hotels\Yatra;


class HotelAvailRQ
{

	private $requestData;


	function __construct ($requestData = [])
	{

		$this->setRequestData ($requestData);
	}


	/**
	 * @return mixed
	 */
	public
	function getRequestData ()
	{
		return $this->requestData;
	}

	/**
	 * @param mixed $requestData
	 */
	public
	function setRequestData ($requestData)
	{
		$this->requestData = (object)($requestData);
	}


	/**
	 * Obtain Request XML
	 *
	 * @param $requestData
	 */
	public
	function requestXML ()
	{
		$xmlRequest = $this->generateXML ();

		return $xmlRequest;
	}

	/**
	 * @param $requestData
	 */
	public function generateXML ()
	{

		// Init Headers
		$xmlRequest = $this->setHeader ();
		$xmlRequest .= $this->setAvailRequestSegments ();
		$xmlRequest .= $this->setFooter ();

		return preg_replace ('/(\v|\s)+/' , ' ' , $xmlRequest);
	}


	/**
	 * Hotel Avail Request attributes :
	 *
	 * @attribute RequestCurrency : INR (Default   )
	 * @attribute Sort Order : TG_RANKING(default), PRICE (low to high), GUEST_RATING (high to low),
	 *            STAR_RATING_ASCENDING, STAR_RATING_DESCENDING, DEALS
	 * @return string
	 */

	public function setHeader ()
	{


		return '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				<soap:Body>
				<OTA_HotelAvailRQ xmlns="http://www.opentravel.org/OTA/2003/05" RequestedCurrency="' . $this->requestData->RequestedCurrency . '" SortOrder="' . $this->requestData->SortOrder . '" 
				Version="0.0" PrimaryLangID="en" SearchCacheLevel="VeryRecent">';
	}


	/**
	 * Footer Closing Tags
	 *
	 * @return string
	 */
	public function setFooter ()
	{
		return '</OTA_HotelAvailRQ></soap:Body></soap:Envelope>';
	}


	/**
	 * AvailRequestSegments Tag
	 *
	 * @return string
	 */
	public function setAvailRequestSegments ()
	{
		$availRequestSegements = "<AvailRequestSegments>";

		$availRequestSegements .= $this->setAvailRequestSegment ();

		$availRequestSegements .= "</AvailRequestSegments>";

		return $availRequestSegements;
	}


	/**
	 * AvailRequestSegment
	 *
	 * @return string
	 */
	public function setAvailRequestSegment ()
	{
		$availRequestSegement = "<AvailRequestSegment>";

		$availRequestSegement .= $this->setHotelSearchCriteria ();

		$availRequestSegement .= "</AvailRequestSegment>";

		return $availRequestSegement;
	}


	/**
	 * HotelSearchCriteria
	 *
	 * @return string
	 */
	public function setHotelSearchCriteria ()
	{
		$HotelSearchCriteria = "<HotelSearchCriteria>";

		$HotelSearchCriteria .= $this->setCriterion ();

		$HotelSearchCriteria .= "</HotelSearchCriteria>";

		return $HotelSearchCriteria;
	}


	/**
	 * HotelSearchCriteria
	 *
	 * @return string
	 */
	public function setCriterion ()
	{
		$Criterion = "<Criterion>";

		// Set Address
		$Criterion .= $this->setAddress ();


		// Set Hotel Reference
		$Criterion .= $this->setHotelRef ();


		// Set Stay Date Range
		$Criterion .= $this->setStayDateRange ();


		// Rate Range
		$Criterion .= $this->setRateRange ();


		// Room Stay Candidate
		$Criterion .= $this->setRoomStayCandidates ();


		// TPA Extension
		$Criterion .= $this->setTPAExtenstions ();


		$Criterion .= "</Criterion>";

		return $Criterion;
	}


	/**
	 * Room Stay Candidates
	 *
	 * @return string
	 */
	public function setAddress ()
	{
		$Address = "<Address>";

		if($this->requestData->searchCityType == 1)
			$Address .= '<CityName>' . $this->requestData->CityName . '</CityName>';
		$Address .= '<CountryName Code="' . $this->requestData->CountryName . '"></CountryName>';

		$Address .= "</Address>";

		return $Address;
	}


	/**
	 * Stay Date Range
	 *
	 * @return string
	 */
	public function setStayDateRange ()
	{
		$StayDateRange = '<StayDateRange Start="' . $this->requestData->StayDateRangeStart . '" End="' . $this->requestData->StayDateRangeEnd . '"/>';
		return $StayDateRange;
	}


	/**
	 * Stay Date Range
	 *
	 * @return string
	 */
	public function setRateRange ()
	{
		$RateRange = '<RateRange MinRate="" MaxRate=""/>';
		return $RateRange;
	}


	/**
	 * Hotel Ref
	 *
	 * @return string
	 */
	public function setHotelRef ()
	{
		$AreaIdStr = '';
		
		if($this->requestData->searchCityType == 2 && $this->requestData->searchAreaId) {
			$AreaIdStr = 'AreaID = "'.$this->requestData->searchAreaId.'"';
		}

		$HotelRef = "<HotelRef ".$AreaIdStr."/>";

		return $HotelRef;
	}


	/**
	 * Room Stay Candidates
	 *
	 * @return string
	 */
	public function setRoomStayCandidates ()
	{
		$RoomStayCandidates = "<RoomStayCandidates>";

		foreach($this->requestData->roomDetails as $key => $aVal) :
			$RoomStayCandidates .= $this->setRoomStayCandidate ($aVal);
		endforeach;

		$RoomStayCandidates .= "</RoomStayCandidates>";

		return $RoomStayCandidates;
	}


	/**
	 * Set Room Stay Candidate
	 *
	 *
	 * @return string
	 */
	public function setRoomStayCandidate ($aVal)
	{   

		$RoomStayCandidate = "<RoomStayCandidate>";

		$RoomStayCandidate .= $this->setGuestCounts ($aVal);

		$RoomStayCandidate .= "</RoomStayCandidate>";

		return $RoomStayCandidate;
	}


	/**
	 * Set Guest Counts Tag
	 *
	 * @return string
	 */
	public function setGuestCounts ($aGuestData)
	{
		$GuestCounts = "<GuestCounts>";
			$GuestCounts .= $this->setGuestCount ($aGuestData);
		$GuestCounts .= "</GuestCounts>";

		return $GuestCounts;
	}


	/**
	 * Set Guest Count
	 * Guest Count
	 *
	 * @AgeQualifyingCode :
	 *                    (value)10 Denotes Adult
	 *                    (value) 8 Denotes Child
	 *
	 * @return string
	 */
	public function setGuestCount ($aGuestData)
	{
		$adultCount = $aGuestData['adult'];
		$aChild 	= $aGuestData['children'];

		$sReturn 	= '<GuestCount count="'.$adultCount.'" AgeQualifyingCode="10"/>';
		foreach($aChild as $key => $aChildNode):
			$childCount = $aChildNode['count'];
		    $sChildNode = '<GuestCount Age ="'.$aChildNode['age'].'" AgeQualifyingCode="8" Count="1"/>';
			$sReturn   .= str_repeat($sChildNode, $childCount); ////bcs we need to separate each childs in separate nodes. even they are in same age
		endforeach;

		return $sReturn;

	}


	/**
	 * @return string
	 */
	public function setTPAExtenstions ()
	{
		$TPA_Extensions = "<TPA_Extensions>";

		$TPA_Extensions .= $this->setPagination ();

		$TPA_Extensions .= $this->setUserAuthentication ();

		$TPA_Extensions .= "</TPA_Extensions>";

		return $TPA_Extensions;

	}


	/**
	 * Set Pagination
	 *
	 * @return string
	 */
	public function setPagination ()
	{
		return '<Pagination enabled="' . $this->requestData->is_paginate_enabled . '" hotelsFrom="' . $this->requestData->hotelsFrom . '" hotelsTo="' . $this->requestData->hotelsTo . '"/>';

	}


	/**
	 * Set User Authentication
	 *
	 * @return string
	 */
	public function setUserAuthentication ()
	{
		return '<UserAuthentication password="' . $this->requestData->password . '" propertyId="' . $this->requestData->propertyId . '" username="' . $this->requestData->username . '"/>';

	}


}