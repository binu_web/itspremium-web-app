<?php
/**
 * Prepara Yatra Hotel Cancel Booking Request
 * @author Midhun
 * @Created on 12-07-2017
 */
namespace App\XMLRequests\Hotels\Yatra;

class HotelConfirmCancelRQ
{
    private $requestData;

    public function __construct ($requestData = [])
    {

        $this->setRequestData ($requestData);
    }

    /**
     * @return mixed
     */

    public function getRequestData ()
    {
        return $this->requestData;
    }

    /**
     * @param mixed $requestData
     */

    public function setRequestData ($requestData)
    {
        $this->requestData = (object)($requestData);
    }

    /**
     * Obtain Request XML
     *
     * @param $requestData
     */

    public function requestXML ()
    {
        $xmlRequest = $this->generateXML ();

        return $xmlRequest;
    }

    /**
     * @param $requestData
     */
    private function generateXML ()
    {

        // Init Headers
        $xmlRequest = $this->setHeader ();
        $xmlRequest .= $this->setAvailRequestSegments ();
        $xmlRequest .= $this->setFooter ();

        return preg_replace ('/(\v|\s)+/' , ' ' , $xmlRequest);
    }

    /**
     * Hotel Avail Request attributes :
     * @return string
     */

    private function setHeader ()
    {


        return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.opentravel.org/OTA/2003/05">
				<soapenv:Header/>
				<soapenv:Body>';

    }

    /**
     * AvailRequestSegments Tag
     *
     * @return string
     */
    private function setAvailRequestSegments ()
    {
        $availRequestSegements = '<ns:OTA_CancelRQ CancelType="'.$this->requestData->cancelType.'" Version="1.0">';

        $availRequestSegements .= $this->setAvailRequestSegment ();

        $availRequestSegements .= "</ns:OTA_CancelRQ>";

        return $availRequestSegements;
    }

    private function setAvailRequestSegment()
    {
        $availRequestSegements = $this->setPOSSegment();
        $availRequestSegements .= '<ns:UniqueID ID="'.$this->requestData->final_booking_unique_id.'" />';
        $availRequestSegements .= $this->hotelCancelVerificationSegments();
        $availRequestSegements .= $this->hotelCancelExtensionSegments();

        return $availRequestSegements;
    }

    private function setPOSSegment()
    {
        return 	'<ns:POS>
					<ns:Source>
						<ns:RequestorID ID="'.$this->requestData->propertyId.'" MessagePassword="'.$this->requestData->password.'">
								<ns:CompanyName Code="'.$this->requestData->userName.'"/>
						</ns:RequestorID>
					</ns:Source>
				</ns:POS>';
    }

    public function hotelCancelVerificationSegments()
    {
        $hotelCancelVerification = '<ns:Verification>
                                        <ns:PersonName>
                                            <ns:Surname>'.$this->requestData->customer_surname.'</ns:Surname>
                                        </ns:PersonName>
                                        <ns:Email>'.$this->requestData->customer_email_id.'</ns:Email>
                                    </ns:Verification>';

        return $hotelCancelVerification;
    }

    public function hotelCancelExtensionSegments()
    {
        $hotelCancelExtension = '<ns:TPA_Extensions>
                                    <ns:CancelDates>'.$this->requestData->stay_start_date.'</ns:CancelDates>
                                </ns:TPA_Extensions>';

        return $hotelCancelExtension;
    }


    private function setFooter ()
    {
        return '</soapenv:Body></soapenv:Envelope>';
    }
}