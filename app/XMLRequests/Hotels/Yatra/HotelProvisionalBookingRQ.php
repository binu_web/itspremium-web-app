<?php
/**
 * Prepara Yatra Hotel Provisional Booking Request
 * @author Harinarayanan 
 * @Created on 12-07-2017
 */
namespace App\XMLRequests\Hotels\Yatra;

class HotelProvisionalBookingRQ
{
	private $requestData;

	public function __construct ($requestData = [])
	{

		$this->setRequestData ($requestData);
	}

	/**
	 * @return mixed
	 */
	
	public function getRequestData ()
	{
		return $this->requestData;
	}

	/**
	 * @param mixed $requestData
	 */
	
	public function setRequestData ($requestData)
	{
		$this->requestData = (object)($requestData);
	}

	/**
	 * Obtain Request XML
	 *
	 * @param $requestData
	 */
	
	public function requestXML ()
	{
		$xmlRequest = $this->generateXML ();

		return $xmlRequest;
	}

	/**
	 * @param $requestData
	 */
	private function generateXML ()
	{

		// Init Headers
		$xmlRequest = $this->setHeader ();
		$xmlRequest .= $this->setAvailRequestSegments ();
		$xmlRequest .= $this->setFooter ();

		return preg_replace ('/(\v|\s)+/' , ' ' , $xmlRequest);
	}

	/**
	 * Hotel Avail Request attributes :
	 *
	 * @return string
	 */

	private function setHeader ()
	{


		return '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				<soap:Body>';
				
	}

	/**
	 * AvailRequestSegments Tag
	 *
	 * @return string
	 */
	private function setAvailRequestSegments ()
	{
		$availRequestSegements = '<OTA_HotelResRQ xmlns="http://www.opentravel.org/OTA/2003/05" CorrelationID="" TransactionIdentifier="'.$this->requestData->transactionIdentifier.'" Version="">';

		$availRequestSegements .= $this->setAvailRequestSegment ();

		$availRequestSegements .= "</OTA_HotelResRQ>";

		return $availRequestSegements;
	}


	private function setAvailRequestSegment()
	{
		$availRequestSegements = $this->setPOSSegment();
		$availRequestSegements .= '<UniqueID Type="" ID="" />';
		$availRequestSegements .= $this->hotelReservationSegments();

		return $availRequestSegements;
	}


	private function setPOSSegment()
	{
		return 	'<POS>
					<Source ISOCurrency="INR">
						<RequestorID MessagePassword="'.$this->requestData->password.'" ID="'.$this->requestData->propertyId.'">
								<CompanyName Code="'.$this->requestData->userName.'" ></CompanyName> 
						</RequestorID>
					</Source>
				</POS>';


	}


	private function hotelReservationSegments()
	{
		$hotelReservation = '<HotelReservations>';
		$hotelReservation .= $this->hotelReservationSegment();
		$hotelReservation .= '</HotelReservations>';
		return $hotelReservation;
	}

	private function hotelReservationSegment()
	{
		$hotelreservation  = '<HotelReservation>';
		$hotelreservation .= $this->roomStaysSegment();
		$hotelreservation .= $this->resGuestsSegment();
		$hotelreservation .= $this->resGlobalInfoSegment();
		$hotelreservation .= '</HotelReservation>';
		return $hotelreservation;
	}

	private function roomStaysSegment()
	{
		$roomstay 		= '<RoomStays>';
		$roomstay 		.= $this->roomStaySegment();
		$roomstay 		.= '</RoomStays>';
		return $roomstay;
	}

	private function roomStaySegment()
	{	
		$roomStay   =  '<RoomStay>';
		$roomStay 	.=  $this->roomTypesSegment();
		$roomStay 	.= $this->ratePlansSegment();
		$roomStay 	.= $this->guestCountsSegment();
		$roomStay 	.= '<TimeSpan End="'.$this->requestData->StayDateRangeEnd.'" Start="'.$this->requestData->StayDateRangeStart.'" />';
		$roomStay 	.= $this->totalSegment();
		$roomStay 	.= '<BasicPropertyInfo HotelCode="'.$this->requestData->HotelCode.'" />';
		$roomStay   .= $this->commentsSegment();
		$roomStay   .= '</RoomStay>';

		return $roomStay;

	}

	private function roomTypesSegment()
	{
		$roomType = '<RoomTypes>';
	 	$roomType .= $this->roomTypeSegment();
	 	$roomType .= '</RoomTypes>';
	 	return $roomType;  
	}

	private function roomTypeSegment()
	{
		$roomType = '<RoomType NumberOfUnits="'.$this->requestData->nofUnits.'" RoomTypeCode="'.$this->requestData->roomTypeCode.'" />';
		return $roomType;
	}

	private function ratePlansSegment()
	{
		$ratePlan  = '<RatePlans>';
		$ratePlan  .= $this->ratePlanSegment();
		$ratePlan  .= '</RatePlans>';
		return $ratePlan;
	}

	private function ratePlanSegment()
	{
		$ratePlan = '<RatePlan RatePlanCode="'.$this->requestData->ratePlanCode.'" />';
		return $ratePlan;
	}

	private function guestCountsSegment()
	{
		$guestCounts = '<GuestCounts IsPerRoom="false">';
		$guestCounts .= $this->guestCountSegment(); //looping here
		$guestCounts .= '</GuestCounts>';
		return $guestCounts;
	}

	private function guestCountSegment()
	{	
		$aGuestCount = $this->requestData->guestCount;
		$guestCounts = '';
		$resGuestRPH = 0;
		foreach($aGuestCount as $key => $aVal) {
			$adultCnt = $aVal['adult'];
			$aChildren = isset($aVal['children']) ? $aVal['children'] : [];
			$guestCounts .= '<GuestCount ResGuestRPH="'.$resGuestRPH.'" AgeQualifyingCode="10" Count="'.$adultCnt.'" />';
			foreach($aChildren as $key => $aChild) {
				$childCount = $aChild['count'];
				$childNode 	= '<GuestCount ResGuestRPH="'.$resGuestRPH.'" AgeQualifyingCode="8" Count="1"  age ="'.$aChild['age'].'"/>';
				$guestCounts .= str_repeat($childNode, $childCount);
			}
			$resGuestRPH++;
		}
		
		return $guestCounts;
	}

	private function totalSegment()
	{
		$totalSegment = '<Total AmountBeforeTax="'.$this->requestData->AmountBeforeTax.'" CurrencyCode="'.$this->requestData->currencyCode.'">
								<Taxes Amount="'.$this->requestData->taxAmount.'" />
						</Total>';

		return $totalSegment;
	}

	private function commentsSegment()
	{
		$commentSegment   = '<Comments>';
		$commentSegment  .= $this->commentSegment();
		$commentSegment  .= '</Comments>';
		return $commentSegment;
	}

	private function commentSegment()
	{
		$commentSegment = '<Comment>';
		$commentSegment .= '<Text>'.$this->requestData->userComments.'</Text>';
		$commentSegment .= '</Comment>';
		return $commentSegment;
	}

	private function resGuestsSegment()
	{
		$resGuestSegment = '<ResGuests>';
		$resGuestSegment .= $this->resGuestSegment();
		$resGuestSegment .= '</ResGuests>';
		return $resGuestSegment;
	}

	private function resGuestSegment()
	{
		$resGuest = '<ResGuest>';
		$resGuest .= $this->profileSegement();
		$resGuest .= '</ResGuest>';
		return $resGuest;
	}

	private function profileSegement()
	{
		$profileSegment = '<Profiles>';
		$profileSegment .= '<ProfileInfo>';
		$profileSegment .= $this->profile();
		$profileSegment .= '</ProfileInfo>';
		$profileSegment .= '</Profiles>';
		return $profileSegment;
	}

	private function profile()
	{
		$profile  = '<Profile ProfileType="1">'; //should be always 1 as per the doc
		$profile  .= $this->customerSegment();
		$profile  .= '</Profile>';
		return $profile;

	}

	private function customerSegment()
	{
		$customerDetails = $this->requestData->customerDetails;
		$customer = '<Customer>';
		$customer .= $this->personNameSegment();
		$customer .= '<Telephone AreaCityCode="'.$customerDetails['telephoneAreaCode'].'" CountryAccessCode="'.$customerDetails['countryAccessCode'].'"
						 Extension="0" PhoneNumber="'.$customerDetails['phoneNumber'].'" PhoneTechType="'.$customerDetails['PhoneTechType'].'" />';
		$customer .= '<Email>'.$customerDetails['emailId'].'</Email>';
		$customer .= $this->addressSegment();
		$customer .= '</Customer>';
		return $customer;
	}

	private function personNameSegment()
	{
		$customerDetails = $this->requestData->customerDetails;
		$person   = '<PersonName>';
		$person   .= '<NamePrefix>'.$customerDetails['prefix'].'</NamePrefix>';
		$person   .= '<GivenName>'.$customerDetails['firstName'].'</GivenName>';
		$person   .= '<MiddleName />';
		$person   .= '<Surname>'.$customerDetails['surname'].'</Surname>';
		$person   .= '</PersonName>';
		return $person;

	}

	private function addressSegment()
	{
		$customerDetails = $this->requestData->customerDetails;
		$address = '<Address>';
		$address .= '<AddressLine>'.$customerDetails['addressLine1'].'</AddressLine>';
		$address .= '<AddressLine>'.$customerDetails['addressLine2'].'</AddressLine>';
		$address .= '<CityName>'.$customerDetails['cityName'].'</CityName>';
		$address .= '<PostalCode>'.$customerDetails['postalCode'].'</PostalCode>';
		$address .= '<StateProv>'.$customerDetails['stateProv'].'</StateProv>';
		$address .= '<CountryName>'.$customerDetails['countryName'].'</CountryName>';
		$address .= '</Address>';
		return $address;
	}

	private function resGlobalInfoSegment()
	{
		$globalInfo = '<ResGlobalInfo>';
		$globalInfo .= '<Guarantee GuaranteeType="'.$this->requestData->guaranteeType.'" />';
		$globalInfo .= '</ResGlobalInfo>';
		return $globalInfo;
	}

	/**
	 * Footer Closing Tags
	 *
	 * @return string
	 */
	private function setFooter ()
	{
		return '</soap:Body></soap:Envelope>';
	}







}


