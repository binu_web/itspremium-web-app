<?php
/**
 * Created by SubLime.
 * @author: Harinarayanan
 * @created on : 5/07/2017
 * Time: 10:01 AM
 */
namespace App\XMLRequests\Hotels\Yatra;

class HotelDetailSearchRQ
{
	private $requestData;

	public function __construct ($requestData = [])
	{

		$this->setRequestData ($requestData);
	}

	/**
	 * @return mixed
	 */
	
	public function getRequestData ()
	{
		return $this->requestData;
	}

	/**
	 * @param mixed $requestData
	 */
	
	public function setRequestData ($requestData)
	{
		$this->requestData = (object)($requestData);
	}

	/**
	 * Obtain Request XML
	 *
	 * @param $requestData
	 */
	
	public function requestXML ()
	{
		$xmlRequest = $this->generateXML ();

		return $xmlRequest;
	}


	/**
	 * @param $requestData
	 */
	public function generateXML ()
	{

		// Init Headers
		$xmlRequest = $this->setHeader ();
		$xmlRequest .= $this->setAvailRequestSegments ();
		$xmlRequest .= $this->setFooter ();

		return preg_replace ('/(\v|\s)+/' , ' ' , $xmlRequest);
	}


	/**
	 * Hotel Avail Request attributes :
	 *
	 * @attribute RequestCurrency : INR (Default   )
	 * @attribute Sort Order : TG_RANKING(default), PRICE (low to high), GUEST_RATING (high to low),
	 *            STAR_RATING_ASCENDING, STAR_RATING_DESCENDING, DEALS
	 * @return string
	 */

	public function setHeader ()
	{


		return '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				<soap:Body>
				<OTA_HotelAvailRQ xmlns="http://www.opentravel.org/OTA/2003/05" RequestedCurrency="' . $this->requestData->RequestedCurrency . '" SortOrder="' . $this->requestData->SortOrder . '" CorrelationID="'.$this->requestData->correlationId.'" SearchCacheLevel="Live">'; 
				
	}


	/**
	 * AvailRequestSegments Tag
	 *
	 * @return string
	 */
	public function setAvailRequestSegments ()
	{
		$availRequestSegements = "<AvailRequestSegments>";

		$availRequestSegements .= $this->setAvailRequestSegment ();

		$availRequestSegements .= "</AvailRequestSegments>";

		return $availRequestSegements;
	}


	/**
	 * AvailRequestSegment
	 *
	 * @return string
	 */
	public function setAvailRequestSegment ()
	{
		$availRequestSegement = "<AvailRequestSegment>";

		$availRequestSegement .= $this->setHotelSearchCriteria ();

		$availRequestSegement .= "</AvailRequestSegment>";

		return $availRequestSegement;
	}

	/**
	 * HotelSearchCriteria
	 *
	 * @return string
	 */
	public function setHotelSearchCriteria ()
	{
		$HotelSearchCriteria = "<HotelSearchCriteria>";

		$HotelSearchCriteria .= $this->setCriterion ();

		$HotelSearchCriteria .= "</HotelSearchCriteria>";

		return $HotelSearchCriteria;
	}


	/**
	 * HotelSearchCriteria
	 *
	 * @return string
	 */
	public function setCriterion ()
	{
		$Criterion = "<Criterion>";

		


		// Set Hotel Reference
		$Criterion .= $this->setHotelRef ();


		// Set Stay Date Range
		$Criterion .= $this->setStayDateRange ();


		
		// Room Stay Candidate
		$Criterion .= $this->setRoomStayCandidates ();


		// TPA Extension
		$Criterion .= $this->setTPAExtenstions ();


		$Criterion .= "</Criterion>";

		return $Criterion;
	}


	/**
	 * Hotel Ref
	 *
	 * @return string
	 */
	public function setHotelRef ()
	{
		
		$hotelRefTag = '<HotelRef HotelCode ="'.$this->requestData->hotelCode.'"/>';
		
		return $hotelRefTag;
	}


	/**
	 * Stay Date Range
	 *
	 * @return string
	 */
	public function setStayDateRange ()
	{
		$stayDateRange = '<StayDateRange End="' .$this->requestData->StayDateRangeEnd. '" Start="' .$this->requestData->StayDateRangeStart. '"/>';
		return $stayDateRange;
	}


	/**
	 * Room Stay Candidates
	 *
	 * @return string
	 */
	public function setRoomStayCandidates ()
	{
		$RoomStayCandidates = "<RoomStayCandidates>";

		foreach($this->requestData->roomDetails as $key => $aVal) :
			$index   = $key;
			$RoomStayCandidates .= $this->setRoomStayCandidate ($aVal, $index);
		endforeach;

		$RoomStayCandidates .= "</RoomStayCandidates>";

		return $RoomStayCandidates;
	}

	/**
	 * Set Room Stay Candidate
	 * 
	 *
	 * @return string
	 */
	public function setRoomStayCandidate ($aVal, $roomIndex)
	{   

		$RoomStayCandidate = "<RoomStayCandidate>";

		$RoomStayCandidate .= $this->setGuestCounts ($aVal, $roomIndex);

		$RoomStayCandidate .= "</RoomStayCandidate>";

		return $RoomStayCandidate;
	}


	/**
	 * Set Guest Counts Tag
	 *
	 * @return string
	 */
	public function setGuestCounts ($aGuestData, $roomIndex)
	{
		$GuestCounts = "<GuestCounts>";
			$GuestCounts .= $this->setGuestCount ($aGuestData, $roomIndex);
		$GuestCounts .= "</GuestCounts>";

		return $GuestCounts;
	}


	/**
	 * Set Guest Count
	 * Guest Count
	 *
	 * @AgeQualifyingCode :
	 *                    (value)10 Denotes Adult
	 *                    (value) 8 Denotes Child
	 *
	 * @return string
	 */
	public function setGuestCount ($aGuestData, $roomIndex)
	{
		$adultCount = $aGuestData['adult'];
		$aChild 	= $aGuestData['children'];

		$sReturn 	= '<GuestCount AgeQualifyingCode="10" count="'.$adultCount.'"/>';
		foreach($aChild as $key => $aChildNode):
			$childCount = $aChildNode['count'];
			$childNode  = '<GuestCount AgeQualifyingCode="8" Age ="'.$aChildNode['age'].'" Count="1"/>';
			$sReturn 	.= str_repeat($childNode, $childCount); //bcs we need to separate each childs in separate nodes. even they are in same age
		endforeach;

		return $sReturn;

	}


	/**
	 * @return string
	 */
	public function setTPAExtenstions ()
	{
		$TPA_Extensions = '<TPA_Extensions SeoEnabled= "false" >';

		$TPA_Extensions .= '<Pagination hotelsFrom="0" hotelsTo="0"/>';

		$TPA_Extensions .= $this->setUserAuthentication ();

		$TPA_Extensions .= "</TPA_Extensions>";

		return $TPA_Extensions;

	}



	/**
	 * Set User Authentication
	 *
	 * @return string
	 */
	public function setUserAuthentication ()
	{
		return '<UserAuthentication password="' . $this->requestData->password . '" propertyId="' . $this->requestData->propertyId . '" username="' . $this->requestData->username . '"/>';

	}


	/**
	 * Footer Closing Tags
	 *
	 * @return string
	 */
	public function setFooter ()
	{
		return '</OTA_HotelAvailRQ></soap:Body></soap:Envelope>';
	}










}
