<?php
/**
 * Prepara Yatra Hotel Provisional Booking Request
 * @author Harinarayanan 
 * @Created on 12-07-2017
 */
namespace App\XMLRequests\Hotels\Yatra;

class HotelFinalBookingRQ
{
	private $requestData;

	public function __construct ($requestData = [])
	{

		$this->setRequestData ($requestData);
	}

	/**
	 * @return mixed
	 */
	
	public function getRequestData ()
	{
		return $this->requestData;
	}

	/**
	 * @param mixed $requestData
	 */
	
	public function setRequestData ($requestData)
	{
		$this->requestData = (object)($requestData);
	}

	/**
	 * Obtain Request XML
	 *
	 * @param $requestData
	 */
	
	public function requestXML ()
	{
		$xmlRequest = $this->generateXML ();

		return $xmlRequest;
	}

	/**
	 * @param $requestData
	 */
	private function generateXML ()
	{

		// Init Headers
		$xmlRequest = $this->setHeader ();
		$xmlRequest .= $this->setAvailRequestSegments ();
		$xmlRequest .= $this->setFooter ();

		return preg_replace ('/(\v|\s)+/' , ' ' , $xmlRequest);
	}

	/**
	 * Hotel Avail Request attributes :
	 * @return string
	 */

	private function setHeader ()
	{


		return '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				<soap:Body>';
				
	}

	/**
	 * AvailRequestSegments Tag
	 *
	 * @return string
	 */
	private function setAvailRequestSegments ()
	{
		$availRequestSegements = '<OTA_HotelResRQ xmlns="http://www.opentravel.org/OTA/2003/05" CorrelationID="" TransactionIdentifier="'.$this->requestData->transactionIdentifier.'" Version="">';

		$availRequestSegements .= $this->setAvailRequestSegment ();

		$availRequestSegements .= "</OTA_HotelResRQ>";

		return $availRequestSegements;
	}


	private function setAvailRequestSegment()
	{
		$availRequestSegements = $this->setPOSSegment();
		$availRequestSegements .= '<UniqueID Type="'.$this->requestData->uniqueIdType.'" ID="'.$this->requestData->provisionalBookingId.'" />';
		$availRequestSegements .= $this->hotelReservationSegments();

		return $availRequestSegements;
	}

	private function setPOSSegment()
	{
		return 	'<POS>
					<Source ISOCurrency="INR">
						<RequestorID MessagePassword="'.$this->requestData->password.'" ID="'.$this->requestData->propertyId.'">
								<CompanyName Code="'.$this->requestData->userName.'"></CompanyName> 
						</RequestorID>
					</Source>
				</POS>';
	}

	private function hotelReservationSegments()
	{
		$hotelReservation = '<HotelReservations>
									<HotelReservation>
										<ResGlobalInfo>
											<Guarantee GuaranteeType="'.$this->requestData->guaranteeType.'"/>
										</ResGlobalInfo>
									</HotelReservation>
							</HotelReservations>';

		return $hotelReservation;

	}

	private function setFooter ()
	{
		return '</soap:Body></soap:Envelope>';
	}





}