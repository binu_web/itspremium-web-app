<?php

namespace App\Http\Middleware;

use Closure;
use Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Repositories\ApiRepository\UserApiRepository;


class AuthenticateApi
{


    /**
     * Handle Request if the request is valid one used Token
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        try {

			if ($user = JWTAuth::parseToken ()->toUser ()) {

				$user = \GuzzleHttp\json_decode ($user);
				if (!$user_exist = UserApiRepository::authenicateUserForJwt ($user->email , $user->id)) {
					return Response::json (['status' => 'error' , 'msg' => 'user not found'] , 404);
				}


			} else {
				return Response::json (['status' => 'error' , 'msg' => 'Unauthorized access'] , 401);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
			return response ()->json (['status' => 'error' , 'msg' => 'token expired'] , $e->getStatusCode ());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
			return response ()->json (['status' => 'error' , 'msg' => 'token invalid'] , $e->getStatusCode ());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
			return response ()->json (['status' => 'error' , 'msg' => 'token absent'] , $e->getStatusCode ());
		}
		catch (TokenBlacklistedException $e) {
			return response ()->json (['status' => 'error' , 'msg' => 'token blacklisted'] , $e->getStatusCode ());
		}
		catch (JWTException $e) {
			return response ()->json (['status' => 'error' , 'msg' => 'token invalid'] , $e->getStatusCode ());
		}

		return $next($request);
	}
}
