<?php

namespace App\Http\Controllers;

use App\DataTables\LocationDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateLocationRequest;
use App\Http\Requests\UpdateLocationRequest;
use App\Repositories\LocationRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\DB;
use App\Models\Service;
use App\Models\LocationService;
use App\Repositories\LocationServiceRepository;

class LocationController extends AppBaseController
{
	/** @var  LocationRepository */
	private $locationServiceRepository;
	private $LocationRepository;


	public function __construct (LocationServiceRepository $locationServiceRepo , LocationRepository $locationRepo)
	{
		$this->locationServiceRepository = $locationServiceRepo;
		$this->LocationRepository = $locationRepo;
	}

	/**
	 * Display a listing of the Location.
	 *
	 * @param LocationDataTable $locationDataTable
	 *
	 * @return Response
	 */
	public function index (LocationDataTable $locationDataTable)
	{
//        $query = DB::table('locations')
//            ->join('location_services','location_services.location','=','locations.id')
//            ->join('service_apis','service_apis.id','=','location_services.service')
//            ->select('location_services.*','service_apis.*')
////        ->where('locations.id','=','6')
//            ->get();

//dd($query);
		return $locationDataTable->render ('locations.index');
	}

	/**
	 * Show the form for creating a new Location.
	 *
	 * @return Response
	 */
	public function create ()
	{
		$service = new Service();
		$serviceArr = array();
		foreach ($service->get () as $serve) {
			if ($serve->toArray ()['is_active'] == 1) {
				$serviceCat = $serve->toArray ()['name'];
				foreach ($serve->serviceApisList->toArray () as $ser) {
					if ($ser['is_active'] == 1) {
						$serviceArr[$serviceCat][$ser['id']] = $ser['provider_name'];
					}
				}
			}
		}
		$selected_service = array();
		return view ('locations.create' , ['service' => $serviceArr , 'selected_service' => $selected_service]);
	}

	/**
	 * Store a newly created Location in storage.
	 *
	 * @param CreateLocationRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateLocationRequest $request)
	{
		$this->validate ($request , [
			'location' => 'required|max:255' ,
			'distance' => 'required' ,
			'service' => 'required' ,
		]);
		try {
			$input = $request->all ();
			$location = $this->LocationRepository->create ($input);
			if ($location) {
				$locationService = $this->locationServiceRepository->insert ($input , $location);
				Flash::success ('Location  saved successfully.');
				return redirect (route ('locations.index'));
			} else {
				Flash::error ('Some Technical problems');
				return redirect (route ('locations.create'));
			}
		}
		catch (\Exception $e) {
			Flash::error ('Invalid data');
			return redirect (route ('locations.create'));
		}

	}

	/**
	 * Display the specified Location.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$serviceNamesArr = array();
		$location = $this->LocationRepository->findWithoutFail ($id);
		if (empty($location)) {
			Flash::error ('Location not found');

			return redirect (route ('locations.index'));
		}
		foreach ($location->locationService ()->get () as $ser) {
			$serviceNamesArr[$ser->serviceApis->serviceCategory->name][] = $ser->serviceApis->provider_name;
		}
		$service = '';

		foreach ($serviceNamesArr as $key => $value) {
			$service = $key . ' : ' . implode (', ' , $value);
			$data[] = "\n\t \r" . $service;
		}
//        $serviceNames = implode(',', $serviceNamesArr);
		$created_user = $location->createdBy->first_name;
		$updated_user = $location->updatedBy->first_name;
		return view ('locations.show' , ['location' => $location , 'created_user' => $created_user , 'updated_user' => $updated_user , 'services' => implode (',' , $data)]);
	}

	/**
	 * Show the form for editing the specified Location.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$location = $this->LocationRepository->findWithoutFail ($id);
		if (empty($location)) {
			Flash::error ('Location not found');

			return redirect (route ('locations.index'));
		}
		$service = new Service();

		$serviceArr = array();
		foreach ($service->get () as $serve) {
			if ($serve->toArray ()['is_active'] == 1) {
				$serviceCat = $serve->toArray ()['name'];
				foreach ($serve->serviceApisList->toArray () as $ser) {
					if ($ser['is_active'] == 1) {
						$serviceArr[$serviceCat][$ser['id']] = $ser['provider_name'];
					}
				}
			}
		}
		$selArray = DB::table ('location_services')
			->where ('location_services.location' , '=' , $id)
			->select ('location_services.service')
			->get ();
		foreach ($selArray as $key => $value) {
			$selectedServiceArr[] = $value->service;
		}
		return view ('locations.edit' , ['service' => $serviceArr , 'selected_service' => $selectedServiceArr , 'location' => $location]);

	}

	/**
	 * Update the specified Location in storage.
	 *
	 * @param  int                  $id
	 * @param UpdateLocationRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateLocationRequest $request)
	{
		$location = $this->LocationRepository->findWithoutFail ($id);

		if (empty($location)) {
			Flash::error ('Location not found');

			return redirect (route ('locations.index'));
		}
		$locationService = $this->locationServiceRepository->update ($request->all () , $id);

		$location = $this->LocationRepository->updateLocation ($request->all () , $id);

		Flash::success ('Location updated successfully.');

		return redirect (route ('locations.index'));
	}

	/**
	 * Remove the specified Location from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$location = $this->LocationRepository->findWithoutFail ($id);

		if (empty($location)) {
			Flash::error ('Location not found');

			return redirect (route ('locations.index'));
		}

		$this->locationServiceRepository->delete ($id);
		$this->LocationRepository->delete ($id);

		Flash::success ('Location deleted successfully.');

		return redirect (route ('locations.index'));
	}
}
