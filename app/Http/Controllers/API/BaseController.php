<?php

namespace App\Http\Controllers\API;

use Illuminate\Routing\Controller;
use Response;

class BaseController extends Controller
{

	/**
	 * Create Common Success Reposne
	 *
	 * @param     $result  : The response that is requested for
	 * @param     $message : Message related to the success response
	 * @param int $code    : Http Status Code
	 *
	 * @author Binoop V
	 * @return mixed
	 */
	public function sendResponse ($message , $code = 200 , $result = [])
	{
		//echo 'result';
		//dd($result);
		//print_r($result);

		//$result['hotel_search'][0]['rooms'][0]['room_tax_amount'] = 2051.82;
		//die;
		$successResponse = ['status' => 'success' , 'msg' => $message , 'result' => $result];
		return response()->Json($successResponse);
		//return Response::json ($successResponse , $code);
	}

	/**
	 * Create Common Failure Response
	 *
	 * @param     $error : The error response of the issue
	 * @param int $code  : Http Status Code
	 *
	 * @author Binoop V
	 * @return mixed
	 */
	public function sendError ($error , $code = 404)
	{

		httpFailureLog ($error);


		if ($code == 0) {
			$code = 500;
			$error = 'system failure';
		}

		$errorResponse = ['status' => 'error' , 'msg' => $error];
		return Response::json ($errorResponse , $code);
	}


}
