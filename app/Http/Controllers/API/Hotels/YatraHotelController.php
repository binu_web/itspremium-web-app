<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 6/28/2017
 * Time: 4:32 PM
 */

namespace App\Http\Controllers\API\Hotels;


use App\Http\Controllers\API\BaseController;
use App\Services\HttpClient;
use App\XMLRequests\Hotels\Yatra\HotelAvailRQ;
use App\XMLRequests\Hotels\Yatra\HotelMultiVendorRQ;
use App\XMLRequests\Hotels\Yatra\HotelDetailSearchRQ;
use Illuminate\Http\Request;
use App\Services\XmlToJson;
use App\Models\YatraLocation;
use App\APIResponses\Hotels\Yatra\HotelSearchResult;
use Exception;
use App\Models\YatraHotelDetail;
use App\Services\Yatra\HotelBookingProcess;
use App\Services\Yatra\HotelCancelProcess;
use App\Models\ServiceBooked;

use JWTAuth;
use App\Repositories\ApiRepository\UserApiRepository;
use App\Repositories\ApiRepository\ServiceMetaKeyRepository;
use App\Repositories\ApiRepository\ServiceLogRepository;
use Log;

class YatraHotelController extends BaseController
{

	const YATRA_API_USER_NAME   = 'testsell';
	const YATRA_API_PROPERTY_ID = '1000000002';
	const YATRA_API_PASSWORD 	= 'test@123';
	const YATRA_API_SEARCH_URL  = 'http://stage-external.travelguru.com/services-2.0/tg-services/TGServiceEndPoint';
	const YATRA_API_BOOKING_URL = 'http://stage-external.travelguru.com/services-2.0/tg-services/TGBookingServiceEndPoint';
	const YATRA_SERVICE_API_ID  = 5;
	const SERVICE_CATEGORY_ID   = 3;
	const WEBHOOK_KEY_SECRET 	= '26pNuKkyhRdDqUhm6q73NFAi';
	const PROVI_BOOKING_UNIQUE_ID_TYPE = 23;
	const SERVICE_CHARGE 		= 2;
	const TAX_FOR_SERVICE_CHARGE = 1;


	protected $userApiRepo;
	protected $serviceMetaKeyRepo;
	protected $hotelSearchRepo;
	protected $oServiceRequestLog;

	/**
	 * Yatra Hotel Controller
	 * YatraHotelController constructor.
	 */
	public function __construct (\stdClass $stdObject)
	{
		$this->userApiRepo 			= property_exists ($stdObject , 'user_api') ? $stdObject->user_api : new UserApiRepository();
		$this->serviceMetaKeyRepo 	= property_exists ($stdObject , 'service_api') ? $stdObject->service_api : new ServiceMetaKeyRepository();
		$this->oServiceRequestLog    = new ServiceLogRepository();
		
	}

	/**
	 * Get Last search params
	*/

	public function getLastSearchParams($request)
	{
		try{
			$userId 		= JWTAuth::parseToken ()->toUser ()->id;
			$aLastSearchLog = $this->hotelSearchRepo->getLastSearch($userId);
			$aLastSearchLog  = $aLastSearchLog ? $aLastSearchLog : null;

			if(!$aLastSearchLog) 
				throw new Exception("no previous search found", 404);

			return [
				'search_log' => $aLastSearchLog,
			];
				
		}
		catch(Exception $e) {
			return ['error' => TRUE, 'message' => $e->getMessage(), 'code' => $e->getCode()];
		}
	}

	/**
	 * Location search
	 */

	public function searchLocation($request)
	{

		try {

			$aAllLocation = YatraLocation::getData();

			$keyword 	  = trim($request->get('key'));
			$keyword 	  = strtolower($keyword);

			$aMatchingLocations = [];
			$aMatchingCities 	= [];

			foreach($aAllLocation as $key => $aVal) {
				$_name   = strtolower($aVal['name']);
				$_type   = $aVal['type'];
				
				if($keyword && strpos($_name, $keyword) !== false) {
					if($_type == 1)
						$aMatchingCities[] = $aVal;
					else
						$aMatchingLocations[] = $aVal;
				}
			}

			//search in hotel:
			$aAllHotels 		= YatraHotelDetail::getAllHotels();
			$aMatchingHotels 	= []; //match with hotelnames

			foreach($aAllHotels as $key => $aVal) {
				$_hotelName = $aVal['hotel_name'];
				if($keyword && stripos($_hotelName, $keyword) !== false)
					$aMatchingHotels[] = $aVal;
			}

			if(!$aMatchingCities && !$aMatchingLocations && !$aMatchingHotels)
				throw new Exception('No result found', 404);


			$aResult = array('cities'   => array_slice($aMatchingCities, 0, 20),
							'locations' => array_slice($aMatchingLocations, 0, 20),
							'hotels'	=> array_slice($aMatchingHotels, 0, 20),
							);

			return $aResult;

		}
		catch (Exception $e) {
			return ['error' => TRUE, 'message' => $e->getMessage(), 'code' => $e->getCode()];
		}

		


	}
	

	/**
	 * Yatra Hotel Search
	 */
	public function getHotels (Request $request)
	{
		
		try{
			

            $oUser 					= JWTAuth::parseToken ()->toUser ();
			$userId 				= $oUser->id;
         

			$serviceEndPointUrl = Self::YATRA_API_SEARCH_URL; //need to keep in config file

			$locationId   = $request->get('location_id');
			$stayDateFrom = $request->get('stay_date_from');
			$stayDateTo   = $request->get('stay_date_to');
			
			$pageNum 	  = $request->get('page', 1);
			$roomDetails  = $request->get('room_details');  /* JSON encode format : [{"adult":1,"children":[{"age":10,"count":1}]}]*/
			$roomDetails  = json_decode($roomDetails, true);
			$sortOrder 	  = $request->get('sort_order');

			$stayDateFrom = formatDate('d-m-Y', 'Y-m-d', $stayDateFrom);
			$stayDateTo   = formatDate('d-m-Y', 'Y-m-d', $stayDateTo);
			$aResponse    = [];

			$aMandatory   = [$locationId, $stayDateFrom, $stayDateTo];

			$aError 	  = [];
			if(checkArrayIsEmpty($aMandatory) || !$roomDetails) {
				$aError[] = 'Mandatory fields are missing';
			}

		

			/*$roomDetails  = [
				["adult" => 1, "children" => [["age" => 10, "count" => 1]]],
				["adult" => 2, "children" => [["age" => 5, "count" => 1]]],
				["adult" => 1, "children" => []],
		   ]; *///this is not worked on booking


		    if(!$sortOrder)
				$sortOrder 			= 'TG_RANKING';
			
			$oLocation 	  = YatraLocation::find($locationId);
			$type 		  = isset($oLocation->type) ? $oLocation->type : null;
			$cityName 	  = isset($oLocation->name) ? $oLocation->name: null;
			$aAreaId      = isset($oLocation->area_id) ? $oLocation->area_id : null;

			$noOfHotelsPerPage = 20;
			$hotelsFrom 	   = sprintf('%02d', (($pageNum-1)*10 + 1));
			$hotelsTo 		   = sprintf("%02d", (($pageNum-1)*10 + $noOfHotelsPerPage));

			if(!$oLocation){
				$aError[] = 'Location not exists in db';
			}

			if($aError) {
				throw new Exception(implode(',', $aError), 400);
			}


			$aRequestData 	   = [
				'RequestedCurrency' 	=> 'INR',
				'SortOrder'				=> $sortOrder,
				'username'				=> Self::YATRA_API_USER_NAME,
				'propertyId'			=> Self::YATRA_API_PROPERTY_ID,
				'password'				=> Self::YATRA_API_PASSWORD,
				'is_paginate_enabled' 	=> 'true',
				'CountryName'		    => 'India',
				'hotelsFrom'		    => $hotelsFrom,
				'hotelsTo'			    => $hotelsTo,
				'CityName'			    => $cityName,
				'StayDateRangeStart'    => $stayDateFrom,
				'StayDateRangeEnd' 	    => $stayDateTo,
				'roomDetails'			=> $roomDetails,
				'searchCityType'		=> $type,
				'searchCityName'		=> $cityName,
				'searchAreaId'			=> $aAreaId


			];

			$oHotelAvail = new HotelAvailRQ($aRequestData);
			$file 		 = $oHotelAvail->requestXML ();
			//echo htmlspecialchars($file); die;

			// Set up Rest Client Request
			$client = new HttpClient(['headers' => ['Content-Type' => 'application/xml']]);

			$client->setUrl ($serviceEndPointUrl);
			$client->setParams ($file);

			// Post XML Request
			$response = $client->xmlPost ();
			$xmlResponse = $client->getResponseXML();
			#echo htmlspecialchars($xmlResponse); die;
			//echo "<pre>";
			//dd($response); die;

			// HandLe Error Response Check for Exception
			if ($response instanceof \Exception) {
				throw new \Exception($response->getMessage () , $response->getCode ());
			}

			/**
			 * Laravel Helper function to get the value inside an array
			 *
			 * more details @ https://laravel.com/docs/5.3/helpers#method-array-get
			 */

				
			
			//echo 'city search';
			#dd($response);
			//manage error:
			$response = array_get ($response , 'Envelope.soap:Body.OTA_HotelAvailRS'); //'
			$response = $response ? $response : [];

			//dd($response);
			// Handle Incase of Any Error In Response
			$error = $this->handleResponseError ($response);
			if ($error) {
		
				throw new Exception($error , 400);
			}

/*
			$aHotelData 			= array_get($response, 'RoomStays');
			
		
			$aHotelData 			= $aHotelData ? $aHotelData : [];*/
		


			if(!$response) {
				throw new Exception('No data found', 404);
			}

			$oHotelSearchResult 	= new HotelSearchResult($response);

				
			$aResponse 				= $oHotelSearchResult->getResponse();


			/*echo 'formated';
			dd($aResponse);*/


			//entry to search log:


			$aUserDetails 			 = [
				'customer_email_id' => $oUser->email,
				'customer_mobile'   => $oUser->mobile

			];

			return [
					'hotel_details' 	  			=> $aResponse,
					'service_charge' 				=> Self::SERVICE_CHARGE,
					'tax_for_service_charge' 		=> Self::TAX_FOR_SERVICE_CHARGE,
					'user_details'					=> $aUserDetails,
			];
			
			

		}
		catch(Exception $e) {
			return ['error' => TRUE, 'message' => $e->getMessage(), 'code' => $e->getCode()];
		}
	
	}

	/**
	 * Hotel details search 
	 * @param $request
	 *
	 * @author Harinarayanan
	 * 
	 */

	public function getHotelDetails($request)
	{
		try{
			
			$oUser 		  = JWTAuth::parseToken()->toUser();

			$stayDateFrom = $request->get('stay_date_from');
			$stayDateTo   = $request->get('stay_date_to');
			$hotelCode 	  = $request->get('hotel_code'); //case if user select hotel from the autocomplete
			$roomDetails  = $request->get('room_details');  /* JSON encode format : [{"adult":1,"children":[{"age":10,"count":1}]}]*/
			$roomDetails  = json_decode($roomDetails, true);
			$stayDateFrom = formatDate('d-m-Y', 'Y-m-d', $stayDateFrom);
			$stayDateTo   = formatDate('d-m-Y', 'Y-m-d', $stayDateTo);

			$aMandatory   = [$stayDateFrom, $stayDateTo, $hotelCode];

			$aError 	  = [];
			if(checkArrayIsEmpty($aMandatory) || !$roomDetails) {
				$aError[] = 'Mandatory fields are missing';
			}

			if($aError) {
				throw new Exception(implode(',', $aError), 400);
			}

			//get correlationId - next auto incrementind id:
			$correlationId 		= "44444";


			$aRequestData 	   = [
				'hotelCode'				=> $hotelCode,
				'RequestedCurrency' 	=> 'INR',
				'SortOrder'				=> '',
				'username'				=> Self::YATRA_API_USER_NAME,
				'propertyId'			=> Self::YATRA_API_PROPERTY_ID,
				'password'				=> Self::YATRA_API_PASSWORD,
				'is_paginate_enabled' 	=> 'false',
				'CountryName'		    => 'India',
				'hotelsFrom'		    => 0,
				'hotelsTo'			    => 1,
				'StayDateRangeStart'    => $stayDateFrom,
				'StayDateRangeEnd' 	    => $stayDateTo,
				'roomDetails'			=> $roomDetails,
				'correlationId'			=> $correlationId, //log AI ID get from http log table
			];

			$oHotelDetailsSearchRQ = new HotelDetailSearchRQ($aRequestData);
			
			$xmlRequest 		   = $oHotelDetailsSearchRQ->requestXML();

			 //start of section to insert into service log
	        $aRequestLog = [
	            'user_id' 			=> $oUser->id,
	            'service_id' 		=> Self::SERVICE_CATEGORY_ID,
	            'service_api_id' 	=> Self::YATRA_SERVICE_API_ID,
	            'event_type_id' 	=> 2,
	            'http_type' 		=> 'REQUEST',
	            'data_type' 		=> 'XML',
	            'data' 				=> serialize($xmlRequest),
	        ];

	        $rRequestLog = $this->oServiceRequestLog->addServiceLog($aRequestLog);
			
			#echo htmlentities($xmlRequest); die;

			$client = new HttpClient(['headers' => ['Content-Type' => 'application/xml']]);
			$client->setUrl (Self::YATRA_API_SEARCH_URL);
			$client->setParams ($xmlRequest);

			$response 		  = $client->xmlPost ();
			$xmlResponse 	  = $client->getResponseXML();

			 //start of section to insert into service log
	        $aRequestLog = [
	            'user_id' 			=> $oUser->id,
	            'service_id' 		=> Self::SERVICE_CATEGORY_ID,
	            'service_api_id' 	=> Self::YATRA_SERVICE_API_ID,
	            'event_type_id' 	=> 2,
	            'http_type' 		=> 'RESPONSE',
	            'data_type' 		=> 'XML',
	            'data' 				=> serialize($xmlResponse),
	        ];

        	$rRequestLog = $this->oServiceRequestLog->addServiceLog($xmlResponse);
			##echo htmlentities($xml); die;
		

			// HandLe Error Response Check for Exception
			if ($response instanceof \Exception) {
				throw new Exception($response->getMessage () , $response->getCode ());
			}

			$response = array_get ($response , 'Envelope.soap:Body.OTA_HotelAvailRS'); //'
			$response = $response ? $response : [];

			// Handle Incase of Any Error In Response
			$error = $this->handleResponseError ($response);
			if ($error) {
				throw new Exception($error , 400);
			}

			

			$oHotelSearchResult 	= new HotelSearchResult($response);

					
			$aResponse 				= $oHotelSearchResult->getResponse();

			$aUserDetails 			 = [
				'customer_email_id' => $oUser->email,
				'customer_mobile'   => $oUser->mobile

			];

			return [
					'hotel_details' 	  			=> isset($aResponse[0]) ? $aResponse[0] : null,
					'service_charge' 				=> Self::SERVICE_CHARGE,
					'tax_for_service_charge' 		=> Self::TAX_FOR_SERVICE_CHARGE,
					'user_details'					=> $aUserDetails,
			];
			
			//echo 'response';

			//dd($aResponse);

		}
		catch(Exception $e) {
			return ['error' => TRUE, 'message' => $e->getMessage(), 'code' => $e->getCode()];
		}
	}



	/**
	 * process provisional booking
	 * @param $request
	 *
	 * @author Harinarayanan
	 * 
	 */
	public function processProvisionalBooking(Request $request)
	{
		try{
			$oUser 					= JWTAuth::parseToken ()->toUser ();
			$userId 				= $oUser->id;
			#dd($oUser);

			$guestDetails 			= $request->get('room_details');
			$aGuestDetails 			= json_decode($guestDetails, true);
			$customerDetais 		= $request->get('customer_details');
			$aCustomerDetails 	    = json_decode($customerDetais, true);
			$hotelCode 				= $request->get('hotel_code');
			//$noOfUnits 				= $request->get('no_of_rooms');
			$roomTypeCode 		    = $request->get('room_type_code');
			$ratePlanCode 			= $request->get('rate_plan_code');
			$stayDateFrom 			= $request->get('stay_date_from');
			$stayDateTo 			= $request->get('stay_date_to');

			$baseRoomRateBeforeTax  = $request->get('base_room_rate_before_tax');
			$roomTaxAmount 			= $request->get('room_tax_amount');
			$addtionalGuestAmountBeforeTax = $request->get('additional_guest_amount_before_tax');
			$yatraTotalDiscount 		   = $request->get('yatra_total_discount');
			$netYatraRoomRateBeforeTax 	   = $request->get('net_yatra_amount_before_tax');
			$userComments 				   = $request->get('user_comments');
			$gauranteeType 			   	   = $request->get('gaurantee_type');
			$promoCode 				 	   = $request->get('promo_code');
			$taxName 					   = $request->get('tax_name');


		
			$noOfUnits 					= count($aGuestDetails);
			if(!$gauranteeType) 
				$gauranteeType 			= 'PrePay';

			$stayDateFrom 				= formatDate('d-m-Y', 'Y-m-d', $stayDateFrom);
			$stayDateTo 				= formatDate('d-m-Y', 'Y-m-d', $stayDateTo);

			$aMadatory 					= [$userId, $hotelCode, $roomTypeCode, $ratePlanCode, $stayDateFrom, $stayDateTo, $baseRoomRateBeforeTax, $roomTaxAmount,
			 $addtionalGuestAmountBeforeTax, $yatraTotalDiscount];

			$aError 					= [];
			$bValid						= true;
			if(checkArrayIsEmpty($aMadatory) || !$aGuestDetails || !$aCustomerDetails) {
				$aError[] = 'Mandatory fields are missing';
			}

			if($aError) {
				throw new Exception(implode(',', $aError), 400);
			}

			///////dummy data:
				/*$aGuestDetails = [
					["adult" => 1, "children" => [["age" => 10, "count" => 1]]],
					["adult" => 2, "children" => [["age" => 5, "count" => 1]]],
					["adult" => 1, "children" => []],
			   ];*/

			/*    $aGuestDetails  = [
					["adult" => 1, "children" => [["age" => 10, "count" => 1]]],
					["adult" => 2, "children" => []],
			   ];*/


		/*	$transactionId 		= '';
			$hotelCode 			= '00000153';
			$nofUnits 			= 3;
			$roomTypeCode 		=  "0000000317";
			$ratePlanCode 		=  "0000072542";
			$stayDateFrom 		= '2017-07-22';
			$stayDateTo   		= '2017-07-25';
			$yatraAPIAmountWithAddtionalGuestAmountBeforeTax 	= 67476;
			$yatraAPITaxAmount 		    		= 14794.88;
			$userComments 				= 'testing';
			$guaranteeType 				= 'PrePay';


			$aCustomerDetails 	= [
					'prefix' => 'Mr',
					'firstName' => 'Hari',
					'middileName' => 'ts',
					'surname'	=> 'tst',
					'telephoneAreaCode' => '78',
					'countryAccessCode' => '91',
					'extension'			=> 0,
					'phoneNumber'		=> '9745476712',
					'emailId'			=> 'hari@gmail.com',
					'addressLine1'		=> '13 line',
					'addressLine2'		=> '2nd ave',
					'cityName' 			=> 'Cochin',
					'postalCode'		=> '68012',
					'stateProv'			=> '',
					'countryName'		=> 'India',
					'PhoneTechType'			=> 5, 

				];

			*/

			//dummy date ends


			//amount need to pay will be calculated from Our server - using service charge, tax for service charge, promocode
			$oHotelBookingProcess 	    = new HotelBookingProcess();

			$aAmountDetails 			= $oHotelBookingProcess->calculateNetAmountToPay($netYatraRoomRateBeforeTax, $roomTaxAmount, $promoCode);
		
		

			$netAmountToPay 			= $aAmountDetails['net_amount_to_pay'];
			$aPromocodeDetails 			= $aAmountDetails['promocode_details'];
			$promoId 					= isset($aPromocodeDetails['promo_id']) ? $aPromocodeDetails['promo_id'] : null;
			$discountAmount 			= isset($aPromocodeDetails['discount_amount']) ? $aPromocodeDetails['discount_amount'] : null;
			$discountPercentage 		= (isset($aPromocodeDetails['discount_type']) && $aPromocodeDetails['discount_type'] == 1 && $aPromocodeDetails['discount']) ? $aPromocodeDetails['discount'] : null;
			$serviceCharge 				= $aAmountDetails['service_charge'];
			$serviceChargeTax 			= $aAmountDetails['service_charge_tax'];
			$serviceChargePercentage    = $aAmountDetails['service_charge_percentage'];
			$serviceChargeTaxPercentage = $aAmountDetails['service_charge_tax_percentage'];
			$serviceTaxName 			= $aAmountDetails['service_tax_name'];

		
	

			$request = [
				'transactionIdentifier' => '',
				'isoCurrency'			=> 'INR',
				'nofUnits'				=> $noOfUnits,
				'roomTypeCode' 			=> $roomTypeCode,
				'ratePlanCode'			=> $ratePlanCode,
				'guestCount'			=> $aGuestDetails,
				'StayDateRangeStart'    => $stayDateFrom,
				'StayDateRangeEnd' 		=> $stayDateTo,
				'AmountBeforeTax' 		=> $netYatraRoomRateBeforeTax,
				'currencyCode'			=> 'INR',
				'taxAmount'				=> $roomTaxAmount,
				'HotelCode'				=> $hotelCode,
				'userComments'			=> $userComments,
				'customerDetails'		=> $aCustomerDetails,
				'propertyId'			=> Self::YATRA_API_PROPERTY_ID,
				'password'				=> Self::YATRA_API_PASSWORD,
				'userName'				=> Self::YATRA_API_USER_NAME,
				'serviceUrl'			=> Self::YATRA_API_BOOKING_URL,
				'guaranteeType'			=> $gauranteeType,
				'userId'				=> $userId,
				'serviceApiId'			=> Self::YATRA_SERVICE_API_ID,
				'serviceId'				=> Self::SERVICE_CATEGORY_ID,
				'rateWithOutAdditionalGuestAmount' => $baseRoomRateBeforeTax,
				'additionalGuestAmount'			   => $addtionalGuestAmountBeforeTax,
				'serviceCharge'					   => $serviceCharge,
				'serviceChargeTax'				   => $serviceChargeTax,
				'promoId'						   => $promoId,
				'discountAmount'				   => $discountAmount,
				'discountPercentage'			   => $discountPercentage,
				'hotelDiscountAmount'			   => $yatraTotalDiscount,
				'netAmountToPay'				   => $netAmountToPay,
				'hotelTaxName'					   => $taxName,
				'serviceChargePercentage'		   => $serviceChargePercentage,
				'serviceChargeTaxPercentage' 	   => $serviceChargeTaxPercentage,
				'serviceTaxName'				   => $serviceTaxName,

			];



		
			$oHotelBookingProcess = new HotelBookingProcess($request);
			$response 			  = $oHotelBookingProcess->makeProvisionalBooking();

			return $response;
		}
		catch(Exception $e) {
			return ['error' => TRUE, 'message' => $e->getMessage(), 'code' => $e->getCode()];
		}
	


	}

	/*
	* process Final Booking - if amount > 0 this call will from payment gateway webhook
	*
	*
	*/
	public function processFinalBooking(Request $request)
	{

		try{

		#dd($request)
		/*$aError = [];
			
		//receive webhook and get values

		$razorPaySignature 		= $request->get('X-Razorpay-Signature');
		$requestBodyString 		= json_encode($request->toArray());
		$hash 					= hash_hmac('sha256', $requestBodyString, Self::WEBHOOK_KEY_SECRET);

		if($razorPaySignature != $hash) {
			Log::critical('unauthorized access to the razor pay webhook route');
			Log::critical('Request'. json_encode($request->toArray()));
			Log::critical('Terminating the process');
			die;
		}

		$event 			= $request->get('event');
		$payLoad 		= $request->get('payload');
		$aPayLoad 		= json_decode($payLoad, true);
		$aEntity 		= array_get($aPayLoad, 'payment.entity');
		$aNotes         = array_get($aPayLoad, 'payment.entity.notes');
		$aEntity 		= $aEntity ? $aEntity : [];*/
		
		//if($event == 'payment.authorized' && isset($aEntity['status']) && $aEntity['status'] == 'authorized') {

			/*$paymentReferanceId = isset($aEntity['id']) ? $aEntity['id'] : null;
			$bookingId 			= isset($aNotes['booking_id']) ? $aNotes['booking_id'] : null;

			if(!$paymentReferanceId || !$bookingId) {
				Log::critical('Either Payment reference id or booking Id is absent');
				Log::critical('Request '.json_encode($request->toArray()));
				Log::critical('Terminating the process');
				die;
			}*/

			//temp setup
			$bookingId = $request->get('booking_id');
			$paymentReferanceId = null;

			//
			

			$oServiceMetaKeyRepository = new ServiceMetaKeyRepository();
			$aWhere 				   = ['service_id' => Self::SERVICE_CATEGORY_ID, 'service_api_id' => Self::YATRA_SERVICE_API_ID, 'booking_id' => $bookingId];
			$provisionalBookingId 	   = $oServiceMetaKeyRepository->getValidYatraProvisionalBookingId($aWhere);
			#die($provisionalBookingId);
			if(!$provisionalBookingId) {
				throw new Exception('Invalid booking id or provisional id ('.$bookingId.')', 500);
			}

			$oServiceBooked = ServiceBooked::find($bookingId);
			$userId = isset($oServiceBooked->user_id) ? $oServiceBooked->user_id : null;

			$aRequest = [
				'transactionIdentifier' => $bookingId,
				'propertyId'			=> Self::YATRA_API_PROPERTY_ID,
				'password'				=> Self::YATRA_API_PASSWORD,
				'userName'				=> Self::YATRA_API_USER_NAME,
				'serviceUrl'			=> Self::YATRA_API_BOOKING_URL,
				'uniqueIdType'			=> Self::PROVI_BOOKING_UNIQUE_ID_TYPE,
				'provisionalBookingId'	=> $provisionalBookingId,
				'guaranteeType'			=> 'PrePay',
				'serviceApiId'			=> Self::YATRA_SERVICE_API_ID,
				'serviceId'				=> Self::SERVICE_CATEGORY_ID,
				'paymentReferanceId'	=> $paymentReferanceId,
				'userId' 				=> $userId,

			];


			$oHotelBookingProcess = new HotelBookingProcess($aRequest);
			$aFinalBooking 		  = $oHotelBookingProcess->makeFinalBooking();

			

			
			return $aFinalBooking;

		}
		catch(Exception $e) {
			return ['error' => TRUE, 'message' => $e->getMessage(), 'code' => $e->getCode()];
		}


		
	}

	/*
	* get booking status 
	*
	*
	*/
	public function getBookingStatus($request)
	{
		try{

			$oUser 			    = JWTAuth::parseToken ()->toUser ();
			$userId 		    = $oUser->id;
			$bookingId 			= $request->get('booking_id');
			$aReturn 			= $this->serviceMetaKeyRepo->getYatraBookingStatus($bookingId, $userId);
			$error 			    = $aReturn['sError'];
			if($error) {
				throw new Exception ($error, 400);
			}
			$aBookingDetails    = $aReturn['aData'];

			
			$oHotelBookingProcess = new HotelBookingProcess();
			$aFormattedResult 	  = $oHotelBookingProcess->formatBookingDetails($aBookingDetails);
			return $aFormattedResult;


		}
		catch(Exception $e) {
			return ['error' => TRUE, 'message' => $e->getMessage(), 'code' => $e->getCode()];
		}


	}

	public function processCancel($request){

		try{

			$bookingId = $request['booking_id'];
			$cancelDates = $request['cancel_dates'];





			$oServiceMetaKeyRepository = new ServiceMetaKeyRepository();
			$aWhere 	= ['service_id' => Self::SERVICE_CATEGORY_ID, 'service_api_id' => Self::YATRA_SERVICE_API_ID, 'booking_id' => $bookingId];
			$yatraBookingDetails = $oServiceMetaKeyRepository->getYatraBookingDetails($aWhere);
			if (checkArrayIsEmpty([$yatraBookingDetails]))
					throw new Exception('No booking available for this booking id' , 400);

			$aCancelDate = explode(",",$cancelDates);


//			$previousValue = null;
//			foreach($aCancelDate as $Dates)
//			{
//				if (($Dates < $yatraBookingDetails['stay_start_date']) || ($Dates >= $yatraBookingDetails['stay_end_date']) ) {
//					throw new Exception('Invalid cancel dates', 400);
//				}
//
//
////				$diff = abs(strtotime($Dates) - strtotime($previousValue));
////				$previousValue = $Dates;
////				$years = floor($diff / (365*60*60*24));
////				$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
////				$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
////
////				echo '--'.$days;
//			}


			//checking the cancel dates in between start and end date of booking, dates are in series

			$paymentReferanceId = null;
			$aRequest = [
				'transactionIdentifier' 	=> $bookingId,
				'propertyId'				=> Self::YATRA_API_PROPERTY_ID,
				'password'					=> Self::YATRA_API_PASSWORD,
				'userName'					=> Self::YATRA_API_USER_NAME,
				'serviceUrl'				=> Self::YATRA_API_BOOKING_URL,
				'uniqueIdType'				=> Self::PROVI_BOOKING_UNIQUE_ID_TYPE,
				'provisionalBookingId'		=> $yatraBookingDetails['provisional_booking_unique_id'],
				'customer_surname'			=> $yatraBookingDetails['customer_surname'],
				'customer_email_id'			=> $yatraBookingDetails['customer_email_id'],
				'stay_start_date'			=> $yatraBookingDetails['stay_start_date'],
				'stay_end_date'				=> $yatraBookingDetails['stay_end_date'],
				'final_booking_unique_id'	=> $yatraBookingDetails['final_booking_unique_id'],
				'serviceApiId'			 	=> Self::YATRA_SERVICE_API_ID,
				'serviceId'					=> Self::SERVICE_CATEGORY_ID,
				'paymentReferanceId'		=> $paymentReferanceId,
				'cancelType'				=> 'Initiate',
				'cancelDates'				=> $aCancelDate,
			];


			$oHotelCancelProcess  = new HotelCancelProcess($aRequest);
			$aProcessCancel		  = $oHotelCancelProcess->processCancel();

			$aError 			  = $aProcessCancel['aError'];

			return $aProcessCancel;


		} catch (Exception $e) {
			return ['error' => TRUE, 'message' => $e->getMessage()];
		}

	}

	public function confirmCancel($bookingId)
	{
		try{
			$oServiceMetaKeyRepository = new ServiceMetaKeyRepository();
			$aWhere 	= ['service_id' => Self::SERVICE_CATEGORY_ID, 'service_api_id' => Self::YATRA_SERVICE_API_ID, 'booking_id' => $bookingId];
			$yatraBookingDetails = $oServiceMetaKeyRepository->getYatraBookingDetails($aWhere);
			if (checkArrayIsEmpty([$yatraBookingDetails]))
				throw new Exception('No booking available for this booking id' , 400);

			$paymentReferanceId = null;
			$aRequest = [
				'transactionIdentifier' 	=> $bookingId,
				'propertyId'				=> Self::YATRA_API_PROPERTY_ID,
				'password'					=> Self::YATRA_API_PASSWORD,
				'userName'					=> Self::YATRA_API_USER_NAME,
				'serviceUrl'				=> Self::YATRA_API_BOOKING_URL,
				'uniqueIdType'				=> Self::PROVI_BOOKING_UNIQUE_ID_TYPE,
				'provisionalBookingId'		=> $yatraBookingDetails['provisional_booking_unique_id'],
				'customer_surname'			=> $yatraBookingDetails['customer_surname'],
				'customer_email_id'			=> $yatraBookingDetails['customer_email_id'],
				'stay_start_date'			=> $yatraBookingDetails['stay_start_date'],
				'stay_end_date'				=> $yatraBookingDetails['stay_end_date'],
				'final_booking_unique_id'	=> $yatraBookingDetails['final_booking_unique_id'],
				'serviceApiId'			 	=> Self::YATRA_SERVICE_API_ID,
				'serviceId'					=> Self::SERVICE_CATEGORY_ID,
				'paymentReferanceId'		=> $paymentReferanceId,
				'cancelType'				=> 'Cancel',
			];

			$oHotelCancelConfirm  = new HotelCancelProcess($aRequest);
			$aConfirmCancel		  = $oHotelCancelConfirm->confirmCancel();

			$aError 			  = $aConfirmCancel['aError'];

			return $aConfirmCancel;


		} catch (Exception $e) {
			return ['error' => TRUE, 'message' => $e->getMessage()];
		}
	}




	/*
	* update search as favourite
	*
	*
	*/

	public function setAsFavouriteSearch($request)
	{
		try{
			$oUser 		 = JWTAuth::parseToken()->toUser();
			$userId 	 = $oUser->id;
			$isFavourite = $request->get('is_favourite'); //1 or 0;
			$locationId  = $request->get('location_id');
			if(!$isFavourite)
				$isFavourite = 0;

			$aMandatory  = [$userId, $locationId];

			$bValid 	 = true;
			if(checkArrayIsEmpty($aMandatory)) {
				$bValid = false;
			}

			if(!$bValid) {
		
				throw new Exception ('Mandatory fields are missing', 400);
			}


			$update = $this->hotelSearchRepo->setasFavourite($isFavourite, $locationId, $userId);

			$message = 'Location marked as favourite';
			if(!$isFavourite)
				$message = 'Location removed from favourite list';


			return [
				'message'    => $message,
			];




		}
		catch(Exception $e) {

			return  ['error' => TRUE, 'message' => $e->getMessage(), 'code' => $e->getCode()];
		}
		
	}

	/*
	* get search log
	*
	*
	*/

	public function getSearchLog($request)
	{
		try{
			$oUser  = JWTAuth::parseToken()->toUser();
			$userId = $oUser->id;
			$aData  = $this->hotelSearchRepo->getLog($userId);
			$aData  = [];
			if(!$aData)
				throw new Exception('No search log found', 404);

			return [
				'search_log' => $aData
			];
		}
		catch(Exception $e) {
			return  ['error' => TRUE, 'message' => $e->getMessage(), 'code' => $e->getCode()];
		}
	}

	/*
	* get booking history
	*
	*
	*/

	public function getBookingHistory($request)
	{
		try{
			$oUser  = JWTAuth::parseToken()->toUser();
			$userId = $oUser->id;
			

			$aData  = $this->serviceMetaKeyRepo->getBookingHistory($userId, Self::SERVICE_CATEGORY_ID, Self::YATRA_SERVICE_API_ID);
			
			if(!$aData)
				throw new Exception('No search log found', 404);

			return [
				'booking_history' => $aData,
			];

		}	
		catch(Exception $e) {
			return  ['error' => TRUE, 'message' => $e->getMessage(), 'code' => $e->getCode()];
		}
	}





	/**
	 * Handle Response Error From Yatra
	 *
	 * @param $response
	 */
	public function handleResponseError ($response)
	{
		
		if (isset($response['Errors'])) {
			$oHotelBookingProcess = new HotelBookingProcess();
			return $oHotelBookingProcess->yatraErrorResponse($response['Errors']);
		
		} else {
			return false;
		}
	}


}



