<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 6/20/2017
 * Time: 4:43 PM
 */

namespace App\Http\Controllers\API\Hotels;


use App\Http\Controllers\API\BaseController;
use App\Services\HttpClient;
use Illuminate\Http\Request;
use Exception;
use App\Repositories\ApiRepository\ServiceMetaKeyRepository;
use App\Repositories\ApiRepository\UserApiRepository;
use Log;
use App\Repositories\ApiRepository\HotelSearchLogRepository;
use JWTAuth;


class HotelController extends BaseController
{
	/**
	 * Define Namespace
	 *
	 * @var string
	 */
	public $namespace = 'App\Http\Controllers\API\Hotels\\';

	public $userApiRepo;
	public $serviceMetaKeyRepo;
	public $stdObject;
	public $hotelSearchRepo;
	/**
	 * Hotel Controller constructor.
	 * Load Default Definitions
	 */
	public function __construct (UserApiRepository $userApiRepository, ServiceMetaKeyRepository $serviceMetaKeyRepository)
	{	

		$this->userApiRepository = $userApiRepository;
		$this->serviceMetaKeyRepo = $serviceMetaKeyRepository;

		$this->stdObject = new \stdClass();

		$this->stdObject->user_api 		= $this->userApiRepository;
		$this->stdObject->service_api 	= $this->serviceMetaKeyRepo;
		$this->hotelSearchRepo        	= new HotelSearchLogRepository();
	}

	/**
	 * Get Last search params
	*/

	public function getLastSearchParams(Request $request)
	{
		try{
			$controller = new YatraHotelController($this->stdObject);

			$aResponse  = $controller->getLastSearchParams ($request);

			if(is_array($aResponse) && isset($aResponse['error'])) 
				throw new Exception($aResponse['message'], $aResponse['code']);

			return $this->sendResponse ('last search params' , 200 , $aResponse);
		}
		catch(Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}



	/**
	 * Hotels Serach - Functionality
	*/

	public function searchLocation(Request $request)
	{
		try {

			$controller = new YatraHotelController($this->stdObject);

			$aResponse = $controller->searchLocation ($request);
			/*echo 'out';
			dd($aResponse);*/


			if(is_array($aResponse) && isset($aResponse['error'])) 
				throw new Exception($aResponse['message'], $aResponse['code']);

			return $this->sendResponse ('location autocomplete' , 200 , $aResponse);

		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}




	/**
	 * Hotels Serach - Functionality
	 */
	public function getHotels (Request $request)
	{
/*		$this->extractDumbExcel();
		die;*/
		try {

			// Get
			$hotelCode  = $request->get('hotel_code');
			$userId 	= JWTAuth::parseToken ()->toUser ()->id;
			$controller = new YatraHotelController($this->stdObject);
			if(!$hotelCode) {
				$aResponse  = $controller->getHotels ($request);
			}
			else {
				$aResponse = $controller->getHotelDetails ($request);
			}
			
			

			if(is_array($aResponse) && isset($aResponse['error'])) 
				throw new Exception($aResponse['message'], $aResponse['code']);

			$this->hotelSearchRepo->insertLog($request, $userId);

			return $this->sendResponse ('hotel search detail' , 200 , $aResponse);
			
			


		}
		catch (Exception $e) {
			
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/*
	*
	* This api is for booking test purpose
	*
	*
	*/
	public function getHotelDetails(Request $request)
	{
		try {


			// Get

			$controller = new YatraHotelController($this->stdObject);

			
			$aResponse = $controller->getHotelDetails ($request);

			if(is_array($aResponse) && isset($aResponse['error'])) 
				throw new Exception($aResponse['message'], $aResponse['code']);



			return $this->sendResponse ('hotel details' , 200 , $aResponse);


		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/*
	*
	*
	*
	*
	*/

	public function processProvisionalBooking(Request $request)
	{
		try {

			// Get
			$controller 			= new YatraHotelController($this->stdObject);
			$aResp   				= $controller->processProvisionalBooking($request);


			

			if(is_array($aResp) && isset($aResp['error'])) 
				throw new Exception($aResp['message'], $aResp['code']);

			$provisionalBookingId 	= $aResp['provisionalBookingId'];
			$bookingId 			  	= $aResp['bookingId'];
			$yatraFinalBookingId  	= $aResp['yatraFinalBookingId'];
			$netAmountToPay 	  	= $aResp['netAmountToPay'];


			$aResponse = [
				'booking_status' 		 => $yatraFinalBookingId ? 'Completed' : 'In progress',
				'net_amount_to_pay' 	 => $netAmountToPay,
				'provisional_booking_id' => $provisionalBookingId,
				'booking_id'			 => $bookingId,
				'yatra_final_booking_id' => $yatraFinalBookingId
			];

			$message = '';
			if($yatraFinalBookingId)
				$message = 'Hotel booked successfully';
			else if($provisionalBookingId)
				$message = 'Hotel booking in progress. Please proceed payment';

			return $this->sendResponse($message, 200, $aResponse);

			

		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	public function processFinalBooking(Request $request)
	{
		try {

			// Get

			$controller = new YatraHotelController($this->stdObject);

			$aResponse = $controller->processFinalBooking($request);

			if(is_array($aResponse) && isset($aResponse['error'])) 
				throw new Exception($aResponse['message'], $aResponse['code']);



			return $this->sendResponse ('Hotel booking completed successfully', 200, $aResponse);


		}
		catch (Exception $e) {
			Log::critical('Error While Final Booking Yatra Hotel:');
			Log::critical($e->getMessage ());
			Log::critical('Error Ends');
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/*
	*
	*
	*
	*
	*/

	public function getBookingStatus(Request $request)
	{
		try {


			// Get

			$controller = new YatraHotelController($this->stdObject);

			$aData = $controller->getBookingStatus($request);
			
			if(is_array($aData) && isset($aData['error'])) 
				throw new Exception($aResponse['message'], $aResponse['code']);


			return $this->sendResponse ('Booking completed successfully', 200, $aData);


		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
		
	}


	/**
	*
	*
	*
	*
	*/
	public function setAsFavouriteSearch(Request $oRequest)
	{
		try {

			// Get

			$controller = new YatraHotelController($this->stdObject);

							
			$aReturn = $controller->setAsFavouriteSearch($oRequest);

			if(is_array($aReturn) && isset($aReturn['error'])) 
				throw new Exception($aReturn['message'], $aReturn['code']);


		
			return $this->sendResponse ($aReturn['message'], 200, null);


		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/**
	*
	*
	*
	*
	*/

	public function getSearchLog(Request $oRequest)
	{
		try {

			// Get

			$controller = new YatraHotelController($this->stdObject);

			$aData = $controller->getSearchLog($oRequest);

			if(is_array($aData) && isset($aData['error'])) 
				throw new Exception($aData['message'], $aData['code']);

			return $this->sendResponse ('', 200, $aData);


		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/**
	*
	*
	*
	*
	*/

	public function getBookingHistory(Request $oRequest)
	{
		try {


			// Get

			$controller = new YatraHotelController($this->stdObject);
			$aData 		= $controller->getBookingHistory($oRequest);

			if(is_array($aData) && isset($aData['error'])) 
				throw new Exception($aData['message'], $aData['code']);

			return $this->sendResponse ('', 200, $aData);


		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * @param Request $request
	 * @author Midhun
	 * Initialize cancel in hotel booking
	 */
	public function processCancel(Request $request)
	{

		try {

			$service = $request->has ('service') ? $request->get ('service') : null;
			$booking_id = $request->has('booking_id') ? $request->get('booking_id') : null;
			$cancel_dates = $request->has('cancel_dates') ? $request->get('cancel_dates') : null;

			// Check if service key word exists
			if (checkArrayIsEmpty([$service]))
				throw new Exception('service key word required', 400);

			// Check if restaurant id is requested
			if (checkArrayIsEmpty([$booking_id]))
				throw new Exception('booking id required', 400);

			// Check if restaurant id is requested
			if (checkArrayIsEmpty([$cancel_dates]))
				throw new Exception('Cancel dates required', 400);

			// Create Class Based on Service
			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service accessed' , 403);

			// Create hotel Object
			$hotel = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($hotel , 'processCancel'))
				throw new Exception('invalid service request accessed' , 404);

			$api_request = [
				'booking_id' => $booking_id,
				'cancel_dates' => $cancel_dates,
			];

			// Request Service Based Requests
			$details = $hotel->processCancel ($api_request);
			// Booking Failed
			if (is_array($details) && isset($details['error']))
				throw new Exception($details['message'], 500);

			return $this->sendResponse('Cancellation initialed  successfully', 200, $details);

		}
		catch (Exception $e) {
		return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	public function confirmCancel(Request $request)
	{
		try {

			$service = $request->has ('service') ? $request->get ('service') : null;
			$booking_id = $request->has('booking_id') ? $request->get('booking_id') : null;

			$dates = $request->has('dates') ? $request->get('dates') : null;

			// Check if service key word exists
			if (checkArrayIsEmpty([$service]))
				throw new Exception('service key word required', 400);

			// Check if restaurant id is requested
			if (checkArrayIsEmpty([$booking_id]))
				throw new Exception('booking id required', 400);

			// Create Class Based on Service
			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service accessed' , 403);

			// Create hotel Object
			$hotel = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($hotel , 'confirmCancel'))
				throw new Exception('invalid service request accessed' , 404);

			// Request Service Based Requests
			$details = $hotel->confirmCancel ($booking_id);
			// Booking Failed
			if (is_array($details) && isset($details['error']))
				throw new Exception($details['message'], 500);

			return $this->sendResponse('Booking cancelled successfully', 200, $details);

		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	


}