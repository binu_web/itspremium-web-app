<?php
namespace App\Http\Controllers\API;

use App\Models\NewsCategory;
use App\Models\NewsBookmark;
use App\Repositories\ApiRepository\NewsCategoryRepository;
use App\Repositories\ApiRepository\NewsRepository;
use Exception;
use Illuminate\Http\Request;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class NewsController extends BaseController
{
    /**
     * NewsController constructor.
     *
     */
    public function __construct(NewsCategoryRepository $NewsCategoryRepository, NewsRepository $NewsRepository)
    {
        $this->newsCategoryRepository = $NewsCategoryRepository;
        $this->newsRepository = $NewsRepository;
    }


    /**
     * @author Midhun
     * @return mixed
     * Return news categories on request
     */
    public function category()
    {
        try {

            $result = $this->newsCategoryRepository->getCategory();

            if (empty($result))
                throw new Exception('news category is empty', 500);

            // Return Successful Token
            return $this->sendResponse('news category listed', 201, $result);

        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $e->getCode());
        }


    }

    /**
     * List all news with categories with created_at desc
     * @author Midhun
     */
    public function newsList(Request $request)
    {
        try {
            $category = $request->get('category');

            if (!empty($category))
                $result = $this->newsRepository->getNewsBasedCategory($category);
            else
                $result = $this->newsRepository->getNews();

            if (empty($result))
                throw new Exception('news list is empty', 500);

            // Return Successful Token
            return $this->sendResponse('news listed', 201, $result);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Get single news
     * @param $id
     * @author Midhun
     * @return mixed
     */

    public function news($id = null)
    {

        try {

            $input = array($id);

            if (checkArrayIsEmpty($input))
                throw new Exception('Request is invalid', 400);

            $result = $this->newsRepository->getNews_id($id);
            if (empty($result))
                throw new Exception('news is empty', 500);

            // Return Successful Token
            return $this->sendResponse('news listed', 201, $result);

        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * save single news
     * @param news_id
     * @author Frijo
     */

    public function setNewsBookmark(Request $request)
    {
        try {
            // check request data
            if ($request->has ('error'))
                throw new Exception($request->get ('error') , 403);
            //user login  Details
            $user = JWTAuth::parseToken ()->toUser ();

            //check news id
            $news_id = $request->has ('news_id') ? $request->get ('news_id') : null;
            //check is save field

            if (checkArrayIsEmpty ([$news_id]))
                throw new Exception('News id missing' , 400);

            if($request->get ('news_book_id')){
                $request = [
                    'news_id'=>$news_id,
                    'news_book_id'=>$request->get ('news_book_id'),
                    'is_saved'=>$request->get('is_saved'),
                ];
                $result = $this->newsRepository->setNewsById($request,$user->id);
            }else{

                $result = $this->newsRepository->setNews($news_id,$user->id);
            }

            // Return Successful message
            return $this->sendResponse('news saved', 201, $result);

        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }
    /**
     * List News
     * @param news_id,user_id
     * @author Frijo
     *
     */
    public function getBookNewsList(Request $request){
        try {

            //user login  Details
           $user = JWTAuth::parseToken()->toUser();

            //get news data
            $result = $this->newsRepository->getNewsById($user->id);

            //check news result
            if (empty($result))
                throw new Exception('news is empty', 500);
            // return success news list
            return $this->sendResponse('news list', 201, $result);

        }catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $e->getCode());
        }
    }
    /**
     * Set News Log
     * @param news id,sourse link
     * author Frijo
     */
    public function setNewsLog(Request $request){

        //user login  Details
        $user = JWTAuth::parseToken()->toUser();
        //check news id
        $news_id = $request->has ('news_id') ? $request->get ('news_id') : null;
        //check source_link
        $source_link = $request->has ('source_link') ? $request->get ('source_link') : null;

        //check is save field
            if (checkArrayIsEmpty ([$news_id]))
                throw new Exception('News fields missing' , 400);
        $request = [
            'news_id'=>$news_id,
            'source_link'=>$request->get('source_link'),
        ];
        //get news data
        $result = $this->newsRepository->setNewsLog($request,$user->id);

        // Return Successful message
            return $this->sendResponse('news saved', 201, $result);
    }


}