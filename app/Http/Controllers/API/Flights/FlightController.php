<?php
/**
 * Created by PhpStorm.
 * User: binu
 * Date: 6/20/2017
 * Time: 4:43 PM
 */

namespace App\Http\Controllers\API\Flights;


use App\Http\Controllers\API\BaseController;
use Illuminate\Http\Request;
use App\Repositories\ApiRepository\ServiceMetaKeyRepository;
use App\Repositories\ApiRepository\UserApiRepository;
use Log;
use stdClass;
use Exception;

class FlightController extends BaseController
{


	public $userApiRepo;
	public $serviceMetaKeyRepo;
	public $stdObject;

	/**
	 * FlightController constructor.
	 * Load Default Definitions
	 */
	public function __construct (UserApiRepository $userApiRepository, ServiceMetaKeyRepository $serviceMetaKeyRepository)
	{
		$this->userApiRepository = $userApiRepository;
		$this->serviceMetaKeyRepo = $serviceMetaKeyRepository;

		$this->stdObject = new stdClass();

		$this->stdObject->user_api = $this->userApiRepository;
		$this->stdObject->service_api = $this->serviceMetaKeyRepo;
	}


	/*
	* Airport/city auto complete
	* if reqd then we can avoid this call & make api call directly to the vendor from mobile
	*
	*/
	
	public function getAirPorts()
	{

	}
	

	/**
	 * Hotels Serach
	 */
	public function searchFlights (Request $request)
	{
		try {
			$controller = new YatraController($this->stdObject);


			$aResponse = $controller->searchFlights ($request);
			
			

			if(is_array($aResponse) && isset($aResponse['error'])) 
				throw new Exception($aResponse['message'], $aResponse['code']);

			return $this->sendResponse ('location autocomplete' , 200 , $aResponse);
		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}
}