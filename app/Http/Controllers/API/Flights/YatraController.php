<?php
/**
 * To manage Yatra Flight services
 *@author Harinarayanan T
 * Date: 26/07/2017
 * 
 */

namespace App\Http\Controllers\API\Flights;

use App\Http\Controllers\API\BaseController;
use App\Services\HttpClient;
use Exception;
use Illuminate\Http\Request;
use Log;
use stdClass;
use App\APIResponses\Flights\Yatra\FlightSearchResult;


class YatraController extends BaseController
{
	protected $userApiRepo;
	protected $serviceMetaKeyRepo;

	const SANDBOX_SERVICE_URL 	 = 'https://flight.yatra.com/air-service';
	const PRODUCTION_SERVICE_URL = 'https://flight.yatra.com/air-service';
	const TENENT_NAME = 'abc';


	public function __construct(stdClass $stdObject)
	{
		$this->userApiRepo 			= property_exists ($stdObject , 'user_api') ? $stdObject->user_api : new UserApiRepository();
		$this->serviceMetaKeyRepo 	= property_exists ($stdObject , 'service_api') ? $stdObject->service_api : new ServiceMetaKeyRepository();
	}


	/*
	*
	*
	*
	*/
	public function searchFlights($request)
	{
		try {
			$type 						= $request->get('type');
			$origin 					= $request->get('origin');
			$originCountry 				= $request->get('origin_country');
			$destination 				= $request->get('destination');
			$destinationCountry 		= $request->get('destination_country');
			$flightDepartDate 			= $request->get('flight_depart_date');
			$noOfAdults 				= $request->get('no_of_adults');
			$noOfChildren 				= $request->get('no_of_children');
			$noOfInfants 				= $request->get('no_of_infants');
			$flightReturnDate 			= $request->get('flight_return_date'); //format : dd/mm/yyyyy
			$class 						= $request->get('class');

			$aMandatory 				= [$type, $origin, $destination,$flightDepartDate,$noOfAdults,$noOfChildren,$noOfInfants];
			if($type == 'R')
				$aMandatory[] 			= $flightReturnDate;

			if(checkArrayIsEmpty($aMandatory)) {
				new Exception ('Mandatory Fields are missing', 400);
			}

			if(!$originCountry)
				$originCountry = 'IN';
			if(!$destinationCountry)
				$destinationCountry = 'IN';

			
			$aRequest = [
				'type' 			 		=> $type,
				'viewName' 		 		=> 'normal',
				'flexi'  				=> 0,
				'noOfSegments' 			=> 1,
				'origin' 				=> $origin,
				'originCountry' 		=> $originCountry,
				'destination' 			=> $destination,
				'destinationCountry'    => $destinationCountry,
				'flight_depart_date'    => $flightDepartDate,
				'ADT' 					=> $noOfAdults,
				'CHD' 					=> $noOfChildren,
				'INF' 					=> $noOfInfants,
				'class' 				=> $class,
				'hb' 					=> 0,
				'source' 				=> 'fresco-home',
				'booking-type' 			=> 'official',
			];

			if($type == 'R')
				$aRequest['flight_return_date'] = $flightReturnDate;

			// Create Client Object
			$client = new HttpClient ();
			$client->setUrl ($this->getSearchUrl ());
			$client->setParams($aRequest);
			#dd($client);

			$response   = $client->get();
			$aResponse  = json_decode(json_encode($response), true);

			#dd($aResponse);

			$aData 		= array_get($aResponse, 'data.resultData.0');
			
			
			if($aData['isError'] > 0 || !$aData['isFlights']) {
				throw new Exception ('No search result found', 404);
			}


			$oFlightSearchResult = new FlightSearchResult;
			$aFormattedData 	 = $oFlightSearchResult->formatSearchResult($aResponse);

			

			return $response;




		}
		catch(Exception $e) {
			echo $e->getMessage(); die;
			return ['error' => TRUE, 'message' => $e->getMessage(), 'code' => $e->getCode()];
		}




	
	}

	/**
	 * Return Sandbox / Production url based on the enviroment
	 */
	public function getSearchUrl ()
	{
		$baseUrl = (env ('APP_ENV' , 'production') == 'production') ? self::PRODUCTION_SERVICE_URL : self::SANDBOX_SERVICE_URL;
		$tenentName = self::TENENT_NAME.'dom';
		return $baseUrl.'/'.$tenentName.'/search'; //here the space after search is important. if we send invalid data, if the space not there then the response is some wired.


	}
}

