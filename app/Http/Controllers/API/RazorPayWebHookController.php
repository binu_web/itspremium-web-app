<?php
namespace App\Http\Controllers\API;

use Exception;
use Illuminate\Http\Request;
use Response;
use Illuminate\Routing\Controller;
use Log;
use Razorpay\Api\Api;
use App\Repositories\ApiRepository\ServiceMetaKeyRepository;
use App\Repositories\ApiRepository\WalletRepository;


class RazorPayWebHookController extends Controller
{
	/*
	*
	*
	*
	*/

	const RAZOR_PAY_API_KEY   = 'rzp_test_YRe1oGObKPkX3D';
	const RAZOR_PAY_SECRET_KEY  = '26pNuKkyhRdDqUhm6q73NFAi';


	public function receiveWebHook(Request $oRequest)
	{

		#$aRequest = $oRequest->toArray();
		$razorPaySignature = $oRequest->header('X-Razorpay-Signature');
		$secret 		   = self::RAZOR_PAY_SECRET_KEY;

		$requestBody 	   =  json_encode($oRequest->toArray());

		$hash 			   = hash_hmac('sha256', $requestBody, $secret);


		Log::info("RazorPay WebhookRequest Start");
		Log::info('Header signature : '.$razorPaySignature);
		Log::info('hash '.$hash);
		Log::info('all Headers: '. json_encode($oRequest->header()));

		if($hash == $razorPaySignature)
			$msg = 'Hash is matching';
		else
			$msg = 'hash is not matching';

		Log::info($msg);

		Log::info(serialize($oRequest->all()));
		Log::info(json_encode($oRequest->toArray()));
		Log::info("RazorPay WebhookRequest END");
	}

	/**
	 * @param $response
	 * @author Midhun
	 * Send request to refund
	 */

	public function processFullRefund($request)
	{
		try{

			$api = new Api(self::RAZOR_PAY_API_KEY,self::RAZOR_PAY_SECRET_KEY);

			// capture the payment
				$payment = $api->payment->fetch($request['payment_unique_id']);
				$capture = $payment->capture(array('amount' => $request['refund_amount']));

			// initiate the payment
			$payment = $api->payment->fetch($request['payment_unique_id']);
			$refund = $payment->refund();


			$return = ['status' => 'ERROR','msg'=>'Can not process refund'];
			if (!is_null([$refund]))
			{
				$aMetaValue = [
						'refund_unique_id' => $refund->id,
						'refund_entity' => $refund->id,
						'refund_amount' => $refund->amount,
						'refund_currency' => $refund->currency,
						'refund_created_at'  => $refund->created_at,
				];

				$aData 				 = [
					'metaKeyValue' => $aMetaValue,
					'bookingId'	   => $request['booking_id'],
					'serviceApiId' => $request['service_id'],
					'status' => 'REFUNDED',
					'message'	 => 'Refund processed successfully',

				];
			//TODO : uncomment the below lines once migration run for enum (midhun)
//				$oServiceMetakeyRepository = new ServiceMetaKeyRepository();
//				$oServiceMetakeyRepository->updateYatraBookingAsRefunded($aData);

				$return =  [
					'status' => 'SUCCESS',
					'aData'  => $aData,
					'message'	 => 'Refund processed successfully',
				];

			}

			return $return;
		}
		catch (\Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}

	}


	/**
	 * @param $paymentId
	 * @author Midhun
	 * send partial amount details
	 */
	public function processPartialRefund($request)
	{

		try{
		$api = new Api(self::RAZOR_PAY_API_KEY,self::RAZOR_PAY_SECRET_KEY);

		// capture the payment
		$payment = $api->payment->fetch($request['payment_unique_id']);
		$capture = $payment->capture(array('amount' => $request['refund_amount']));

		// initiate the payment
		$payment = $api->payment->fetch($request['payment_unique_id']);
		$refund = $payment->refund(array('amount' => $request['partial_refund_amount']));

			$return = ['status' => 'ERROR','msg'=>'Can not process refund'];
			if (!is_null([$refund]))
			{
				$aMetaValue = [
					'refund_unique_id' => $refund->id,
					'refund_entity' => $refund->id,
					'refund_amount' => $refund->amount,
					'refund_currency' => $refund->currency,
					'refund_created_at'  => $refund->created_at,
				];

				$aData 				 = [
					'metaKeyValue' => $aMetaValue,
					'bookingId'	   => $request['booking_id'],
					'serviceApiId' => $request['service_id'],
					'status' => 'PARTIALLY REFUNDED',
					'message'	 => 'Partial refund processed successfully',

				];

				//TODO : uncomment the below lines once migration run for enum (midhun)
//				$oServiceMetakeyRepository = new ServiceMetaKeyRepository();
//				$oServiceMetakeyRepository->updateYatraBookingAsRefunded($aData);

				$return =  [
					'status' => 'SUCCESS',
					'aData'  => $aData,
					'message'	 => 'Partial refund processed successfully',
				];

			}

			return $return;


		}
		catch (\Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}
	}


	public function processFullRefundToWallet($request)
	{
		try{

			//Update the refund amount to wallet and keeps its log

			$aWallet = [
				'user_id' => $request['user_id'],
				'amount' => $request['refund_amount'],
				'service_id' => $request['service_id'],
				'transaction_type' => 'CREDIT',
				'transaction_method' => 'REFUND',
				'booking_id' => $request['booking_id'],
				'full_payment' => 1,
			];

			$return = ['status' => 'ERROR','msg'=>'Can not process refund'];


			// update the value based into wallet

			$oWallet = new WalletRepository();
			$rWalletResponse = $oWallet->addAmountInWallet($aWallet);

			if (!is_null([$rWalletResponse]))
			{
				$return =  [
					'status' => 'SUCCESS',
					'message'	 => 'Refund processed successfully',
				];
			}

    		return $return;


		}
		catch (\Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}
	}

	public function processPartialRefundToWallet($request)
	{
		try{

			//Update the refund amount to wallet and keeps its log

			$aWallet = [
				'user_id' => $request['user_id'],
				'amount' => $request['partial_refund_amount'],
				'service_id' => $request['service_id'],
				'transaction_type' => 'CREDIT',
				'transaction_method' => 'REFUND',
				'booking_id' => $request['booking_id'],
				'full_payment' => 0,
			];

			$return = ['status' => 'ERROR','msg'=>'Can not process refund'];


			// update the value based into wallet

			$oWallet = new WalletRepository();
			$rWalletResponse = $oWallet->addAmountInWallet($aWallet);

			if (!is_null([$rWalletResponse]))
			{
				$return =  [
					'status' => 'SUCCESS',
					'message'	 => 'Refund processed successfully',
				];
			}

			return $return;


		}
		catch (\Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}
	}

}

