<?php

namespace App\Http\Controllers\API;

use App\Repositories\ApiRepository\DeviceRepository;
use App\Repositories\ApiRepository\UserRepository;
use App\Repositories\ApiRepository\WalletRepository;
use App\Services\SMSApi;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Storage;
use App\Models\User;



class UserController extends BaseController
{
	private $userRepository;
	private $deviceRepository;
	private $walletRepository;

	/**
	 * UserController constructor.
	 *
	 * @param \App\Repositories\ApiRepository\UserRepository $userRepository
	 */
	public function __construct (UserRepository $userRepository , DeviceRepository $deviceRepository , WalletRepository $walletRepository)
	{
		$this->userRepository = $userRepository;
		$this->deviceRepository = $deviceRepository;
		$this->walletRepository = $walletRepository;
	}


	/**
	 * User registration
	 *
	 * @param Request $request
	 *
	 * @author Midhun
	 * @return mixed
	 */

	public function register (Request $request)
	{
		try {

			$email = $request->get ('email');
			$password = $request->get ('password');
			$mobile_number = $request->get ('mobile_number');
			$f_name = $request->get ('f_name');
			$l_name = $request->get ('l_name');
			$device_id = intval ($request->get ('device_id' , 0) , 10);
			$country = intval ($request->get ('country' , null) , 10);
			$facebook_id = $request->get ('fb_id' , null);
			$google_id = $request->get ('google_id' , null);

			$input = array($email , $password , $mobile_number , $f_name);

			if (checkArrayIsEmpty ($input))
				throw new Exception('Request is invalid' , 400);


			if (!(checkEmailFormat ($email)))
				throw new Exception('Invalid email id' , 400);


			if (empty($device_id))
				throw new Exception('Device id is empty' , 400);

			$user_exists = $this->userRepository->check_email_exists ($email);

			if ($user_exists) {
				throw new Exception('Email id already exists' , 409);
			}

			// Check if Mobile user Exist
			$user_exists = $this->userRepository->check_mobile_exists ($mobile_number);
			if ($user_exists) {
				throw new Exception('Mobile Number Already exists' , 409);
			}


			// Google Id
			if (!is_null ($google_id)) {
				$user_exists = $this->userRepository->serachUserThroughSocial ('google_id' , $google_id);
				if ($user_exists) {
					throw new Exception('Google account already registered. Please Login' , 409);
				}
			}

			// Facebook
			if (!is_null ($facebook_id)) {
				$user_exists = $this->userRepository->serachUserThroughSocial ('facebook_id' , $facebook_id);
				if ($user_exists) {
					throw new Exception('Facebook account already registered. Please Login' , 409);
				}
			}




			// Validate Mobile Number
			if (!(validate_mobilenumber ($mobile_number)))
				throw new Exception('Invalid mobile number' , 409);

			// Create User
			$user = $this->userRepository->regUser ($request->all ());

			if (!$user)
				throw new Exception('User not registered' , 500);

			$userId = $user->id;
			if ($userId) {
				// create a verification code and send the same to user device for confirmation
				$otp_token = $this->userRepository->createOtpVerification ($mobile_number);


				if (!$otp_token)
					throw new Exception('OTP token is not updated' , 500);

			}

			// Update User to Device
			$regDevice = $this->deviceRepository->checkAndUpdateDevice ($device_id , $userId);

			// Add a new raw to wallet for this user
			$wallet = $this->walletRepository->insertUserToWallet($userId);

			return $this->sendResponse ('user is registered' , 201);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * verify the requested otp with mobile number
	 *
	 * @param Request         $request
	 * @param Authenticatable $user
	 *
	 * @author Midhun
	 * @return mixed
	 */

	public function verifyOtp (Request $request)
	{
		try {

			$otp_token = $request->get ('otp_token');
			$mobile_number = $request->get ('mobile_number');

			$input = array($mobile_number , $otp_token);

			if (checkArrayIsEmpty ($input))
				throw new Exception('Request is invalid' , 400);

			if (!(validate_mobilenumber ($mobile_number)))
				throw new Exception('Invalid mobile number' , 409);

			$verify = $this->userRepository->verifyToken ($mobile_number , $otp_token);

			if (!$verify)
				throw new Exception('OTP token is not verified' , 412);

			return $this->sendResponse ('OTP token is verified' , 201);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * resend otp if the pre-send otp is not received
	 *
	 * @param Request $request
	 *
	 * @author Midhun
	 * @return mixed
	 */

	public function resendOtp (Request $request)
	{
		try {

			$mobile_number = $request->get ('mobile_number');

			$input = array($mobile_number);

			if (checkArrayIsEmpty ($input))
				throw new Exception('Request is invalid' , 400);

			if (!(validate_mobilenumber ($mobile_number)))
				throw new Exception('Invalid mobile number' , 400);


			$otp_token = $this->userRepository->createOtpVerification ($mobile_number);

			if (!$otp_token)
				throw new Exception('OTP token is not updated' , 412);


			// MAIL API
			return $this->sendResponse ('OTP token has been sent' , 201);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * Send otp to mobile number for forget password
	 *
	 * @param Request $request
	 *
	 * @author Midhun
	 * @return mixed
	 */
	public function forgetPassword (Request $request)
	{

		try {

			$email = $request->get ('email');

			$input = array($email);

			if (checkArrayIsEmpty ([$email]))
				throw new Exception('Request is invalid' , 400);

			if (!checkEmailFormat ($email))
				throw new Exception('Invalid email id' , 400);

			// Get Phone number registered with the email
			$user = $this->userRepository->getMobileNumber ($email);

			if (is_null ($user) || empty($user))
				throw new Exception('No registered user found' , 404);

			$otp_token = $this->userRepository->createOtpVerification ($user->mobile);

			if (!$otp_token)
				throw new Exception('OTP token is not updated' , 412);

			return $this->sendResponse ('OTP sent to registered mobile number' , 201 , ['mobile' => $user->mobile]);


		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	public function resetPassword (Request $request)
	{
		try {

			$email = $request->get ('email');

			$password = $request->get ('password');

			$input = array($email , $password);

			if (checkArrayIsEmpty ($input))
				throw new Exception('Request is invalid' , 400);

			if (!checkEmailFormat ($email))
				throw new Exception('Invalid email id' , 400);

			// Get Phone number registered with the email
			$user = $this->userRepository->check_email_exists ($email);

			if (is_null ($user) || empty($user))
				throw new Exception('No registered user found' , 404);

			$passwordStatus = $this->userRepository->updatePassword ($email , $password);

			if (!$passwordStatus)
				throw new Exception('password is not updated' , 412);

			return $this->sendResponse ('password is updated successfully' , 201);


		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * @author Midhun
	 * Find the user used services
	 */
	public function accounts ()
	{
		try {
			//get user  details
			$user = JWTAuth::parseToken ()->toUser ();
			//Collect the user account details
			$account_details = $this->userRepository->getUserServices ($user->id);

			return $this->sendResponse ('user account details listed' , 200 , $account_details);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

    /**
     * @author Harinarayanan
     * Disconnect account
     */
    public function disconnectAccount(Request $request)
    {
        try{
            
            $oUser 			= JWTAuth::parseToken()->toUser();
            $userId 		= $oUser->id;
            $serviceApiId   = $request->get('service_api_id');

            if(checkArrayIsEmpty([$userId, $serviceApiId])) {
                throw new Exception('Mandatory fields are missing', 400);
            }

            $this->userRepository->disconnectService($serviceApiId, $userId);

            return $this->sendResponse('Service disconnected successfully', 200, true);

        }
        catch(Exception $e) {
            return $this->sendError ($e->getMessage () , $e->getCode ());
        }
    }

	

	/**
	 * Get all user bookings based in the services
	 */

	public function bookingHistory ()
	{
		try {
			//get user  details
			$user = JWTAuth::parseToken ()->toUser ();
			//Collect the user booking details
			$booking_details = $this->userRepository->getUserBookings ($user->id);
			return $this->sendResponse ('User booking details listed' , 200 , $booking_details);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 * @return mixed
	 *         Get single booking details
	 */
	public function bookingDetails (Request $request)
	{
		try {
			$booking_id = $request->has ('booking_id') ? $request->get ('booking_id') : null;
			// Check if service key word exists
			if (checkArrayIsEmpty ([$booking_id]))
				throw new Exception('booking id required' , 400);

			$booking_details = $this->userRepository->getUserBookingDetails ($booking_id);
			return $this->sendResponse ('booking details listed' , 200 , $booking_details);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 *         Save user feedbacks from form in account section
	 */
	public function postFeedback (Request $request)
	{
		try {
			$user = JWTAuth::parseToken ()->toUser ();
			$feedback = $request->has ('feedback') ? $request->get ('feedback') : null;

			if (checkArrayIsEmpty ([$feedback]))
				throw new Exception('Feedback text is empty' , 400);

			$post_feedback = $this->userRepository->postUserFeedback ($feedback , $user->id);
			return $this->sendResponse ('Feedback saved successfully.' , 200);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/**
	 * Get Profile
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	public function getProfile (Request $request)
	{
		try {
			$user = JWTAuth::parseToken ()->toUser ();

			$profile = $this->userRepository->getUserProfile ($user->id);
			return $this->sendResponse ('Profile details listed' , 200 , $profile);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * Get Notifications
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	public function getNotifications (Request $request)
	{
		try {
			$user = JWTAuth::parseToken ()->toUser ();

			$profile = $this->userRepository->getUserNotifications ($user->id);
			return $this->sendResponse ('User notifications listed' , 200 , $profile);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/**
	 * Delete Notifications
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	public function deleteNotifications (Request $request)
	{
		try {

			$notification_id = $request->has ('notification_id') ? $request->get ('notification_id') : null;

			if (checkArrayIsEmpty ([$notification_id]))
				throw new Exception('notification id is empty' , 400);

			$profile = $this->userRepository->deleteUserNotifications ($notification_id);
			return $this->sendResponse ('User notifications deleted' , 200 , $profile);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * Get booking details when click on notification
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */


	public function getBookingDetails(Request $request)
	{
		try{
			$bookingId 	     = $request->get('booking_id');
			#echo $bookingId; die;
			$aBookingDetails = $this->userRepository->getNotificationDetails($bookingId);

			return $this->sendResponse('booking details', 200, $aBookingDetails);

		}
		catch(Exception $e) {
			echo $e->getMessage();
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	public function getWalletDetails()
	{
		try{
		//get user  details
		$user = JWTAuth::parseToken ()->toUser ();

			$details = $this->walletRepository->getWalletDetails($user->id);

			return $this->sendResponse('wallet details listed', 200, $details);

		}
		catch(Exception $e) {
			echo $e->getMessage();
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}


	}

    /*
     *
     *
     */
    public function editProfile()
    {
        try{
            $oUser = JWTAuth::parseToken()->toUser();

        }
        catch(Exception $e) {

            return $this->sendError ($e->getMessage () , $e->getCode ());
        }
    }

    /*
     *
     *
     */
    public function updateProfile(Request $oRequest)
    {
        try{

            $oUser  = JWTAuth::parseToken()->toUser();
            $userId = $oUser->id;

            $firstName = $oRequest->get('first_name');
            $lastName  = $oRequest->get('last_name');
            $country   = $oRequest->get('country');
            $profileImage = $oRequest->get('profile_pic'); //base64 image


            if(checkArrayIsEmpty([$firstName, $lastName]))
                throw new Exception('Mandatory fields are missing', 400);

            $oUser             = User::find($userId);
            $oUser->first_name = $firstName;
            $oUser->last_name  = $lastName;
            $oUser->country    = $country;
           // $oUser->updated_at = date('Y-m-d H:i:s');



            if($profileImage) {
                $aProfileImage = explode(',', $profileImage);
                $imageStr      = isset($aProfileImage[1])? $aProfileImage[1] : null;
                $imageFormat   = isset($aProfileImage[0]) ? $aProfileImage[0] : null;
                $aImageExt     = explode(';', $imageFormat);
                $imageType     = isset($aImageExt[0]) ? $aImageExt[0] : null;
                //echo $imageStr; die;
                $aAllowedTypes = ['data:image/png', 'data:image/jpeg'];

                if(!$imageStr || (!in_array($imageType, $aAllowedTypes)))
                    throw new Exception('Invalid Image', 400);

                $image         = base64_decode($imageStr);
                $imageName     = $userId."_dp.png";
                $path          = storage_path().'/app/avatar/'.$imageName;

                file_put_contents($path, $image);
                $oUser->profile_pic = $path;
            }

            $oUser->save();



            return $this->sendResponse('Profile details updated successfully', 200, true);
        }
        catch(Exception $e) {
           # echo $e->getMessage();die;
            return $this->sendError ($e->getMessage () , $e->getCode ());
        }
    }


}
