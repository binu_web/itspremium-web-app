<?php

namespace App\Http\Controllers\API\Cabs;

use App\Http\Controllers\API\BaseController;
use App\Repositories\ApiRepository\UserApiRepository;
use App\Repositories\ApiRepository\ServiceMetaKeyRepository;

use App\Services\HttpClient;
use App\Services\Uber;
use Exception;
use Response;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Services\Uber\Client;
use Illuminate\Support\Facades\Log;
use DB;
use App\Models\ServiceBooked;
use App\Models\ServiceMetaData;
use App\Models\User;

/**
 * SERVICES RELATED TO Uber CAB SERVICES
 *
 * Class UberController
 *
 * @package App\Http\Controllers\API\Cabs
 */
class UberController extends BaseController
{
	/**
	 *  TODO : App Service Deafult Constant (Based on Service Added)
	 */
	const API_SERVICE_CATEGORY_ID = 1;
	const API_SERVICE_ID = 2;
	const SERVICE_NAME = 'uber';

	/**
	 * Uber Server Credientials
	 */
	const UBER_CLIENT_ID = 'rWAyTL71fwZ6Rg8suASy47FH0uUkDvQq';
	const UBER_CLIENT_SECRET = 'o8leWy9ELuJVYGpm7lLpFqs4QSymWOah49mLuRfj';
	const UBER_SERVER_TOKEN = 'Sr3hkn0h1wt3-Mmtmy-L6CZOt5N4Bg1TAzgQT2ud';
	/**
	 * Uber OAuth URL
	 */
	const PRODUCTION_O_AUTH_URL = '';                                                                                    // TODO : Fill in Basic Details
	const SANDBOX_O_AUTH_URL = 'https://login.uber.com/oauth/v2/authorize?client_id=' . self::UBER_CLIENT_ID . '&response_type=code&state=';

	/**
	 * Uber API URL
	 */
	const SANDBOX_URL = 'https://sandbox-api.uber.com/v1.2/';
	const PRODUCTION_URL = 'https://api.uber.com/v1.2/';

	// TOODO : Redirect URI should be as provided in the Uber API Dashboard
	const REDIRECT_URI = 'https://kometonline.com/staging/itzpremium/api/cab/access-token';


	/**
	 * Uber API ENDPOINTS
	 */
	const EP_RIDE_AVAILABILITY = 'v1/products';
	const EP_RIDE_ESTIMATE = '/v1/products';
	const EP_CAB_BOOKING = '/v1/bookings/create';
	const EP_RIDE_TRACKING = '/v1/bookings/track_ride';
	const EP_RIDE_CANCELLATION = '/v1/bookings/cancel';
	const EP_BOOKING_FEEDBACK = '/v1/bookings/feedback';
	const EP_SOS_SIGNAL = '/v1/sos/signal';
	const EP_BOOKING_HISTORY = '/v1/bookings/my_rides';


	// Define Variables
	protected $user_id;
	protected $userApiRepo;
	public $serviceMetaKeyRepo;


	/**
	 * UberController constructor.
	 *
	 * @param \App\Repositories\ApiRepository\UserApiRepository $userApiRepo
	 */
	public function __construct (\stdClass $stdObject)
	{

		$this->userApiRepo = property_exists ($stdObject , 'user_api') ? $stdObject->user_api : new UserApiRepository();
		$this->serviceMetaKeyRepo = property_exists ($stdObject , 'service_api') ? $stdObject->service_api : new ServiceMetaKeyRepository();

	}


	/**
	 * Get Uber Default Configuration Settings
	 */
	public function getDefaultUberConfig ($access_token = "")
	{
		$uberConfig = array(
			'access_token' => $access_token ,
			'server_token' => self::UBER_SERVER_TOKEN ,
			'version' => 'v1.2' , // optional, default 'v1.2'
			'locale' => 'en_US' , // optional, default 'en_US'

		);

		// Usage of Sandbox
		if (env ('APP_ENV' , 'production') != 'production') {
			$uberConfig['use_sandbox'] = true;
		}


		return $uberConfig;

	}

	/**
	 * Get OAuth URL
	 *
	 * @return string
	 */
	public function getOAuthUrl ()
	{
		return (env ('APP_ENV' , 'production') == 'production') ? self::PRODUCTION_O_AUTH_URL : self::SANDBOX_O_AUTH_URL;
	}


	/**
	 * Get API url
	 *
	 * @return string
	 */
	public function getUrl ()
	{
		return (env ('APP_ENV' , 'production') == 'production') ? self::PRODUCTION_URL : self::SANDBOX_URL;

	}

	/**
	 * Authorize URL to Register in Uber
	 *
	 * @author Binoop V
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function authorize ($userId)
	{
		try {
			$state = self::SERVICE_NAME . URL_VALUE_DELIMITER . $userId;
			$url = $this->getOAuthUrl () . $state;
			return redirect ($url);
		}
		catch (Exception $e) {
			throw exception ($e);

		}
	}

	/**
	 * Uber Redirect URL to user oauth.
	 *
	 * Reference : https://developers.Ubercabs.com/docs/access-token
	 *
	 * Sample response
	 * Success :
	 * http://binu.itzpremium.demo/api/Uber/token/1#access_token=9e2c88b352584eacaa6e00fa574689eb&state=state123&scope=profile%20booking&token_type=bearer&expires_in=15552000
	 * Error : http://binu.itzpremium.demo/api/Uber/token/1#error=access_denied&state=state123
	 *
	 * @author Binoop V
	 */
	public function token (Request $request , $userId)
	{
		try {


			$provider = new Uber([
				'clientId' => self::UBER_CLIENT_ID ,
				'clientSecret' => self::UBER_CLIENT_SECRET ,
				'redirectUri' => self::REDIRECT_URI
			]);

			$code = $request->code;

			// Try to get an access token (using the authorization code grant)
			$token = $provider->getAccessToken ('authorization_code' , [
				'code' => $code
			]);


			$requestParams = $request->all ();


			// Check if Access token is generated
			$access_token = $token->getToken ();
			$refresh_token = $token->getRefreshToken ();
			$token_expiry = $token->getExpires ();
			if (is_null ($access_token))
				throw new Exception('access token not generated' , 401);


			// Check if User Token Exists for A User
			$token_id = $this->userApiRepo->checkTokenExists ($userId , self::API_SERVICE_ID);

			if ($token_id) {
				// TODO : update the api access
				$status = $this->userApiRepo->updateToken (['access_token' => $access_token , 'refresh_token' => $refresh_token , 'token_expiry' => $token_expiry , 'is_active' => 1] , $token_id);

			} else {
				// TODO : create api access token
				$status = $this->userApiRepo->createToken (['access_token' => $access_token , 'refresh_token' => $refresh_token , 'token_expiry' => $token_expiry , 'service_api_id' => self::API_SERVICE_ID , 'user_id' => $userId]);
			}


			if (!$status)
				throw new Exception('access token not saved' , 500);

			return $this->sendResponse ('user is registered' , 201);

		}
		catch (Exception $e) {
			Log::emergency ($e);
			return $this->sendError ("Uber Error : " . $e->getMessage () , $e->getCode ());

		}
	}

	/**
	 * Uber OAuth Access Token Redirect URL
	 *
	 * Reference : https://developers.Ubercabs.com/docs/webhook
	 * TODO : Implement feature to detect state changes & send push notification to devices.
	 *
	 * @author Binoop V
	 */
	public function stateChange (Request $request)
	{

		dd ('Uber');

	}


	/**
	 * TODO : RIDE SEARCH & RIDE ESTIMATE
	 *
	 * Reference : https://developers.Ubercabs.com/docs/ride-availablity
	 */
	public function rideSearch ($request)
	{
		try {

			// state uber account is not available first
			$uber_account_exists = FALSE;

			$client = new Client(array(
				'server_token' => self::UBER_SERVER_TOKEN ,
//				'access_token' => $user_acs_token->access_token ,
				'use_sandbox' => true , // optional, default false
				'version' => 'v1.2' , // optional, default 'v1.2'
				'locale' => 'en_US' , // optional, default 'en_US'
			));

			// Taken the user details
			$user = JWTAuth::parseToken ()->toUser ();

			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();
			if ($user_acs_token)
				$uber_account_exists = TRUE;

			$uber_response = array();
			// CHeck if Pick up location is updated
			if (checkArrayIsEmpty ([$request['drop_lat'] , $request['drop_lng']])) {
				// Ride Serach Reqeust
				$searchRequest = [
					'latitude' => $request['pickup_lat'] ,
					'longitude' => $request['pickup_lng'] ,
				];

				// Time Estimate Request
				$timeRequest = [
					'start_latitude' => $request['pickup_lat'] ,
					'start_longitude' => $request['pickup_lng'] ,
				];

				$response = $client->getProducts (
					$searchRequest
				);


				$timeResponse = $client->getTimeEstimates (
					$timeRequest
				);


				// Looping the response which
				foreach ($response->products as $resp) {

					$product_id = $resp->product_id;

					$timeEstimate = array_filter ($timeResponse->times , function ($v) use ($product_id) {
						return $v->product_id == $product_id;
					});

					$timeEstimate = array_values ($timeEstimate);
					$uber_response[] = [
						'service_name' => 'uber' ,
						'service_id' => self::API_SERVICE_ID ,
						'product_id' => $resp->product_id ,
						'capacity' => $resp->capacity ,
						'image' => $resp->image ,
						'display_name' => $resp->display_name ,
						'product_group' => $resp->product_group ,
						'description' => $resp->description ,
						'cash_enabled' => $resp->cash_enabled ,
						'eta' => isset ($timeEstimate[0]) ? ($timeEstimate[0]->estimate / 60) : null ,
						'account_exists' => $uber_account_exists ,
						'ride_later_enabled' => false];
				}
			} else {
				$request = [
					'start_latitude' => $request['pickup_lat'] ,
					'start_longitude' => $request['pickup_lng'] ,
					'end_latitude' => $request['drop_lat'] ,
					'end_longitude' => $request['drop_lng']
				];

				$response = $client->getPriceEstimates (
					$request
				);

				$timeResponse = $client->getTimeEstimates (
					$request
				);

				#echo 'here';
				#dd($timeResponse);

				foreach ($response->prices as $resp) {

					$product_id = $resp->product_id;
					$timeEstimate = array_filter ($timeResponse->times , function ($v) use ($product_id) {
						return $v->product_id == $product_id;
					});

					$timeEstimate = array_values ($timeEstimate);


					$uber_response[] = ['service_name' => 'uber' ,
						'service_id' => self::API_SERVICE_ID ,
						'product_id' => $resp->product_id ,
						'display_name' => $resp->display_name ,
						'product_group' => $resp->localized_display_name ,
						'distance' => $resp->distance ,
						'low_estimate' => $resp->low_estimate ,
						'high_estimate' => $resp->high_estimate ,
						'duration' => $resp->duration ,
						'price_estimate' => $resp->estimate ,
						'currency' => $resp->currency_code ,
						'eta' => isset ($timeEstimate[0]) ? ($timeEstimate[0]->estimate / 60) : null ,
						'account_exists' => $uber_account_exists ,
						'ride_later_enabled' => false];
				}
			}

			if (!$uber_response)
				throw new Exception('uber cab details not listed' , 400);
			return $uber_response;
		}
		catch (Exception $e) {
			$e->serviceError = self::SERVICE_NAME;
			httpFailureLog ($e);
			return [];
		}

	}


	/*
	* get Ride details of Uber
	*
	*
	*/

	public function getRideDetails ($request)
	{
		try {
			$pickUpLocation = $request['pickup_loc'];
			$pickUpLat = $request['pickup_lat'];
			$pickUpLong = $request['pickup_lng'];
			$dropLoc = $request['drop_loc'];
			$dropLat = $request['drop_lat'];
			$dropLong = $request['drop_lng'];
			$rideTime = $request['ride_time'];
			//$rideOption 	= $request['ride_option'];
			$productId = $request['product_id'];

			#echo 'product id'.$productId;

			if (empty($productId)) {
				throw new Exception('cab product id is missing' , 400);
			}


			//get client:

			$user = JWTAuth::parseToken ()->toUser ();
			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();
			$client = new Client($this->getDefaultUberConfig ($user_acs_token->access_token));

			//get product using product id

			$oProduct = $client->getProduct ($productId);


			if (!$oProduct->product_id) //this code not working
				throw new Exception('This cab not available for now' , 404);


			$displayName = $oProduct->display_name;
			$seatingCapacity = $oProduct->capacity;
			$cabImage = $oProduct->image;
			$shortDesc = $oProduct->short_description;
			$productGroup = $oProduct->product_group;
			$description = $oProduct->description;

			#print_r($product);

			//time estimate request

			$timeEstimateRequest = [
				'start_latitude' => $pickUpLat ,
				'start_longitude' => $pickUpLong ,
				'product_id' => $productId ,
			];


			$oTimeEstResponse = $client->getTimeEstimates ($timeEstimateRequest);


			$times = isset($oTimeEstResponse->times[0]) ? $oTimeEstResponse->times[0] : null;

			$uberDisplayName = isset($times->display_name) ? $times->display_name : null;
			$estimateTime = isset($times->estimate) ? $times->estimate : null;

			if ($estimateTime === null)
				throw new Exception('This cab not available for now (time not matching)' , 404);


			//request ride estimate

			$aRideRequest = [
				'product_id' => $productId ,
				'start_latitude' => $pickUpLat ,
				'start_longitude' => $pickUpLong ,
				'end_latitude' => $dropLat ,
				'end_longitude' => $dropLong ,
			];


			//print_r($oPriceDetails); die;


			$oRideRequestsEstimate = $client->getRequestEstimate ($aRideRequest);


			#print_r($rideRequestsEstimate);

			$oFare = isset($oRideRequestsEstimate->fare) ? $oRideRequestsEstimate->fare : null;
			$oTrip = isset($oRideRequestsEstimate->trip) ? $oRideRequestsEstimate->trip : null;
			$pickEstimate = isset($oRideRequestsEstimate->pickup_estimate) ? $oRideRequestsEstimate->pickup_estimate : null; //return in minutes
			#echo $pickEstimate; 
			#dd($oRideRequestsEstimate);
			#dd($oRideRequestsEstimate); 
			if ($pickEstimate === null)
				throw new Exception ('This cab is not avaialble for now (ETA returns null)' , 404);

			$priceValue = isset($oFare->value) ? $oFare->value : null;
			$fareId = isset($oFare->fare_id) ? $oFare->fare_id : null;
			$displayPrice = isset($oFare->display) ? $oFare->display : null;
			$currencyCode = isset($oFare->currency_code) ? $oFare->currency_code : null;

			$distanceUnit = isset($oTrip->distance_unit) ? $oTrip->distance_unit : null;
			$durationEstimate = isset($oTrip->duration_estimate) ? $oTrip->duration_estimate : null;
			$distanceEstimate = isset($oTrip->distance_estimate) ? $oTrip->distance_estimate : null;

			if ($distanceUnit == 'mile') {
				$distanceEstimate = round (1.61 * $distanceEstimate , 2);
				$distanceUnit = 'km';
			}

			$durationEstimate = $durationEstimate ? $durationEstimate / 60 : null;


			$aUberResponse = [
				'service_name' => 'uber' ,
				'service_id' => self::API_SERVICE_ID ,
				'product_id' => $productId ,
				'display_name' => $displayName ,
				'product_group' => $productGroup ,
				'distance' => $distanceEstimate ,
				'distance_unit' => $distanceUnit ,
				'duration' => $durationEstimate , //minutes
				'eta' => $pickEstimate , //minutes
				'ride_later_enabled' => false ,
				'capcity' => $seatingCapacity ,
				'image' => $cabImage ,
				'fare_amount' => $priceValue ,
				'currency' => $currencyCode ,
				'display_fare' => $displayPrice , //with currency code
				'account_exists' => true

			];

			return $aUberResponse;


		}

		catch (Exception $e) {
			#echo $e->getMessage(); die;

			return ['error' => TRUE , 'message' => $e->getMessage () , 'code' => $e->getCode ()];
		}


	}


	/**
	 * TODO : RIDE BOOKING
	 *
	 * Reference :https://developers.Ubercabs.com/docs/cab-booking
	 */
	public function book ($request)
	{
		try {


			$request_array = [
				'product_id' => $request['product_id'] ,
				'start_latitude' => $request['pickup_lat'] ,
				'start_longitude' => $request['pickup_lng'] ,
				'end_latitude' => $request['drop_lat'] ,
				'end_longitude' => $request['drop_lng'] ,
			];


			//

			// Check service keyword is available
			if (checkArrayIsEmpty ([$request_array['product_id']]))
				throw new Exception('product id is missing' , 400);


			$user = JWTAuth::parseToken ()->toUser ();

			//TODO : change the service id once we fixed


			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();


			$client = new Client($this->getDefaultUberConfig ($user_acs_token->access_token));


			//get est fare:
			$oRequestEstimate = $client->getRequestEstimate (array(
				'product_id' => $request['product_id'] ,
				'start_latitude' => $request['pickup_lat'] ,
				'start_longitude' => $request['pickup_lng'] ,
				'end_latitude' => $request['drop_lat'] , // optional
				'end_longitude' => $request['drop_lng'] , // optional
			));


			$fare = isset($oRequestEstimate->fare) ? $oRequestEstimate->fare : null;
			$fareAmount = isset($fare->value) ? $fare->value : null;
			$currencyCode = isset($fare->currency_code) ? $fare->currency_code : null;

			$oTrip = isset($oRequestEstimate->trip) ? $oRequestEstimate->trip : null;
			$pickEstimate = isset($oRequestEstimate->pickup_estimate) ? $oRequestEstimate->pickup_estimate : null; //return in minutes

			$distanceUnit = isset($oTrip->distance_unit) ? $oTrip->distance_unit : null;
			$durationEstimate = isset($oTrip->duration_estimate) ? $oTrip->duration_estimate : null;
			$distanceEstimate = isset($oTrip->distance_estimate) ? $oTrip->distance_estimate : null;

			if ($distanceUnit == 'mile') {
				$distanceEstimate = round (1.61 * $distanceEstimate , 2);
				$distanceUnit = 'km';
			}

			$durationEstimate = $durationEstimate ? $durationEstimate / 60 : null;

			#echo $fareAmount."--".$currencyCode; die;

			$aOtherparam = [
				'fare_amount' => $fareAmount ,
				'currency_code' => $currencyCode ,
				'distance_estimate' => $distanceEstimate ,
				'duration_estimate' => $durationEstimate ,
				'distance_unit' => $distanceUnit ,
			];

			//print_r($requestEstimate); die;


			try {
				$request = $client->requestRide (
					$request_array
				);
			}
			catch (Exception $e) {
				// Obtain the errors
				$body = $e->getBody ();
				
				if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['code'])) {
					switch ($body->code) {
						case "not_found":
							throw new Exception('The product not exist.' , 400);
							break;

						case "unauthorized":
							throw new Exception('User credientials are invalid' , 401);
							break;
						default:
							throw new Exception($body , $e->getCode ());
							break;

					}
				}
				if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['errors'])) {
					$body_errors = $body->errors;
					switch ($body_errors[0]->code) {
						case "current_trip_exists":
							throw new Exception('The user is currently on a trip.' , 400);
							break;
						default:
							throw new Exception($body_errors[0]->title , $body_errors[0]->status);
							break;
					}
				}
			}

			if (env ('APP_ENV' , 'production') != 'production') {
				$updateRequest = $client->setSandboxRequest ($request->request_id , array('status' => 'accepted'));
//				$request = $updateRequest;
			}
			if (checkResponseObjectProperties (self::SERVICE_NAME , $request , ['request_id'])) {
				// Log booking details
//                $insert = $this->serviceMetaKeyRepo->addUberBookDetails(self::API_SERVICE_CATEGORY_ID, self::API_SERVICE_ID, $user->id, $request);
			}


			if (checkResponseObjectProperties (self::SERVICE_NAME , $request , ['request_id'])) {
				// Log booking details
				$insert = $this->serviceMetaKeyRepo->addUberBookDetails (self::API_SERVICE_CATEGORY_ID , self::API_SERVICE_ID , $user->id , $request , $aOtherparam);
			}

			return $insert;
		}
		catch (Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage () , 'code' => $e->getCode ()];
		}

	}


	/**
	 * TODO :
	 * To Get Ride Tracking
	 * Reference : https://developers.Ubercabs.com/docs/cab-track-ride
	 */

	public function updateDestination ($request)
	{
		try {

			#print_r($request); die;
			$bookingId = $request['booking_id'];
			$newDestinationLat = $request['new_destination_lat'];
			$newDestinationLng = $request['new_destination_lng'];
			$serviceId = $request['service_id'];
			$serviceApiId = $request['service_api_id'];

			$serviceMetaKeys = ['pickup_latitude' , 'pickup_longitude' , 'request_id' , 'product_id'];


			#dd(get_class_methods($this->serviceMetaKeyRepo));
			#echo $serviceApiId;
			#echo $bookingId;

			$aMetaValues = $this->serviceMetaKeyRepo->getMetaKeyValues ($serviceApiId , $bookingId , $serviceMetaKeys);
			#dd($aMetaValues);

			$requestId = $aMetaValues['request_id'];
			$pickupLat = $aMetaValues['pickup_latitude'];
			$pickupLng = $aMetaValues['pickup_longitude'];
			$productId = $aMetaValues['product_id'];

			$oUser = JWTAuth::parseToken ()->toUser ();

			$userAccessToken = $oUser->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();

			$client = new Client($this->getDefaultUberConfig ($userAccessToken->access_token));

			//dd(get_class($client));
			//dd(get_class_methods($client));
			//update request:

			$updateRequest = [
				'end_latitude' => $newDestinationLat ,
				'end_longitude' => $newDestinationLng ,

			];

			DB::beginTransaction ();


			try {
				$oRequestEstimate = $client->getRequestEstimate (array(
					'product_id' => $productId ,
					'start_latitude' => $pickupLat ,
					'start_longitude' => $pickupLng ,
					'end_latitude' => $newDestinationLat , // optional
					'end_longitude' => $newDestinationLng , // optional
				));
			}
			catch (Exception $e) {
				$body = $e->getBody ();


				if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['errors'])) {
					$body_errors = $body->errors;

					switch ($body_errors[0]->code) {
						case "distance_exceeded":
							throw new Exception('Distance between two points exceeds 100 miles' , 400);
							break;

						case "unauthorized":
							throw new Exception('User credientials are invalid' , 401);
							break;

						case "validation_failed":
							throw new Exception('Invalid request' , 400);
							break;

						case "trip_not_found":
							throw new Exception("Trip Not found" , 404);
							break;

						default:
							throw new Exception($body , $e->getCode ());
							break;
					}
				}

				if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['code'])) {
					switch ($body->code) {

						case "unauthorized":
							throw new Exception('User credientials are invalid' , 401);
							break;

						case "distance_exceeded":
							throw new Exception('Distance between two points exceeds 100 miles' , 400);
							break;

						case "validation_failed":
							throw new Exception('Invalid request' , 400);
							break;

						case "trip_not_found":
							throw new Exception("Trip Not found" , 404);
							break;


						default:
							throw new Exception('Request failed due to unknown reason' , $e->getCode ());
							break;

					}
				}


			}


			try {

				$updateRequest = $client->setRequest ($requestId , $updateRequest);

			}
			catch (Exception $e) {
				Log::info ("Uber Error Body");
				Log::alert ($e);
				Log::alert (serialize ($e->getBody ()));
				$body = $e->getBody ();

				if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['errors'])) {
					$body_errors = $body->errors;

					switch ($body_errors[0]->code) {
						case "distance_exceeded":
							throw new Exception('Distance between two points exceeds 100 miles' , 400);
							break;

						case "unauthorized":
							throw new Exception('User credientials are invalid' , 401);
							break;

						case "validation_failed":
							throw new Exception('Invalid request' , 400);
							break;

						case "trip_not_found":
							throw new Exception("Trip Not found" , 404);

						default:
							throw new Exception($body , $e->getCode ());
							break;
					}
				}

				// Error Response Based on Code
				if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['code'])) {
					switch ($body->code) {

						case "unauthorized":
							throw new Exception('User credientials are invalid' , 401);
							break;

						case "distance_exceeded":
							throw new Exception('Distance between two points exceeds 100 miles' , 400);
							break;

						case "validation_failed":
							throw new Exception('Invalid request' , 400);
							break;

						case "destination_change_not_allowed":
							throw new Exception("Destination change is not allowed." , 403);
							break;

						case "trip_not_found":
							throw new Exception("Trip Not found" , 404);
							break;

						default:
							throw new Exception('Request failed due to unknown reason' , $e->getCode ());
							break;

					}
				}

			}


			$fare = isset($oRequestEstimate->fare) ? $oRequestEstimate->fare : null;
			$fareAmount = isset($fare->value) ? $fare->value : null;
			$currencyCode = isset($fare->currency_code) ? $fare->currency_code : null;


			$oTrip = isset($oRequestEstimate->trip) ? $oRequestEstimate->trip : null;
			$pickEstimate = isset($oRequestEstimate->pickup_estimate) ? $oRequestEstimate->pickup_estimate : null; //return in minutes

			$distanceUnit = isset($oTrip->distance_unit) ? $oTrip->distance_unit : null;
			$durationEstimate = isset($oTrip->duration_estimate) ? $oTrip->duration_estimate : null;
			$distanceEstimate = isset($oTrip->distance_estimate) ? $oTrip->distance_estimate : null;

			if ($distanceUnit == 'mile') {
				$distanceEstimate = round (1.61 * $distanceEstimate , 2);
				$distanceUnit = 'km';
			}

			$durationEstimate = $durationEstimate ? $durationEstimate / 60 : null;


			$updateData = [
				'fare_amount' => $fareAmount ,
				'currency_code' => $currencyCode ,
				'distance_estimate' => $distanceEstimate ,
				'duration_estimate' => $durationEstimate ,
				'distance_unit' => $distanceUnit ,
				'destination_latitude' => $newDestinationLat ,
				'destination_longitude' => $newDestinationLng ,

			];


			$this->serviceMetaKeyRepo->updateMetaDataValues (self::API_SERVICE_ID , $bookingId , $updateData);

			DB::commit ();

			#die('u out');

			return 1;


		}
		catch (Exception $e) {
			#dd($e->getMessage());
			DB::rollback ();
			return ['error' => TRUE , 'message' => $e->getMessage () , 'code' => $e->getCode ()];
		}


	}


	/**
	 * TODO : RIDE TRACKING
	 * To Get Ride Tracking
	 * Reference : https://developers.Ubercabs.com/docs/cab-track-ride
	 */
	public function track ($request)
	{
		try {


			$request_id = $request['request_id'];
			$booking_id = $request['booking_id'];

			$user = JWTAuth::parseToken ()->toUser ();

			//TODO : change the service id once we fixed
			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();

			$client = new Client($this->getDefaultUberConfig ($user_acs_token->access_token));

			if (empty($request_id)) {
				// Cancel the current ride
				try {
					$request = $client->getCurrentRequest ();

				}
				catch (Exception $e) {
					$body = $e->getBody ();

					// Error Based on Error Code
					if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['code'])) {

						switch ($body->code) {
							case "invalid_request_id":
								throw new Exception('request_id is invalid' , 400);
								break;

							case "unauthorized":
								throw new Exception('user credientials are invalid' , 401);
								break;

							default:
								throw new Exception('request not processed' , 500);
								break;
						}
					}

					// Handling Errors
					if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['errors'])) {
						$body_errors = $body->errors;

						switch ($body_errors[0]->code) {
							case "no_current_trip":
								throw new Exception('User is not currently on a trip.' , 400);
								break;
						}
					}
				}
			} else {
				// Cancel Ride Based on
				try {
					$request = $client->getRequest ($request_id);
				}
				catch (Exception $e) {
					$body = $e->getBody ();
					if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['code'])) {

						switch ($body->code) {
							case "invalid_request_id":
								throw new Exception('request_id is invalid' , 400);
								break;

							case "unauthorized":
								throw new Exception('user credientials are invalid' , 401);
								break;

							default:
								throw new Exception('request not processed' , 500);
								break;
						}
					}

					// Handling Errors
					if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['errors'])) {
						$body_errors = $body->errors;
						switch ($body_errors[0]->code) {
							case "no_current_trip":
								throw new Exception('User is not currently on a trip.' , 400);
								break;
						}
					}
				}
			}

			if (!$request)
				throw new Exception('invalid ride entry' , 400);
			// Log tracking details
			$insert = $this->serviceMetaKeyRepo->UpdateUberBookDetails ($request , $booking_id , self::API_SERVICE_ID);
			if (!$insert)
				throw new Exception('Invalid booking id' , 400);


			return $insert;
		}
		catch (Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}

	}


	/**
	 * TODO : RIDE CANCEL
	 *
	 * Reference : https://developers.Ubercabs.com/docs/ride-cancel
	 */
	public function cancel ($request)
	{
		try {

			// Pass limit in param
			$request_id = $request['request_id'];
			$booking_id = $request['booking_id'];

			$user = JWTAuth::parseToken ()->toUser ();

			//TODO : change the service id once we fixed
			$user_acs_token = $user->userAccessTokenbyservice ($service_id = 2)->first ();

			//
			$client = new Client($this->getDefaultUberConfig ($user_acs_token->access_token));
			#$request_id = null;
			if (empty($request_id)) {

				// Cancel the current id
				try {
					$delete = $client->cancelCurrentRequest ();
					if (checkArrayIsEmpty ([$delete])) {
						$cancel_update = $this->serviceMetaKeyRepo->UpdateUberCancel ($booking_id , self::API_SERVICE_ID);
						if (!$cancel_update)
							throw new Exception('Invalid booking id' , 400);
					}


				}
				catch (Exception $e) {

					$body = $e->getBody ();

					if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['code'])) {

						switch ($body->code) {
							case "invalid_request_id":
								throw new Exception('request_id is invalid' , 400);
								break;

							case "unauthorized":
								throw new Exception('user credientials are invalid' , 401);
								break;

							default:
								throw new Exception('request not processed' , 500);
								break;
						}
					}
				}
			} else {
				// Cancel the current id
				try {
					$delete = $client->cancelRequest ($request_id);
					if (checkArrayIsEmpty ([$delete])) {
						$cancel_update = $this->serviceMetaKeyRepo->UpdateUberCancel ($booking_id , self::API_SERVICE_ID);
						if (!$cancel_update)
							throw new Exception('Invalid booking id' , 400);
					}
				}
				catch (Exception $e) {
					$body = $e->getBody ();
					if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['code'])) {

						switch ($body->code) {
							case "invalid_request_id":
								throw new Exception('request_id is invalid' , 400);
								break;

							case "unauthorized":
								throw new Exception('user credientials are invalid' , 401);
								break;

							default:
								throw new Exception('request not processed' , 500);
								break;
						}
					}
				}
			}

			return [];

		}
		catch (Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}

	}


	/**
	 * TODO : FEEDBACK ABOUT RIDE
	 *
	 * Reference : https://developers.Ubercabs.com/docs/ride-feedback
	 */
	public function feedback (Request $request)
	{
		try {
			// TODO : Obtain Variables

			// TODO : Based on User Obtain the Access-token (Authorization) to provide feedback of ride (user_access_token)

			// TODO : Implment Guzzle HTTP to send request


			// TODO : Handle Response

		}
		catch (Exception $e) {
			return $this->sendError ("Uber Error : " . $e->getMessage () , $e->getCode ());
		}

	}


	/**
	 * TODO : SOS SIGNAL
	 *
	 * Reference : https://developers.Ubercabs.com/docs/sos-signal
	 */
	public function sosSignal (Request $request)
	{
		try {
			// TODO : Obtain Variables

			// TODO : Based on User Obtain the Access-token (Authorization) to send sos distress call (user_access_token)

			// TODO : Implment Guzzle HTTP to send request

			// TODO : Handle Response

		}
		catch (Exception $e) {
			return $this->sendError ("Uber Error : " . $e->getMessage () , $e->getCode ());
		}

	}


	/**
	 * TODO : Booking History
	 *
	 * Reference : https://developers.Ubercabs.com/docs/my-rides
	 */
	public function history (Request $request)
	{
		try {

			// Error Response Handling
			if ($request->has ('error'))
				throw new Exception($request->get ('error') , 403);

			// Pass limit in param
			$limit = $request->has ('limit') ? $request->get ('limit') : null;
			// Pass offset in param
			$offset = $request->has ('offset') ? $request->get ('offset') : null;


			$request = [

				'limit' => $limit ,
				'offset' => $offset
			];


			$user = JWTAuth::parseToken ()->toUser ();

			//TODO : change the service id once we fixed
			$user_acs_token = $user->userAccessTokenbyservice ($service_id = 2)->first ();

			$client = new Client($this->getDefaultUberConfig ($user_acs_token->access_token));

			$client->setVersion ('v1.1'); // or v1.1
			$history = $client->getHistory (
				$request
			);

			if (!$history)
				throw new Exception('No history found' , 400);

			return $this->sendResponse ('History details listed' , 200 , $history);

		}
		catch (Exception $e) {
			Log::emergency ($e);
			return $this->sendError ("Uber Error : " . $e->getMessage () , $e->getCode ());
		}

	}

	/**
	 *  TODO : add webhook URL in uber developer dashboard after ssl certification
	 */

	public function webhook (Request $request)
	{
		/**
		 *  TODO : read X-Uber-Signature from request header and assign to $hashed_value,make hash value of the request body with the client secret as the hashing key and assign to $hashed_expected
		 */
		try {

			$myarray = $request->all ();

			$eventType 	= isset($myarray['event_type']) ? $myarray['event_type'] : null;
			$requestId  = isset($myarray['meta']['resource_id']) ? $myarray['meta']['resource_id'] : null;

			$result = [
				'event_id' 			=> isset($myarray['event_id']) ? $myarray['event_id'] : null,
				'event_time' 		=> isset($myarray['event_time'])? $myarray['event_time'] : null,
				'event_type' 		=> isset($myarray['event_type'])? $myarray['event_type'] : null,
				'meta_user_id' 		=> isset($myarray['meta']['user_id'])? $myarray['meta']['user_id'] : null,
				'meta_resource_id' 	=> isset($myarray['meta']['resource_id'] ) ? $myarray['meta']['resource_id'] : null,
				'status' 			=> isset($myarray['meta']['status'] ) ? $myarray['meta']['status'] : null,
				'resource_href' 	=> isset($myarray['resource_href']) ? $myarray['resource_href'] : null,
			];

			switch($eventType) {
				case "requests.status_changed":
				$response = $this->serviceMetaKeyRepo->updateStatusOfBookingForUber (self::API_SERVICE_ID , $result);
				break;

				case "requests.receipt_ready":
					$response = $this->callReceiptAPI($requestId);
				break;

			}

			
			return $this->sendResponse ('Webhook called successfully' , 200 , ''); //in live this does not required to give response
		}
		catch (Exception $e) {
			#echo $e->getMessage(); die;
			Log::emergency ($e);
			return $this->sendError ("Uber Error :" . $e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * @param $a
	 * @param $b
	 *
	 * @return bool
	 * to compare two hashed values.(to authenticate uber webhook call
	 */
	public function hash_compare ($a , $b)
	{
		if (!is_string ($a) || !is_string ($b)) {
			return false;
		}

		$len = strlen ($a);
		if ($len !== strlen ($b)) {
			return false;
		}

		$status = 0;
		for ($i = 0; $i < $len; $i++) {
			$status |= ord ($a[$i]) ^ ord ($b[$i]);
		}
		return $status === 0;
	}


	/**
	 * @param $a
	 * @param $b
	 *
	 * @return bool
	 * to compare two hashed values.(to authenticate uber webhook call
	 */

	private function callReceiptAPI($requestId)
	{
		#$requestId = '90f842d0-5ae2-4440-aeae-a1f0a0eb5d03';
		$booked_id_of_service_booked = ServiceMetaData
			::join ('service_meta_key' , 'service_meta_key.id' , '=' , 'service_meta_data.meta_key_id')
			->select ('service_meta_data.booked_id')
			->where ('service_meta_key.meta_key' , 'request_id')
			->where ('service_meta_key.service_id' , self::API_SERVICE_ID)
			->where ('service_meta_data.value' , $requestId)
			->getQuery ()// Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
			->get ();

		if ($booked_id_of_service_booked->count () == 0) {
			throw new Exception('booking not found', 404);
		}

		$booked_id = $booked_id_of_service_booked[0]->booked_id;
		#$booked_id = 81;
		$oServiceBooked = ServiceBooked::find($booked_id);

		$userId 		= $oServiceBooked->user_id;


		//get Ola Access Token:

		$oUser = User::find($userId);

		$oUserAccessToken = $oUser->userAccessTokenbyservice(self::API_SERVICE_ID)->first();

		$client = new Client($this->getDefaultUberConfig ($oUserAccessToken->access_token));

		#dd($client);


		try{
			$receipt = $client->getRequestReceipt($requestId);
		}
		catch(Exception $e) {
			$body = $e->getBody();
			//dd($body);
			if (checkResponseObjectProperties (self::SERVICE_NAME , $body , ['code'])) {
					switch ($body->code) {
						case "not_found":
							throw new Exception('The product not exist.' , 400);
							break;

						case "unauthorized":

							throw new Exception($body->message , 401);
							break;
						default:
							throw new Exception($body , $e->getCode ());
							break;

					}
				}
		}

		//for terms description : https://developer.uber.com/docs/riders/references/api/v1.2/requests-request_id-receipt-get

	/*	$receipt = new \stdClass;
		$receipt->subtotal = 1200;
		$receipt->total_charged = 850;
		$receipt->total_owed 	= 100;
		$receipt->total_fare 	= 1300;
		$receipt->currency_code = 'INR';
		$receipt->duration 		= 5;
		$receipt->distance 	    = 15;
		$receipt->distance_label = 'km';*/


		$subTotal 		= $receipt->subtotal;
		$totalCharged 	= $receipt->total_charged;
		$totalOwed 		= $receipt->total_owed;
		$totalFare 		= $receipt->total_fare;
		$currencyCode   = $receipt->currency_code;
		$duration 		= $receipt->duration; //HH:MM:SS format
		$distance 		= $receipt->distance;
		$distanceUnit   = $receipt->distance_label; //anyway we need to change mile to km

		if($distanceUnit == 'miles') {
			$distance 		= 1.60 * $distance;
			$distanceUnit 	= 'km';
		}


		$aData = [
			'receipt_subtotal' 		=> $subTotal,
			'receipt_total_charged' => $totalCharged,
			'receipt_total_owed'	=> $totalOwed,
			'receipt_total_fare'    => $totalFare,
			'receipt_travelled_duration' => $duration,
			'receipt_travelled_distance' => $distance,
			'distance_unit'				 => $distanceUnit

		];

		$this->serviceMetaKeyRepo->updateMetaDataValues(self::API_SERVICE_ID, $booked_id, $aData);

		return 1;


	}

}