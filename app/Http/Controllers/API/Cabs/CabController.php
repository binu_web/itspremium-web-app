<?php


namespace App\Http\Controllers\API\Cabs;

use App\Http\Controllers\API\BaseController;
use App\Repositories\ApiRepository\CabLogRepository;
use App\Repositories\ApiRepository\UserApiRepository;
use Illuminate\Http\Request;
use DB;
use Exception;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\API\Cabs\OlaController;
use App\Http\Controllers\API\Cabs\UberController;
use App\Repositories\ApiRepository\ServiceMetaKeyRepository;
use App\Models\ServiceBooked;

class CabController extends BaseController
{

	/**
	 * Define Namespace
	 *
	 * @var string
	 */
	public $namespace = 'App\Http\Controllers\API\Cabs\\';

	// Log Repository
	public $cabLogRepostiory;
	public $userApiRepo;
	public $userApiRepository;
	public $uberController;
	public $olaController;

	public $serviceMetaKeyRepo;
	public $stdObject;

	const SERVICE_NAME_UBER = 'uber';
	const SERVICE_NAME_OLA = 'ola';
	const API_SERVICE_CATEGORY_ID = 1;

	const OLA_API_SERVICE_ID  = 1;
	const UBER_API_SERVICE_ID = 2;


	/**
	 * CabController constructor.
	 *
	 * @param \App\Repositories\ApiRepository\CabLogRepository $cabLogRepository
	 */
	public function __construct (CabLogRepository $cabLogRepository , UserApiRepository $userApiRepository , ServiceMetaKeyRepository $serviceMetaKeyRepository)
	{
		$this->cabLogRepostiory = $cabLogRepository;
		$this->userApiRepository = $userApiRepository;
		$this->userApiRepository = $userApiRepository;
		$this->serviceMetaKeyRepo = $serviceMetaKeyRepository;
		$this->stdObject = new \stdClass();

		$this->stdObject->user_api = $this->userApiRepository;
		$this->stdObject->service_api = $this->serviceMetaKeyRepo;

	}


	/**
	 * Cab Controller to Authorize the Service
	 *
	 * @param $service
	 */
	public function authorize ($service)
	{

		try {
			$user = JWTAuth::parseToken ()->toUser ();
			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service accessed' , 403);

			$cab = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($cab , 'authorize'))
				throw new Exception('invalice service request accessed' , 500);

			// Call Method
			return $cab->authorize ($user->id);

		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	public function token (Request $request)
	{
		try {
			// Error Response Handling
			if ($request->has ('error'))
				throw new Exception($request->get ('error') , 400);


			// Pass UserId in State param
			$state = $request->has ('state') ? $request->get ('state') : null;

			if (is_null ($state)) {
				throw new Exception('invalid request sent' , 400);
			}

			// Obtain Service and User ID Details
			list($service , $userId) = explode (URL_VALUE_DELIMITER , $state);


			if (is_null ($userId) || empty($userId)) {
				throw new Exception('invalid request sent' , 400);
			}

			// Create Controller Class
			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service request made' , 403);

			// Declare Object

			$cab = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($cab , 'token'))
				throw new Exception('invalid service method accessed' , 500);


			// Call Service Method
			return $cab->token ($request , $userId);
		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/**
	 * Common API For Obtaining the Rides Available For user from multiple providers
	 *
	 * @author Binoop V
	 */
	public function rideSearch (Request $request)
	{
		try {

			// Pass pickup_loc in param
			$pickup_loc = $request->has ('pickup_loc') ? $request->get ('pickup_loc') : null;
			// Pass pickup_lat in param
			$pickup_lat = $request->has ('pickup_lat') ? $request->get ('pickup_lat') : null;
			// Pass pickup_lng in param
			$pickup_lng = $request->has ('pickup_lng') ? $request->get ('pickup_lng') : null;

			// Pass drop_loc in param
			$drop_loc = $request->has ('drop_loc') ? $request->get ('drop_loc') : null;
			// Pass pickup_lat in param
			$drop_lat = $request->has ('drop_lat') ? $request->get ('drop_lat') : null;
			// Pass pickup_lng in param
			$drop_lng = $request->has ('drop_lng') ? $request->get ('drop_lng') : null;

			// Pass ride_time in param
			$ride_time = $request->has ('ride_time') ? $request->get ('ride_time') : date ('Y-m-d H:i:s');
			// Pass ride_option in param
			$ride_option = $request->has ('ride_option') ? $request->get ('ride_option') : null;
			// Pass category in param
			$category = $request->has ('category') ? $request->get ('category') : null;
			// Pass service in param
			$service = $request->has ('service') ? $request->get ('service') : null;


			if (!is_numeric ($pickup_lat) || !is_numeric ($pickup_lng)) {
				throw new Exception('invalid pickup coordinates' , 401);
			}

			$request = [
				'pickup_loc' => $pickup_loc ,
				'pickup_lat' => $pickup_lat ,
				'pickup_lng' => $pickup_lng ,
				'drop_loc' => $drop_loc ,

				'drop_lat' => $drop_lat ,
				'drop_lng' => $drop_lng ,
				'ride_time' => $ride_time ,
				'ride_option' => $ride_option ,
				'category' => $category
			];


			//insert user log details
			$user = JWTAuth::parseToken ()->toUser ();

			$inser_log = $this->cabLogRepostiory->Insert_log_details ($request , $user->id);


			if (checkArrayIsEmpty ([$pickup_lat , $pickup_lng]))
				throw new Exception('pickup coordinates missing' , 400);


			// TODO : uncomment the below line once merge with branch 34
			$response = array();
			if (!empty($service)) {
				if ($service == self::SERVICE_NAME_OLA) {

					$olaClass = $this->namespace . 'OlaController';
					$olaObject = new $olaClass($this->stdObject);

					$ola = $olaObject->rideSearch ($request);
					if (checkArrayIsEmpty ($ola))
						$ola = [];
					$response = array_merge ($ola);
				} elseif ($service == self::SERVICE_NAME_UBER) {

					$UberClass = $this->namespace . 'UberController';
					$uber_object = new $UberClass($this->stdObject);

					$uber = $uber_object->rideSearch ($request);
					if (checkArrayIsEmpty ($uber))
						$uber = [];
					$response = array_merge ($uber);
				} else {
					throw new Exception('invalid service key word' , 400);
				}
			} else {
				$UberClass = $this->namespace . 'UberController';
				$uber_object = new $UberClass($this->stdObject);
				$uber = $uber_object->rideSearch ($request);
				if (checkArrayIsEmpty ($uber))
					$uber = [];

				$OlaClass = $this->namespace . 'OlaController';
				$Ola_object = new $OlaClass($this->stdObject);

				$ola = $Ola_object->rideSearch ($request);
				if (checkArrayIsEmpty ($ola))
					$ola = [];
				$response = array_merge ($ola , $uber);
			}


			return $this->sendResponse ('Cab details listed' , 200 , $response);


		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * Common API For Obtaining the specific drive details
	 *
	 * @author Binoop V
	 */

	public function getRideDetails(Request $request)
	{
		try{

			// Pass pickup_loc in param
			$pickup_loc = $request->has ('pickup_loc') ? $request->get ('pickup_loc') : null;
			// Pass pickup_lat in param
			$pickup_lat = $request->has ('pickup_lat') ? $request->get ('pickup_lat') : null;
			// Pass pickup_lng in param
			$pickup_lng = $request->has ('pickup_lng') ? $request->get ('pickup_lng') : null;

			// Pass drop_loc in param
			$drop_loc = $request->has ('drop_loc') ? $request->get ('drop_loc') : null;
			// Pass pickup_lat in param
			$drop_lat = $request->has ('drop_lat') ? $request->get ('drop_lat') : null;
			// Pass pickup_lng in param
			$drop_lng = $request->has ('drop_lng') ? $request->get ('drop_lng') : null;

			// Pass ride_time in param
			$ride_time = $request->has ('ride_time') ? $request->get ('ride_time') : date ('Y-m-d H:i:s');
			// Pass ride_option in param
			$ride_option = $request->has ('ride_option') ? $request->get ('ride_option') : null;
			// Pass category in param - ola case only
			//$category = $request->has ('category') ? $request->get ('category') : null;
			// Pass service in param
			$service = $request->has ('service') ? $request->get ('service') : null;

			$productId  = $request->has('product_id') ? $request->get('product_id') : null;

			if (!is_numeric ($pickup_lat) || !is_numeric ($pickup_lng)) {
				throw new Exception('invalid pickup coordinates' , 401);
			}


			//insert user log details
			$user = JWTAuth::parseToken ()->toUser ();

			if (checkArrayIsEmpty ([$pickup_lat , $pickup_lng]))
				throw new Exception('pickup coordinates missing' , 400);
			if(checkArrayIsEmpty ([$drop_lat , $drop_lng]))
				throw new Exception('drop coordinates missing' , 400);

			if(empty($service))
				throw new Exception ('Service name is missing', 400);


			$aResponse 		= [];

			$request = [
				'pickup_loc' => $pickup_loc ,
				'pickup_lat' => $pickup_lat ,
				'pickup_lng' => $pickup_lng ,
				'drop_loc'   => $drop_loc ,

				'drop_lat' => $drop_lat ,
				'drop_lng' => $drop_lng ,
				'ride_time' => $ride_time ,
				'category' => $productId,
				'product_id' => $productId,
			];


			if($service == self::SERVICE_NAME_OLA) {
				$olaClass = $this->namespace.'OlaController';
				$oOlaObj  = new $olaClass($this->stdObject);
				$aOla 	  = $oOlaObj->getRideDetails($request);

				if(isset($aOla['error']) && $aOla['error'])
					throw new Exception ($aOla['message'],  $aOla['code']);
				$aResponse 		= $aOla;

			}
			else if($service == self::SERVICE_NAME_UBER) {
				//

				$UberClass 		= $this->namespace . 'UberController';
				$uber_object 	= new $UberClass($this->stdObject);

				$aUber 			= $uber_object->getRideDetails ($request);

				
				if(isset($aUber['error']) && $aUber['error'])
					throw new Exception ($aUber['message'],  $aUber['code']);
				$aResponse 		= $aUber;

			}
			else {
				throw new Exception('Invalid service name', 400);
			}

			$aRes = [];
			$aRes['ride_details']	 = $aResponse;

			return $this->sendResponse ('ride details' , 200 , $aRes);
		}
		catch(Exception $e) {
			#echo $e->getMessage(); die;
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 * @return mixed
	 *
	 */
	public function book (Request $request)
	{
		try {

			// Pass pickup_lat in param
			$pickup_lat = $request->has ('pickup_lat') ? $request->get ('pickup_lat') : null;
			// Pass pickup_lng in param
			$pickup_lng = $request->has ('pickup_lng') ? $request->get ('pickup_lng') : null;
			// Pass dropLat in param
			$drop_lat = $request->has ('drop_lat') ? $request->get ('drop_lat') : null;
			// Pass drop_lng in param
			$drop_lng = $request->has ('drop_lng') ? $request->get ('drop_lng') : null;
			// Pass drop_lng in param
			$pickup_mode = $request->has ('pickup_mode') ? $request->get ('pickup_mode') : null;
			// Pass category in param
			$category = $request->has ('category') ? $request->get ('category') : [];
			// Pass product_id in param
			$product_id = $request->has ('product_id') ? $request->get ('product_id') : null;
			// Pass product_id in param
			$service = $request->has ('service') ? $request->get ('service') : null;

			$request = [
				'pickup_lat' => $pickup_lat ,
				'pickup_lng' => $pickup_lng ,
				'drop_lat' => $drop_lat ,
				'drop_lng' => $drop_lng ,
				'category' =>  $product_id,
				'pickup_mode' => $pickup_mode ,
				'product_id' => $product_id ,
			];


			// Check service keyword is available
			if (checkArrayIsEmpty ([$service]))
				throw new Exception('service keyword missing' , 400);

			// Check if Drop ate location is updated
			if (checkArrayIsEmpty ([$pickup_lat , $pickup_lng]))
				throw new Exception('pickup coordinates missing' , 400);

			// CHeck if Pick up location is updated
			if (checkArrayIsEmpty ([$drop_lat , $drop_lng]))
				throw new Exception('drop coordinates missing' , 400);

			// Create Controller Class
			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';


			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service request made' , 403);

			// Declare Object

			$cab = new $controllerClass($this->stdObject);
			// Check if Method Exists
			if (!method_exists ($cab , 'token'))
				throw new Exception('invalid service method accessed' , 500);



			// Call Service Method
			$cab_book = $cab->book ($request);
			

			// Booking Failed
			if (is_array ($cab_book) && isset($cab_book['error']))
				throw new Exception($cab_book['message'] , isset($cab_book['code']) ? $cab_book['code'] : 500);

			return $this->sendResponse ('Booking Created' , 200 , $cab_book);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 *         Reference : https://developers.olacabs.com/docs/ride-later
	 *         allows user to book a ride in a future date
	 */
	public function rideLater (Request $request)
	{

		try {
			// Pass pickup_lat in param
			$pickup_lat = $request->has ('pickup_lat') ? $request->get ('pickup_lat') : null;
			// Pass pickup_lng in param
			$pickup_lng = $request->has ('pickup_lng') ? $request->get ('pickup_lng') : null;
			// Pass dropLat in param
			$pickup_mode = $request->has ('pickup_mode') ? $request->get ('pickup_mode') : null;
			// Pass category in param
			$category = $request->has ('category') ? $request->get ('category') : [];
			//Pass pickup date with time format(DD/MM/YYYY HH:MM)
			$pickup_time = $request->has ('pickup_time') ? $request->get ('pickup_time') : null;
			// Pass product_id in param
			$service = $request->has ('service') ? $request->get ('service') : null;

			$request = [
				'pickup_lat' => $pickup_lat ,
				'pickup_lng' => $pickup_lng ,
				'category' => $category ,
				'pickup_mode' => $pickup_mode ,
				'pickup_time' => $pickup_time ,
			];
			// Check service keyword is available
			if (checkArrayIsEmpty ([$service]))
				throw new Exception('service keyword missing' , 400);

			// Check if Drop ate location is updated
			if (checkArrayIsEmpty ([$pickup_lat , $pickup_lng]))
				throw new Exception('pickup coordinates missing' , 400);

			// Check service keyword is available
			if (checkArrayIsEmpty ([$pickup_time]))
				throw new Exception('pickup time missing' , 400);

			// Create Controller Class
			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service request made' , 403);

			// Declare Object

			$cab = new $controllerClass($this->stdObject);
			// Check if Method Exists
			if (!method_exists ($cab , 'token'))
				throw new Exception('invalid service method accessed' , 500);


			// Call Service Method
			$cab_ride_later = $cab->rideLater ($request);

			// Booking Failed
			if (is_array ($cab_ride_later) && isset($cab_ride_later['error']))
				throw new Exception($cab_ride_later['message'] , 500);

			return $this->sendResponse ('Ride later confirmed. You will receive ride details 15 min before pickup time' , 200 , $cab_ride_later);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/*
	*
	*
	*
	*
	*/

	public function updateDestination(Request $request)
	{
		try{

			$bookingId 			= $request->get('booking_id');
			$newDestinationLat  = $request->get('new_destination_lat');
			$newDestinationLng  = $request->get('new_destination_lng');
			$oServiceBooked 	= ServiceBooked::find($bookingId);

			if(checkArrayIsEmpty([$bookingId]))
				throw new Exception('Booking Id required', 400);
			if(checkArrayIsEmpty([$newDestinationLat, $newDestinationLng]))
				throw new Exception('New destination cordinates are required', 400);

			if(!$oServiceBooked || (isset($oServiceBooked->id) && !in_array($oServiceBooked->service_api_id, [Self::UBER_API_SERVICE_ID, Self::OLA_API_SERVICE_ID])))
				throw new Exception ('Invalid booking id', 400);



			$serviceApiId = $oServiceBooked->service_api_id;
			$serviceId    = $oServiceBooked->service_id;

			$request 	  				= $request->toArray();
			$request['service_id'] 	 	= $serviceId;
			$request['service_api_id'] 	= $serviceApiId;

			$resp 						= 0;

			if($serviceApiId == Self::UBER_API_SERVICE_ID) {
				
				$uberClass 		= $this->namespace . 'UberController';
				$oUberObj 		= new $uberClass($this->stdObject);
				$update 		= $oUberObj->updateDestination($request);

				$resp 			= $update;
			}
			else if($serviceApiId == Self::OLA_API_SERVICE_ID) {
				$olaClass 		= $this->namespace.'OlaController';
				$oOlaObj 		= new $olaClass($this->stdObject);
				$update  		= $oOlaObj->updateDestination($request);
				$resp 			= $update;
			}

			if(isset($resp['error']))
				throw new Exception ($resp['message'], $resp['code']);

			return $this->sendResponse ('drop location updated successfully' , 200 , '');
		}
		catch(Exception $e) {
			#echo $e->getMessage();  die;
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
		

	}


	/**
	 * Cab Tracking Feature
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	public function track (Request $request)
	{
		try {

			$request_id = $request->has ('request_id') ? $request->get ('request_id') : null;
			//Pass the booking value of itzprmium booking id
			$booking_id = $request->has ('booking_id') ? $request->get ('booking_id') : null;
			// Pass product_id in param
			$service = $request->has ('service') ? $request->get ('service') : null;

			// Check service keyword is available
			if (checkArrayIsEmpty ([$service]))
				throw new Exception('service keyword missing' , 400);

			if (checkArrayIsEmpty ([$booking_id]))
				throw new Exception('booking id missing' , 400);


			$request = [
				'booking_id' => $booking_id ,
				'request_id' => $request_id ,
			];

			// Create Controller Class
			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service request made' , 403);

			// Declare Object

			$cab = new $controllerClass($this->stdObject);
			// Check if Method Exists
			if (!method_exists ($cab , 'token'))
				throw new Exception('invalid service method accessed' , 500);


			// Call Service Method
			$cab_track = $cab->track ($request);

			// Booking Failed
			if (is_array ($cab_track) && isset($cab_track['error']))
				throw new Exception($cab_track['message'] , 500);

			return $this->sendResponse ('Ride tracked and details updated' , 200 , $cab_track);


		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 *         Cancel the ride
	 */
	public function cancel (Request $request)
	{

		try {

			// Pass limit in param
			$request_id = $request->has ('request_id') ? $request->get ('request_id') : null;
			$service = $request->has ('service') ? $request->get ('service') : null;


			$booking_id = $request->has ('booking_id') ? $request->get ('booking_id') : null;
			$reason = $request->has ('reason') ? $request->get ('reason') : null;

			// Check service keyword is available
			if (checkArrayIsEmpty ([$service]))
				throw new Exception('service keyword missing' , 400);

			// TODO : Remove the comment
			if (checkArrayIsEmpty ([$booking_id]))
				throw new Exception('booking id missing' , 400);


			if (checkArrayIsEmpty ([$reason]))
				throw new Exception('reason missing' , 400);

			$request = [
				'request_id' => $request_id ,
				'booking_id' => $booking_id ,
				'reason' => $reason ,
			];

			// Create Controller Class
			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service request made' , 403);

			// Declare Object

			$cab = new $controllerClass($this->stdObject);
			// Check if Method Exists
			if (!method_exists ($cab , 'token'))
				throw new Exception('invalid service method accessed' , 500);


			// Call Service Method
			$cab_cancel = $cab->cancel ($request);

			// Cancelled Failed
			if (is_array ($cab_cancel) && isset($cab_cancel['error']))
				throw new Exception($cab_cancel['message'] , 500);

			return $this->sendResponse ('Ride canceled successfully' , 200 , $cab_cancel);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/**
	 * Get Favorite Pickup locations and Drop Off Location based on user
	 *
	 *
	 * @author Frijo
	 * @return mixed
	 *
	 */
	public function favorites (Request $request)
	{
		try {

			// finding login user details to sort favorites lists
			$user = JWTAuth::parseToken ()->toUser ();

			//finding cab favorites lists.
			$result = $this->cabLogRepostiory->getCabLog ($user->id);

			return $this->sendResponse ('Cab log lists' , 200 , $result);
		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/**
	 * Set a location has favorite
	 *
	 *
	 * @author Frijo
	 * @return mixed
	 *
	 */
	public function setFavorite (Request $request)
	{
		try {

			if (!$user = JWTAuth::parseToken ()->authenticate ()) {
				return response ()->json (['user_not_found'] , 404);
			}
			//pass favorites change parameter
			$change_fav = $request->has ('is_favorite') ? $request->get ('is_favorite') : 0;
			$location_id = $request->has ('location_id') ? $request->get ('location_id') : 0;

			if (empty ($location_id))
				throw new \Exception('location id is missing' , 400);

			//update cab log details
			$result = $this->cabLogRepostiory->setCabLogFavorite ($location_id , $change_fav);

			return $this->sendResponse ('favourites saved' , 201);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 *         get the booking details based on user
	 */
	public function history (Request $request)
	{
		try {
			$user = JWTAuth::parseToken ()->toUser ();
			$result = $this->cabLogRepostiory->getCabHistory ($user->id);
			if (empty ($result))
				throw new Exception('Cab history empty' , 403);

			return $this->sendResponse ('Cab history listed' , 200 , $result);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * Get History Details of Ride
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */

	public function historyDetails (Request $request)
	{
		try {
			// Pass limit in param
			$booking_id = $request->has ('booking_id') ? $request->get ('booking_id') : null;

			$result = $this->cabLogRepostiory->getHistoryDetails ($booking_id);
			if (empty ($result))
				throw new Exception('invalid booking id' , 403);

			return $this->sendResponse ('booking details listed' , 200 , $result);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/**
	 * @param Request $request
	 * @author Midhun
	 * check is there any ongoing rides
	 */
	public function checkOngoingRide(Request $request)
	{
		try {
			$user = JWTAuth::parseToken()->toUser();

			$aRequest = ['user_id' => $user->id, 'service_category_id' => SELF::API_SERVICE_CATEGORY_ID];

			$result = $this->cabLogRepostiory->getOngoingRideDetails($aRequest);

			// Booking Failed
			if (is_array ($result) && isset($result['error']))
				throw new Exception('Technical error' ,500);

			if (checkArrayIsEmpty ([$result]))
			{
				return $this->sendResponse ('User have no booking' , 200 , $result);

			}
			else{
				return $this->sendResponse ('User have a booking already' , 200 , $result);

			}
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	public function getCancelReasons(Request $request)
	{

		try {

			// Pass limit in param
			$booking_id = $request->has ('booking_id') ? $request->get ('booking_id') : null;
			// service will be always ola due to the call only for ola
			$service = self::SERVICE_NAME_OLA;

			// Check service keyword is available
			if (checkArrayIsEmpty ([$service]))
				throw new Exception('service keyword missing' , 400);

			// TODO : Remove the comment
			if (checkArrayIsEmpty ([$booking_id]))
				throw new Exception('booking id missing' , 400);

			$request = [
				'booking_id' => $booking_id ,
			];

			// Create Controller Class
			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service request made' , 403);

			// Declare Object

			$cab = new $controllerClass($this->stdObject);
			// Check if Method Exists
			if (!method_exists ($cab , 'getCancelReasons'))
				throw new Exception('invalid service method accessed' , 500);


			// Call Service Method
			$cancel_reasons = $cab->getCancelReasons ($request);

			if (checkArrayIsEmpty ([$cancel_reasons]))
				throw new Exception('No reasons found' , 500);

			// Cancelled Failed
			if (is_array ($cancel_reasons) && isset($cancel_reasons['error']))
				throw new Exception($cancel_reasons['message'] , 500);

				return $this->sendResponse ('Cancel reasons and cancellation policy listed' , 200 , $cancel_reasons);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}
}
