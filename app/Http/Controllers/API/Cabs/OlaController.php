<?php

namespace App\Http\Controllers\API\Cabs;

use App\Http\Controllers\API\BaseController;
use App\Models\ServiceMetaData;
use App\Models\ServiceMetaKey;
use App\Models\UserApiAccessToken;
use App\Repositories\ApiRepository\ServiceMetaKeyRepository;
use App\Repositories\ApiRepository\UserAccessTokenRepository;
use App\Repositories\ApiRepository\UserApiRepository;
use App\Services\HttpClient;
use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use DB;


/**
 * SERVICES RELATED TO OLA CAB SERVICES
 *
 * Class OlaController
 *
 * @package App\Http\Controllers\API\Cabs
 */
class OlaController extends BaseController
{
	/**
	 *  TODO : App Service Deafult Constant (Based on Service Added)
	 */
	const API_SERVICE_CATEGORY_ID = 1;
	const API_SERVICE_ID = 1;
	const SERVICE_NAME = 'ola';

	/**
	 * Ola Server Credientials
	 */
	const X_APP_TOKEN = 'eb2a6426918a4daf8018f17f19634070';                        // To be passed via each ola api request via header.
	const CLIENT_ID = 'M2JlNzI3NzctOGJkMi00ZjZkLWI5YzUtN2JkNjdhNzk0OThh';

	/**
	 * Ola OAuth URL
	 */
	const PRODUCTION_O_AUTH_URL = 'https://sandbox-t1.olacabs.com/oauth2/authorize?response_type=token&client_id=&redirect_uri=http://binu.itzpremium.demo/api/cab/ola/token&scope=profile%20booking&state=';
//	const SANDBOX_O_AUTH_URL = 'https://sandbox-t1.olacabs.com/oauth2/authorize?response_type=token&client_id=M2JlNzI3NzctOGJkMi00ZjZkLWI5YzUtN2JkNjdhNzk0OThh&redirect_uri=http://binu.itzpremium.demo/api/cab/ola/token&scope=profile%20booking&state=';
	const SANDBOX_O_AUTH_URL = 'https://sandbox-t1.olacabs.com/oauth2/authorize?response_type=token&client_id=M2JlNzI3NzctOGJkMi00ZjZkLWI5YzUtN2JkNjdhNzk0OThh&redirect_uri=https://kometonline.com/staging/itzpremium/api/cab/access-token&scope=profile%20booking&state=';

	/**
	 * OLA API URL
	 */
	const SANDBOX_URL = 'http://sandbox-t.olacabs.com/v1/';
	const SANDBOX_URL_2 = 'http://sandbox-t1.olacabs.com/';
	const PRODUCTION_URL = '';


	/**
	 * OLA API ENDPOINTS
	 */
	const EP_RIDE_AVAILABILITY = 'products';
	const EP_RIDE_ESTIMATE = 'products';
	const EP_CAB_BOOKING = 'bookings/create';
	const EP_RIDE_TRACKING = 'bookings/track_ride';
	const EP_RIDE_CANCELLATION = 'bookings/cancel';
	const EP_BOOKING_FEEDBACK = 'bookings/feedback';
	const EP_SOS_SIGNAL = 'sos/signal';
	const EP_BOOKING_HISTORY = 'bookings/my_rides';
	const EP_UPDATE_DROP_LOCATION  = 'bookings/drop_location';
	const EP_GET_CANCEL_REASONS  = 'bookings/cancel/reasons';

	/**
	 * Ola default latitude and longitude locations
	 */

	const pickup_lat = '13.007046';
	const pickup_lng = '77.688839';
	const drop_lat = '13.017046';
	const drop_lng = '77.698839';

	const OLA_CAB_CATEGORIES = ['micro' , 'mini' , 'sedan' , 'prime' , 'lux' , 'suv' , 'rental' , 'outstation']; //share - omitted


	// Define Variables
	protected $user_id;
	protected $userApiRepo , $userAccessTokenRepository;
	public $serviceMetaKeyRepo;

	/**
	 * OlaController constructor.
	 *
	 * @param \App\Repositories\ApiRepository\UserApiRepository $userApiRepo
	 */

	public function __construct (\stdClass $stdObject)
	{

		$this->userApiRepo = property_exists ($stdObject , 'user_api') ? $stdObject->user_api : new UserApiRepository();
		$this->serviceMetaKeyRepo = property_exists ($stdObject , 'service_api') ? $stdObject->service_api : new ServiceMetaKeyRepository();

	}

	/**
	 * Get OAuth URL
	 *
	 * @return string
	 */
	public function getOAuthUrl ()
	{
		return (env ('APP_ENV' , 'production') == 'production') ? self::PRODUCTION_O_AUTH_URL : self::SANDBOX_O_AUTH_URL;
	}


	/**
	 * Get API url
	 *
	 * @return string
	 */
	public function getUrl ($sand_box_1 = null)
	{

		if ($sand_box_1 != null)
			return (env ('APP_ENV' , 'production') == 'production') ? self::PRODUCTION_URL : self::SANDBOX_URL;

		return (env ('APP_ENV' , 'production') == 'production') ? self::PRODUCTION_URL : self::SANDBOX_URL_2;

	}


	/**
	 * Authorize URL to Register in OLA
	 *
	 * @author Binoop V
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function authorize ($userId)
	{
		try {

			$state = self::SERVICE_NAME . URL_VALUE_DELIMITER . $userId;
			$url = $this->getOAuthUrl () . $state;
			return redirect ($url);
		}
		catch (Exception $e) {
			throw exception ($e);

		}
	}

	/**
	 * Ola Redirect URL to user oauth.
	 *
	 * Reference : https://developers.olacabs.com/docs/access-token
	 *
	 * Sample response
	 * Success :
	 * http://binu.itzpremium.demo/api/ola/token/1#access_token=9e2c88b352584eacaa6e00fa574689eb&state=state123&scope=profile%20booking&token_type=bearer&expires_in=15552000
	 * Error : http://binu.itzpremium.demo/api/ola/token/1#error=access_denied&state=state123
	 *
	 * @author Binoop V
	 */
	public function token (Request $request , $userId)
	{
		try {

			$requestParams = $request->all ();

			// Check if Access token is generated
			$access_token = $request->has ('access_token') ? $request->get ('access_token') : null;
			$token_expiry = $request->has ('expires_in') ? $request->get ('expires_in') : null;
			if (is_null ($access_token))
				throw new Exception('access token not generated' , 401);

			// Check if User Token Exists for A User
			$token_id = $this->userApiRepo->checkTokenExists ($userId , self::API_SERVICE_ID);

			if ($token_id) {
				// TODO : update the api access
				$status = $this->userApiRepo->updateToken (['access_token' => $access_token , 'token_expiry' => $token_expiry , 'is_active' => 1] , $token_id);

			} else {
				// TODO : create api access token
				$status = $this->userApiRepo->createToken (['access_token' => $access_token , 'token_expiry' => $token_expiry , 'service_api_id' => self::API_SERVICE_ID , 'user_id' => $userId]);
			}


			if (!$status)
				throw new Exception('access token not saved' , 500);

			return $this->sendResponse ('user is registered' , 201);

		}
		catch (Exception $e) {
			return $this->sendError ("Ola Error :" . $e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * Ola OAuth Access Token Redirect URL
	 *
	 * Reference : https://developers.olacabs.com/docs/webhook
	 * TODO : Implement feature to detect state changes & send push notification to devices.
	 *
	 * @author Binoop V
	 */
	public function webhook (Request $request)
	{
		try {

			Log::info ("STATE CHANGE REQUEST FROM OLA :" . serialize ($request->all ()));

			if ($request->has ('request_type') && $request->get ('request_type' , '') == 'WEBHOOK_URL_VALIDATION') {
				Log::warning ("Request is correct");
				return response ()->json (['success' => true , 'name' => 'webandcrafts']);
			}
			if ($request->has ('booking_status')) {
				$status = $request->get ('booking_status');
				switch ($status) {
					case "CALL_DRIVER":
						$myarray = $request->all ();
						$result = ['booking_id' => $myarray['booking_id'] ,
							'booking_status' => $myarray['booking_status'] ,
							'timestamp' => $myarray['timestamp'] ,
							'driver_name' => $myarray['driver_name'] ,
							'driver_number' => $myarray['driver_number'] ,
							'cab_type' => $myarray['cab_type'] ,
							'cab_number' => $myarray['cab_number'] ,
							'car_model' => $myarray['car_model'] ,
							'car_color' => $myarray['car_color'] ,
						];

						$response = $this->serviceMetaKeyRepo->updateStatusOfBookingForOla (self::API_SERVICE_ID , $result);
						break;
					case "CLIENT_LOCATED":

						$myarray = $request->all ();
						$result = ['booking_id' => $myarray['booking_id'] ,
							'booking_status' => $myarray['booking_status'] ,
							'timestamp' => $myarray['timestamp'] ,
							'driver_lat' => $myarray['driver_lat'] ,
							'driver_lng' => $myarray['driver_lng']
						];

						$response = $this->serviceMetaKeyRepo->updateStatusOfBookingForOla (self::API_SERVICE_ID , $result);
						break;
					case "IN_PROGRESS":
						$myarray = $request->all ();
						$result = ['booking_id' => $myarray['booking_id'] ,
							'booking_status' => $myarray['booking_status'] ,
							'timestamp' => $myarray['timestamp']
						];

						$response = $this->serviceMetaKeyRepo->updateStatusOfBookingForOla (self::API_SERVICE_ID , $result);
						break;
					case "BOOKING_COMPLETED":

						$myarray = $request->all ();
						$result = ['booking_id' => $myarray['booking_id'] ,
							'booking_status' => $myarray['booking_status'] ,
							'timestamp' => $myarray['timestamp'] ,

							'trip_info_amount' => $myarray['trip_info']->amount ,
							'trip_info_payable_amount' => $myarray['trip_info']->payable_amount ,
							'trip_info_distance_value' => $myarray['trip_info']->distance->value ,
							'trip_info_distance_unit' => $myarray['trip_info']->distance->unit ,
							'trip_info_wait_time_value' => $myarray['trip_info']->wait_time->value ,
							'trip_info_wait_time_unit' => $myarray['trip_info']->wait_time->unit ,
							'trip_info_discount' => $myarray['trip_info']->discount ,
							'trip_info_advance' => $myarray['trip_info']->advance ,
							'driver_name' => $myarray['driver_name']
						];

						$response = $this->serviceMetaKeyRepo->updateStatusOfBookingForOla (self::API_SERVICE_ID , $result);
						break;
					case "CANCELLED":
						$myarray = $request->all ();
						$result = ['booking_id' => $myarray['booking_id'] ,
							'booking_status' => $myarray['booking_status'] ,
							'cancellation_reason' => $myarray['cancellation_reason'] ,
							'timestamp' => $myarray['timestamp']
						];

						$response = $this->serviceMetaKeyRepo->updateStatusOfBookingForOla (self::API_SERVICE_ID , $result);
						break;
					case "ALLOTMENT_FAILED":
						$myarray = $request->all ();
						$result = ['booking_id' => $myarray['booking_id'] ,
							'booking_status' => $myarray['booking_status'] ,
							'cancellation_reason' => $myarray['cancellation_reason'] ,
							'timestamp' => $myarray['timestamp']
						];
						$response = $this->serviceMetaKeyRepo->updateStatusOfBookingForOla (self::API_SERVICE_ID , $result);

						break;
					default:
						throw new Exception('No proper status found' , 404);
				}
				return $response;
			} else {
				throw new Exception('No status found' , 404);
			}

		}
		catch (Exception $e) {
			return $this->sendError ("Ola Error :" . $e->getMessage () , $e->getCode ());
		}

	}


	/**
	 * TODO : RIDE SEARCH & RIDE ESTIMATE
	 *
	 * Reference : https://developers.olacabs.com/docs/ride-availablity
	 */
	public function rideSearch ($request)
	{
		try {

			// state ola account is not available first
			$ola_account_exists = FALSE;
			// Create Client Object
			$client = new HttpClient (['headers' => ['X-APP-TOKEN' => self::X_APP_TOKEN]]);

			// Taken the user details
			$user = JWTAuth::parseToken ()->toUser ();
			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();
			if ($user_acs_token)
				$ola_account_exists = TRUE;


			if (checkArrayIsEmpty ([$request['drop_lat'] , $request['drop_lng']])) {

				$request = [
					'pickup_lat' => (self::pickup_lat == null) ? $request['pickup_lat'] : self::pickup_lat ,
					'pickup_lng' => (self::pickup_lng == null) ? $request['pickup_lng'] : self::pickup_lng ,
				];


				// Set Up Client Request
				$client->setUrl (self::SANDBOX_URL . self::EP_RIDE_AVAILABILITY);
				$client->setParams ($request);
				// Obtain Response
				$response = $client->get ();

				foreach ($response->data->categories as $resp) {
					$ola_response[] = [
						'service_name' => 'ola' ,
						'service_id' => Self::API_SERVICE_CATEGORY_ID ,
						'product_id' => $resp->id ,
						'capacity' => null ,
						'image' => $resp->image ,
						'display_name' => $resp->display_name ,
						'eta' => $resp->eta ,
						'ride_later_enabled' => $resp->ride_later_enabled ,
						'account_exists' => $ola_account_exists];
				}


			} else {
				$request = [
					'pickup_lat' => (self::pickup_lat == null) ? $request['pickup_lat'] : self::pickup_lat ,
					'pickup_lng' => (self::pickup_lng == null) ? $request['pickup_lng'] : self::pickup_lng ,
					'drop_lat' => (self::drop_lat == null) ? $request['drop_lat'] : self::drop_lat ,
					'drop_lng' => (self::drop_lng == null) ? $request['drop_lng'] : self::drop_lng ,
				];

				// Set Up Client Request
				$client->setUrl (self::SANDBOX_URL . self::EP_RIDE_AVAILABILITY);
				$client->setParams ($request);
				// Obtain Response
				$response = $client->get ();
				// find the price details
				$rideEstimates = [];
				foreach ($response->data->ride_estimate as $rideEstimate) {

					$rideEstimates[$rideEstimate->category] = [$rideEstimate];
				}

				// combined the final result
				$ola_response = [];

				#echo json_encode($response->data); die;

				foreach ($response->data->categories as $categories) {

					if ($categories->id == 'share') {
						continue;
					}

					if (!isset($rideEstimates[$categories->id]))
						continue;

					$ola_response[] = [
						'service_name' => 'ola' ,
						'service_id' => Self::API_SERVICE_CATEGORY_ID ,
						'product_id' => $categories->id ,
						'capacity' => null ,
						'image' => $categories->image ,
						'display_name' => $categories->display_name ,
						'eta' => $categories->eta ,
						'amount_max' => $rideEstimates[$categories->id][0]->amount_max ,
						'distance' => $rideEstimates[$categories->id][0]->distance ,
						'duration' => $rideEstimates[$categories->id][0]->travel_time_in_minutes ,
						'price_estimate' => $rideEstimates[$categories->id][0]->amount_min . '-' . $rideEstimates[$categories->id][0]->amount_max ,
						'currency' => $categories->currency ,
						'ride_later_enabled' => $categories->ride_later_enabled ,
						'account_exists' => $ola_account_exists ,
					];
				}
			}
			if (!$ola_response)
				throw new Exception('ola cab details not listed' , 400);

			return $ola_response;
		}
		catch (Exception $e) {
			$e->serviceError = self::SERVICE_NAME;
			httpFailureLog ($e);
			return ['error' => TRUE , 'message' => $e->getMessage () , 'code' => $e->getCode ()];
		}

	}


	/**
	 *
	 *
	 *
	 *
	 */

	public function getRideDetails ($request)
	{
		try {


			$pickUpLocation = $request['pickup_loc'];
			$pickUpLat = $request['pickup_lat'];
			$pickUpLong = $request['pickup_lng'];
			$dropLoc = $request['drop_loc'];
			$dropLat = $request['drop_lat'];
			$dropLong = $request['drop_lng'];
			$rideTime = $request['ride_time'];
			$rideCategory = $request['category'];


			if (!$rideCategory || !in_array ($rideCategory , Self::OLA_CAB_CATEGORIES))
				throw new Exception('Cab category/product id is invalid or missing' , 400);


			// state ola account is not available first
			$ola_account_exists = FALSE;

			// Create Client Object
			$client = new HttpClient (['headers' => ['X-APP-TOKEN' => self::X_APP_TOKEN]]);
			// Taken the user details

			$user = JWTAuth::parseToken ()->toUser ();


			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();
			if ($user_acs_token)
				$ola_account_exists = TRUE;

			$aRequest = [
				'pickup_lat' => $pickUpLat ,
				'pickup_lng' => $pickUpLong ,
				'drop_lat' => $dropLat ,
				'drop_lng' => $dropLong ,
				'category' => $rideCategory
			];

			// Set Up Client Request
			$client->setUrl (self::SANDBOX_URL . self::EP_RIDE_AVAILABILITY);
			$client->setParams ($request);
			// Obtain Response
			$response = $client->get ();

			//dd($response);
			// find the price details
			$rideEstimates = [];

			if (checkResponseObjectProperties (self::SERVICE_NAME , $response->data , ['code'])) {

				switch ($response->data->code) {
					case "INVALID_CITY":
						throw new Exception($response->data->message , 403);
					default:
						throw new Exception($response->data->message , 400);
				}
			}


			foreach ($response->data->ride_estimate as $rideEstimate) {
				$rideEstimates[$rideEstimate->category] = $rideEstimate;

			}

			if (!$rideEstimates)
				throw new Exception('Ride estimates not exists for this cab' , 404);

			$oCategory = isset($response->data->categories[0]) ? $response->data->categories[0] : [];

			if (!$oCategory)
				throw new Exception ('Cab not available' , 404);

			if (!isset($rideEstimates[$oCategory->id]))
				throw new Exception('Ride estimates(2) not exists for this cab' , 404);

			$aOlaRespone = [
				'service_name' => 'ola' ,
				'service_id' => Self::API_SERVICE_CATEGORY_ID ,
				'product_id' => $oCategory->id ,
				'capacity' => null ,
				'image' => $oCategory->image ,
				'display_name' => $oCategory->display_name ,
				'eta' => $oCategory->eta ,
				'amount_max' => $rideEstimates[$oCategory->id]->amount_max ,
				'distance' => $rideEstimates[$oCategory->id]->distance ,
				'duration' => $rideEstimates[$oCategory->id]->travel_time_in_minutes ,
				'price_estimate' => $rideEstimates[$oCategory->id]->amount_min . '-' . $rideEstimates[$oCategory->id]->amount_max ,
				'currency' => $oCategory->currency ,
				'ride_later_enabled' => $oCategory->ride_later_enabled ,
				'account_exists' => $ola_account_exists ,
			];


			//dd($aOlaRespone);
			return $aOlaRespone;


			// combined the final result
			#$aOlaResponse = 


		}
		catch (Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage () , 'code' => $e->getCode ()];
		}
	}


	/**
	 * TODO : RIDE BOOKING
	 *
	 * Reference :https://developers.olacabs.com/docs/cab-booking
	 */
	public function book ($request)
	{
		try {
			//Create array for client request
			$request = [
				'pickup_lat' => $request['pickup_lat'] ,
				'pickup_lng' => $request['pickup_lng'] ,
				'drop_lat' => $request['drop_lat'] ,
				'drop_lng' => $request['drop_lng'] ,
				'category' => $request['category'] ,
				'pickup_mode' => $request['pickup_mode'] ,
			];

			// Check category keyword is available
			if (checkArrayIsEmpty ([$request['category']]))
				throw new Exception('car category is missing' , 400);

			// Check pickup mode keyword is available
			if (checkArrayIsEmpty ([$request['pickup_mode']]))
				throw new Exception('pickup mode is missing' , 400);

			// Taken the user details
			$user = JWTAuth::parseToken ()->toUser ();
			// Find the user access token once logged in the service
//            $get_ola_token = $this->userAccessTokenRepository->getAccestoken($user->id);
			//TODO : change the service id once we fixed
			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();

			// Create Client Object
			$client = new HttpClient (['headers' => ['X-APP-TOKEN' => self::X_APP_TOKEN , 'Authorization' => 'Bearer ' . $user_acs_token->access_token , 'Content-Type' => "application/json"]]);


			$aRideRequest = [
				'pickup_lat' => $request['pickup_lat'] ,
				'pickup_lng' => $request['pickup_lng'] ,
				'drop_lat' => $request['drop_lat'] ,
				'drop_lng' => $request['drop_lng'] ,
				'category' => $request['category']
			];

			// Set Up Client Request - get ride estimate
			$client->setUrl (self::SANDBOX_URL . self::EP_RIDE_AVAILABILITY);
			$client->setParams ($aRideRequest);
			// Obtain Response
			$rideEstimate = $client->get ();


			$aRideEst = [];
			if (isset($rideEstimate->data->ride_estimate)) {
				foreach ($rideEstimate->data->ride_estimate as $ride) {
					$aRideEst[$ride->category] = $ride;

				}
			}


			$oCategory = isset($rideEstimate->data->categories[0]) ? $rideEstimate->data->categories[0] : [];

			$category = $request['category'];
			$minFare = isset($aRideEst[$category]->amount_min) ? $aRideEst[$category]->amount_min : null;
			$maxFare = isset($aRideEst[$category]->amount_max) ? $aRideEst[$category]->amount_max : null;
			$distance = isset($aRideEst[$category]->distance) ? $aRideEst[$category]->distance : null;
			$duration = isset($aRideEst[$category]->travel_time_in_minutes) ? $aRideEst[$category]->travel_time_in_minutes : null;
			$currency = isset($oCategory->currency) ? $oCategory->currency : null;

			$aOtherparams = [
				'min_fare' => $minFare ,
				'max_fare' => $maxFare ,
				'distance' => $distance ,
				'duration' => $duration ,
				'currency' => $currency ,
				'drop_lat' => $request['drop_lat'] ,
				'drop_lng' => $request['drop_lng'] ,
				'pickup_lat' => $request['pickup_lat'] ,
				'pickup_lng' => $request['pickup_lng']

			];


			// Set Up Client Request booking
			$client->setUrl ($this->getUrl ('sandbox1') . self::EP_CAB_BOOKING);
			$client->setParams ($request);

			// Obtain Response
			$response = $client->post ();


			// Check if Response is available
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {

				$responseData = $response->data;

				// Check if error is in response
				if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['code'])) {

					switch ($responseData->code) {
						case "INVALID_PICKUP_MODE":
							throw new Exception('pickup mode is incorrect' , 400);
							break;
						case "INVALID_CAR_CATEGORY":
							throw new Exception('car category is incorrect' , 400);
							break;
						case "INVALID_LAT_LNG":
							throw new Exception('latitude or longitude is incorrect' , 400);
							break;
						case "INVALID_DROP_LAT_LNG":
							throw new Exception('drop latitude or longitude is incorrect' , 400);
							break;
						case "NO_CABS_AVAILABLE":
							throw new Exception('ride you were trying to book is no longer available due to high demand' , 400);
							break;
						case "SAME_PICKUP_DROP":
							throw new Exception('pickup and drop at locations are same' , 400);
							break;
						case "BOOKING_RESPONSE_FAILURE":
							throw new Exception('Sorry, something went wrong. Please try again later.' , 400);
							break;

					}

				}


				// Log booking details
				$insert = $this->serviceMetaKeyRepo->addOladBookDetails (self::API_SERVICE_CATEGORY_ID , self::API_SERVICE_ID , $user->id , $responseData , $aOtherparams);
			}
			return $insert;

		}
		catch (Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage () , 'code' => $e->getCode ()];
		}

	}

	/**
	 * @param Request $request
	 *
	 * @return mixed
	 * TODO:Ride Later
	 * Reference : https://developers.olacabs.com/docs/ride-later
	 */
	public function rideLater ($request)
	{
		try {
			//Create array for client request
			$request = [
				'pickup_lat' => $request['pickup_lat'] ,
				'pickup_lng' => $request['pickup_lng'] ,
				'category' => $request['category'] ,
				'pickup_mode' => $request['pickup_mode'] ,
				'pickup_time' => $request['pickup_time'] ,

			];

			// Check category keyword is available
			if (checkArrayIsEmpty ([$request['category']]))
				throw new Exception('car category is missing' , 400);

			// Check pickup mode keyword is available
			if (checkArrayIsEmpty ([$request['pickup_mode']]))
				throw new Exception('pickup mode is missing' , 400);

			// Taken the user details
			$user = JWTAuth::parseToken ()->toUser ();

			// User Access token
			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();

			// Create Client Object
			$client = new HttpClient (['headers' => ['X-APP-TOKEN' => self::X_APP_TOKEN , 'Authorization' => 'Bearer ' . $user_acs_token->access_token , 'Content-Type' => "application/json"]]);

			// Set Up Client Request
			$client->setUrl ($this->getUrl ('sand_box1') . self::EP_CAB_BOOKING);
			$client->setParams ($request);

			// Obtain Response
			$response = $client->post ();

			// Check if Response is available
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {

				$responseData = $response->data;

				// Check if error is in response
				if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['code'])) {

					switch ($responseData->code) {
						case "PICK_TIME_NOT_IN_RANGE":
							throw new Exception('pickup time not in the range' , 400);
							break;
						case "INVALID_PICK_UP_TIME_FORMAT":
							throw new Exception('time mode is incorrect' , 400);
							break;
						case "RIDE_LATER_NOT_AVAILABLE_FOR_CATEGORY":
							throw new Exception('ride later is not available for this category' , 400);
							break;
						case "INVALID_PICKUP_MODE":
							throw new Exception('pickup mode is incorrect' , 400);
							break;
						case "INVALID_CAR_CATEGORY":
							throw new Exception('car category is incorrect' , 400);
							break;
						case "INVALID_LAT_LNG":
							throw new Exception('latitude or longitude is incorrect' , 400);
							break;
						case "INVALID_DROP_LAT_LNG":
							throw new Exception('drop latitude or longitude is incorrect' , 400);
							break;
						case "NO_CABS_AVAILABLE":
							throw new Exception('ride you were trying to book is no longer available due to high demand' , 400);
							break;
						case "SAME_PICKUP_DROP":
							throw new Exception('pickup and drop at locations are same' , 400);
							break;
						case "BOOKING_RESPONSE_FAILURE":
							throw new Exception('Sorry, something went wrong. Please try again later.' , 400);
							break;

					}

				}
				// Log booking details
				$insert = $this->serviceMetaKeyRepo->addOlaRideLater (self::API_SERVICE_CATEGORY_ID , self::API_SERVICE_ID , $user->id , $responseData);
			}

			return $insert;

		}
		catch (Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}

	}


	/**
	 * TODO :
	 * To Get Ride Tracking
	 * Reference : https://developers.Ubercabs.com/docs/cab-track-ride
	 */

	public function updateDestination ($request)
	{
		try {
			$bookingId = $request['booking_id'];
			$newDestinationLat = $request['new_destination_lat'];
			$newDestinationLng = $request['new_destination_lng'];
			$serviceId = $request['service_id'];
			$serviceApiId = $request['service_api_id'];

			$serviceMetaKeys = ['booking_id' , 'cab_type' , 'pickup_lat' , 'pickup_lng'];

			$aMetaValues = $this->serviceMetaKeyRepo->getMetaKeyValues ($serviceApiId , $bookingId , $serviceMetaKeys);
			//dd($aMetaValues); die;

			$bookingId = $aMetaValues['booking_id'];
			$pickupLat = $aMetaValues['pickup_lat'];
			$pickupLng = $aMetaValues['pickup_lng'];
			$cabCategory = $aMetaValues['cab_type'];

			if (!$bookingId)
				throw new Exception ('invalid booking id' , 400);


			DB::beginTransaction ();

			// Taken the user details
			$user = JWTAuth::parseToken ()->toUser ();
			// Find the user access token once logged in the service
//            $get_ola_token = $this->userAccessTokenRepository->getAccestoken($user->id);
			//TODO : change the service id once we fixed
			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();

			// Create Client Object
			$client = new HttpClient (['headers' => ['X-APP-TOKEN' => self::X_APP_TOKEN , 'Authorization' => 'Bearer ' . $user_acs_token->access_token , 'Content-Type' => "application/json"]]);

			$client->setUrl ($this->getUrl ('sandbox1') . self::EP_UPDATE_DROP_LOCATION);

			$aUpdRequest = [
				'drop_lat' => $newDestinationLat ,
				'drop_lng' => $newDestinationLng ,
				'booking_id' => $bookingId
			];

			#print_r($aUpdRequest); die;
			$client->setParams ($aUpdRequest);

			$response = $client->post ();


			$data = isset($response->data) ? $response->data : null;

			if ($data) {
				$status = isset($data->status) ? $data->status : null;
				$msg = isset($data->text) ? $data->text : null;
			}

			if ($status == 'FAILURE' || !$status) {
				if (!$msg) $msg = 'update failed';
				throw new Exception($msg , 400);
			} else {
				//get new ride estimate:

				$aRideRequest = [
					'pickup_lat' => $pickupLat ,
					'pickup_lng' => $pickupLng ,
					'drop_lat' => $newDestinationLat ,
					'drop_lng' => $newDestinationLng ,
					'category' => $cabCategory
				];

				// Set Up Client Request - get ride estimate
				$client->setUrl (self::SANDBOX_URL . self::EP_RIDE_AVAILABILITY);
				$client->setParams ($aRideRequest);
				// Obtain Response
				$rideEstimate = $client->get ();

				$aRideEst = [];
				if (isset($rideEstimate->data->ride_estimate)) {
					foreach ($rideEstimate->data->ride_estimate as $ride) {
						$aRideEst[$ride->category] = $rideEstimate;

					}
				}

				$oCategory = isset($response->data->categories[0]) ? $response->data->categories[0] : [];

				$category = $cabCategory;
				$minFare = isset($aRideEst[$category]->amount_min) ? $aRideEst[$category]->amount_min : null;
				$maxFare = isset($aRideEst[$category]->amount_max) ? $aRideEst[$category]->amount_max : null;
				$distance = isset($aRideEst[$category]->distance) ? $aRideEst[$category]->distance : null;
				$duration = isset($aRideEst[$category]->travel_time_in_minutes) ? $aRideEst[$category]->travel_time_in_minutes : null;
				$currency = isset($oCategory->currency) ? $oCategory->currency : null;

				$aRideparams = [
					'min_fare' => $minFare ,
					'max_fare' => $maxFare ,
					'distance' => $distance ,
					'duration' => $duration ,
					'currency' => $currency ,
					'drop_lat' => $newDestinationLat ,
					'drop_lng' => $newDestinationLng

				];

				$this->serviceMetaKeyRepo->updateMetaDataValues (self::API_SERVICE_ID , $bookingId , $aRideparams);

				DB::commit ();

			}


			return 1;


		}
		catch (Exception $e) {
			DB::rollback ();
			return ['error' => TRUE , 'message' => $e->getMessage () , 'code' => $e->getCode ()];
		}
	}


	/**
	 * TODO : RIDE TRACKING
	 *
	 * Reference : https://developers.olacabs.com/docs/cab-track-ride
	 *
	 * @auther Developer 12
	 *
	 * ola tracking ride
	 */
	public function track ($request)
	{
		try {

			$booking_id = $request['booking_id'];
			//user login  Details
			$user = JWTAuth::parseToken ()->toUser ();

			//TODO : change the service id once we fixed
			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();

			$client = new HttpClient (['headers' => ['X-APP-TOKEN' => self::X_APP_TOKEN , 'Authorization' => 'Bearer ' . $user_acs_token->access_token]]);

			// Collect the uber booking_id from service_meta_table
			$ola_booking_id = $this->serviceMetaKeyRepo->getOlaBookingdetails ($booking_id , self::API_SERVICE_ID);
			if (!$ola_booking_id)
				throw new Exception('Invalid ola booking id' , 400);


			$request = [
				'booking_id' => $ola_booking_id ,
			];

			// Set Up Client Request
			$client->setUrl ($this->getUrl ('sandbox1') . self::EP_RIDE_TRACKING);
			$client->setParams ($request);

			// Obtain Response
			$response = $client->get ();
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {
				$responseData = $response->data;
				// Handling error conditions
				if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['code'])) {
					switch ($response->data->code) {
						case "BOOKING_ID_BELONGS_TO_DIFFERENT_USER":
							throw new Exception('booking id is incorrect' , 400);
					}


				}
				// Log tracking details
				$update = $this->serviceMetaKeyRepo->UpdateolaBookDetails ($responseData , $booking_id , self::API_SERVICE_ID);

				return $update;

			}
		}
		catch (Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}
	}

	/**
	 * TODO : RIDE CANCEL
	 *
	 * Reference : https://developers.olacabs.com/docs/ride-cancel
	 */
	public function cancel ($request)
	{
		try {

			$booking_id = $request['booking_id'];
			$reason = $request['reason'];

			//user login  Details
			$user = JWTAuth::parseToken ()->toUser ();

			// get user access token by user id
			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();

			//ola api data
			$client = new HttpClient (['headers' => ['X-APP-TOKEN' => self::X_APP_TOKEN , 'Authorization' => 'Bearer ' . $user_acs_token->access_token]]);

			// Collect the uber booking_id from service_meta_table
			$ola_booking_id = $this->serviceMetaKeyRepo->getOlaBookingdetails ($booking_id , self::API_SERVICE_ID);
			if (!$ola_booking_id)
				throw new Exception('Invalid ola booking id' , 400);

			$request = [
				'booking_id' => $ola_booking_id ,
				'reason' => $reason ,
			];

			// Set Up Client Request
			$client->setUrl ($this->getUrl () . self::EP_RIDE_CANCELLATION);
			$client->setParams ($request);

			// Obtain Response
			$response = $client->get ();
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {

				$responseData = $response->data;
				if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['code'])) {
					switch ($responseData->code) {
						case "INVALID_CRN_ID":
							throw new Exception('booking cancellation failed' , 400);
					}
				}
				if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['reason'])) {

					switch ($responseData->reason) {
						case "INVALID_BOOKING_ID":
							throw new Exception('booking does not exist' , 400);
					}
				}
			}
			return $responseData;
		}
		catch (Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}


	}


	/**
	 * TODO : FEEDBACK ABOUT RIDE
	 *
	 * Reference : https://developers.olacabs.com/docs/ride-feedback
	 */
	public function feedback (Request $request)
	{
		try {
			// TODO : Obtain Variables

			// TODO : Based on User Obtain the Access-token (Authorization) to provide feedback of ride (user_access_token)

			// TODO : Implment Guzzle HTTP to send request

			// TODO : Handle Response

		}
		catch (Exception $e) {
			return $this->sendError ("Ola Error :" . $e->getMessage () , $e->getCode ());
		}

	}


	/**
	 * TODO : SOS SIGNAL
	 *
	 * Reference : https://developers.olacabs.com/docs/sos-signal
	 */
	public function sosSignal (Request $request)
	{
		try {

			//user login  Details
			if (!$user = JWTAuth::parseToken ()->toUser ()) {
				throw new \Exception('User not found' , 404);
			}


			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();

			//ola api data
			$client = new HttpClient (['headers' => ['Authorization' => 'Bearer ' . $user_acs_token->access_token , 'X-APP-TOKEN' => self::X_APP_TOKEN , 'Content-Type' => 'application/json']]);

			// Pass pickup_lat in param
			$booking_id = $request->has ('booking_id') ? $request->get ('booking_id') : null;

			if (checkArrayIsEmpty ([$booking_id]))
				throw new Exception('booking id missing' , 400);

			$request = [
				'booking_id' => $booking_id ,
			];

			// Set Up Client Request
			$client->setUrl (self::SANDBOX_URL . self::EP_SOS_SIGNAL);
			$client->setParams ($request);

			// Obtain Response
			$response = $client->post ();

			dd ($response);
			// TODO : Obtain Variables


		}
		catch (Exception $e) {
			return $this->sendError ("Ola Error :" . $e->getMessage () , $e->getCode ());
		}

	}


	/**
	 * TODO : Booking History
	 *
	 * Reference : https://developers.olacabs.com/docs/my-rides
	 * Author Developer 12
	 */
	public function history (Request $request)
	{
		try {

			//user login  Details
			$user = JWTAuth::parseToken ()->toUser ();

			//TODO : change the service id once we fixed
			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();

			//ola api data
			$client = new HttpClient (['headers' => ['Authorization' => 'Bearer ' . $user_acs_token->access_token , 'X-APP-TOKEN' => self::X_APP_TOKEN]]);

			// Set Up Client Request
			$client->setUrl (self::SANDBOX_URL . self::EP_BOOKING_HISTORY);
			$response = $client->get ();

			return $this->sendResponse ('Ride History' , 200 , $response->data);

		}
		catch (Exception $e) {
			return $this->sendError ("Ola Error :" . $e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * @param $request
	 * @author Midhun
	 * get cancel reasons for ola
	 */

	public function getCancelReasons($request)
	{
		try{

			$bookingId = $request['booking_id'];

			$serviceApiId = self::API_SERVICE_ID;

			$serviceMetaKeys    = ['booking_id','cab_type','pickup_lat','pickup_lng'];

			$aMetaValues 	 	= $this->serviceMetaKeyRepo->getMetaKeyValues($serviceApiId, $bookingId, $serviceMetaKeys);


			$user = JWTAuth::parseToken ()->toUser ();
			// Find the user access token once logged in the service
//            $get_ola_token = $this->userAccessTokenRepository->getAccestoken($user->id);
			//TODO : change the service id once we fixed
			$user_acs_token = $user->userAccessTokenbyservice (self::API_SERVICE_ID)->first ();

			$client = new HttpClient (['headers' => ['X-APP-TOKEN' => self::X_APP_TOKEN , 'Authorization' => 'Bearer ' . $user_acs_token->access_token , 'Content-Type' => "application/json"]]);

			$client->setUrl ($this->getUrl ('sandbox1') . self::EP_GET_CANCEL_REASONS);

			$aUpdRequest   = [
				'category' 		=> $aMetaValues['cab_type'],
				'pickup_lat' 		=> $aMetaValues['pickup_lat'],
				'pickup_lng' 	=> $aMetaValues['pickup_lng']
			];

			$client->setParams ($aUpdRequest);
			$response = $client->get();

			$category = $aMetaValues['cab_type'];

			$return = [];
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {

				$responseData = $response->data;

				$cancelation_policy = [
					'cancellation_charge' => $responseData->cancellation_policies->$category->cancellation_charge,
					'currency' => $responseData->cancellation_policies->$category->currency,
					'cancellation_charge_applies_after_time' => $responseData->cancellation_policies->$category->cancellation_charge_applies_after_time,
					'time_unit' => $responseData->cancellation_policies->$category->time_unit,
				];

				$return = [
					'cancel_reasons' => $responseData->cancel_reasons->$category,
					'cancel_policy' => $cancelation_policy,
				];

			}

			return $return;
		}
		catch (Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}

	}

}



