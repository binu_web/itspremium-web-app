<?php

namespace App\Http\Controllers\API;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;
use App\Soap\Request\OTA_HotelAvailRQ;
use App\Soap\Response\OTA_HotelAvailRS;

class SoapController
{
    /**
     * @var SoapWrapper
     */
    protected $soapWrapper;

    /**
     * SoapController constructor.
     *
     * @param SoapWrapper $soapWrapper
     */
    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;
    }

    /**
     * Use the SoapWrapper
     */
    public function show()
    {
        $this->soapWrapper->add('Currency', function ($service) {
            $service
                ->wsdl('http://stage-external.travelguru.com/services-2.0/tg-services/TGServiceEndPoint?WSDL')
                ->trace(true)
                ->classmap([
                    OTA_HotelAvailRQ::class,
                    OTA_HotelAvailRS::class,

                ])
                ->cache(WSDL_CACHE_NONE) // Optional: Set the WSDL cache
                ->options([
                    'username' => 'testsell',
                    'password' => 'test@123'
                ])
            ;
        });

        // Without classmap
        $response = $this->soapWrapper->call('Currency.OTA_HotelAvailRQ', [
            'RequestedCurrency' => 'INR',
            'SortOrder'   => 'TG_RANKING',
            'CityName'     => 'AMRITSAR',
            'CountryName'       => 'INDIA',
        ]);

        var_dump($response);

        // With classmap
        $response = $this->soapWrapper->call('Currency.OTA_HotelAvailRS', [
            new OTA_HotelAvailRQ('INR', 'TG_RANKING', 'AMRITSAR', 'INDIA')
        ]);

        var_dump($response);
        exit;
    }
}