<?php

/*
*
*
*
*
*/
namespace App\Http\Controllers\API;

use Exception;
use Illuminate\Http\Request;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Repositories\ApiRepository\PromoCodeRepository;

class PromoCodeController extends BaseController
{
	protected $promoCodeRepo;

	public function __construct(PromoCodeRepository $promoCodeRepo)
	{
		$this->promoCodeRepo = $promoCodeRepo;
		
	}


	public function getAllPromoCodes(Request $oRequest)
	{
		try{

			$oUser  = JWTAuth::parseToken()->toUser();
			$userId = $oUser->id;

			$aAllPromoCodes = $this->promoCodeRepo->getAllPromoCodes();
			if(!$aAllPromoCodes)
				throw new Exception('no records found', 404);

			$aPromoCodes = [];
			$aPromoCodes['promo_codes'] = $aAllPromoCodes;

			return $this->sendResponse ('promocode details' , 200, $aPromoCodes);
		}
		catch(Exception $e) {
			//echo $e->getMessage(); die;
			return $this->sendError($e->getMessage(),$e->getCode());
		}
	}

	public function getServicePromoCodes($serviceId)
	{
		try{
			$oUser  = JWTAuth::parseToken()->toUser();
			$userId = $oUser->id;

			$aPromoCodes = $this->promoCodeRepo->getServicePromoCodes($serviceId);

			if(!$aPromoCodes) 
				throw new Exception("no records found", 404);

			$aPromCode = [];
			$aPromCode['promo_codes'] = $aPromoCodes;

			return $this->sendResponse ('promocode details' , 200, $aPromCode);
				
		}
		catch(Exception $e) {
			//echo $e->getMessage(); die;
			return $this->sendError($e->getMessage(),$e->getCode());
		}
	}



}