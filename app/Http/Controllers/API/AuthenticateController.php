<?php

namespace App\Http\Controllers\API;

use App\Repositories\ApiRepository\DeviceRepository;
use App\Repositories\ApiRepository\UserRepository;
use Exception;
use Illuminate\Http\Request;
use Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;


class AuthenticateController extends BaseController
{
	private $userRepository , $deviceRepository;

	/**
	 * UserController constructor.
	 *
	 * @param \App\Repositories\ApiRepository\UserRepository $userRepository
	 */
	public function __construct (UserRepository $userRepository , DeviceRepository $deviceRepository)
	{
		$this->userRepository = $userRepository;
		$this->deviceRepository = $deviceRepository;
	}

	/**
	 * @author Binoop V
	 */
	public function authenticate (Request $request)
	{
		try {
			// grab credentials from the request
			$credentials = $request->only ('email' , 'password');


			if (!$request->has ('email'))
				throw new Exception('Email is missing' , 400);

			// Check for Social Login
			if ($request->has ('fb_id')) {                // Facebook

				if (empty($request->fb_id))
					throw new Exception('Request is invalid' , 400);

				// CHeck if User Is avaliable
				$user = $this->userRepository->authenicateUserThroughSocial ($request->email , 'facebook_id' , $request->fb_id);
			} else if ($request->has ('google_id')) {    // Google

				if (empty($request->google_id))
					throw new Exception('Request is invalid' , 400);

				// CHeck if user is available in google
				$user = $this->userRepository->authenicateUserThroughSocial ($request->email , 'google_id' , $request->google_id);
			} else {                                    // Normal Login
				// Custom Function : Written in app/Helpers/Common.php (Look at it for futher explaination )
				if (checkArrayIsEmpty ($credentials))
					throw new Exception('Request is invalid' , 400);

				// Check if User is Available in the System
				$user = $this->userRepository->authenicateUser ($request->email , $request->password);
			}


			if (!$user)
				throw new Exception('No user is found' , 401);


			// Update User Device if user is logged from a new device,
			$device = $request->get ('device_id' , 0);

			if (!empty($device))
				$this->deviceRepository->checkAndUpdateDevice ($device , $user->id);

			try {
				// Create JWT Token if User is available using User Object
				$token = JWTAuth::fromUser ($user);
			}
			catch (JWTException $e) {
				// something went wrong whilst attempting to encode the token
				return $this->sendError ('Could not create token' , 500);
			}

			// Return Successful Token
			return $this->sendResponse ('user is authenticated' , 201 , compact ('token'));

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * Refersh Token and return to Client.
	 *
	 * @author Binoop V
	 * @return mixed
	 */
	public function getToken ()
	{
		// Referesh Token and set it to Client
		try {
			$token = JWTAuth::getToken ();
			if (!$token) {
				throw new BadRequestHtttpException('Token not provided');
			}
			try {
				$token = JWTAuth::refresh ($token);
			}
			catch (TokenInvalidException $e) {
				throw new AccessDeniedHttpException('The token is invalid');
			}

			return $this->sendResponse ('token is refreshed' , 201 , compact ('token'));

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}


	/**
	 * Register Device and handle FCM registerations
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @author Binoop V
	 * @return mixed
	 */
	public function deviceRegisteration (Request $request)
	{
		try {

			//
			$device_id = $request->get ('device_id' , '');
			$fcm_token = $request->get ('fcm_token' , '');


			if (checkArrayIsEmpty ($request->all ()))
				throw new Exception('Request is invalid' , 400);

			$device = $this->deviceRepository->checkIfRegistered ($device_id , $fcm_token);

			if ($device) {
				$id = $device->id;
				// TODO : Update The System
				$this->deviceRepository->updateFCMToken ($id , $fcm_token);

			} else {
				// TODO : Regiser Device
				$id = $this->deviceRepository->createDevice ($request->all ());
			}

			if (!$id)
				throw new Exception('device not registered' , 500);

			return $this->sendResponse ('device is updated.' , 201 , compact ('id'));

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}


	}

}
