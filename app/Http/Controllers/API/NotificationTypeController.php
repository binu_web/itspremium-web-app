<?php

namespace App\Http\Controllers\API;

use App\Models\NotificationType;
use App\Repositories\ApiRepository\NotificationRepository;
use App\Repositories\ApiRepository\NotificationTypeRepository;
use Exception;
use Illuminate\Http\Request;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;


class NotificationTypeController extends BaseController
{
	private $notificationRepository;
	private $notificationTypeRepository;

	/**
	 * UserController constructor.
	 *
	 * @param \App\Repositories\ApiRepository\UserRepository $userRepository
	 */
	public function __construct (NotificationRepository $notificationRepository , NotificationTypeRepository $notificationTypeRepository)
	{
		$this->notificationRepository = $notificationRepository;
		$this->notificationTypeRepository = $notificationTypeRepository;
	}

	/**
	 * Send otp to mobile number for forget password
	 * @param Request $request
	 * @author Midhun
	 * @return mixed
	 */
	public  function  setNotification(Request $request){

		try{
			$input=$request->all();
			// Error Response Handling
			if ($request->has('error'))
				throw new Exception($request->get('error'), 403);
			// user login details
			$user = JWTAuth::parseToken ()->toUser ();
			//pass notification name
			$name = $request->has ('notification_name') ? $request->get ('notification_name') : null;
			unset($input['notification_name']);
			// Pass notification type
			$type = $request->has ('notification_type') ? $request->get ('notification_type') : null;
			// Pass notification type
			unset($input['notification_type']);
			$service_api_id = $request->has ('service_api_id') ? $request->get ('service_api_id') : null;
			unset($input['service_api_id']);
			// notification Id
			$notification_type_id = $request->has ('notification_type_id') ? $request->get ('notification_type_id') : null;
			unset($input['notification_type_id']);
			$request = [
					'notification_name' => $name ,
					'notification_type' => $type ,
					'service_api_id' => $service_api_id ,
					'notification_type_id'=>$notification_type_id,
					'user_id' => $user->id ,
			];
			// CHeck if Pick up location is updated
			if (checkArrayIsEmpty([$request,$input]))
				throw new Exception('Notification data missing', 400);

			$result=$this->notificationRepository->setNotificatin($request,$input);
			return $this->sendResponse ('message' , 201,$result);
		}
		catch(\Exception $e){
			return $this->sendError($e->getMessage(),$e->getCode());
		}
	}
	public function listNotification(Request $request){

		// user login details
		$user = JWTAuth::parseToken ()->toUser ();
		// list data based on user id
		$result=$this->notificationRepository->listNotificatin($user->id);
		// result response
		return $this->sendResponse ('Notification List' , 201,$result);
	}
}
