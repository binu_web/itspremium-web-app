<?php

namespace App\Http\Controllers\API\Restaurants;

use App\Http\Controllers\API\BaseController;
use App\Models\UserApiAccessToken;
use App\Repositories\ApiRepository\UserAccessTokenRepository;
use App\Repositories\ApiRepository\UserApiRepository;
use App\Services\HttpClient;
use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use GuzzleHttp\Client;


/**
 * SERVICES RELATED TO ZOMATO RESTAURANT SERVICES
 *
 * Class ZomatoController
 *
 * @package App\Http\Controllers\API\Restaurants
 */
class ZomatoController extends BaseController
{

	/**
	 *  TODO : App Service Deafult Constant (Based on Service Added)
	 */
	const API_SERVICE_ID = 3;
	const SERVICE_NAME = 'zomato';

	/**
	 * Zomato Server Credientials
	 */
//	const USER_KEY = 'bc429b7e4d1c6d0c13ece8cced1a1f94';                                                                // To be passed via each ola api request via header.
	const USER_KEY = '8234409b02e5c5e5c54e9647c00d255e';                                                                // To be passed via each ola api request via header.



	/**
	 * Zomato API URL
	 */
	const SANDBOX_URL = 'https://developers.zomato.com/api/v2.1/';
	const PRODUCTION_URL = '';


	/**
	 * Zomato API ENDPOINTS
	 */
	const RES_LOCATIONS = 'locations';
	const RES_SEARCH = 'search';
	const RES_DETAILS = 'restaurant';
	const RES_DAILY_MENU = 'dailymenu';
	const RES_FOODTYPE = 'categories';
	const RES_REVIEWS = 'reviews';


	// Define Variables
	protected $user_id;
	protected $userApiRepo;


	/**
	 * OlaController constructor.
	 *
	 * @param \App\Repositories\ApiRepository\UserApiRepository $userApiRepo
	 */
	public function __construct (UserApiRepository $userApiRepo)
	{
		$this->userApiRepo = $userApiRepo;
	}


	/**
	 * Return Sandbox / Production url based on the enviroment
	 */
	public function getUrl ()
	{
		return (env ('APP_ENV' , 'production') == 'production') ? self::PRODUCTION_URL : self::SANDBOX_URL;
	}


	/**
	 * TODO : LOCATIONS
	 * Get the locations list using zomato API
	 * Reference : https://developers.zomato.com/documentation#!/location/locations
	 */
	public function getLocations ($request)
	{
		try {
			// Create Client Object
			$client = new HttpClient (['headers' => ['user-key' => self::USER_KEY]]);

			// Set Up Client Request
			$client->setUrl ($this->getUrl () . self::RES_LOCATIONS);
			$client->setParams ($request);

			// Obtain Response
			$response = $client->get ();


			$result = [];

			// Check if Response is available
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data' , 'location_suggestions'])) {
				$result = $response->data->location_suggestions;
			}

			return $result;

		}
		catch (Exception $e) {
			return FALSE;
		}

	}

	/**
	 * @author Midhun
	 * @return mixed|\stdClass
	 * Find the foodtype based on zomato
	 */

	public function getRestaurantCategory ()
	{

		try {
			$result = [];

			// Create Client Object
			$client = new HttpClient (['headers' => ['user-key' => self::USER_KEY]]);

			// Set Up Client Request
			$client->setUrl ($this->getUrl () . self::RES_FOODTYPE);
			$client->setParams ();

			// Obtain Response
			$response = $client->get ();

			// Check if Response is available
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data' , 'categories'])) {
				$categories = $response->data->categories;
				foreach ($categories as $category) {
					$result[] = $category->categories;
				}

			}

			return $result;

		}
		catch (Exception $e) {
			return FALSE;
		}
	}

	/**
	 *  Get the list of restaurant based Locations
	 *
	 * @param $request
	 *
	 * @author Midhun
	 * Reference : https://developers.zomato.com/documentation#!/restaurant/search
	 */

	public function getRestaurantSearch (Request $request)
	{
		try {

			// Create Zomato Request
			$result = [];
			$zomatoRequest = [
				'lat' => $request->get ('lat') ,
				'lon' => $request->get ('long') ,
				'category' => $request->get ('category' , '') ,
				'sort' => 'rating' ,                                                        // TODO : 3 options -> cost,rating & real_distance
				'order' => 'desc' ,
				'start' => $request->get ('start' , 1) ,


			];

			// Create Client Object
			$client = new HttpClient (['headers' => ['user-key' => self::USER_KEY]]);

			// Set Up Client Request
			$client->setUrl ($this->getUrl () . self::RES_SEARCH);
			$client->setParams ($zomatoRequest);

			// Obtain Response
			$response = $client->get ();

			// Check if Response is available
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {
				$responseData = $response->data;

				// Customize Result

				$result = [];
				foreach ($responseData->restaurants as $restaurants) {
					$restaurant = $restaurants->restaurant;
					$result[] = ['service_name' => self::SERVICE_NAME ,
						'service_id' => self::API_SERVICE_ID ,
						// Restaurant Details
						'id' => $restaurant->id ,
						'name' => $restaurant->name ,
						'cuisines' => $restaurant->cuisines ,
						'average_cost_for_two' => $restaurant->average_cost_for_two ,
						'currency' => $restaurant->currency ,
						'has_online_delivery' => $restaurant->has_online_delivery ,
						'is_delivering_now' => $restaurant->is_delivering_now ,
						'has_table_booking' => $restaurant->has_table_booking ,


						// Location Details
						'address' => $restaurant->location->address ,
						'city' => $restaurant->location->city ,
						'zipcode' => $restaurant->location->zipcode ,
						'locality' => $restaurant->location->locality ,
						'latitude' => $restaurant->location->latitude ,
						'longitude' => $restaurant->location->longitude ,

						// Ratings
						'rating' => $restaurant->user_rating->aggregate_rating ,
						'rating_text' => $restaurant->user_rating->rating_text ,
						'rating_votes' => $restaurant->user_rating->votes ,

						// Images & URL
						'url' => $restaurant->url ,
						'featured_image' => $restaurant->featured_image ,
						'img_thumb' => $restaurant->thumb ,
						'menu_url' => $restaurant->menu_url ,
						'photos_url' => $restaurant->thumb ,
					];
				}

			}

			return $result;

		}
		catch (Exception $e) {
			return FALSE;
		}

	}

	/**
	 * @param $request
	 *
	 * @author Midhun
	 * @return mixed|\stdClass
	 *         Single restaurant details based on zomato
	 */

	public function getRestaurantDetails (Request $request)
	{
		try {

			$zomatoRequest = ['res_id' => $request->res_id];

			// Create Client Object
			$client = new HttpClient (['headers' => ['user-key' => self::USER_KEY]]);

			// Set Up Client Request
			$client->setUrl ($this->getUrl () . self::RES_DETAILS);
			$client->setParams ($zomatoRequest);

			// Obtain Response
			$response = $client->get ();
			// Check if Response is available
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {
				$restaurant = $response->data;

				$gallery = [];
				if (checkResponseObjectProperties (self::SERVICE_NAME , $restaurant , ['photos'])) {
					foreach($restaurant->photos as $pics){

						$gallery[] = $pics->photo->url;
					}
				}

				$result['details'] = [
					// Restaurant Details
					'id' => $restaurant->id ,
					'name' => $restaurant->name ,
					'cuisines' => $restaurant->cuisines ,
					'average_cost_for_two' => $restaurant->average_cost_for_two ,
					'currency' => $restaurant->currency ,
					'has_online_delivery' => $restaurant->has_online_delivery ,
					'is_delivering_now' => $restaurant->is_delivering_now ,
					'has_table_booking' => $restaurant->has_table_booking ,


					// Location Details
					'address' => $restaurant->location->address ,
					'city' => $restaurant->location->city ,
					'zipcode' => $restaurant->location->zipcode ,
					'locality' => $restaurant->location->locality ,
					'latitude' => $restaurant->location->latitude ,
					'longitude' => $restaurant->location->longitude ,

					// Ratings
					'rating' => $restaurant->user_rating->aggregate_rating ,
					'rating_text' => $restaurant->user_rating->rating_text ,
					'rating_votes' => $restaurant->user_rating->votes ,

					// Images & URL
					'photos'=>$gallery,
					'url' => $restaurant->url ,
					'featured_image' => $restaurant->featured_image ,
					'img_thumb' => $restaurant->thumb ,
					'menu_url' => $restaurant->menu_url ,
					'photos_url' => $restaurant->thumb ,
				];

				// Get Daily Menu
				$dailyMenu = $this->getDailyMenu ($request);
				$result['daily_menu'] = (!$dailyMenu) ? [] : $dailyMenu;


				// Get Reviews
				$reviews = $this->getReviews ($request);
				$result['reviews'] = (!$reviews) ? [] : $reviews;

			}


			// Get Daily Menu List


			return $result;

		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 *  Daily menu returns based on zomato api
	 *
	 * @param $request
	 *
	 * @author Midhun
	 * @return mixed|\stdClass
	 *
	 */

	public function getDailyMenu (Request $request)
	{
		try {


			$zomatoRequest = ['res_id' => $request->res_id];

			// Create Client Object
			$client = new HttpClient (['headers' => ['user-key' => self::USER_KEY]]);
			// Set Up Client Request
			$client->setUrl ($this->getUrl () . self::RES_DAILY_MENU);
			$client->setParams ($zomatoRequest);
			// Obtain Response
			$response = $client->get ();


			// TODO : Handle Menu, No Menu Items are being listed
			if (!$response)
				throw new Exception('Daily Menu is not available' , 400);

			return $response;

		}
		catch (Exception $e) {
			return FALSE;
		}
	}


	/**
	 * Get Zomato Hotel Review API
	 *
	 * @param $request
	 *
	 * @author Midhun
	 * @return mixed|\stdClass
	 *
	 */

	public function getReviews ($request)
	{
		try {

			$zomatoRequest = ['res_id' => $request->res_id , 'start' => $request->get ('start' , 1)];

			// Create Client Object
			$client = new HttpClient (['headers' => ['user-key' => self::USER_KEY]]);
			// Set Up Client Request
			$client->setUrl ($this->getUrl () . self::RES_REVIEWS);
			$client->setParams ($zomatoRequest);
			// Obtain Response
			$response = $client->get ();
			$result = [];


			// Check and Parse Values
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data' , 'user_reviews'])) {
				$reviews = [];
				$user_reviews = $response->data->user_reviews;

				foreach ($user_reviews as $user_review) {
					$reviews[] = $user_review->review;
				}

				$result['reviews'] = $reviews;
				$result['review_start'] = $request->get ('start' , 1) + 5;
			}

			return $result;
		}
		catch (Exception $e) {
			return FALSE;
		}
	}


}