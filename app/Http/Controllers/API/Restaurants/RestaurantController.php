<?php
namespace App\Http\Controllers\API\Restaurants;

use App\Http\Controllers\API\BaseController;
use App\Repositories\ApiRepository\UserApiRepository;
use App\Repositories\ApiRepository\ResLogRepository;

use Illuminate\Http\Request;
use DB;
use Exception;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\API\Restaurants\ZomatoController;
use App\Http\Controllers\API\Restaurants\DineoutController;
use App\Repositories\ApiRepository\ServiceMetaKeyRepository;


class RestaurantController extends BaseController
{

	/**
	 * Define Namespace
	 *
	 * @var string
	 */
	public $namespace = 'App\Http\Controllers\API\Restaurants\\';


	public $searchLimit = 10;
	// Log Repository
	public $userApiRepo;
	public $serviceMetaKeyRepo;

	public $stdObject;

	const SERVICE_NAME_DINEOUT = 'dineout';

	/**
	 * RestaurantController constructor.
	 *
	 */
	public function __construct (UserApiRepository $userApiRepository , ServiceMetaKeyRepository $serviceMetaKeyRepository , ResLogRepository $resLogRepository)
	{
		$this->userApiRepository = $userApiRepository;
		$this->serviceMetaKeyRepo = $serviceMetaKeyRepository;
		$this->resLogRepository = $resLogRepository;


		$this->stdObject = new \stdClass();

		$this->stdObject->user_api = $this->userApiRepository;
		$this->stdObject->service_api = $this->serviceMetaKeyRepo;

	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 *
	 * get the locations list from service
	 */

	public function locations (Request $request)
	{

		try {
			// Pass pickup_loc in param
			$query = $request->has ('query') ? $request->get ('query') : null;

			$request = [
				'query' => $query ,
			];

			$zomato = $this->zomatoController->getLocations ($request);

			if (!$zomato)
				$zomato = [];

			$response = ['zomato' => $zomato];


			if (checkArrayIsEmpty ($response))
				throw new Exception('no results found' , 404);

			// Send Response
			return $this->sendResponse ('Location details listed' , 200 , $response);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * @author Midhun
	 * @return mixed
	 * Find the food types from zomato
	 */

	public function restaurantCategory ()
	{
		try {
			$zomato = $this->zomatoController->getRestaurantCategory ();

			if (!$zomato)
				$zomato = [];

			$response = ['zomato' => $zomato];

			if (checkArrayIsEmpty ($response))
				throw new Exception('no results found' , 400);

			return $this->sendResponse ('restruant cateogory listed' , 200 , $response);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 *
	 * Get the hotels list based on the city id
	 *
	 * @param Request $request
	 *
	 * @author Midhun
	 *
	 */
	public function restaurantSearch (Request $request)
	{

		try {

			// Obtain Location Details (Current location lat & long)
			$latitude = $request->has ('lat') ? $request->get ('lat') : null;
			$longitude = $request->has ('long') ? $request->get ('long') : null;
			$location = $request->has ('location') ? $request->get ('location') : null;
			$start = $request->has ('start') ? $request->get ('start') : null;

			// Pass service in param
			$service = $request->has ('service') ? $request->get ('service') : null;

			$request->request->add (['count' => $this->searchLimit]);
			// TODO : Filter Logic

			// Request Validation
			if (checkArrayIsEmpty ([$latitude , $longitude]))
				throw new Exception('location details are required' , 400);
			// ensure latitude and longitude coordinates are numeric
			if (!is_numeric ($latitude) || !is_numeric ($longitude)) {
				throw new Exception('invalid latitude or longitude coordinates' , 401);
			}

			// Request Validation
			if (checkArrayIsEmpty ([$start]))
				throw new Exception('start parameter required' , 400);


			//insert user log details
			$user = JWTAuth::parseToken ()->toUser ();
			$insert_log = $this->resLogRepository->Insert_log_details ($request , $user->id);

			// Dineout Request

			// TODO : uncommet the condition if multiple services added
//			if($service==self::SERVICE_NAME_DINEOUT) {

			$dineoutClass = $this->namespace . 'DineoutController';
			$dineoutObject = new $dineoutClass($this->stdObject);

			$dineout = $dineoutObject->getRestaurantSearch ($request);

			if(!$dineout)
				throw new Exception('Sorry! The service is not available now' , 400);

			if (is_array ($dineout) && isset($dineout['error']))
				throw new Exception($dineout['message'] , 500);

			//			}

			$response = ['dineout' => $dineout];
			$reponseMessage = "No Restaurants available";

			if (!checkArrayIsEmpty ($response)) {
				// Start Next Set of Results From
				$response['start_from'] = $request->get ('start' , 1) + $this->searchLimit;
				$reponseMessage = "Restaurants listed";
			} else {
				$response = ['dineout' => []];
			}


			return $this->sendResponse ($reponseMessage , 200 , $response);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 * @return mixed
	 *         To get the details of restaurants
	 */

	public function restaurantDetails (Request $request)
	{
		try {

			// Pass pickup_loc in param
			$res_id = $request->has ('res_id') ? $request->get ('res_id') : null;
			$service = $request->has ('service') ? $request->get ('service') : null;


			if (checkArrayIsEmpty ([$res_id , $service]))
				throw new Exception('request parameters are empty' , 400);


			// Create Class Based on Service
			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service accessed' , 403);

			// Create Restuarnt Object
			$restaurant = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($restaurant , 'getRestaurantDetails'))
				throw new Exception('invalid service request accessed' , 404);

			// Request Service Based Requests
			$details = $restaurant->getRestaurantDetails ($request);

			if (empty($details))
				throw new Exception('No daily menu found' , 404);

			return $this->sendResponse ('Restaurant details obtained' , 200 , $details);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 * @return mixed
	 *         Get the daily menu of restaurant
	 */

	public function restaurantDailyMenu (Request $request)
	{
		try {

			// Pass pickup_loc in param
			$res_id = $request->has ('res_id') ? $request->get ('res_id') : null;
			$service = $request->has ('service') ? $request->get ('service') : null;

			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service accessed' , 403);

			$restaurant = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($restaurant , 'getDailyMenu'))
				throw new Exception('invalid service request accessed' , 500);

			$details = $restaurant->getDailyMenu ($request);


			if (!($details))
				throw new Exception('No daily menu found' , 404);

			return $this->sendResponse ('Daily menu details listed' , 200 , $details);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 * @return mixed
	 * Get the Reviews of restaurant
	 */

	public function restaurantReview (Request $request)
	{
		try {


			// Pass pickup_loc in param
			$res_id = $request->has ('res_id') ? $request->get ('res_id') : null;
			$service = $request->has ('service') ? $request->get ('service') : null;

			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service accessed' , 403);

			$restaurant = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($restaurant , 'getReviews'))
				throw new Exception('invalid service request accessed' , 500);

			$details = $restaurant->getReviews ($request);

			if (!($details))
				throw new Exception('No reviews available' , 404);

			return $this->sendResponse ('reviews listed' , 200 , $details);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 *         Generate booking for restaurants
	 */

	public function restaurantGenerateBooking (Request $request)
	{
		try {

			// finding login user details to sort favorites lists
			$user = JWTAuth::parseToken ()->toUser ();

			$service = $request->has ('service') ? $request->get ('service') : null;

			$rest_id = $request->has ('rest_id') ? $request->get ('rest_id') : null;
			$booking_time = $request->has ('booking_time') ? $request->get ('booking_time') : null;
			$booking_date = $request->has ('booking_date') ? $request->get ('booking_date') : null;
			$people = $request->has ('people') ? $request->get ('people') : null;
			$male = $request->has ('male') ? $request->get ('male') : null;
			$offer_id = $request->has ('offer_id') ? $request->get ('offer_id') : null;

			$diner_name = $request->has ('diner_name') ? $request->get ('diner_name') : null;
			$diner_email = $request->has ('diner_email') ? $request->get ('diner_email') : null;
			$diner_phone = $request->has ('diner_phone') ? $request->get ('diner_phone') : null;
			$spcl_request = $request->has ('spcl_request') ? $request->get ('spcl_request') : null;
			$jet_privilege_no = $request->has ('jet_privilege_no') ? $request->get ('jet_privilege_no') : null;


			// Check if service key word exists
			if (checkArrayIsEmpty ([$service]))
				throw new Exception('service key word required' , 400);

			// Check if restaurant id is requested
			if (checkArrayIsEmpty ([$rest_id]))
				throw new Exception('restaurant id required' , 400);

			// Check if booking date and booking time is requested
			if (checkArrayIsEmpty ([$booking_date , $booking_time]))
				throw new Exception('booking date & booking time is required' , 400);

			// Check if people details inserted
			if (checkArrayIsEmpty ([$people , $male]))
				throw new Exception('people and male parameters required' , 400);

			if (!is_numeric ($people) || !is_numeric ($male)) {
				throw new Exception('invalid people or male numbers' , 401);
			}

			// Validate Mobile Number
			if (!(validate_mobilenumber ($diner_phone)))
				throw new Exception('Invalid mobile number' , 409);

			// Check if people details inserted
			if (checkArrayIsEmpty ([$diner_name]))
				$diner_name = $user->first_name;

			// Check if people details inserted
			if (checkArrayIsEmpty ([$diner_email]))
				$diner_email = $user->email;

			// Check if people details inserted
			if (checkArrayIsEmpty ([$diner_phone]))
				$diner_phone = $user->mobile;


			// Basic API Request
			$api_request = [
				'name' => $diner_name ,
				'phone' => $diner_phone ,
				'email' => $diner_email ,
				'booking_date' => $booking_date ,
				'booking_time' => $booking_time ,
				'rest_id' => $rest_id ,
				'people' => $people ,
				'male' => $male ,
			];


			// Set if Offer is available
			if (!is_null ($offer_id) || !empty($offer_id)) {
				$api_request['offer_id'] = $offer_id;
			}

			// Set if Special Request is Available
			if (!is_null ($spcl_request) || !empty($spcl_request)) {
				$api_request['spcl_request'] = $spcl_request;
			}


			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service accessed' , 403);

			$restaurant = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($restaurant , 'generateBooking'))
				throw new Exception('invalid service request accessed' , 500);

			$res_book = $restaurant->generateBooking ($api_request);

			// Booking Failed
			if (is_array ($res_book) && isset($res_book['error']))
				throw new Exception($res_book['message'] , 500);

			return $this->sendResponse ('Booking Created' , 200 , $res_book);


		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 *
	 * Confirm booking for restaurant
	 */
	public function confirmBooking (Request $request)
	{
		try {
			// finding login user details to sort favorites lists
			$service = $request->has ('service') ? $request->get ('service') : null;

			$booking_id = $request->has ('booking_id') ? $request->get ('booking_id') : null;

			// Check if service key word exists
			if (checkArrayIsEmpty ([$service]))
				throw new Exception('service key word required' , 400);

			// Check if restaurant id is requested
			if (checkArrayIsEmpty ([$booking_id]))
				throw new Exception('booking id required' , 400);

			$api_request = [
				'booking_id' => $booking_id ,
			];

			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service accessed' , 403);

			$restaurant = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($restaurant , 'confirmBooking'))
				throw new Exception('invalid service request accessed' , 500);

			// Call Service Method
			$res_book_confirm = $restaurant->confirmBooking ($api_request);

			// Booking Failed
			if (is_array ($res_book_confirm) && isset($res_book_confirm['error']))
				throw new Exception($res_book_confirm['message'] , 500);

			return $this->sendResponse ('Booking confirmed and order created' , 200 , $res_book_confirm);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}


	/**
	 * Cancel Booking API
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	public function cancelBooking (Request $request)
	{
		try {
			// finding login user details to sort favorites lists
			$service = $request->has ('service') ? $request->get ('service') : null;

			$booking_id = $request->has ('booking_id') ? $request->get ('booking_id') : null;

			// Check if service key word exists
			if (checkArrayIsEmpty ([$service]))
				throw new Exception('service key word required' , 400);

			// Check if restaurant id is requested
			if (checkArrayIsEmpty ([$booking_id]))
				throw new Exception('booking id required' , 400);

			$api_request = [
				'booking_id' => $booking_id ,
			];

			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service accessed' , 403);

			$restaurant = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($restaurant , 'cancelBooking'))
				throw new Exception('invalid service request accessed' , 500);

			// Call Service Method
			$res_book_cancel = $restaurant->cancelBooking ($api_request);

			// Booking Failed
			if (is_array ($res_book_cancel) && isset($res_book_cancel['error']))
				throw new Exception($res_book_cancel['message'] , 500);

			return $this->sendResponse ('Booking cancelled successfully' , 200 , $res_book_cancel);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}


	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 *         get the booking slots of a restaurant
	 */
	public function getRestaurantSlots (Request $request)
	{
		try {
			// finding login user details to sort favorites lists
			$service = $request->has ('service') ? $request->get ('service') : null;

			$restaurant_id = $request->has ('restaurant_id') ? $request->get ('restaurant_id') : null;
			$date = $request->has ('date') ? $request->get ('date') : null;

			// Check if date word exists
			if (checkArrayIsEmpty ([$date]))
				throw new Exception('date is required' , 400);

			// Check if service key word exists
			if (checkArrayIsEmpty ([$service]))
				throw new Exception('service key word required' , 400);

			// Check if restaurant id is requested
			if (checkArrayIsEmpty ([$restaurant_id]))
				throw new Exception('restaurant id required' , 400);

			$api_request = [
				'restaurant_id' => $restaurant_id ,
				'date' => $date ,
			];

			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service accessed' , 403);

			$restaurant = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($restaurant , 'getRestaurantSlots'))
				throw new Exception('invalid service request accessed' , 500);

			// get slots
			$res_slots = $restaurant->getRestaurantSlots ($api_request);

			// if not get slots  Failed
			if (is_array ($res_slots) && isset($res_slots['error']))
				throw new Exception($res_slots['message'] , 500);

			return $this->sendResponse ('Restaurant booking slots listed' , 200 , $res_slots);


		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 *         Get the current status of the booking
	 */

	public function getDinerBookingDetails (Request $request)
	{

		try {
			// finding login user details to sort favorites lists
			$service = $request->has ('service') ? $request->get ('service') : null;

			$booking_id = $request->has ('booking_id') ? $request->get ('booking_id') : null;

			// Check if service key word exists
			if (checkArrayIsEmpty ([$service]))
				throw new Exception('service key word required' , 400);

			// Check if restaurant id is requested
			if (checkArrayIsEmpty ([$booking_id]))
				throw new Exception('booking id required' , 400);

			$api_request = [
				'booking_id' => $booking_id ,
			];

			$controllerClass = $this->namespace . ucfirst ($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists ($controllerClass))
				throw new Exception('invalid service accessed' , 403);

			$restaurant = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists ($restaurant , 'dinerBookingDetails'))
				throw new Exception('invalid service request accessed' , 500);

			// Call Service Method
			$res_book_update = $restaurant->dinerBookingDetails ($api_request);

			// Booking Failed
			if (is_array ($res_book_update) && isset($res_book_update['error']))
				throw new Exception($res_book_update['message'] , 500);

			return $this->sendResponse ('Booking updated successfully' , 200 , $res_book_update);

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * @param Request $request
	 *
	 * @author Midhun
	 *         get the log details based on user
	 */
	public function getLogHistory (Request $request)
	{
		try {
			$user = JWTAuth::parseToken ()->toUser ();
			$result = $this->resLogRepository->getResLogHistory ($user->id);
			if (checkArrayIsEmpty ([$result]))
				throw new Exception('Restaurant location log empty' , 403);

			return $this->sendResponse ('Restaurant location log listed' , 200 , $result);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * Set a location has favorite
	 *
	 *
	 * @author Frijo
	 * @return mixed
	 *
	 */
	public function setFavorite (Request $request)
	{
		try {

			$user = JWTAuth::parseToken ()->toUser ();
			//pass favorites change parameter
			$change_fav = $request->has ('is_favorite') ? $request->get ('is_favorite') : 0;
			$location_id = $request->has ('location_id') ? $request->get ('location_id') : 0;

			if (checkArrayIsEmpty ([$location_id]))
				throw new Exception('location id is empty' , 400);

			//update cab log details
			$result = $this->resLogRepository->setResLogFavorite ($location_id , $change_fav);

			return $this->sendResponse ('favourites saved' , 201);
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * Get Favorite locations based on user
	 *
	 *
	 * @author Frijo
	 * @return mixed
	 *
	 */
	public function getFavoriteLocations (Request $request)
	{
		try {

			// finding login user details to sort favorites lists
			$user = JWTAuth::parseToken ()->toUser ();

			//finding cab favorites lists.
			$result = $this->resLogRepository->getResFavoriteLocations ($user->id);

			return $this->sendResponse ('Favourite locations listed' , 200 , $result);
		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	public function getConfirmDetails (Request $request)
	{

		try {
			// finding login user details to sort favorites lists
			$service = $request->has('service') ? $request->get('service') : null;

			$booking_id = $request->has('booking_id') ? $request->get('booking_id') : null;

			// Check if service key word exists
			if (checkArrayIsEmpty([$service]))
				throw new Exception('service key word required', 400);

			// Check if restaurant id is requested
			if (checkArrayIsEmpty([$booking_id]))
				throw new Exception('booking id required', 400);

			$api_request = [
				'booking_id' => $booking_id,
			];

			$controllerClass = $this->namespace . ucfirst($service) . 'Controller';

			// Check If Class Exists
			if (!class_exists($controllerClass))
				throw new Exception('invalid service accessed', 403);

			$restaurant = new $controllerClass($this->stdObject);

			// Check if Method Exists
			if (!method_exists($restaurant, 'getConfirmDetails'))
				throw new Exception('invalid service request accessed', 500);

			// Call Service Method
			$res_book_update = $restaurant->getConfirmDetails($api_request);

			// Booking Failed
			if (is_array($res_book_update) && isset($res_book_update['error']))
				throw new Exception($res_book_update['message'], 500);

			return $this->sendResponse('Booking details listed', 200, $res_book_update);

		} catch (\Exception $e) {
			return $this->sendError($e->getMessage(), $e->getCode());
		}
	}




}