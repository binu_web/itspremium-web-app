<?php

namespace App\Http\Controllers\API\Restaurants;

use App\Http\Controllers\API\BaseController;
use App\Models\UserApiAccessToken;
use App\Repositories\ApiRepository\ServiceMetaKeyRepository;
use App\Repositories\ApiRepository\UserAccessTokenRepository;
use App\Repositories\ApiRepository\UserApiRepository;
use App\Services\HttpClient;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use GuzzleHttp\Client;

/**
 * SERVICES RELATED TO DINEOUT RESTAURANT SERVICES
 *
 * Class DineoutController
 *
 * @package App\Http\Controllers\API\Restaurants
 */
class DineoutController extends BaseController
{

	const API_SERVICE_CATEGORY_ID = 2;
	const API_SERVICE_ID = 4;
	const SERVICE_NAME = 'dineout';

	/**
	 * Dineout Server Credientials
	 */

	const AUTH_ID = 1049;
	// To be passed via each ola api request via header.
	const AUTH_KEY = 'webandcrafts-sdfsdf-sdfsdf-qas34-sdfsdf';
	const SECRET_KEY = 'e249c439ed7697df2a4b045d97d4b9b7e1854c3ff8dd668c779013653913572e';

	/**
	 * Dineout API URL
	 */
	const SANDBOX_URL = 'http://api.dineoutdeals.in/external_api/api_v3/';
	const PRODUCTION_URL = '';

	/**
	 * Dineout API ENDPOINTS
	 */
	const RES_SEARCH = 'search_restaurants';
	const RES_DETAILS = 'restaurant_detail';
	const RES_REVIEWS = 'reviews';
	const RES_GENERATE_BOOKING = 'generate_booking';
	const RES_CONFIRM_BOOKING = 'generate_order';
	const RES_CANCEL_BOOKING = 'cancel_booking';
	const RES_TIME_SLOTS = 'get_restaurant_slot';
	const RES_GET_DINER_BOOKING_DETAILS = 'get_diner_booking_detail';

	protected $userApiRepo;
	public $serviceMetaKeyRepo;


	/**
	 * DineoutController constructor.
	 *
	 * @param \App\Repositories\ApiRepository\UserApiRepository $userApiRepo
	 */
	public function __construct (\stdClass $stdObject)
	{
		$this->userApiRepo = property_exists ($stdObject , 'user_api') ? $stdObject->user_api : new UserApiRepository();
		$this->serviceMetaKeyRepo = property_exists ($stdObject , 'service_api') ? $stdObject->service_api : new ServiceMetaKeyRepository();


	}

	/**
	 * Return Sandbox / Production url based on the enviroment
	 */
	public function getUrl ()
	{
		return (env ('APP_ENV' , 'production') == 'production') ? self::PRODUCTION_URL : self::SANDBOX_URL;
	}

	/**
	 *  Get the list of restaurant based Locations
	 *
	 * @param $request
	 *
	 * @author Midhun
	 * Reference : http://api.dineoutdeals.in/external_api/api_v2/search_restaurants
	 */

	public function getRestaurantSearch (Request $request)
	{
		try {

			// Create Dineout Request
			$result = [];

			$DinoutRequest = [
				'lat' => $request->get ('lat') ,
				'long' => $request->get ('long') ,
				'start' => $request->get ('start')
			];

			$input = input_string_generator ($DinoutRequest);
			$hash_key = HMAC_hashkey_generator ('md5' , $input , self::SECRET_KEY);


			// Create Client Object
			$client = new HttpClient (['headers' => ['Auth-id' => self::AUTH_ID , 'Auth-key' => self::AUTH_KEY , 'Hash-key' => $hash_key]]);

			// Set Up Client Request
			$client->setUrl ($this->getUrl () . self::RES_SEARCH);
			$client->setParams ($DinoutRequest);

			// Obtain Response
			$response = $client->get ();
			// Check if Response is available

			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {
				$responseData = $response->data;
//				dd($responseData->error_msg);
				// Customize Result
				if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['error_msg'])) {
					switch ($responseData->error_msg) {
						case "Unauthorized Access.":
							throw new Exception('Sorry! The service is not available now', 400);
							break;
						default:
							throw new Exception('' . $responseData->error_msg);
							break;
					}
				}
				else {

					$result = [];
					foreach ($responseData->output_params as $restaurant) {
						//split latitude and longitude
						$lat_and_long = string_split(',', $restaurant->lat_lng);
						//convert cousines array into string
						$cousine_string = implode(", ", $restaurant->cuisine);


						$result[] = ['service_name' => self::SERVICE_NAME,
							'service_id' => self::API_SERVICE_ID,
//                        // Restaurant Details
							'id' => $restaurant->r_id,
							'name' => $restaurant->profile_name,

							'cuisines' => $cousine_string,
							'average_cost_for_two' => $restaurant->costFor2,
							'currency' => null,
							'has_online_delivery' => null,
							'is_delivering_now' => null,
							'has_table_booking' => $restaurant->booking_enable,

							// Location Details
							'address' => $restaurant->address,
							'city' => $restaurant->city_name,
							'zipcode' => null,
							'locality' => $restaurant->landmark,
							'latitude' => $lat_and_long[0],
							'longitude' => $lat_and_long[1],


							// Ratings
							'rating' => $restaurant->avg_rating,
							'rating_text' => null,
							'rating_votes' => null,
							// Images & URL
							'url' => $restaurant->url,
							'featured_image' => $restaurant->img,
							'img_thumb' => null,
							'menu_url' => null,
							'photos_url' => null,

						];
					}
				}

			}

			return $result;


		}
		catch (Exception $e) {
			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}

	}

	/**
	 * @param $request
	 *
	 * @author Midhun
	 *         generate booking with api dineout
	 */

	public function generateBooking ($request)
	{

		try {

			$user = JWTAuth::parseToken ()->toUser ();

			$input = input_string_generator ($request);
			$hash_key = HMAC_hashkey_generator ('md5' , $input , self::SECRET_KEY);
			// Create Client Object
			$client = new HttpClient (['headers' => ['Auth-id' => self::AUTH_ID , 'Auth-key' => self::AUTH_KEY , 'Hash-key' => $hash_key]]);

			// Set Up Client Request
			$client->setUrl ($this->getUrl () . self::RES_GENERATE_BOOKING);
			$client->setParams ($request);


			// Obtain Response
			$response = $client->post ();

			// Check if Response is available
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {
				$responseData = $response->data;

				// Check if error is in response
				if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['error_msg'])) {
					switch ($responseData->error_msg) {
						case "Dining date time Cannot be less than current date time":
							throw new Exception('Please choose a booking time greater than the current time.' , 400);
							break;
						case "Error: Bookings need to be made at least 30 minutes in advance.":
							throw new Exception('Bookings need to be made at least 30 minutes in advance.' , 400);
							break;
						case "Phone number is not valid":
							throw new Exception('Phone number is not valid' , 400);
							break;
						case "This offer is not valid for the date time provided":
							throw new Exception('This offer is not valid for the date time provided' , 400);
							break;
						case "Hash-key does not match":
							throw new Exception('Some fields are null ' , 400);
							break;
						default:
							throw new Exception('' . $responseData->error_msg);
							break;

					}
				} else if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['output_params'])) {
					// Log booking details
					$insert = $this->serviceMetaKeyRepo->addDineoutBookDetails (self::API_SERVICE_CATEGORY_ID , self::API_SERVICE_ID , $user->id , $responseData , $request);

				}
			}

			return $insert;
		}
		catch (Exception $e) {

			return ['error' => TRUE , 'message' => $e->getMessage ()];
		}

	}

	/**
	 * @param $request
	 *
	 * @author Midhun
	 *         confirm booking with api provider dineout
	 * THIS FUNCTION IS OBSOLATE. can be remove once confirmed
	 */

	public function confirmBooking ($request)
	{
		$booking_id = $request['booking_id'];

		$user = JWTAuth::parseToken ()->toUser ();

		$dineout_booking_id = $this->serviceMetaKeyRepo->getOlaBookingdetails ($booking_id , self::API_SERVICE_ID);

		if (!$dineout_booking_id)
			throw new Exception('Invalid dineout  booking id' , 400);

		$request = [
			'obj_id' => $dineout_booking_id ,
			'obj_type' => 'booking' ,
			'bill_amount' => '2000' ,
			'order_id' => self::AUTH_ID ,
		];

		$input = input_string_generator ($request);
		$hash_key = HMAC_hashkey_generator ('md5' , $input , self::SECRET_KEY);
		// Create Client Object
		$client = new HttpClient (['headers' => ['Auth-id' => self::AUTH_ID , 'Auth-key' => self::AUTH_KEY , 'Hash-key' => $hash_key]]);

		// Set Up Client Request
		$client->setUrl ($this->getUrl () . self::RES_CONFIRM_BOOKING);
		$client->setParams ($request);

		// Obtain Response
		$response = $client->post ();
		// Check if Response is available
		if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {
			$responseData = $response->data;

			// Check if error is in response
			if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['error_msg'])) {

				switch ($responseData->error_msg) {
					case "Payment can be done on confirmed Booking Only.":
						throw new Exception('Payment can be done on confirmed Booking Only.' , 400);
						break;

					default:
						throw new Exception('' . $responseData->error_msg);
						break;
				}
			}

			// Log booking details
			$update = $this->serviceMetaKeyRepo->updateDineoutBookDetails ($responseData , $booking_id , self::API_SERVICE_ID);

			return $update;
		}

	}

	/**
	 * @param $request
	 *
	 * @author Midhun
	 *         Cancel the booking
	 */

	public function cancelBooking ($request)
	{
		$booking_id = $request['booking_id'];

		$user = JWTAuth::parseToken ()->toUser ();

		$dineout_details = $this->serviceMetaKeyRepo->getdineoutBookingdetails ($booking_id , self::API_SERVICE_ID);


		if (!$dineout_details)
			throw new Exception('Invalid dineout  booking id' , 400);

		$request = [
			'phone' => $dineout_details['diner_phone'] ,
			'booking_id' => $dineout_details['display_id'] ,
		];

		$input = input_string_generator ($request);
		$hash_key = HMAC_hashkey_generator ('md5' , $input , self::SECRET_KEY);
		// Create Client Object
		$client = new HttpClient (['headers' => ['Auth-id' => self::AUTH_ID , 'Auth-key' => self::AUTH_KEY , 'Hash-key' => $hash_key]]);

		// Set Up Client Request
		$client->setUrl ($this->getUrl () . self::RES_CANCEL_BOOKING);
		$client->setParams ($request);

		// Obtain Response
		$response = $client->post ();
		// Check if Response is available
		if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {
			$responseData = $response->data;

			// Check if error is in response
			if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['error_msg'])) {
				switch ($responseData->error_msg) {
					case "Invalid Booking":
						throw new Exception('Invalid booking.' , 400);
						break;
					case "Booking id not valid":
						throw new Exception('Booking id not valid' , 400);
						break;
					case "Your booking already cancelled.":
						throw new Exception('Your booking already cancelled.' , 400);
						break;
					default:
						throw new Exception('' . $responseData->error_msg);
						break;
				}
			} else if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['output_params'])) {
				// Log booking details
				$cancel = $this->serviceMetaKeyRepo->cancelDineoutBooking ($booking_id , self::API_SERVICE_ID);
			}
			return [];
		}

	}

	/**
	 * @param $request
	 *
	 * @author Midhun
	 *         get available timeslots of a restaurant
	 */
	public function getRestaurantSlots ($request)
	{
		$request = [
			'restaurant_id' => $request['restaurant_id'] ,
			'timestamp' => strtotime ($request['date']) ,
		];

		$input = input_string_generator ($request);
		$hash_key = HMAC_hashkey_generator ('md5' , $input , self::SECRET_KEY);
		// Create Client Object
		$client = new HttpClient (['headers' => ['Auth-id' => self::AUTH_ID , 'Auth-key' => self::AUTH_KEY , 'Hash-key' => $hash_key]]);

		// Set Up Client Request
		$client->setUrl ($this->getUrl () . self::RES_TIME_SLOTS);
		$client->setParams ($request);

		// Obtain Response
		$response = $client->get ();
		// Check if Response is available
		if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {
			$responseData = $response->data;

			if (checkArrayIsEmpty([$responseData]))
				throw new Exception('Sorry you can not book restaurant right now.', 400);

			// Check if error is in response
			if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['error_msg'])) {
				switch ($responseData->error_msg) {

					case "Invalid Date":
						throw new Exception('Invalid Date' , 400);
						break;
					case "The Restaurant Id field must contain an integer.":
						throw new Exception('The Restaurant Id field must contain an integer.' , 400);
						break;
					case "The Timestamp field must contain an integer.":
						throw new Exception('The Timestamp field must contain an integer.' , 400);
						break;
					case null: // response as error and return null values
						throw new Exception('Sorry you can not book restaurant right now.' , 400);
						break;
					default:
						throw new Exception('' . $responseData->error_msg);
						break;
				}
			}

			// Log booking details
			$converted_slots = [];


			foreach ($responseData->output_params->data->slots as $slots) {


				$time = date ('H:i' , strtotime ('+1 hour'));
				if ($time < date ("Hi" , strtotime ($slots))) {

					$converted_slots[] = ['key' => date ("Hi" , strtotime ($slots)) , 'value' => date ("g:i a" , strtotime ($slots))];

				}
			}

			return $converted_slots;
		}


	}

	/**
	 * @param $request
	 *
	 * @author Midhun
	 *         Get diner booking details
	 */

	public function dinerBookingDetails ($request)
	{
		$booking_id = $request['booking_id'];

		$user = JWTAuth::parseToken ()->toUser ();

		$dineout_details = $this->serviceMetaKeyRepo->getdineoutBookingdetails ($booking_id , self::API_SERVICE_ID);

		if (!$dineout_details)
			throw new Exception('Invalid dineout  booking id' , 400);

		$request = [
			'phone' => $dineout_details['diner_phone'] ,
			'booking_id' => $dineout_details['display_id'] ,
		];

		$input = input_string_generator ($request);
		$hash_key = HMAC_hashkey_generator ('md5' , $input , self::SECRET_KEY);
		// Create Client Object
		$client = new HttpClient (['headers' => ['Auth-id' => self::AUTH_ID , 'Auth-key' => self::AUTH_KEY , 'Hash-key' => $hash_key]]);

		// Set Up Client Request
		$client->setUrl ($this->getUrl () . self::RES_GET_DINER_BOOKING_DETAILS);
		$client->setParams ($request);

		// Obtain Response
		$response = $client->post ();
		// Check if Response is available
		if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {
			$responseData = $response->data;

			// Check if error is in response
			if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['error_msg'])) {
				switch ($responseData->error_msg) {
					case "Invalid Booking":
						throw new Exception('Invalid booking.' , 400);
						break;
					case "Booking id not valid":
						throw new Exception('Booking id not valid' , 400);
						break;
					case "Phone number is not valid":
						throw new Exception('Phone number is not valid' , 400);
						break;

					default:
						throw new Exception('' . $responseData->error_msg);
						break;
				}
			} else if (checkResponseObjectProperties (self::SERVICE_NAME , $responseData , ['output_param'])) {

				// Log booking details
				$update = $this->serviceMetaKeyRepo->updateDineoutBookDetails ($responseData , $booking_id , self::API_SERVICE_ID);

				return $update;
			}

			//TODO : update the booking details as per the response to service booked and service meta data
		}

	}

	/**
	 * @author Midhun
	 * @return mixed|\stdClass
	 *         Single restaurant details based on dineout
	 */

	public function getRestaurantDetails (Request $request)
	{
		try {

			$dineoutRequest = ['rest_id' => $request->res_id];

			// Create Client Object
			$input = '{"rest_id":"' . $dineoutRequest['rest_id'] . '"}';

			$hash_key = HMAC_hashkey_generator ('md5' , $input , self::SECRET_KEY);

			// Create Client Object
			$client = new HttpClient (['headers' => ['Auth-id' => self::AUTH_ID , 'Auth-key' => self::AUTH_KEY , 'Hash-key' => $hash_key]]);
			// Set Up Client Request
			$client->setUrl ($this->getUrl () . self::RES_DETAILS);
			$client->setParams ($dineoutRequest);

			// Obtain Response
			$response = $client->get ();
//            dd($response->data->output_params->offer_data);

			// Check if Response is available
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {
				$restaurant = $response->data->output_params;

				$cousine_string = implode (", " , $restaurant->cuisine);

				$offers = array();
				foreach ($restaurant->offer_data as $offer_data) {

					$offers[] = [
						'offer_id' => $offer_data->offer_id ,
						'offer_title' => $offer_data->title ,
						'offer_description' => $offer_data->description ,
						'offer_start_date' => $offer_data->start_date ,
						'offer_end_date' => $offer_data->end_date ,
						'offer_display_str' => $offer_data->display_str ,
					];
				}


				$result['details'] = [
					// Restaurant Details
					'id' => isset($restaurant->id) ? $restaurant->id : null ,
					'name' => isset($restaurant->name) ? $restaurant->name : null ,
					'description' => isset($restaurant->description) ? $restaurant->description : null ,
					'cuisines' => $cousine_string ,
					'cuisines_list' => isset($restaurant->cuisine) ? $restaurant->cuisine : [] ,
					'average_cost_for_two' => isset($restaurant->cost_for_two) ? $restaurant->cost_for_two : null ,
					'currency' => null ,
					'has_online_delivery' => null ,
					'is_delivering_now' => null ,
					'has_table_booking' => isset($restaurant->booking_enable) ? $restaurant->booking_enable : 0 ,


//                    // Location Details
					'phone' => isset($restaurant->phone) ? $restaurant->phone : [] ,
					'address' => isset($restaurant->address) ? $restaurant->address : null ,
					'city' => isset($restaurant->city) ? $restaurant->city : null ,
					'zipcode' => null ,
					'locality' => isset($restaurant->locality) ? $restaurant->locality : null ,
					'latitude' => isset($restaurant->latitude) ? $restaurant->latitude : null ,
					'longitude' => isset($restaurant->longitude) ? $restaurant->longitude : null ,

//                    // Ratings
					'rating' => isset($restaurant->avg_rating) ? $restaurant->avg_rating : null ,

					'rating_text' => null ,
					'rating_votes' => null ,

//                    // Images & URL

					'photos' => isset($restaurant->photo) ? $restaurant->photo : null ,
					'url' => isset($restaurant->url) ? $restaurant->url : null ,
					'featured_image' => $restaurant->photo[0] ,
					'menu' => isset($restaurant->menu) ? $restaurant->menu : null ,
					'offers' => $offers ,
					'img_thumb' => null ,
					'menu_url' => null ,
					'photos_url' => null ,


					//Simialar Restuarnts
					'similar_restaurants' => isset($restaurant->similar_restaurants) ? $restaurant->similar_restaurants : [] ,
					'restaurant_timings' => isset($restaurant->restaurant_timings) ? $restaurant->restaurant_timings : [] ,
					'tags' => isset($restaurant->tags) ? $restaurant->tags : [] ,

				];

//                // Get Daily Menu
//                $dailyMenu = $this->getDailyMenu ($request);
//                $result['daily_menu'] = (!$dailyMenu) ? [] : $dailyMenu;
//

				// Get Reviews
				$reviews = $this->getReviews ($request);
				$result['reviews'] = (!$reviews) ? ['reviews' => []] : $reviews;

			}

			// Get Daily Menu List


			return $result;

		}
		catch (Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}

	}

	/**
	 * Get Zomato Hotel Review API
	 *
	 * @param $request
	 *
	 * @author Midhun
	 * @return mixed|\stdClass
	 *
	 */

	public function getReviews ($request)
	{
		try {

			$dineoutRequest = ['rest_id' => $request->res_id];

			// Create Client Object
			$input = '{"rest_id":"' . $dineoutRequest['rest_id'] . '"}';

			$hash_key = HMAC_hashkey_generator ('md5' , $input , self::SECRET_KEY);

			// Create Client Object
			$client = new HttpClient (['headers' => ['Auth-id' => self::AUTH_ID , 'Auth-key' => self::AUTH_KEY , 'Hash-key' => $hash_key]]);
			// Set Up Client Request
			$client->setUrl ($this->getUrl () . self::RES_REVIEWS);
			$client->setParams ($dineoutRequest);
			// Obtain Response
			$response = $client->get ();
			$result = [];

			// Check and Parse Values
			if (checkResponseObjectProperties (self::SERVICE_NAME , $response , ['data'])) {
				$reviews = [];
				$user_reviews = $response->data->output_params;


				$result['reviews'] = $user_reviews;
				$result['review_start'] = $request->get ('start' , 1) + 5;
			}

			return $result;
		}
		catch (Exception $e) {
			return FALSE;
		}
	}


	public function webhook (Request $request)
	{

		Log::info ("Dineout WebhookRequest Start");
		Log::info (serialize ($request->all ()));
		Log::info ("Dineout WebhookRequest END");

		if ($request->has ('booking_status')) {
			$status = $request->get ('booking_status');

					$myarray = $request->all ();
					$result = [
						'booking_id' => isset($myarray['booking_id']) ? $myarray['booking_id'] : null,
						'b_id' => isset($myarray['b_id']) ? $myarray['b_id'] : null ,
						'affiliate_id' => isset($myarray['affiliate_id']) ? $myarray['affiliate_id'] : null ,
						'disp_id' => isset($myarray['disp_id']) ? $myarray['disp_id'] :null,
						'offer_text' => isset($myarray['offer_text']) ? $myarray['offer_text'] :null,
						'offer_id' => isset($myarray['offer_id']) ? $myarray['offer_id'] :null,
						'booking_status' => isset($myarray['booking_status']) ? $myarray['booking_status'] : null,
						'table_allocation_status' => isset($myarray['table_allocation_status']) ? $myarray['table_allocation_status'] : null,
						'status' => isset($myarray['status']) ? $myarray['status'] : null,
						'url' => isset($myarray['url']) ? $myarray['url'] : null,
					];
					$response = $this->serviceMetaKeyRepo->updateStatusOfBookingForDineout (self::API_SERVICE_ID , $result);

			return $response;
		} else {
			throw new Exception('No status found' , 404);
		}


	}

	public function getConfirmDetails($request)
	{
		try{
				$booking_id = $request['booking_id'];
				$dineout_details = $this->serviceMetaKeyRepo->getdineoutBookingConfirmation ($booking_id , self::API_SERVICE_ID);

				return $dineout_details;

			}
		catch (Exception $e) {

		return ['error' => TRUE , 'message' => $e->getMessage ()];
		}
	}

}