<?php

namespace App\Http\Controllers;

use App\DataTables\AdminLogTypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAdminLogTypeRequest;
use App\Http\Requests\UpdateAdminLogTypeRequest;
use App\Repositories\AdminLogTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AdminLogTypeController extends AppBaseController
{
	/** @var  AdminLogTypeRepository */
	private $adminLogTypeRepository;

	public function __construct (AdminLogTypeRepository $adminLogTypeRepo)
	{
		$this->adminLogTypeRepository = $adminLogTypeRepo;
	}

	/**
	 * Display a listing of the AdminLogType.
	 *
	 * @param AdminLogTypeDataTable $adminLogTypeDataTable
	 *
	 * @return Response
	 */
	public function index (AdminLogTypeDataTable $adminLogTypeDataTable)
	{
		return $adminLogTypeDataTable->render ('admin_log_types.index');
	}

	/**
	 * Show the form for creating a new AdminLogType.
	 *
	 * @return Response
	 */
	public function create ()
	{
		return view ('admin_log_types.create');
	}

	/**
	 * Store a newly created AdminLogType in storage.
	 *
	 * @param CreateAdminLogTypeRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateAdminLogTypeRequest $request)
	{
		$input = $request->all ();

		$adminLogType = $this->adminLogTypeRepository->create ($input);

		Flash::success ('Admin Log Type saved successfully.');

		return redirect (route ('adminLogTypes.index'));
	}

	/**
	 * Display the specified AdminLogType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$adminLogType = $this->adminLogTypeRepository->findWithoutFail ($id);

		if (empty($adminLogType)) {
			Flash::error ('Admin Log Type not found');

			return redirect (route ('adminLogTypes.index'));
		}

		return view ('admin_log_types.show')->with ('adminLogType' , $adminLogType);
	}

	/**
	 * Show the form for editing the specified AdminLogType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$adminLogType = $this->adminLogTypeRepository->findWithoutFail ($id);

		if (empty($adminLogType)) {
			Flash::error ('Admin Log Type not found');

			return redirect (route ('adminLogTypes.index'));
		}

		return view ('admin_log_types.edit')->with ('adminLogType' , $adminLogType);
	}

	/**
	 * Update the specified AdminLogType in storage.
	 *
	 * @param  int                      $id
	 * @param UpdateAdminLogTypeRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateAdminLogTypeRequest $request)
	{
		$adminLogType = $this->adminLogTypeRepository->findWithoutFail ($id);

		if (empty($adminLogType)) {
			Flash::error ('Admin Log Type not found');

			return redirect (route ('adminLogTypes.index'));
		}

		$adminLogType = $this->adminLogTypeRepository->update ($request->all () , $id);

		Flash::success ('Admin Log Type updated successfully.');

		return redirect (route ('adminLogTypes.index'));
	}

	/**
	 * Remove the specified AdminLogType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$adminLogType = $this->adminLogTypeRepository->findWithoutFail ($id);

		if (empty($adminLogType)) {
			Flash::error ('Admin Log Type not found');

			return redirect (route ('adminLogTypes.index'));
		}

		$this->adminLogTypeRepository->delete ($id);

		Flash::success ('Admin Log Type deleted successfully.');

		return redirect (route ('adminLogTypes.index'));
	}
}
