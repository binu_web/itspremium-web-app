<?php

namespace App\Http\Controllers;

use App\DataTables\UserLogDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUserLogRequest;
use App\Http\Requests\UpdateUserLogRequest;
use App\Repositories\UserLogRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class UserLogController extends AppBaseController
{
	/** @var  UserLogRepository */
	private $userLogRepository;

	public function __construct (UserLogRepository $userLogRepo)
	{
		$this->userLogRepository = $userLogRepo;
	}

	/**
	 * Display a listing of the UserLog.
	 *
	 * @param UserLogDataTable $userLogDataTable
	 *
	 * @return Response
	 */
	public function index (UserLogDataTable $userLogDataTable)
	{
		return $userLogDataTable->render ('user_logs.index');
	}

	/**
	 * Show the form for creating a new UserLog.
	 *
	 * @return Response
	 */
	public function create ()
	{
		return view ('user_logs.create');
	}

	/**
	 * Store a newly created UserLog in storage.
	 *
	 * @param CreateUserLogRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateUserLogRequest $request)
	{
		$input = $request->all ();

		$userLog = $this->userLogRepository->create ($input);

		Flash::success ('User Log saved successfully.');

		return redirect (route ('userLogs.index'));
	}

	/**
	 * Display the specified UserLog.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$userLog = $this->userLogRepository->findWithoutFail ($id);

		if (empty($userLog)) {
			Flash::error ('User Log not found');

			return redirect (route ('userLogs.index'));
		}

		return view ('user_logs.show')->with ('userLog' , $userLog);
	}

	/**
	 * Show the form for editing the specified UserLog.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$userLog = $this->userLogRepository->findWithoutFail ($id);

		if (empty($userLog)) {
			Flash::error ('User Log not found');

			return redirect (route ('userLogs.index'));
		}

		return view ('user_logs.edit')->with ('userLog' , $userLog);
	}

	/**
	 * Update the specified UserLog in storage.
	 *
	 * @param  int                 $id
	 * @param UpdateUserLogRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateUserLogRequest $request)
	{
		$userLog = $this->userLogRepository->findWithoutFail ($id);

		if (empty($userLog)) {
			Flash::error ('User Log not found');

			return redirect (route ('userLogs.index'));
		}

		$userLog = $this->userLogRepository->update ($request->all () , $id);

		Flash::success ('User Log updated successfully.');

		return redirect (route ('userLogs.index'));
	}

	/**
	 * Remove the specified UserLog from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$userLog = $this->userLogRepository->findWithoutFail ($id);

		if (empty($userLog)) {
			Flash::error ('User Log not found');

			return redirect (route ('userLogs.index'));
		}

		$this->userLogRepository->delete ($id);

		Flash::success ('User Log deleted successfully.');

		return redirect (route ('userLogs.index'));
	}
}
