<?php

namespace App\Http\Controllers;

use App\DataTables\UserRefundTable;
use App\Http\Requests;
use App\Models\AdminUser;
use App\Repositories\WalletRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Foundation\Auth\User;
use Response;
use Auth;
use Image;
use App\Models\Wallet;
use App\Models\WalletLog;
use App\Models\ServiceApi;
use App\Http\Requests\UpdateWalletRequest;
use App\Http\Controllers\API\RazorPayWebHookController;

use Illuminate\Http\Request;


class UserRefundController extends AppBaseController
{
    /** @var  ServiceRepository */
    private $feedbackRepository;

    public function __construct (WalletRepository $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }

    /**
     * Display a listing of the Service.
     *
     * @param ServiceDataTable $serviceDataTable
     *
     * @return Response
     */
    public function index (UserRefundTable $useRefundDataTable)
    {
        return $useRefundDataTable->render ('user_refund.index');
    }

    public function show($id)
    {
        $owallet       = Wallet::find($id);
        $aWallet_log = WalletLog::select()->where('wallet_id','=',$owallet->id)->where('deleted_at','=',null)->get()->toArray();
        $aUser = User::select()->where('id','=',$owallet->user_id)->first();
        $service_apis = ServiceApi::where ('deleted_at' , NULL)->pluck ('provider_name' , 'id')->toArray ();

        return view ('user_refund.show')->with ('wallet' , $owallet)->with ('walletLog' , $aWallet_log)->with ('user' , $aUser)->with ('service_api' , $service_apis);

    }

    public function edit($id)
    {
        $owallet       = Wallet::find($id);
        $aUser = User::where ('deleted_at' , NULL)->where ('is_active' , '!=' , '0')->pluck ('email' , 'id')->toArray ();


        return view ('user_refund.edit')->with ('wallet' , $owallet)->with ('user' , $aUser);

    }
    /**
     * Update the specified ServiceApi in storage.
     *
     * @param  int                    $id
     * @param UpdateServiceApiRequest $request
     *
     * @return Response
     */
    public function update ($id , UpdateWalletRequest $request)
    {
       // form validation
        $this->validate ($request , [
            'balance' => 'required' ,

        ]);

        $serviceApi = $this->walletRepository->findWithoutFail ($id);

        if (empty($serviceApi)) {
            Flash::error ('Wallet is not found');

            return redirect (route ('user_refund.index'));
        }

        $input = $request->all ();
        $userId = Auth::id ();

       if (!isset($input['is_active']))
            $input['is_active'] = 0;

        $input['created_by'] = $userId;
        $input['updated_by'] = $userId;

        $serviceApi = $this->walletRepository->update ($input , $id);

        Flash::success ('wallet updated successfully.');

        return redirect (route ('user_refund.index'));
    }

    /**
     * Remove the specified ServiceApi from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy ($id)
    {
        $serviceApi = $this->walletRepository->findWithoutFail ($id);

        if (empty($serviceApi)) {
            Flash::error ('wallet is  not found');

            return redirect (route ('user_refund.index'));
        }

        $this->walletRepository->delete ($id);

        Flash::success ('wallet is deleted successfully.');

        return redirect (route ('user_refund.index'));
    }



}
