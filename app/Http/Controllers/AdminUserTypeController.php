<?php

namespace App\Http\Controllers;

use App\DataTables\AdminUserTypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAdminUserTypeRequest;
use App\Http\Requests\UpdateAdminUserTypeRequest;
use App\Repositories\AdminUserTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AdminUserTypeController extends AppBaseController
{
	/** @var  AdminUserTypeRepository */
	private $adminUserTypeRepository;

	public function __construct (AdminUserTypeRepository $adminUserTypeRepo)
	{
		$this->adminUserTypeRepository = $adminUserTypeRepo;
	}

	/**
	 * Display a listing of the AdminUserType.
	 *
	 * @param AdminUserTypeDataTable $adminUserTypeDataTable
	 *
	 * @return Response
	 */
	public function index (AdminUserTypeDataTable $adminUserTypeDataTable)
	{
		return $adminUserTypeDataTable->render ('admin_user_types.index');
	}

	/**
	 * Show the form for creating a new AdminUserType.
	 *
	 * @return Response
	 */
	public function create ()
	{
		return view ('admin_user_types.create');
	}

	/**
	 * Store a newly created AdminUserType in storage.
	 *
	 * @param CreateAdminUserTypeRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateAdminUserTypeRequest $request)
	{
		$input = $request->all ();

		$adminUserType = $this->adminUserTypeRepository->create ($input);

		Flash::success ('Admin User Type saved successfully.');

		return redirect (route ('adminUserTypes.index'));
	}

	/**
	 * Display the specified AdminUserType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$adminUserType = $this->adminUserTypeRepository->findWithoutFail ($id);

		if (empty($adminUserType)) {
			Flash::error ('Admin User Type not found');

			return redirect (route ('adminUserTypes.index'));
		}

		return view ('admin_user_types.show')->with ('adminUserType' , $adminUserType);
	}

	/**
	 * Show the form for editing the specified AdminUserType.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$adminUserType = $this->adminUserTypeRepository->findWithoutFail ($id);

		if (empty($adminUserType)) {
			Flash::error ('Admin User Type not found');

			return redirect (route ('adminUserTypes.index'));
		}

		return view ('admin_user_types.edit')->with ('adminUserType' , $adminUserType);
	}

	/**
	 * Update the specified AdminUserType in storage.
	 *
	 * @param  int                       $id
	 * @param UpdateAdminUserTypeRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateAdminUserTypeRequest $request)
	{
		$adminUserType = $this->adminUserTypeRepository->findWithoutFail ($id);

		if (empty($adminUserType)) {
			Flash::error ('Admin User Type not found');

			return redirect (route ('adminUserTypes.index'));
		}

		$adminUserType = $this->adminUserTypeRepository->update ($request->all () , $id);

		Flash::success ('Admin User Type updated successfully.');

		return redirect (route ('adminUserTypes.index'));
	}

	/**
	 * Remove the specified AdminUserType from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$adminUserType = $this->adminUserTypeRepository->findWithoutFail ($id);

		if (empty($adminUserType)) {
			Flash::error ('Admin User Type not found');

			return redirect (route ('adminUserTypes.index'));
		}

		$this->adminUserTypeRepository->delete ($id);

		Flash::success ('Admin User Type deleted successfully.');

		return redirect (route ('adminUserTypes.index'));
	}
}
