<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Controller extends BaseController
{
	use AuthorizesRequests , DispatchesJobs , ValidatesRequests;

	protected function arrayHasEmptyElement (Array $array)
	{
		foreach ($array as $item)
			if (empty($item))
				return true;

		return false;
	}

	function catchException ($e)
	{
		$code = $e->getCode ();
		$message = explode (':' , $e->getMessage ());
		Log::warning ($code . "->" . $e->getMessage () . "(" . $e->getFile () . ":" . $e->getLine () . ")");
		if ($code < 100 || $code > 599)
			$code = 500;

		return array('msg' => trim ($message[0]) , 'code' => $code);
	}
}
