<?php

namespace App\Http\Controllers;

use App\DataTables\NewsSourceDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateNewsSourceRequest;
use App\Http\Requests\UpdateNewsSourceRequest;
use App\Models\NewsSource;
use App\Repositories\NewsSourceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;

class NewsSourceController extends AppBaseController
{
	/** @var  NewsSourceRepository */
	private $newsSourceRepository;

	public function __construct (NewsSourceRepository $newsSourceRepo)
	{
		$this->newsSourceRepository = $newsSourceRepo;
	}

	/**
	 * Display a listing of the NewsCategory.
	 *
	 * @param NewsSourceDataTable $newsSourceDataTable
	 *
	 * @return Response
	 */
	public function index (NewsSourceDataTable $newsSourceDataTable)
	{
		return $newsSourceDataTable->render ('news_sources.index');
	}

	/**
	 * Show the form for creating a new NewsSource.
	 *
	 * @return Response
	 */
	public function create ()
	{
		$client = new \GuzzleHttp\Client();
		$url = "https://newsapi.org/v1/sources?language=en";
		$res = $client->request ('GET' , $url);
		$source = json_decode ($res->getBody ());
		$source = $source->sources;
		$sourceCat = NewsSource::select ('source' , 'source_slug')->get ();
		return view ('news_sources.create')->with (array('source' => $source , 'sourceCategory' => $sourceCat));
	}

	/**
	 * Store a newly created NewsSource in storage.
	 *
	 * @param CreateNewsSourceRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateNewsSourceRequest $request)
	{
		$this->validate ($request , [
			'source' => 'required|unique:news_sources,source,' ,
			'source_slug' => 'required|unique:news_sources,source_slug,'
		]);

		$input = $request->all ();
		$userId = Auth::id ();

		$input['updated_by'] = $userId;
		$input['created_by'] = $userId;

		$newsSource = $this->newsSourceRepository->create ($input);

		Flash::success ('News Source saved successfully.');

		return redirect (route ('newsSources.index'));
	}

	/**
	 * Display the specified NewsSource.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$newsSource = $this->newsSourceRepository->findWithoutFail ($id);

		if (empty($newsSource)) {
			Flash::error ('News Source not found');

			return redirect (route ('newsSources.index'));
		}

		return view ('news_sources.show')->with ('newsSource' , $newsSource);
	}

	/**
	 * Show the form for editing the specified NewsSource.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$newsSource = $this->newsSourceRepository->findWithoutFail ($id);

		$client = new \GuzzleHttp\Client();
		$url = "https://newsapi.org/v1/sources?language=en";
		$res = $client->request ('GET' , $url);
		$source = json_decode ($res->getBody ());
		$source = $source->sources;
		$sourceCat = NewsSource::select ('source' , 'source_slug')->get ();

		if (empty($newsSource)) {
			Flash::error ('News Source not found');

			return redirect (route ('newsSources.index'));
		}

		return view ('news_sources.edit')->with (array('newsSource' => $newsSource , 'source' => $source , 'sourceCategory' => $sourceCat));
	}

	/**
	 * Update the specified NewsSource in storage.
	 *
	 * @param  int                    $id
	 * @param UpdateNewsSourceRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateNewsSourceRequest $request)
	{

		$this->validate ($request , [
			'source' => 'required|unique:news_sources,source,' . $id ,
			'source_slug' => 'required|unique:news_sources,source_slug,' . $id ,
		]);

		$newsSource = $this->newsSourceRepository->findWithoutFail ($id);

		if (empty($newsSource)) {
			Flash::error ('News Source not found');

			return redirect (route ('newsSources.index'));
		}

		$input = $request->all ();
		$userId = Auth::id ();

		$input['updated_by'] = $userId;

		$newsSource = $this->newsSourceRepository->update ($input , $id);

		Flash::success ('News Source updated successfully.');

		return redirect (route ('newsSources.index'));
	}

	/**
	 * Remove the specified NewsSource from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$newsSource = $this->newsSourceRepository->findWithoutFail ($id);

		if (empty($newsSource)) {
			Flash::error ('News Source not found');

			return redirect (route ('newsSources.index'));
		}

		$this->newsSourceRepository->delete ($id);

		Flash::success ('News Source deleted successfully.');

		return redirect (route ('newsSources.index'));
	}
}
