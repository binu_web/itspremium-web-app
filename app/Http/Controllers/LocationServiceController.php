<?php

namespace App\Http\Controllers;

use App\DataTables\LocationServiceDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateLocationServiceRequest;
use App\Http\Requests\UpdateLocationServiceRequest;
use App\Repositories\LocationServiceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\DB;
use App\Models\Service;
use App\Repositories\LocationRepository;

class LocationServiceController extends AppBaseController
{
	/** @var  LocationServiceRepository */
	private $locationServiceRepository;
	private $LocationRepository;

	public function __construct (LocationServiceRepository $locationServiceRepo , LocationRepository $locationRepo)
	{
		$this->locationServiceRepository = $locationServiceRepo;
		$this->LocationRepository = $locationRepo;
	}

	/**
	 * Display a listing of the LocationService.
	 *
	 * @param LocationServiceDataTable $locationServiceDataTable
	 *
	 * @return Response
	 */
	public function index (LocationServiceDataTable $locationServiceDataTable)
	{
		return $locationServiceDataTable->render ('location_services.index');
	}

	/**
	 * Show the form for creating a new LocationService.
	 *
	 * @return Response
	 */
	public function create ()
	{
		$service = new Service();
		$serviceArr = array();
		foreach ($service->get () as $serve) {
			$serviceCat = $serve->toArray ()['name'];
			foreach ($serve->serviceApisList->toArray () as $ser) {
				$serviceArr[$serviceCat][$ser['id']] = $ser['provider_name'];
			}
		}
		$selected_service = array();
		return view ('location_services.create' , ['service' => $serviceArr , 'selected_service' => $selected_service]);
	}

	/**
	 * Store a newly created LocationService in storage.
	 *
	 * @param CreateLocationServiceRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateLocationServiceRequest $request)
	{
		$this->validate ($request , [
			'location' => 'required|max:255' ,
			'distance' => 'required' ,
			'service' => 'required' ,
		]);
		try {
			$input = $request->all ();
			$location = $this->LocationRepository->create ($input);
			if ($location) {
				$locationService = $this->locationServiceRepository->insert ($input , $location);
				Flash::success ('Location Service saved successfully.');
				return redirect (route ('locationServices.index'));
			} else {
				Flash::error ('Some Technical problems');
				return redirect (route ('locationServices.create'));
			}
		}
		catch (\Exception $e) {
			Flash::error ('Invalid data');
			return redirect (route ('locationServices.create'));
		}


//dd($input);

	}

	/**
	 * Display the specified LocationService.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$locationService = $this->locationServiceRepository->findWithoutFail ($id);

		if (empty($locationService)) {
			Flash::error ('Location Service not found');

			return redirect (route ('locationServices.index'));
		}

		return view ('location_services.show')->with ('locationService' , $locationService);
	}

	/**
	 * Show the form for editing the specified LocationService.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$locationService = $this->locationServiceRepository->findWithoutFail ($id);

		if (empty($locationService)) {
			Flash::error ('Location Service not found');

			return redirect (route ('locationServices.index'));
		}

		return view ('location_services.edit')->with ('locationService' , $locationService);
	}

	/**
	 * Update the specified LocationService in storage.
	 *
	 * @param  int                         $id
	 * @param UpdateLocationServiceRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateLocationServiceRequest $request)
	{
		$locationService = $this->locationServiceRepository->findWithoutFail ($id);

		if (empty($locationService)) {
			Flash::error ('Location Service not found');

			return redirect (route ('locationServices.index'));
		}

		$locationService = $this->locationServiceRepository->update ($request->all () , $id);

		Flash::success ('Location Service updated successfully.');

		return redirect (route ('locationServices.index'));
	}

	/**
	 * Remove the specified LocationService from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$locationService = $this->locationServiceRepository->findWithoutFail ($id);

		if (empty($locationService)) {
			Flash::error ('Location Service not found');

			return redirect (route ('locationServices.index'));
		}

		$this->locationServiceRepository->delete ($id);

		Flash::success ('Location Service deleted successfully.');

		return redirect (route ('locationServices.index'));
	}
}
