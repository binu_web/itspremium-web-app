<?php

namespace App\Http\Controllers;

use App\DataTables\NewsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Models\NewsSource;
use Illuminate\Http\Request;
use App\Repositories\NewsRepository;
use App\Models\NewsCategory;
use App\Models\NewsCountry;
use App\Models\Country;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Image;
use DB;

class NewsController extends AppBaseController
{
    /** @var  NewsRepository */
    private $newsRepository;

    public function __construct(NewsRepository $newsRepo)
    {
        $this->newsRepository = $newsRepo;
    }

    /**
     * Display a listing of the News.
     *
     * @param NewsDataTable $newsDataTable
     * @return Response
     */
    public function index(NewsDataTable $newsDataTable)
    {
        return $newsDataTable->render('news.index');
    }

    /**
     * Show the form for creating a new News.
     *
     * @return Response
     */
    public function create()
    {
        $categories = NewsCategory::where('deleted_at', NULL)->where('is_active',1)->pluck('name', 'id')->toArray();

        $countries= DB::table('countries')->select('id', 'name')->where('is_active','=','1')->where('deleted_at',null)->get();
        $selectedCountryArr=array();
        $selectedCountry=array();
        foreach($countries as $key=>$value)
        {
            $countriesArr[$value->id]=$value->name;
        }
        return view('news.create',['newsCategories'=>$categories,'countriesArr'=>$countriesArr,'selectedCountryArr'=>$selectedCountryArr]);
    }

    /**
     * Store a newly created News in storage.
     *
     * @param CreateNewsRequest $request
     *
     * @return Response
     */
    public function store(CreateNewsRequest $request)
    {
        // form validation
        $this->validate($request, [
            'category' => 'required',
            'title' => 'required',
            'description' => 'required',
            'publish_date' => 'required',
            'source_link' => 'required',
            'image'=>'required|mimes:jpeg,jpg,png,gif',
            'country'=>'required',
        ]);

        $input = $request->all();

        $userId = Auth::id();
        $photo = $request->file('image');

        if( !empty($photo) )
        {
            $imagename = time().'.'.$photo->getClientOriginalExtension();
            $path = resource_path('assets/uploads/images/');
            $thumb_img = Image::make($photo->getRealPath())->resize(600, null, function($constraint){
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            });
            $image_save = $thumb_img->save($path.'/'.$imagename,80);

            if($image_save)
                $input['image'] = $imagename;
        }

        if( !isset($input['is_active']) )
            $input['is_active'] = 0;

        if( !isset($input['is_trending']) )
            $input['is_trending'] = 0;

        if( !empty($input['description']) )
            $input['description'] = htmlspecialchars( $input['description'] );

        $input['created_by'] = $userId;
        $input['updated_by'] = $userId;
        //insert news
        $news = $this->newsRepository->createNews($input);
//        $news = $this->newsRepository->create($input);

        Flash::success('News saved successfully.');

        return redirect(route('news.index'));
    }

    /**
     * Display the specified News.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $news = $this->newsRepository->findWithoutFail($id);

        if (empty($news)) {
            Flash::error('News not found');

            return redirect(route('news.index'));
        }
        $country=NewsCountry::where('news_country.news_id','=',$id)
        ->leftJoin('countries','countries.id','=','news_country.country_id')
        ->select('countries.name')->get();
//        $dataCountry='';
        foreach($country as $value){
            $dataCountry[]=$value->name;
        }
        return view('news.show')->with('news', $news)->with('country',implode(',',$dataCountry));
    }

    /**
     * Show the form for editing the specified News.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categories = NewsCategory::where('deleted_at', NULL)->where('is_active',1)->pluck('name', 'id')->toArray();

        $news = $this->newsRepository->findWithoutFail($id);

        //$news->description = htmlspecialchars_decode( $news->description );
        $countriesArr1= NewsCountry::where('news_country.news_id', '=', $id)
            ->select('news_country.*')
            ->get();
        $countries= Country::select('id', 'name')->where('deleted_at','=',null)->get();
        $selectedCountryArr=array();
        $selectedCountry=array();
        foreach($countries as $key=>$value)
        {
            $countriesArr[$value->id]=$value->name;
        }
        foreach($countriesArr1 as $key=>$value)
        {
            $selectedCountryArr[]=$value->country_id;
        }

        if (empty($news)) {
            Flash::error('News not found');

            return redirect(route('news.index'));
        }

//        return view('news.edit')->with('news', $news)->with('newsCategories',$categories);
        return view('news.edit',['news'=>$news,'selectedCountryArr'=>$selectedCountryArr,'country'=>$countriesArr,'newsCategories'=>$categories]);
    }

    /**
     * Update the specified News in storage.
     *
     * @param  int              $id
     * @param UpdateNewsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNewsRequest $request)
    {
        // form validation
        $this->validate($request, [
            'category' => 'required',
            'title' => 'required',
            'description' => 'required',
            'publish_date' => 'required',
            'country' => 'required',
            'image'=>'mimes:jpeg,jpg,png',
        ]);

        $news = $this->newsRepository->findWithoutFail($id);

        if (empty($news)) {
            Flash::error('News not found');

            return redirect(route('news.index'));
        }
        $input = $request->all();
        $userId = Auth::id();
        $photo = $request->file('image');

        if( !empty($photo) )
        {
            $imagename = time().'.'.$photo->getClientOriginalExtension();
            $path = resource_path('assets/uploads/images/');
            $thumb_img = Image::make($photo->getRealPath())->resize(600, null, function($constraint){
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $image_save = $thumb_img->save($path.'/'.$imagename,80);
            $input['image'] = $imagename;
        }
        elseif($input['image_old']==''){
            $this->validate($request, [
                'image'=>'required',
            ]);
        }else{
            $input['image'] = $input['image_old'];
        }

        if( !isset($input['is_active']) )
            $input['is_active'] = 0;

        if( !isset($input['is_trending']) )
            $input['is_trending'] = 0;

        if( !empty($input['description']) )
            $input['description'] = htmlspecialchars( $input['description'] );

        $input['updated_by'] = $userId;
        $news = $this->newsRepository->updateNews($input, $id);
        // TODO : remove below code after finish
//        $news = $this->newsRepository->updateNews($input, $id);

        Flash::success('News updated successfully.');

        return redirect(route('news.index'));
    }

    /**
     * Remove the specified News from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $news = $this->newsRepository->findWithoutFail($id);

        if (empty($news)) {
            Flash::error('News not found');

            return redirect(route('news.index'));
        }
        // delete news
        $this->newsRepository->deleteNews($id);
        // TODO : rewmove below code finish this
//        $this->newsRepository->delete($id);

        Flash::success('News deleted successfully.');

        return redirect(route('news.index'));
    }
    /**
     * Select news by id
     */
    public function SelectNewsById(Request $requests){

        $data=$this->newsRepository->selectNewsbyId($requests->id);
        return response()->json(['status' => 'success', 'data' => ['data' => $data]], 200);
    }

}
