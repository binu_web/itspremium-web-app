<?php

namespace App\Http\Controllers;

use App\DataTables\PaymentMethodDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePaymentMethodRequest;
use App\Http\Requests\UpdatePaymentMethodRequest;
use App\Repositories\PaymentMethodRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Repositories\paymentMethodCountryRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PaymentMethodController extends AppBaseController
{
	/** @var  PaymentMethodRepository */
	private $paymentMethodRepository;
	private $paymentMethodCountryRepository;

	public function __construct (PaymentMethodRepository $paymentMethodRepo , paymentMethodCountryRepository $paymentMethodCountryRepo)
	{
		$this->paymentMethodRepository = $paymentMethodRepo;
		$this->paymentMethodCountryRepository = $paymentMethodCountryRepo;
	}

	/**
	 * Display a listing of the PaymentMethod.
	 *
	 * @param PaymentMethodDataTable $paymentMethodDataTable
	 *
	 * @return Response
	 */
	public function index (PaymentMethodDataTable $paymentMethodDataTable)
	{
		return $paymentMethodDataTable->render ('payment_methods.index');
	}

	/**
	 * Show the form for creating a new PaymentMethod.
	 *
	 * @return Response
	 */
	public function create ()
	{
		$countries = DB::table ('countries')->select ('id' , 'name')->where ('is_active' , '=' , '1')->where ('deleted_at' , null)->get ();
		$selectedCountryArr = array();
		$selectedCountry = array();
		foreach ($countries as $key => $value) {
			$countriesArr[$value->id] = $value->name;
		}
		return view ('payment_methods.create' , ['countriesArr' => $countriesArr , 'selectedCountryArr' => $selectedCountryArr]);
	}

	/**
	 * Store a newly created PaymentMethod in storage.
	 *
	 * @param CreatePaymentMethodRequest $request
	 *
	 * @return Response
	 */
	public function store (CreatePaymentMethodRequest $request)
	{
		$this->validate ($request , [
			'gateway_name' => 'required|max:255' ,
			'api_key' => 'required' ,
			'api_url' => 'required' ,
			'api_secret' => 'required' ,
			'redirect_url' => 'required' ,
			'is_active' => 'required' ,
			'service_countries' => 'required' ,
		]);
		$userId = Auth::id ();
		$input['created_by'] = $userId;
		try {
			$input = $request->all ();
			$paymentMethod = $this->paymentMethodRepository->create ($input);
			if ($paymentMethod) {
				$paymentMethodCountry = $this->paymentMethodCountryRepository->insert ($input , $paymentMethod);
				Flash::success ('Payment Method saved successfully.');
				return redirect (route ('paymentMethods.index'));
			} else {
				Flash::error ('Some Technical problems');
				return redirect (route ('paymentMethods.create'));
			}
		}
		catch (\Exception $e) {
			Flash::error ('Invalid data');
			return redirect (route ('paymentMethods.create'));
		}


	}

	/**
	 * Display the specified PaymentMethod.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$paymentMethod = $this->paymentMethodRepository->findWithoutFail ($id);

		if (empty($paymentMethod)) {
			Flash::error ('Payment Method not found');

			return redirect (route ('paymentMethods.index'));
		}
		$created_user = DB::table ('admin_users')->where ('id' , $paymentMethod->created_by)->first ();
		$updated_user = DB::table ('admin_users')->where ('id' , $paymentMethod->updated_by)->first ();

		return view ('payment_methods.show' , ['paymentMethod' => $paymentMethod , 'created_user' => $created_user , 'updated_user' => $updated_user]);
	}

	/**
	 * Show the form for editing the specified PaymentMethod.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$paymentMethod = $this->paymentMethodRepository->findWithoutFail ($id);
		if (empty($paymentMethod)) {
			Flash::error ('Payment Method not found');

			return redirect (route ('paymentMethods.index'));
		}
		$countriesArr1 = DB::table ('payment_method_countries')
			->where ('payment_method_countries.gateway_id' , '=' , $id)
			->select ('payment_method_countries.*')
			->get ();
		$countries = DB::table ('countries')->select ('id' , 'name')->where ('deleted_at' , '=' , null)->get ();
		$selectedCountryArr = array();
		$selectedCountry = array();
		foreach ($countries as $key => $value) {
			$countriesArr[$value->id] = $value->name;
		}
		foreach ($countriesArr1 as $key => $value) {
			$selectedCountryArr[] = $value->country_id;
		}
		return view ('payment_methods.edit' , ['countriesArr' => $countriesArr , 'selectedCountryArr' => $selectedCountryArr , 'paymentMethod' => $paymentMethod]);
	}

	/**
	 * Update the specified PaymentMethod in storage.
	 *
	 * @param  int                       $id
	 * @param UpdatePaymentMethodRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdatePaymentMethodRequest $request)
	{
		$this->validate ($request , [
			'gateway_name' => 'required|max:255' ,
			'api_key' => 'required' ,
			'api_url' => 'required' ,
			'api_secret' => 'required' ,
			'redirect_url' => 'required' ,
			'is_active' => 'required' ,
			'service_countries' => 'required' ,
		]);
		$input = $request->all ();
		$userId = Auth::id ();
		$input['updated_by'] = $userId;

		$paymentMethod = $this->paymentMethodRepository->findWithoutFail ($id);

		if (empty($paymentMethod)) {
			Flash::error ('Payment Method not found');

			return redirect (route ('paymentMethods.index'));
		}
		$paymentMethod = $this->paymentMethodRepository->update ($input , $id);

//        $this->paymentMethodCountryRepository->update($request->all(),$id);


		Flash::success ('Payment Method updated successfully.');

		return redirect (route ('paymentMethods.index'));
	}

	/**
	 * Remove the specified PaymentMethod from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$paymentMethod = $this->paymentMethodRepository->findWithoutFail ($id);

		if (empty($paymentMethod)) {
			Flash::error ('Payment Method not found');

			return redirect (route ('paymentMethods.index'));
		}
		$this->paymentMethodCountryRepository->delete ($id);
		$this->paymentMethodRepository->delete ($id);

		Flash::success ('Payment Method deleted successfully.');

		return redirect (route ('paymentMethods.index'));
	}
}
