<?php

namespace App\Http\Controllers;

use App\DataTables\PromocodeServiceMapDataTable;
use App\Http\Requests;
use App\Models\AdminUser;
use App\Http\Requests\CreatePromoCodeServiceMapRequest;
use App\Http\Requests\UpdatePromoCodeServiceMapRequest;
use App\Models\PromoCode;
use App\Models\ServiceApi;
use App\Repositories\PromoCodeServiceMapRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Foundation\Auth\User;
use Response;
use Auth;
use Image;


class PromocodeServiceMapController extends AppBaseController
{
    /** @var  ServiceRepository */
    private $feedbackRepository;

    public function __construct (PromoCodeServiceMapRepository $promocodeservicemapRepository)
    {
        $this->promocodeservicemapRepository = $promocodeservicemapRepository;
    }

    /**
     * Display a listing of the Service.
     *
     * @param ServiceDataTable $serviceDataTable
     *
     * @return Response
     */
    public function index (PromocodeServiceMapDataTable $promocodeserviceDataTable)
    {
        return $promocodeserviceDataTable->render ('promocode_service_map.index');
    }

    /**
     * Show the form for creating a new Service.
     *
     * @return Response
     */
    public function create ()
    {

        $services = ServiceApi::where ('deleted_at' , NULL)->pluck ('provider_name' , 'id')->toArray ();
        $promocodes = PromoCode::where ('deleted_at' , NULL)->pluck ('promo_code' , 'id')->toArray ();

        return view ('promocode_service_map.create' , ['services' => $services,'promocodes'=>$promocodes]);
    }

    /**
     * Store a newly created Service in storage.
     *
     * @param CreateServiceRequest $request
     *
     * @return Response
     */
    public function store (CreatePromoCodeServiceMapRequest $request)
    {
        $this->validate ($request , [
            'promocode_id' => 'required' ,
            'service_api_id' => 'required' ,
        ]);
        $input = $request->all ();
        $userId = Auth::id ();
        if (!isset($input['is_active']))
            $input['is_active'] = 0;

        $input['created_by'] = $userId;
        foreach($input['service_api_id'] as $service_id)
        {
            $input['service_api_id'] = $service_id;
            $service = $this->promocodeservicemapRepository->create ($input);
        }

        Flash::success ('Promo code assigned successfully.');

        return redirect (route ('promocode_service_map.index'));
    }

    /**
     * Display the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show ($id)
    {
        $promocode_service_map = $this->promocodeservicemapRepository->findWithoutFail ($id);
        $users = User::all ()->pluck ('first_name' , 'id')->toArray ();
        if (empty($promocode_service_map)) {
            Flash::error ('Promo code map not found');

            return redirect (route ('promocode_service_map.index'));
        }
        $services = ServiceApi::where ('deleted_at' , NULL)->pluck ('provider_name' , 'id')->toArray ();
        $promocodes = PromoCode::where ('deleted_at' , NULL)->pluck ('promo_code' , 'id')->toArray ();

        return view ('promocode_service_map.show')->with ('promocode_service_map' , $promocode_service_map)->with ('users' , $users)->with('services',$services)->with('promocodes',$promocodes);
    }

    /**
     * Show the form for editing the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit ($id)
    {
        $promocode_service_map = $this->promocodeservicemapRepository->findWithoutFail ($id);

        if (empty($promocode_service_map)) {
            Flash::error ('Promo code service map not found');

            return redirect (route ('promocode_service_map.index'));
        }
        $services = ServiceApi::where ('deleted_at' , NULL)->pluck ('provider_name' , 'id')->toArray ();
        $promocodes = PromoCode::where ('deleted_at' , NULL)->pluck ('promo_code' , 'id')->toArray ();

        return view ('promocode_service_map.edit')->with ('promocode_service_map' , $promocode_service_map)->with('services',$services)->with('promocodes',$promocodes);
    }

    /**
     * Update the specified Service in storage.
     *
     * @param  int                 $id
     * @param UpdateServiceRequest $request
     *
     * @return Response
     */
    public function update ($id , UpdatePromoCodeServiceMapRequest $request)
    {
        $promocode_service_map = $this->promocodeservicemapRepository->findWithoutFail ($id);

        if (empty($promocode_service_map)) {
            Flash::error ('Promo code service map not found');

            return redirect (route ('promocode_service_map.index'));
        }
        $input = $request->all ();
        $userId = Auth::id ();

        if (!isset($input['is_active']))
            $input['is_active'] = 0;

        $input['updated_by'] = $userId;


        $feedback = $this->promocodeservicemapRepository->update ($input , $id);

        Flash::success ('Promo code service updated successfully.');

        return redirect (route ('promocode_service_map.index'));
    }

    /**
     * Remove the specified Service from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy ($id)
    {
        $promocode_service_map = $this->promocodeservicemapRepository->findWithoutFail ($id);

        if (empty($promocode_service_map)) {
            Flash::error ('Promo code service map not found');

            return redirect (route ('promocode_service_map.index'));
        }

        $this->promocodeservicemapRepository->delete ($id);

        Flash::success ('Promo code service map deleted successfully.');

        return redirect (route ('promocode_service_map.index'));
    }
}
