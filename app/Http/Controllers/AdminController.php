<?php

namespace App\Http\Controllers;

use App\DataTables\AdminDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAdminRequest;
use App\Http\Requests\UpdateAdminRequest;
use App\Repositories\AdminRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Response;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Support\Facades\Auth;
use Exception;

class AdminController extends AppBaseController
{
	/** @var  AdminRepository */
	private $adminRepository;

	public function __construct (AdminRepository $adminRepo)
	{
		$this->adminRepository = $adminRepo;
	}

	/**
	 * Display a listing of the Admin.
	 *
	 * @param AdminDataTable $adminDataTable
	 *
	 * @return Response
	 */
	public function index (AdminDataTable $adminDataTable)
	{
		return $adminDataTable->render ('admins.index');
	}

	/**
	 * Show the form for creating a new Admin.
	 *
	 * @return Response
	 */
	public function create ()
	{
		$usertype = DB::table ('admin_user_type')->select ('id' , 'type')->get ();
		$selectedAdminTypeArr = array();
		foreach ($usertype as $key => $value) {
			$usertypeArr[$value->id] = $value->type;
		}
		return view ('admins.create' , ['userType' => $usertypeArr , 'selectedAdminType' => $selectedAdminTypeArr ,]);

	}

	/**
	 * Store a newly created Admin in storage.
	 *
	 * @param CreateAdminRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateAdminRequest $request)
	{
		$this->validate ($request , [
			'first_name' => 'required' ,
			'last_name' => 'required' ,
			'mobile' => 'required|numeric' ,
			'email' => 'required|email|unique:admin_users' ,
			'password' => 'required' ,
			'user_type' => 'required' ,

		]);
		try {


			$input = $request->all ();
			$email = Input::get ('email');
			$password = Input::get ('password');
			$passwordSecret = Hash::make ($password);
			unset($input['password']);
			$input['password'] = $passwordSecret;
			$input['created_by'] = Auth::user ()->id;

			if (!isset($input['is_active']))
				$input['is_active'] = 0;
			$emailVal = $this->adminRepository->getEmailCount ($email);
//            dd($input);
			if ($emailVal > 0) {

				Flash::error ('Email Id Already Exist');
				return redirect ()->route ('admins.create');
//                return redirect()->route(...)->with('success', ....);
			} else {
				$admin = $this->adminRepository->create ($input);

				Flash::success ('Admin saved successfully.');
				return redirect (route ('admins.index'));
			}

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getAdminUserType ()
	{
		$usertype = DB::table ('admin_user_type')->select ('id' , 'type')->get ();

		return view ('admins.create' , ['userType' => $usertype]);
	}

	/**
	 * Display the specified Admin.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$admin = $this->adminRepository->findWithoutFail ($id);

		if (empty($admin)) {
			Flash::error ('Admin not found');

			return redirect (route ('admins.index'));
		}

		return view ('admins.show')->with ('admin' , $admin);
	}

	/**
	 * Show the form for editing the specified Admin.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$admin = $this->adminRepository->findWithoutFail ($id);

		$usertype = DB::table ('admin_user_type')->select ('id' , 'type')->get ();
		$adminArr1 = DB::table ('admin_users')->select ('user_type')->where ('id' , '=' , $id)->get ()->toArray ();
		foreach ($usertype as $key => $value) {
			$usertypeArr[$value->id] = $value->type;
		}
		foreach ($adminArr1 as $key => $value) {
			$selectedAdminTypeArr[] = $value->user_type;
		}

		if (empty($admin)) {
			Flash::error ('Admin not found');

			return redirect (route ('admins.index'));
		}

		return view ('admins.edit')->with (['admin' => $admin , 'selectedAdminType' => $selectedAdminTypeArr , 'userType' => $usertypeArr]);
	}


	/**
	 * Update the specified Admin in storage.
	 *
	 * @param  int               $id
	 * @param UpdateAdminRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateAdminRequest $request)
	{
		$admin = $this->adminRepository->findWithoutFail ($id);
		$this->validate ($request , [
			'first_name' => 'required' ,
			'last_name' => 'required' ,
			'mobile' => 'required|numeric' ,
			'email' => 'required|email|unique:admin_users,email,' . $id ,
			'user_type' => 'required' ,

		]);
		if (empty($admin)) {
			Flash::error ('Admin not found');

			return redirect (route ('admins.index'));
		}
		$input = $request->all ();
		$password = Input::get ('password');
		$input['updated_by'] = Auth::user ()->id;
		if (empty($input['password'])) {
			unset($input['password']);
		} else {
			unset($input['password']);
			$passwordSecret = Hash::make ($password);
			$input['password'] = $passwordSecret;
		}

		if (!isset($input['is_active']))
			$input['is_active'] = 0;

		$admin = $this->adminRepository->update ($input , $id);

		Flash::success ('Admin updated successfully.');

		return redirect (route ('admins.index'));
	}

	/**
	 * Remove the specified Admin from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$admin = $this->adminRepository->findWithoutFail ($id);

		if (empty($admin)) {
			Flash::error ('Admin not found');

			return redirect (route ('admins.index'));
		}

		$this->adminRepository->delete ($id);

		Flash::success ('Admin deleted successfully.');

		return redirect (route ('admins.index'));
	}
}
