<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUsersRequest;
use App\Http\Requests\UpdateUsersRequest;
use App\Models\Country;
use App\Repositories\CountryRepository;
use App\Repositories\UsersRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UsersController extends AppBaseController
{
	/** @var  UsersRepository */
	private $usersRepository;
	private $countryRepository;

	public function __construct (UsersRepository $usersRepo , CountryRepository $countryRepo)
	{
		$this->usersRepository = $usersRepo;
		$this->countryRepository = $countryRepo;
	}

	/**
	 * Display a listing of the Users.
	 *
	 * @param UsersDataTable $usersDataTable
	 *
	 * @return Response
	 */
	public function index (UsersDataTable $usersDataTable)
	{
		return $usersDataTable->render ('users.index');
	}

	/**
	 * Show the form for creating a new Users.
	 *
	 * @return Response
	 */
	public function create ()
	{
		$countries = Country::where ('deleted_at' , NULL)->pluck ('name' , 'id');
		return view ('users.create')->with ('countries' , $countries);
	}

	/**
	 * Store a newly created Users in storage.
	 *
	 * @param CreateUsersRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateUsersRequest $request)
	{
		// form validation
		$this->validate ($request , [
			'first_name' => 'required' ,
			'last_name' => 'required' ,
			'mobile' => 'required|numeric' ,
			'email' => 'required|email|unique:users' ,
			'password' => 'required|min:6' ,
			'country' => 'required' ,
		]);
		try {
			$input = $request->all ();

			$user_exists = $this->usersRepository->Check_email_exists ($input['email']);

//			if (!(validate_mobilenumber ($input['mobile'])))
//            throw new Exception('Invalid mobile number' , 409);

				if (!isset($input['is_active']))
					$input['is_active'] = 0;

			$regUser = $this->usersRepository->regUser ($input);


			Flash::success ('Users saved successfully.');

			return redirect (route ('users.index'));

		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * Display the specified Users.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$users = $this->usersRepository->findWithoutFail ($id);

		$countries = Country::where ('deleted_at' , NULL)->pluck ('name' , 'id');

		if (empty($users)) {
			Flash::error ('Users not found');

			return redirect (route ('users.index'));
		}
		return view ('users.show')->with ('users' , $users)
			->with ('countries' , $countries);
	}

	/**
	 * Show the form for editing the specified Users.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{

		$users = $this->usersRepository->findWithoutFail ($id);
		$countries = Country::where ('deleted_at' , NULL)->pluck ('name' , 'id');

		if (empty($users)) {
			Flash::error ('Users not found');

			return redirect (route ('users.index'));
		}

		return view ('users.edit')
			->with ('users' , $users)
			->with ('countries' , $countries);

	}

	/**
	 * Update the specified Users in storage.
	 *
	 * @param  int               $id
	 * @param UpdateUsersRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateUsersRequest $request)
	{
		$this->validate ($request , [
			'first_name' => 'required' ,
			'last_name' => 'required' ,
			'email' => 'required|email|unique:users,email,' . $id ,
			'mobile' => 'required|numeric' ,
			'country' => 'required' ,
		]);
		try {
			$input = $request->all ();
			$users = $this->usersRepository->findWithoutFail ($id);
			$input['created_by'] = Auth::user ()->id;
			if (empty($users)) {
				Flash::error ('Users not found');

				return redirect (route ('users.index'));
			}
			if (!isset($input['is_active']))
				$input['is_active'] = 0;
			$updateuser = $this->usersRepository->updateuser ($input , $id);

			Flash::success ('Users updated successfully.');

			return redirect (route ('users.index'));
		}
		catch (\Exception $e) {
			return $this->sendError ($e->getMessage () , $e->getCode ());
		}
	}

	/**
	 * Remove the specified Users from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$users = $this->usersRepository->findWithoutFail ($id);

		if (empty($users)) {
			Flash::error ('Users not found');

			return redirect (route ('users.index'));
		}
		$this->usersRepository->deactivate ($id);
		//TODO : delete code remove after fix
//        $this->usersRepository->delete($id);

		Flash::success ('Users deleted successfully.');

		return redirect (route ('users.index'));
	}
}
