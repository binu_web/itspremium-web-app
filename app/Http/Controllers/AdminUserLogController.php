<?php

namespace App\Http\Controllers;

use App\DataTables\AdminUserLogDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAdminUserLogRequest;
use App\Http\Requests\UpdateAdminUserLogRequest;
use App\Repositories\AdminUserLogRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AdminUserLogController extends AppBaseController
{
	/** @var  AdminUserLogRepository */
	private $adminUserLogRepository;

	public function __construct (AdminUserLogRepository $adminUserLogRepo)
	{
		$this->adminUserLogRepository = $adminUserLogRepo;
	}

	/**
	 * Display a listing of the AdminUserLog.
	 *
	 * @param AdminUserLogDataTable $adminUserLogDataTable
	 *
	 * @return Response
	 */
	public function index (AdminUserLogDataTable $adminUserLogDataTable)
	{
		return $adminUserLogDataTable->render ('admin_user_logs.index');
	}

	/**
	 * Show the form for creating a new AdminUserLog.
	 *
	 * @return Response
	 */
	public function create ()
	{
		return view ('admin_user_logs.create');
	}

	/**
	 * Store a newly created AdminUserLog in storage.
	 *
	 * @param CreateAdminUserLogRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateAdminUserLogRequest $request)
	{
		$input = $request->all ();

		$adminUserLog = $this->adminUserLogRepository->create ($input);

		Flash::success ('Admin User Log saved successfully.');

		return redirect (route ('adminUserLogs.index'));
	}

	/**
	 * Display the specified AdminUserLog.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$adminUserLog = $this->adminUserLogRepository->findWithoutFail ($id);

		if (empty($adminUserLog)) {
			Flash::error ('Admin User Log not found');

			return redirect (route ('adminUserLogs.index'));
		}

		return view ('admin_user_logs.show')->with ('adminUserLog' , $adminUserLog);
	}

	/**
	 * Show the form for editing the specified AdminUserLog.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$adminUserLog = $this->adminUserLogRepository->findWithoutFail ($id);

		if (empty($adminUserLog)) {
			Flash::error ('Admin User Log not found');

			return redirect (route ('adminUserLogs.index'));
		}

		return view ('admin_user_logs.edit')->with ('adminUserLog' , $adminUserLog);
	}

	/**
	 * Update the specified AdminUserLog in storage.
	 *
	 * @param  int                      $id
	 * @param UpdateAdminUserLogRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateAdminUserLogRequest $request)
	{
		$adminUserLog = $this->adminUserLogRepository->findWithoutFail ($id);

		if (empty($adminUserLog)) {
			Flash::error ('Admin User Log not found');

			return redirect (route ('adminUserLogs.index'));
		}

		$adminUserLog = $this->adminUserLogRepository->update ($request->all () , $id);

		Flash::success ('Admin User Log updated successfully.');

		return redirect (route ('adminUserLogs.index'));
	}

	/**
	 * Remove the specified AdminUserLog from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$adminUserLog = $this->adminUserLogRepository->findWithoutFail ($id);

		if (empty($adminUserLog)) {
			Flash::error ('Admin User Log not found');

			return redirect (route ('adminUserLogs.index'));
		}

		$this->adminUserLogRepository->delete ($id);

		Flash::success ('Admin User Log deleted successfully.');

		return redirect (route ('adminUserLogs.index'));
	}
}
