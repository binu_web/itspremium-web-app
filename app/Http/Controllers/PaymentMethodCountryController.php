<?php

namespace App\Http\Controllers;

use App\DataTables\PaymentMethodCountryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePaymentMethodCountryRequest;
use App\Http\Requests\UpdatePaymentMethodCountryRequest;
use App\Repositories\PaymentMethodCountryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PaymentMethodCountryController extends AppBaseController
{
	/** @var  PaymentMethodCountryRepository */
	private $paymentMethodCountryRepository;

	public function __construct (PaymentMethodCountryRepository $paymentMethodCountryRepo)
	{
		$this->paymentMethodCountryRepository = $paymentMethodCountryRepo;
	}

	/**
	 * Display a listing of the PaymentMethodCountry.
	 *
	 * @param PaymentMethodCountryDataTable $paymentMethodCountryDataTable
	 *
	 * @return Response
	 */
	public function index (PaymentMethodCountryDataTable $paymentMethodCountryDataTable)
	{
		return $paymentMethodCountryDataTable->render ('payment_method_countries.index');
	}

	/**
	 * Show the form for creating a new PaymentMethodCountry.
	 *
	 * @return Response
	 */
	public function create ()
	{
		return view ('payment_method_countries.create');
	}

	/**
	 * Store a newly created PaymentMethodCountry in storage.
	 *
	 * @param CreatePaymentMethodCountryRequest $request
	 *
	 * @return Response
	 */
	public function store (CreatePaymentMethodCountryRequest $request)
	{
		$input = $request->all ();

		$paymentMethodCountry = $this->paymentMethodCountryRepository->create ($input);

		Flash::success ('Payment Method Country saved successfully.');

		return redirect (route ('paymentMethodCountries.index'));
	}

	/**
	 * Display the specified PaymentMethodCountry.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$paymentMethodCountry = $this->paymentMethodCountryRepository->findWithoutFail ($id);

		if (empty($paymentMethodCountry)) {
			Flash::error ('Payment Method Country not found');

			return redirect (route ('paymentMethodCountries.index'));
		}

		return view ('payment_method_countries.show')->with ('paymentMethodCountry' , $paymentMethodCountry);
	}

	/**
	 * Show the form for editing the specified PaymentMethodCountry.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$paymentMethodCountry = $this->paymentMethodCountryRepository->findWithoutFail ($id);

		if (empty($paymentMethodCountry)) {
			Flash::error ('Payment Method Country not found');

			return redirect (route ('paymentMethodCountries.index'));
		}

		return view ('payment_method_countries.edit')->with ('paymentMethodCountry' , $paymentMethodCountry);
	}

	/**
	 * Update the specified PaymentMethodCountry in storage.
	 *
	 * @param  int                              $id
	 * @param UpdatePaymentMethodCountryRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdatePaymentMethodCountryRequest $request)
	{
		$paymentMethodCountry = $this->paymentMethodCountryRepository->findWithoutFail ($id);

		if (empty($paymentMethodCountry)) {
			Flash::error ('Payment Method Country not found');

			return redirect (route ('paymentMethodCountries.index'));
		}

		$paymentMethodCountry = $this->paymentMethodCountryRepository->update ($request->all () , $id);

		Flash::success ('Payment Method Country updated successfully.');

		return redirect (route ('paymentMethodCountries.index'));
	}

	/**
	 * Remove the specified PaymentMethodCountry from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$paymentMethodCountry = $this->paymentMethodCountryRepository->findWithoutFail ($id);

		if (empty($paymentMethodCountry)) {
			Flash::error ('Payment Method Country not found');

			return redirect (route ('paymentMethodCountries.index'));
		}

		$this->paymentMethodCountryRepository->delete ($id);

		Flash::success ('Payment Method Country deleted successfully.');

		return redirect (route ('paymentMethodCountries.index'));
	}
}
