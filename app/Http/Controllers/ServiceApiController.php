<?php

namespace App\Http\Controllers;

use App\DataTables\ServiceApiDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateServiceApiRequest;
use App\Http\Requests\UpdateServiceApiRequest;
use App\Models\AdminUser;
use App\Models\Service;
use App\Repositories\ServiceApiRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Image;

class ServiceApiController extends AppBaseController
{
	/** @var  ServiceApiRepository */
	private $serviceApiRepository;

	public function __construct (ServiceApiRepository $serviceApiRepo)
	{
		$this->serviceApiRepository = $serviceApiRepo;
	}

	/**
	 * Display a listing of the ServiceApi.
	 *
	 * @param ServiceApiDataTable $serviceApiDataTable
	 *
	 * @return Response
	 */
	public function index (ServiceApiDataTable $serviceApiDataTable)
	{

		return $serviceApiDataTable->render ('service_apis.index');
	}

	/**
	 * Show the form for creating a new ServiceApi.
	 *
	 * @return Response
	 */
	public function create ()
	{
		$categories = Service::where ('deleted_at' , NULL)->pluck ('name' , 'id')->toArray ();

		return view ('service_apis.create' , ['serviceCategories' => $categories]);
	}

	/**
	 * Store a newly created ServiceApi in storage.
	 *
	 * @param CreateServiceApiRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateServiceApiRequest $request)
	{
		// form validation
		$this->validate ($request , [
			'provider_name' => 'required' ,
			'service_category' => 'required' ,
			'api_key' => 'required' ,
			'api_url' => 'required',
			'image' => 'required|mimes:jpeg,jpg,png' ,
		]);

		$input = $request->all ();
		$photo = $request->file ('image');

		$userId = Auth::id ();

		if (!empty($photo)) {
			$imagename = time () . '.' . $photo->getClientOriginalExtension ();
			$path = resource_path ('assets/uploads/images/');
			$thumb_img = Image::make ($photo->getRealPath ())->fit (250 , 250);
			$image_save = $thumb_img->save ($path . '/' . $imagename , 80);

			$input['created_by'] = $userId;
			$input['image'] = $imagename;
		}



		if (!isset($input['is_active']))
			$input['is_active'] = 1;

		if (!empty($input['api_metadata']))
			$input['api_metadata'] = htmlspecialchars ($input['api_metadata']);

		$input['created_by'] = $userId;

		$serviceApi = $this->serviceApiRepository->create ($input);


		Flash::success ('Service Api saved successfully.');

		return redirect (route ('serviceApis.index'));
	}


	/**
	 * Display the specified ServiceApi.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$categories = Service::where ('deleted_at' , NULL)->pluck ('name' , 'id')->toArray ();
		$admin_users = AdminUser::all ()->pluck ('first_name' , 'id')->toArray ();
		$serviceApi = $this->serviceApiRepository->findWithoutFail ($id);

		if (empty($serviceApi)) {
			Flash::error ('Service Api not found');

			return redirect (route ('serviceApis.index'));
		}

		return view ('service_apis.show')->with ('serviceApi' , $serviceApi)->with ('serviceCategories' , $categories)->with ('admin_users' , $admin_users);
	}

	/**
	 * Show the form for editing the specified ServiceApi.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{

		$categories = Service::where ('deleted_at' , NULL)->where ('is_active' , '!=' , '0')->pluck ('name' , 'id')->toArray ();

		$serviceApi = $this->serviceApiRepository->findWithoutFail ($id);

		if (empty($serviceApi)) {
			Flash::error ('Service Api not found');

			return redirect (route ('serviceApis.index'));
		}

		return view ('service_apis.edit')->with ('serviceApi' , $serviceApi)->with ('serviceCategories' , $categories);
	}

	/**
	 * Update the specified ServiceApi in storage.
	 *
	 * @param  int                    $id
	 * @param UpdateServiceApiRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateServiceApiRequest $request)
	{
		// form validation
		$this->validate ($request , [
			'provider_name' => 'required' ,
			'service_category' => 'required' ,
			'api_key' => 'required' ,
			'api_url' => 'required'
		]);

		$serviceApi = $this->serviceApiRepository->findWithoutFail ($id);

		if (empty($serviceApi)) {
			Flash::error ('Service Api not found');

			return redirect (route ('serviceApis.index'));
		}

		$input = $request->all ();
		$userId = Auth::id ();
		$photo = $request->file ('image');

		if (!empty($photo)) {
			$imagename = time () . '.' . $photo->getClientOriginalExtension ();
			$path = resource_path ('assets/uploads/images/');
			$thumb_img = Image::make ($photo->getRealPath ())->fit (150 , 150 , function ($constraint) {
				$constraint->aspectRatio ();
			});
			$image_save = $thumb_img->save ($path . '/' . $imagename , 80);
			$input['image'] = $imagename;
		} else {
			$input['image'] = $input['image_old'];
		}

		if (!isset($input['is_active']))
			$input['is_active'] = 0;
		if (!empty($input['api_metadata']))
			$input['api_metadata'] = htmlspecialchars ($input['api_metadata']);

		$input['created_by'] = $userId;
		$input['updated_by'] = $userId;

		$serviceApi = $this->serviceApiRepository->update ($input , $id);

		Flash::success ('Service Api updated successfully.');

		return redirect (route ('serviceApis.index'));
	}

	/**
	 * Remove the specified ServiceApi from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$serviceApi = $this->serviceApiRepository->findWithoutFail ($id);

		if (empty($serviceApi)) {
			Flash::error ('Service Api not found');

			return redirect (route ('serviceApis.index'));
		}

		$this->serviceApiRepository->delete ($id);

		Flash::success ('Service Api deleted successfully.');

		return redirect (route ('serviceApis.index'));
	}
}
