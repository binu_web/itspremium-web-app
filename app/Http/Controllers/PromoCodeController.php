<?php

namespace App\Http\Controllers;

use App\DataTables\PromoCodeDataTable;
use App\Http\Requests;
use App\Models\AdminUser;
use App\Http\Requests\CreatePromoCodeRequest;
use App\Http\Requests\UpdatePromoCodeRequest;
use App\Repositories\PromoCodeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Foundation\Auth\User;
use Response;
use Auth;
use Image;


class PromoCodeController extends AppBaseController
{
    /** @var  ServiceRepository */
    private $feedbackRepository;

    public function __construct (PromoCodeRepository $promocodeRepository)
    {
        $this->promocodeRepository = $promocodeRepository;
    }

    /**
     * Display a listing of the Service.
     *
     * @param ServiceDataTable $serviceDataTable
     *
     * @return Response
     */
    public function index (PromoCodeDataTable $promocodeDataTable)
    {
        return $promocodeDataTable->render ('promocode.index');
    }

    /**
     * Show the form for creating a new Service.
     *
     * @return Response
     */
    public function create ()
    {

        return view ('promocode.create');
    }

    /**
     * Store a newly created Service in storage.
     *
     * @param CreateServiceRequest $request
     *
     * @return Response
     */
    public function store (CreatePromoCodeRequest $request)
    {
        $this->validate ($request , [
            'promo_code' => 'required' ,
            'discount' => 'required' ,
            'valid_from' => 'required' ,
            'valid_to' => 'required' ,
            'no_of_maximum_use' => 'required' ,
        ]);
        $input = $request->all ();
        $userId = Auth::id ();

        if (!isset($input['is_active']))
            $input['is_active'] = 0;

        $input['created_by'] = $userId;
        

        $service = $this->promocodeRepository->create ($input);

        Flash::success ('Promo code saved successfully.');

        return redirect (route ('promocode.index'));
    }

    /**
     * Display the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show ($id)
    {
        $promocode = $this->promocodeRepository->findWithoutFail ($id);
        $users = User::all ()->pluck ('first_name' , 'id')->toArray ();
        if (empty($promocode)) {
            Flash::error ('Promo code not found');

            return redirect (route ('promocode.index'));
        }
        return view ('promocode.show')->with ('promocode' , $promocode)->with ('users' , $users);
    }

    /**
     * Show the form for editing the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit ($id)
    {
        $promocode = $this->promocodeRepository->findWithoutFail ($id);

        if (empty($promocode)) {
            Flash::error ('Promo code not found');

            return redirect (route ('promocode.index'));
        }

        return view ('promocode.edit')->with ('promocode' , $promocode);
    }

    /**
     * Update the specified Service in storage.
     *
     * @param  int                 $id
     * @param UpdateServiceRequest $request
     *
     * @return Response
     */
    public function update ($id , UpdatePromoCodeRequest $request)
    {
        $promocode = $this->promocodeRepository->findWithoutFail ($id);

        if (empty($promocode)) {
            Flash::error ('Promo code not found');

            return redirect (route ('promocode.index'));
        }
        $input = $request->all ();
        $userId = Auth::id ();

        if (!isset($input['is_active']))
            $input['is_active'] = 0;

        $input['updated_by'] = $userId;


        $feedback = $this->promocodeRepository->update ($input , $id);

        Flash::success ('Promo code updated successfully.');

        return redirect (route ('promocode.index'));
    }

    /**
     * Remove the specified Service from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy ($id)
    {
        $promocode = $this->promocodeRepository->findWithoutFail ($id);

        if (empty($promocode)) {
            Flash::error ('Promo code not found');

            return redirect (route ('promocode.index'));
        }

        $this->promocodeRepository->delete ($id);

        Flash::success ('Promo code deleted successfully.');

        return redirect (route ('promocode.index'));
    }
}
