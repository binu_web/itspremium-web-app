<?php

namespace App\Http\Controllers;

use App\DataTables\ServiceBookedDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateServiceBookedRequest;
use App\Http\Requests\UpdateServiceBookedRequest;
use App\Models\Service;
use App\Models\ServiceApi;
use App\Models\User;
use App\Repositories\ServiceBookedRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use App\Quotation;

class ServiceBookedController extends AppBaseController
{
	/** @var  ServiceBookedRepository */
	private $serviceBookedRepository;

	public function __construct (ServiceBookedRepository $serviceBookedRepo)
	{
		$this->serviceBookedRepository = $serviceBookedRepo;
	}

	/**
	 * Display a listing of the ServiceBooked.
	 *
	 * @param ServiceBookedDataTable $serviceBookedDataTable
	 *
	 * @return Response
	 */
	public function index (ServiceBookedDataTable $serviceBookedDataTable)
	{
		return $serviceBookedDataTable->render ('service_bookeds.index');
	}

	/**
	 * Show the form for creating a new ServiceBooked.
	 *
	 * @return Response
	 */
	public function create ()
	{
		return view ('service_bookeds.create');
	}

	/**
	 * Store a newly created ServiceBooked in storage.
	 *
	 * @param CreateServiceBookedRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateServiceBookedRequest $request)
	{
		$input = $request->all ();

		$serviceBooked = $this->serviceBookedRepository->create ($input);

		Flash::success ('Service Booked saved successfully.');

		return redirect (route ('serviceBookeds.index'));
	}

	/**
	 * Display the specified ServiceBooked.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{

		$details = DB::table('service_meta_key')
			->join('service_meta_data', 'service_meta_key.id', '=', 'service_meta_data.meta_key_id')
			->select('meta_key', 'VALUE')
			->where('booked_id',$id)
			->get()->toarray();
		$booking_data = array();

		$users = User::all ()->pluck ('first_name' , 'id')->toArray ();
		$categories = Service::where ('deleted_at' , NULL)->pluck ('name' , 'id')->toArray ();
		$service = ServiceApi::where ('deleted_at' , NULL)->pluck ('provider_name' , 'id')->toArray ();

		foreach($details as $detail){
			if($detail->VALUE == null)
				$d_value = 'NO DATA';
			else
				$d_value = $detail->VALUE;
			$booking_data[$detail->meta_key] =  $d_value;
		}
		$serviceBooked = $this->serviceBookedRepository->findWithoutFail ($id);

		if (empty($serviceBooked)) {
			Flash::error ('Service Booked not found');

			return redirect (route ('serviceBookeds.index'));
		}

		return view ('service_bookeds.show')->with ('serviceBooked' , $serviceBooked)->with ('booking_data' , $booking_data)->with ('users' , $users)->with ('categories' , $categories)->with ('service' , $service);
	}

	/**
	 * Show the form for editing the specified ServiceBooked.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$serviceBooked = $this->serviceBookedRepository->findWithoutFail ($id);

		if (empty($serviceBooked)) {
			Flash::error ('Service Booked not found');

			return redirect (route ('serviceBookeds.index'));
		}

		return view ('service_bookeds.edit')->with ('serviceBooked' , $serviceBooked);
	}

	/**
	 * Update the specified ServiceBooked in storage.
	 *
	 * @param  int                       $id
	 * @param UpdateServiceBookedRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateServiceBookedRequest $request)
	{
		$serviceBooked = $this->serviceBookedRepository->findWithoutFail ($id);

		if (empty($serviceBooked)) {
			Flash::error ('Service Booked not found');

			return redirect (route ('serviceBookeds.index'));
		}

		$serviceBooked = $this->serviceBookedRepository->update ($request->all () , $id);

		Flash::success ('Service Booked updated successfully.');

		return redirect (route ('serviceBookeds.index'));
	}

	/**
	 * Remove the specified ServiceBooked from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$serviceBooked = $this->serviceBookedRepository->findWithoutFail ($id);

		if (empty($serviceBooked)) {
			Flash::error ('Service Booked not found');

			return redirect (route ('serviceBookeds.index'));
		}

		$this->serviceBookedRepository->delete ($id);

		Flash::success ('Service Booked deleted successfully.');

		return redirect (route ('serviceBookeds.index'));
	}
}
