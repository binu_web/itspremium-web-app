<?php

namespace App\Http\Controllers;

use App\DataTables\ServiceMetaDataDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateServiceMetaDataRequest;
use App\Http\Requests\UpdateServiceMetaDataRequest;
use App\Repositories\ServiceMetaDataRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ServiceMetaDataController extends AppBaseController
{
	/** @var  ServiceMetaDataRepository */
	private $serviceMetaDataRepository;

	public function __construct (ServiceMetaDataRepository $serviceMetaDataRepo)
	{
		$this->serviceMetaDataRepository = $serviceMetaDataRepo;
	}

	/**
	 * Display a listing of the ServiceMetaData.
	 *
	 * @param ServiceMetaDataDataTable $serviceMetaDataDataTable
	 *
	 * @return Response
	 */
	public function index (ServiceMetaDataDataTable $serviceMetaDataDataTable)
	{
		return $serviceMetaDataDataTable->render ('service_meta_datas.index');
	}

	/**
	 * Show the form for creating a new ServiceMetaData.
	 *
	 * @return Response
	 */
	public function create ()
	{
		return view ('service_meta_datas.create');
	}

	/**
	 * Store a newly created ServiceMetaData in storage.
	 *
	 * @param CreateServiceMetaDataRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateServiceMetaDataRequest $request)
	{
		$input = $request->all ();

		$serviceMetaData = $this->serviceMetaDataRepository->create ($input);

		Flash::success ('Service Meta Data saved successfully.');

		return redirect (route ('serviceMetaDatas.index'));
	}

	/**
	 * Display the specified ServiceMetaData.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$serviceMetaData = $this->serviceMetaDataRepository->findWithoutFail ($id);

		if (empty($serviceMetaData)) {
			Flash::error ('Service Meta Data not found');

			return redirect (route ('serviceMetaDatas.index'));
		}

		return view ('service_meta_datas.show')->with ('serviceMetaData' , $serviceMetaData);
	}

	/**
	 * Show the form for editing the specified ServiceMetaData.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$serviceMetaData = $this->serviceMetaDataRepository->findWithoutFail ($id);

		if (empty($serviceMetaData)) {
			Flash::error ('Service Meta Data not found');

			return redirect (route ('serviceMetaDatas.index'));
		}

		return view ('service_meta_datas.edit')->with ('serviceMetaData' , $serviceMetaData);
	}

	/**
	 * Update the specified ServiceMetaData in storage.
	 *
	 * @param  int                         $id
	 * @param UpdateServiceMetaDataRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateServiceMetaDataRequest $request)
	{
		$serviceMetaData = $this->serviceMetaDataRepository->findWithoutFail ($id);

		if (empty($serviceMetaData)) {
			Flash::error ('Service Meta Data not found');

			return redirect (route ('serviceMetaDatas.index'));
		}

		$serviceMetaData = $this->serviceMetaDataRepository->update ($request->all () , $id);

		Flash::success ('Service Meta Data updated successfully.');

		return redirect (route ('serviceMetaDatas.index'));
	}

	/**
	 * Remove the specified ServiceMetaData from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$serviceMetaData = $this->serviceMetaDataRepository->findWithoutFail ($id);

		if (empty($serviceMetaData)) {
			Flash::error ('Service Meta Data not found');

			return redirect (route ('serviceMetaDatas.index'));
		}

		$this->serviceMetaDataRepository->delete ($id);

		Flash::success ('Service Meta Data deleted successfully.');

		return redirect (route ('serviceMetaDatas.index'));
	}
}
