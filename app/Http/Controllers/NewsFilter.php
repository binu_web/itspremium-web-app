<?php

namespace App\Http\Controllers;

use App\Models\NewsCategory;
use Illuminate\Http\Request;
use App\Models\NewsSource;
use App\Models\News;
use App\Models\NewsCountry;
use Flash;
use Response;
use Auth;
use DB;

class NewsFilter extends AppBaseController
{
	public $newsCountry , $news;

	public function __construct (NewsCountry $newsCountry , News $news)
	{
		$this->newsCountry = $newsCountry;
		$this->news = $news;
	}

	/**
	 * Retrieve news from source & filter it
	 *
	 */
	public function index (Request $request)
	{
		$source = "";
		$news = array();
		$client = new \GuzzleHttp\Client();
		$news_source = NewsSource::get ();
		$news_category = NewsCategory::where ('deleted_at' , NULL)->where('is_active',1)->pluck ('name' , 'id')->toArray ();

		if ($request->has ('source')) {
			$source = $request->input ('source');
		}

		if (!empty($source)) {
			$url = "https://newsapi.org/v1/articles?source=" . $source . "&sortBy=top&apiKey=2befcaf493a2440599af6704f2649c86";
			$res = $client->request ('GET' , $url);
			$news = json_decode ($res->getBody ());
			$news = $news->articles;

			// remove the new from API
			foreach ($news as $key => $list):

				$count = News::where ('source_link' , $list->url)->count ();

				if ($count)
					unset($news[$key]);

			endforeach;
		}

		$countries = DB::table ('countries')->select ('id' , 'name')->where ('is_active' , '=' , '1')->where ('deleted_at' , null)->get ();
		$selectedCountryArr = array();
		$selectedCountry = array();
		foreach ($countries as $key => $value) {
			$countriesArr[$value->id] = $value->name;
		}

		return view ('news_filter.index')->with (array('news' => $news , 'news_source' => $news_source , 'source' => $source , 'news_category' => $news_category , 'countriesArr' => $countriesArr));
	}

	/**
	 * Add news to news table
	 */
	public function add_news (Request $request)
	{
		$userId = Auth::id ();
		$country = $request->get ('country');
		$this->news->title = $request->title;
		$this->news->category = $request->category;
		$this->news->description = $request->description;
		$this->news->image = $request->image;
		$this->news->source_link = $request->source_link;
		$this->news->publish_date = $request->publish_date;

		if (!isset($request->is_active))
			$this->news->is_active = 0;
		else
			$this->news->is_active = $request->is_active;

		if (!isset($request->is_trending))
			$this->news->is_trending = 0;
		else
			$this->news->is_trending = $request->is_trending;

		$this->news->created_by = $userId;
		$this->news->updated_by = $userId;
		$this->news->save ();
		if ($newsId = $this->news->id) {
			foreach ($country as $value) {
				$query_data[] =
					array(
						'country_id' => $value ,
						'news_id' => $newsId ,
						'created_by' => $userId ,
						'created_at' => date ('Y-m-d H:i:s') ,
						'updated_at' => date ('Y-m-d H:i:s'));
			}
			// insert country Query
			$this->newsCountry->insert ($query_data);
			echo "success";
		} else
			echo "failed";
	}


}
