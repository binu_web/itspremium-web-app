<?php

namespace App\Http\Controllers;

use App\DataTables\ServiceMetaKeyDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateServiceMetaKeyRequest;
use App\Http\Requests\UpdateServiceMetaKeyRequest;
use App\Repositories\ServiceMetaKeyRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ServiceMetaKeyController extends AppBaseController
{
	/** @var  ServiceMetaKeyRepository */
	private $serviceMetaKeyRepository;

	public function __construct (ServiceMetaKeyRepository $serviceMetaKeyRepo)
	{
		$this->serviceMetaKeyRepository = $serviceMetaKeyRepo;
	}

	/**
	 * Display a listing of the ServiceMetaKey.
	 *
	 * @param ServiceMetaKeyDataTable $serviceMetaKeyDataTable
	 *
	 * @return Response
	 */
	public function index (ServiceMetaKeyDataTable $serviceMetaKeyDataTable)
	{
		return $serviceMetaKeyDataTable->render ('service_meta_keys.index');
	}

	/**
	 * Show the form for creating a new ServiceMetaKey.
	 *
	 * @return Response
	 */
	public function create ()
	{
		return view ('service_meta_keys.create');
	}

	/**
	 * Store a newly created ServiceMetaKey in storage.
	 *
	 * @param CreateServiceMetaKeyRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateServiceMetaKeyRequest $request)
	{
		$input = $request->all ();

		$serviceMetaKey = $this->serviceMetaKeyRepository->create ($input);

		Flash::success ('Service Meta Key saved successfully.');

		return redirect (route ('serviceMetaKeys.index'));
	}

	/**
	 * Display the specified ServiceMetaKey.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$serviceMetaKey = $this->serviceMetaKeyRepository->findWithoutFail ($id);

		if (empty($serviceMetaKey)) {
			Flash::error ('Service Meta Key not found');

			return redirect (route ('serviceMetaKeys.index'));
		}

		return view ('service_meta_keys.show')->with ('serviceMetaKey' , $serviceMetaKey);
	}

	/**
	 * Show the form for editing the specified ServiceMetaKey.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$serviceMetaKey = $this->serviceMetaKeyRepository->findWithoutFail ($id);

		if (empty($serviceMetaKey)) {
			Flash::error ('Service Meta Key not found');

			return redirect (route ('serviceMetaKeys.index'));
		}

		return view ('service_meta_keys.edit')->with ('serviceMetaKey' , $serviceMetaKey);
	}

	/**
	 * Update the specified ServiceMetaKey in storage.
	 *
	 * @param  int                        $id
	 * @param UpdateServiceMetaKeyRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateServiceMetaKeyRequest $request)
	{
		$serviceMetaKey = $this->serviceMetaKeyRepository->findWithoutFail ($id);

		if (empty($serviceMetaKey)) {
			Flash::error ('Service Meta Key not found');

			return redirect (route ('serviceMetaKeys.index'));
		}

		$serviceMetaKey = $this->serviceMetaKeyRepository->update ($request->all () , $id);

		Flash::success ('Service Meta Key updated successfully.');

		return redirect (route ('serviceMetaKeys.index'));
	}

	/**
	 * Remove the specified ServiceMetaKey from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$serviceMetaKey = $this->serviceMetaKeyRepository->findWithoutFail ($id);

		if (empty($serviceMetaKey)) {
			Flash::error ('Service Meta Key not found');

			return redirect (route ('serviceMetaKeys.index'));
		}

		$this->serviceMetaKeyRepository->delete ($id);

		Flash::success ('Service Meta Key deleted successfully.');

		return redirect (route ('serviceMetaKeys.index'));
	}
}
