<?php

namespace App\Http\Controllers;

use App\DataTables\CountryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCountryRequest;
use App\Http\Requests\UpdateCountryRequest;
use App\Repositories\CountryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CountryController extends AppBaseController
{
	/** @var  CountryRepository */
	private $countryRepository;

	public function __construct (CountryRepository $countryRepo)
	{
		$this->countryRepository = $countryRepo;
	}

	/**
	 * Display a listing of the Country.
	 *
	 * @param CountryDataTable $countryDataTable
	 *
	 * @return Response
	 */
	public function index (CountryDataTable $countryDataTable)
	{
		return $countryDataTable->render ('countries.index');
	}

	/**
	 * Show the form for creating a new Country.
	 *
	 * @return Response
	 */
	public function create ()
	{
		return view ('countries.create');
	}

	/**
	 * Store a newly created Country in storage.
	 *
	 * @param CreateCountryRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateCountryRequest $request)
	{

		$this->validate ($request , [
			'name' => 'required|unique:countries,name,' ,
			'iso2' => 'required|unique:countries,iso2,' ,
			'iso3' => 'required|unique:countries,iso3,' ,
			'phone_code' => 'required|numeric|unique:countries,phone_code,' ,
			'nicename' => 'required|unique:countries,nicename' ,
			'country_code' => 'required|numeric|min:0|unique:countries,country_code' ,
		]);
		$input = $request->all ();
		if (!isset($input['is_active'])) {
			$input['is_active'] = 0;
		}
		$updatecountry = $this->countryRepository->add_country ($input);

		// TODO : Remove the below comments once system reaches a stable stage
//        $country = $this->countryRepository->create($input);

		Flash::success ('Country saved successfully.');

		return redirect (route ('countries.index'));
	}

	/**
	 * Display the specified Country.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$country = $this->countryRepository->findWithoutFail ($id);

		if (empty($country)) {
			Flash::error ('Country not found');

			return redirect (route ('countries.index'));
		}

		return view ('countries.show')->with ('country' , $country);
	}

	/**
	 * Show the form for editing the specified Country.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$country = $this->countryRepository->findWithoutFail ($id);

		if (empty($country)) {
			Flash::error ('Country not found');

			return redirect (route ('countries.index'));
		}

		return view ('countries.edit')->with ('country' , $country);
	}

	/**
	 * Update the specified Country in storage.
	 *
	 * @param  int                 $id
	 * @param UpdateCountryRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateCountryRequest $request)
	{
		$country = $this->countryRepository->findWithoutFail ($id);

		$this->validate ($request , [
			'name' => 'required|unique:countries,name,' . $id ,
			'iso2' => 'required|unique:countries,iso2,' . $id ,
			'iso3' => 'required|unique:countries,iso3,' . $id ,
			'phone_code' => 'required|numeric|unique:countries,phone_code,' . $id ,
			'nicename' => 'required|unique:countries,nicename,' . $id ,
			'country_code' => 'required|numeric|min:0|unique:countries,country_code,' . $id ,
		]);
		$input = $request->all ();
		if (empty($country)) {
			Flash::error ('Country not found');
			return redirect (route ('countries.index'));
		}
		if (!isset($input['is_active'])) {
			$input['is_active'] = 0;
		}
		$updatecountry = $this->countryRepository->update_country ($input , $id);

		// TODO : Remove the below comments once system reaches a stable stage
//        $country = $this->countryRepository->update($request->all(), $id);

		Flash::success ('Country updated successfully.');

		return redirect (route ('countries.index'));
	}

	/**
	 * Remove the specified Country from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$country = $this->countryRepository->findWithoutFail ($id);

		if (empty($country)) {
			Flash::error ('Country not found');

			return redirect (route ('countries.index'));
		}

		$this->countryRepository->delete ($id);

		Flash::success ('Country deleted successfully.');

		return redirect (route ('countries.index'));
	}
}
