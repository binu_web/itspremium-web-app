<?php

namespace App\Http\Controllers;

use App\DataTables\FeedbackDataTable;
use App\Http\Requests;
use App\Models\AdminUser;
use App\Http\Requests\CreateFeedbackRequest;
use App\Http\Requests\UpdateFeedbackRequest;
use App\Repositories\FeedbackRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Foundation\Auth\User;
use Response;
use Auth;
use Image;


class FeedbackController extends AppBaseController
{
    /** @var  ServiceRepository */
    private $feedbackRepository;

    public function __construct (FeedbackRepository $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
    }

    /**
     * Display a listing of the Service.
     *
     * @param ServiceDataTable $serviceDataTable
     *
     * @return Response
     */
    public function index (FeedbackDataTable $feedbackDataTable)
    {

        return $feedbackDataTable->render ('feedback.index');
    }

    /**
     * Show the form for creating a new Service.
     *
     * @return Response
     */
    public function create ()
    {

        return view ('feedback.create');
    }

    /**
     * Store a newly created Service in storage.
     *
     * @param CreateServiceRequest $request
     *
     * @return Response
     */
    public function store (CreateFeedbackRequest $request)
    {
        $this->validate ($request , [
            'name' => 'required' ,
            'image' => 'required|mimes:jpeg,jpg,png' ,
        ]);
        $input = $request->all ();
        $userId = Auth::id ();
        $photo = $request->file ('image');

        if (!empty($photo)) {
            $imagename = time () . '.' . $photo->getClientOriginalExtension ();
            $path = resource_path ('assets/uploads/images/');
            $thumb_img = Image::make ($photo->getRealPath ())->fit (250 , 250);
            $image_save = $thumb_img->save ($path . '/' . $imagename , 80);

            $input['created_by'] = $userId;
            $input['image'] = $imagename;
        }
        if (!isset($input['is_active']))
            $input['is_active'] = 0;

        $input['created_by'] = $userId;

        $service = $this->feedbackRepository->create ($input);

        Flash::success ('Service saved successfully.');

        return redirect (route ('services.index'));
    }

    /**
     * Display the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show ($id)
    {
        $feedback = $this->feedbackRepository->findWithoutFail ($id);
        $users = User::all ()->pluck ('first_name' , 'id')->toArray ();
        if (empty($feedback)) {
            Flash::error ('Feedback not found');

            return redirect (route ('feedback.index'));
        }
        return view ('feedback.show')->with ('feedback' , $feedback)->with ('users' , $users);
    }

    /**
     * Show the form for editing the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit ($id)
    {
        $feedback = $this->feedbackRepository->findWithoutFail ($id);

        if (empty($feedback)) {
            Flash::error ('Feedback not found');

            return redirect (route ('feedback.index'));
        }

        return view ('feedback.edit')->with ('feedback' , $feedback);
    }

    /**
     * Update the specified Service in storage.
     *
     * @param  int                 $id
     * @param UpdateServiceRequest $request
     *
     * @return Response
     */
    public function update ($id , UpdateFeedbackRequest $request)
    {
        $feedback = $this->feedbackRepository->findWithoutFail ($id);

        if (empty($feedback)) {
            Flash::error ('Feedback not found');

            return redirect (route ('feedback.index'));
        }
        $input = $request->all ();
        $userId = Auth::id ();

        if (!isset($input['is_active']))
            $input['is_active'] = 0;

        $input['updated_by'] = $userId;

        $feedback = $this->feedbackRepository->update ($input , $id);

        Flash::success ('Feedback updated successfully.');

        return redirect (route ('feedback.index'));
    }

    /**
     * Remove the specified Service from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy ($id)
    {
        $feedback = $this->feedbackRepository->findWithoutFail ($id);

        if (empty($feedback)) {
            Flash::error ('Feedback not found');

            return redirect (route ('feedback.index'));
        }

        $this->feedbackRepository->delete ($id);

        Flash::success ('Feedback deleted successfully.');

        return redirect (route ('feedback.index'));
    }
}
