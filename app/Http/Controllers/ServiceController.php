<?php

namespace App\Http\Controllers;

use App\DataTables\ServiceDataTable;
use App\Http\Requests;
use App\Models\AdminUser;
use App\Http\Requests\CreateServiceRequest;
use App\Http\Requests\UpdateServiceRequest;
use App\Repositories\ServiceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Image;


class ServiceController extends AppBaseController
{
	/** @var  ServiceRepository */
	private $serviceRepository;

	public function __construct (ServiceRepository $serviceRepo)
	{
		$this->serviceRepository = $serviceRepo;
	}

	/**
	 * Display a listing of the Service.
	 *
	 * @param ServiceDataTable $serviceDataTable
	 *
	 * @return Response
	 */
	public function index (ServiceDataTable $serviceDataTable)
	{
		return $serviceDataTable->render ('services.index');
	}

	/**
	 * Show the form for creating a new Service.
	 *
	 * @return Response
	 */
	public function create ()
	{

		return view ('services.create');
	}

	/**
	 * Store a newly created Service in storage.
	 *
	 * @param CreateServiceRequest $request
	 *
	 * @return Response
	 */
	public function store (CreateServiceRequest $request)
	{
		$this->validate ($request , [
			'name' => 'required' ,
			'image' => 'required|mimes:jpeg,jpg,png' ,
		]);
		$input = $request->all ();
		$userId = Auth::id ();
		$photo = $request->file ('image');

		if (!empty($photo)) {
			$imagename = time () . '.' . $photo->getClientOriginalExtension ();
			$path = resource_path ('assets/uploads/images/');
			$thumb_img = Image::make ($photo->getRealPath ())->fit (250 , 250);
			$image_save = $thumb_img->save ($path . '/' . $imagename , 80);

			$input['created_by'] = $userId;
			$input['image'] = $imagename;
		}
		if (!isset($input['is_active']))
			$input['is_active'] = 0;

		$input['created_by'] = $userId;

		$service = $this->serviceRepository->create ($input);

		Flash::success ('Service saved successfully.');

		return redirect (route ('services.index'));
	}

	/**
	 * Display the specified Service.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show ($id)
	{
		$service = $this->serviceRepository->findWithoutFail ($id);
		$admin_users = AdminUser::all ()->pluck ('first_name' , 'id')->toArray ();
		if (empty($service)) {
			Flash::error ('Service not found');

			return redirect (route ('services.index'));
		}
		return view ('services.show')->with ('service' , $service)->with ('admin_users' , $admin_users);
	}

	/**
	 * Show the form for editing the specified Service.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit ($id)
	{
		$service = $this->serviceRepository->findWithoutFail ($id);

		if (empty($service)) {
			Flash::error ('Service not found');

			return redirect (route ('services.index'));
		}

		return view ('services.edit')->with ('service' , $service);
	}

	/**
	 * Update the specified Service in storage.
	 *
	 * @param  int                 $id
	 * @param UpdateServiceRequest $request
	 *
	 * @return Response
	 */
	public function update ($id , UpdateServiceRequest $request)
	{
		$service = $this->serviceRepository->findWithoutFail ($id);

		if (empty($service)) {
			Flash::error ('Service not found');

			return redirect (route ('services.index'));
		}
		$input = $request->all ();
		$userId = Auth::id ();
		$photo = $request->file ('image');

		if (!empty($photo)) {
			$imagename = time () . '.' . $photo->getClientOriginalExtension ();
			$path = resource_path ('assets/uploads/images/');
			$thumb_img = Image::make ($photo->getRealPath ())->fit (150 , 150 , function ($constraint) {
				$constraint->aspectRatio ();
			});
			$image_save = $thumb_img->save ($path . '/' . $imagename , 80);
			$input['image'] = $imagename;
		} else {
			$input['image'] = $input['image_old'];
		}

		if (!isset($input['is_active']))
			$input['is_active'] = 0;

		$input['updated_by'] = $userId;

		$service = $this->serviceRepository->update ($input , $id);

		Flash::success ('Service updated successfully.');

		return redirect (route ('services.index'));
	}

	/**
	 * Remove the specified Service from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy ($id)
	{
		$service = $this->serviceRepository->findWithoutFail ($id);

		if (empty($service)) {
			Flash::error ('Service not found');

			return redirect (route ('services.index'));
		}

		$this->serviceRepository->delete ($id);

		Flash::success ('Service deleted successfully.');

		return redirect (route ('services.index'));
	}
}
