<?php

namespace App\Http\Controllers;

use App\Repositories\DeviceRepository;
use App\Services\PushNotification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
	//
	/**
	 * Display a listing of the Admin.
	 *
	 * @param AdminDataTable $adminDataTable
	 *
	 * @return Response
	 */
	public function index ()
	{
		return view ('notification.index');
	}

	/**
	 * Operation To Send Notification
	 *
	 * @param CreateNewsRequest $request
	 *
	 * @return Response
	 */
	public function store (Request $request , DeviceRepository $device)
	{

		$input = $request->all ();
		$status = false;
		if (checkArrayIsEmpty ($input , ['type']))
			return response ()->json (['status' => 'error' , 'msg' => 'some fields are empty'] , 400);

		$deviceTokens = $device->getDeviceFCMTokens ($request->get ('type'));

		$customData = [];

		if (!empty($deviceTokens))
			$status = PushNotification::sendNotification ($deviceTokens , $request->get ('title') , $request->get ('description') , $customData);

		// Push Notificationss
		if ($status) {
			return response ()->json (['status' => 'success' , 'msg' => 'push notification successfully sent'] , 200);
		} else {
			return response ()->json (['status' => 'error' , 'msg' => 'push notification not send'] , 404);
		}


	}
}
