<?php

namespace App\Http\Controllers;

use App\DataTables\HotelRefundTable;
use App\Http\Requests;
use App\Models\AdminUser;
use App\Repositories\PromoCodeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Foundation\Auth\User;
use Response;
use Auth;
use Image;
use App\Models\ServiceBooked;
use App\Models\ServiceMetaKey;
use App\Models\ServiceMetaData;
use App\Http\Controllers\API\RazorPayWebHookController;

use Illuminate\Http\Request;


class HotelRefundController extends AppBaseController
{
    /** @var  ServiceRepository */
    private $feedbackRepository;

    public function __construct (PromoCodeRepository $promocodeRepository)
    {
        $this->promocodeRepository = $promocodeRepository;
    }

    /**
     * Display a listing of the Service.
     *
     * @param ServiceDataTable $serviceDataTable
     *
     * @return Response
     */
    public function index (HotelRefundTable $hotelrefundDataTable)
    {
        return $hotelrefundDataTable->render ('hotel_refund.index');
    }

    /**
     * Display the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function refund ($id)
    {

        $oBooking       = ServiceBooked::find($id);

        // Collect the details to return.
        $return_key = ['payment_referrance_id','cancel_refund_amount'];

        $return_array= array();
        foreach($return_key as $keys){
            $meta_key_id = ServiceMetaKey::select('id')->where('meta_key','=',$keys)->where('service_id',$oBooking->service_api_id)->first();
            $meta_key_id = $meta_key_id['id'];

            $meta_data = ServiceMetaData::select('value')->where('meta_key_id', '=', $meta_key_id)
                ->where('booked_id', '=', $id)
                ->first();
            $return_array[$keys] = $meta_data['value'];
        }


        //TODO : Currently no values return once it get remove the comments(midhun)
//        $paymentUniqueId = $return_array['$return_array'];
//        $refund_amount = $return_array['cancel_refund_amount'];

        $paymentUniqueId = 'pay_8JQH6XrdqUBoQH';
        $refund_amount = 100;

        $request = [
            'payment_unique_id' => $paymentUniqueId,
            'booking_id' => $id,
            'service_id' => $oBooking->service_api_id,
            'refund_amount' => $refund_amount,
            'user_id' => $oBooking->user_id,
        ];


        $oRazorPayController = new RazorPayWebHookController();
        $returnRazorPay 		   = $oRazorPayController->processFullRefundToWallet($request);

//        $returnRazorPay 		   = $oRazorPayController->processFullRefund($request);


        if (is_array($returnRazorPay) && isset($returnRazorPay['error']))
        {
            Flash::error ($returnRazorPay['message']);
            return redirect (route ('hotel_refund.index'));
        }


        Flash::success ($returnRazorPay['message']);
        return redirect (route ('hotel_refund.index'));
    }

    public function show($id)
    {

        $oBooking       = ServiceBooked::find($id);

        $aMetaKey = ServiceMetaKey::select('meta_key')->where('service_id','=',$oBooking->service_api_id)->get()->toArray();
        $metakey_array = array();
        foreach($aMetaKey as $meta){

            $metakey_array[] = $meta['meta_key'];
        }
        $return_array= array();
        foreach($metakey_array as $keys){
            $meta_key_id = ServiceMetaKey::select('id')->where('meta_key','=',$keys)->where('service_id',$oBooking->service_api_id)->first();
            $meta_key_id = $meta_key_id['id'];

            $meta_data = ServiceMetaData::select('value')->where('meta_key_id', '=', $meta_key_id)
                ->where('booked_id', '=', $id)
                ->first();
            $return_array[$keys] = $meta_data['value'];
        }

        return view ('hotel_refund.show')->with ('booking' , $oBooking)->with ('metadata' , $return_array);

    }

    public function partialRefund()
    {
        $booking_id = $_POST['booking_id'];
        $partial_refund_amount = $_POST['p_amount'].'00'; //Adding the 2 digit

        $oBooking       = ServiceBooked::find($booking_id);
        // Collect the details to return.
        $return_key = ['payment_referrance_id','cancel_refund_amount'];

        $return_array= array();
        foreach($return_key as $keys){
            $meta_key_id = ServiceMetaKey::select('id')->where('meta_key','=',$keys)->where('service_id',$oBooking->service_api_id)->first();
            $meta_key_id = $meta_key_id['id'];

            $meta_data = ServiceMetaData::select('value')->where('meta_key_id', '=', $meta_key_id)
                ->where('booked_id', '=', $booking_id)
                ->first();
            $return_array[$keys] = $meta_data['value'];
        }


        //TODO : Currently no values return once it get remove the commets(midhun)
//        $paymentUniqueId = $return_array['$return_array'];
//        $refund_amount = $return_array['cancel_refund_amount'];
        $paymentUniqueId = 'pay_8Jhl3JYbmpCCKR';
        $refund_amount = 1000;


        $request = [
            'payment_unique_id' => $paymentUniqueId,
            'booking_id' => $booking_id,
            'service_id' => $oBooking->service_api_id,
            'refund_amount' => $refund_amount,
            'partial_refund_amount' => $partial_refund_amount,

        ];

        $oRazorPayController = new RazorPayWebHookController();

        $returnRazorPay 		   = $oRazorPayController->processPartialRefundToWallet($request);

//        $returnRazorPay = $oRazorPayController->processPartialRefund($request);


        if (is_array($returnRazorPay) && isset($returnRazorPay['error']))
        {
            Flash::error ($returnRazorPay['message']);
            return redirect (route ('hotel_refund.index'));
        }

        Flash::success ($returnRazorPay['message']);
        return redirect (route ('hotel_refund.index'));

    }


}
