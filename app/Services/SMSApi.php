<?php
namespace App\Services;

use Config;
use Illuminate\Support\Facades\Log;

class SMSApi
{

	private $url;
	private $userName;
	private $password;
	private $senderId;
	private $method;
	private $apiKey;

	public function __construct ()
	{
		$this->url = Config::get ('sms.URL');
		$this->userName = Config::get ('sms.USERNAME');
		$this->password = Config::get ('sms.PASSWORD');
		$this->senderId = Config::get ('sms.SENDER_ID');
		$this->method = Config::get ('sms.METHOD');
		$this->apiKey = Config::get ('sms.API_KEY');
	}

	/*
	* Send SMS
	*
	*/
	public function send ($message , $toNumber)
	{

		// Sandbox Enviroment
		if (env ('APP_ENV') == 'local') {
			return true;
		}


		if (is_array ($toNumber)) {
			$toNumber = implode (',' , $toNumber);
		}

		$curlUrl = $this->url . "?method=" . urldecode ($this->method) . "&api_key=" . urlencode ($this->apiKey) . "&to=" . urlencode ($toNumber) . "&sender=" . urlencode ($this->senderId) . "&message=" . urlencode ($message);
		//die($curlUrl); 
		$ch = curl_init ($curlUrl);
		curl_setopt ($ch , CURLOPT_RETURNTRANSFER , true);
		$resp = curl_exec ($ch);
		curl_close ($ch);

		$aResp = json_decode ($resp , true);

		if ($aResp['status'] != 'OK') {
			$aMessage['error_msg_from_api'] = $aResp;
			$aMessage['request'] = $curlUrl;
			$logMessage = json_encode ($aMessage);
			Log::error ($logMessage);
			return false;
		}

		return true;

	}

}

