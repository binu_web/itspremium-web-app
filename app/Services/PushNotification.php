<?php

namespace App\Services;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class PushNotification
{
	public $title = "";
	public $body = "";

	/**
	 * Send Notification Push Notification
	 *
	 * @param array  $senders
	 * @param string $title
	 * @param string $content
	 * @param array  $customData
	 *
	 * @author Binoop V
	 * @return bool
	 */
	public static function sendNotification ($senders = [] , $title = "" , $content = "" , $customData = [])
	{
		$optionBuiler = new OptionsBuilder();
		$optionBuiler->setTimeToLive (60 * 20);

		$notificationBuilder = new PayloadNotificationBuilder($title);
		$notificationBuilder->setBody ($content)
			->setSound ('default');

		$dataBuilder = new PayloadDataBuilder();
		$dataBuilder->addData ($customData);

		$option = $optionBuiler->build ();
		$notification = $notificationBuilder->build ();
		$data = $dataBuilder->build ();


		$downstreamResponse = FCM::sendTo ($senders , $option , $notification , $data);
		return ($downstreamResponse->numberSuccess () > 0) ? true : false;
	}

}