<?php
namespace App\Services\Yatra;


use App\XMLRequests\Hotels\Yatra\HotelProvisionalBookingRQ;
use App\XMLRequests\Hotels\Yatra\HotelFinalBookingRQ;
use App\Services\HttpClient;
use App\Services\XmlToJson;
use Exception;
use App\Repositories\ApiRepository\UserApiRepository;
use App\Repositories\ApiRepository\ServiceMetaKeyRepository;
use App\Models\YatraLocation;
use App\Models\YatraHotelDetail;
use App\Repositories\ApiRepository\ServiceLogRepository;

/*
* Class to manage Yatra Hotel Booking Process
* @author Harinarayanan 
* @created on 13-07-2017
*
*/

class HotelBookingProcess
{

	private $requestData 		= [];
	protected $oServiceLog;

	public function __construct($requestData = [])
	{
		$this->requestData 		= $requestData;
		$this->oServiceLog    	= new ServiceLogRepository();
	}


	#public function 

	/*
	*
	*
	*
	*
	*/
	public function makeProvisionalBooking()
	{

		$oHotelProvisionalBookingRq = new HotelProvisionalBookingRQ($this->requestData);
		$xmlRequest 				= $oHotelProvisionalBookingRq->requestXML ();
		$aError 					= [];
		$bookingId 					= null;
		$yatraFinalBookingId 		= null;
		#echo htmlentities($xmlRequest); die;

		// Set up Rest Client Request
		$client = new HttpClient(['headers' => ['Content-Type' => 'application/xml']]);

		#echo $this->requestData['serviceUrl']; die;
		 //start of section to insert into service log
        $aRequestLog = [
            'user_id' 			=> $this->requestData['userId'],
            'service_id' 		=> $this->requestData['serviceId'],
            'service_api_id' 	=> $this->requestData['serviceApiId'],
            'event_type_id' 	=> 3,
            'http_type' 		=> 'REQUEST',
            'data_type' 		=> 'XML',
            'data' 				=> serialize($xmlRequest),
        ];

        $rRequestLog 			= $this->oServiceLog->addServiceLog($aRequestLog);


		$client->setUrl ($this->requestData['serviceUrl']);
		$client->setParams ($xmlRequest);

		// Post XML Request
		$response 		= $client->xmlPost ();
		$xmlResponse 	= $client->getResponseXML();
		//echo htmlentities($xmlResponse); die;

		//Service log
	    $aRequestLog = [
            'user_id' 			=> $this->requestData['userId'],
            'service_id' 		=> $this->requestData['serviceId'],
            'service_api_id' 	=> $this->requestData['serviceApiId'],
            'event_type_id' 	=> 3,
            'http_type' 		=> 'RESPONSE',
            'data_type' 		=> 'XML',
            'data' 				=> serialize($xmlResponse),
        ];


        $rRequestLog 			= $this->oServiceLog->addServiceLog($aRequestLog);

		// HandLe Error Response Check for Exception
		if ($response instanceof Exception) {
			throw new Exception($response->getMessage () , $response->getCode ());
		}

		#echo htmlentities($xmlResponse); die;
		


		

		$response  = array_get($response, 'Envelope.soap:Body.OTA_HotelResRS');
		$response  = $response ? $response : [];

		// Handle Incase of Any Error In Response
		$error = $this->handleResponseError ($response);
		if ($error) {
			throw new Exception($error , 400);
		}

		
		
		#$responeXml = $client->getResponseXML();
		##echo htmlentities($responeXml);

		$aHotelReservation = array_get($response, 'HotelReservations.HotelReservation.UniqueID');
		$aHotelReservation = $aHotelReservation ? $aHotelReservation : [];

		

		$provisionalBookId = isset($aHotelReservation['@ID']) ? $aHotelReservation['@ID']: null;
		$uniqueIdType 	   = isset($aHotelReservation['@Type']) ? $aHotelReservation['@Type'] : null;

		if($provisionalBookId  && $uniqueIdType) {
			//entry to our tables: - repository
			$this->requestData['provisionalBookingId']  		= $provisionalBookId;
			$this->requestData['uniqueIdType']					= $uniqueIdType;
			

			$oServiceMetakeyRepository 			= new ServiceMetaKeyRepository();

			$aBookingDet = $oServiceMetakeyRepository->addYatraHotelBookingDetails($this->requestData);
			$bookingId 	 										= $aBookingDet['booking_id'];
			$this->requestData['transactionIdentifier']			= $bookingId;

			if($this->requestData['netAmountToPay'] == 0) {
				//do final booking api from here itself
				$aBookingDetails 		= $this->makeFinalBooking();
				$bookingId 	     		= $aBookingDetails['bookingId'];
				$yatraFinalBookingId 	= $aBookingDetails['yatraFinalBookingId'];
			}




		}

		

		return [

			'aError' 			   => $aError,
			'provisionalBookingId' => $provisionalBookId,
			'bookingId'			   => $bookingId,
			'yatraFinalBookingId'  => $yatraFinalBookingId,
			'netAmountToPay'	   => $this->requestData['netAmountToPay']
			

		];



		

	}

	/*
	*
	*
	*
	*
	**/
	public function makeFinalBooking()
	{
		
		$oHotelProvisionalBookingRq = new HotelFinalBookingRQ($this->requestData);
		$xmlRequest 				= $oHotelProvisionalBookingRq->requestXML ();

		//service log:

		$aRequestLog = [
            'user_id' 			=> $this->requestData['userId'],
            'service_id' 		=> $this->requestData['serviceId'],
            'service_api_id' 	=> $this->requestData['serviceApiId'],
            'event_type_id' 	=> 4,
            'http_type' 		=> 'REQUEST',
            'data_type' 		=> 'XML',
            'data' 				=> serialize($xmlRequest),
        ];

        $rRequestLog 			= $this->oServiceLog->addServiceLog($aRequestLog);

		#echo htmlentities($xmlRequest);
		$aError 					= [];
		

		// Set up Rest Client Request
		$client = new HttpClient(['headers' => ['Content-Type' => 'application/xml']]);
		#echo $this->requestData['serviceUrl']; die;
		$client->setUrl ($this->requestData['serviceUrl']);
		$client->setParams ($xmlRequest);

		$response 		= $client->xmlPost ();
		$xmlResponse 	= $client->getResponseXML();
		
		//echo htmlentities($xmlResponse);

		//service log:

		$aRequestLog = [
            'user_id' 			=> $this->requestData['userId'],
            'service_id' 		=> $this->requestData['serviceId'],
            'service_api_id' 	=> $this->requestData['serviceApiId'],
            'event_type_id' 	=> 4,
            'http_type' 		=> 'RESPONSE',
            'data_type' 		=> 'XML',
            'data' 				=> serialize($xmlResponse),
        ];



        $rRequestLog 			= $this->oServiceLog->addServiceLog($aRequestLog);
		
		// HandLe Error Response Check for Exception
		if ($response instanceof Exception) {
			throw new Exception($response->getMessage () , $response->getCode ());
		}

        
   		#print_r($response);

		$response          = array_get($response, 'Envelope.soap:Body.OTA_HotelResRS');
		$response  		   = $response ? $response : [];

		// Handle Incase of Any Error In Response
		$error = $this->handleResponseError ($response);
		if ($error) {
			
			throw new Exception($error , 400);
		}	

		

		$aHotelReservation = array_get($response, 'HotelReservations.HotelReservation');
		$aHotelReservation = $aHotelReservation ? $aHotelReservation : [];
	

		$finalBookingId     = null;
		$finalBookingIdType = null;



		if($aHotelReservation) {
			$aUniqueId  		= $aHotelReservation['UniqueID'];
			$finalBookingId 	= $aUniqueId['@ID'];
			$finalBookingIdType = $aUniqueId['@Type'];

			$aRoomStay 			= $aHotelReservation['RoomStays']['RoomStay'];
			$aRoomType 			= $aRoomStay['RoomTypes']['RoomType'];
			$aRatePlan 			= $aRoomStay['RatePlans']['RatePlan'];
			$aCancelPenalityDesc = array_get($aRatePlan, 'CancelPenalties.CancelPenalty.PenaltyDescription');
			$aCancelPenalityDesc = $aCancelPenalityDesc ? $aCancelPenalityDesc : [];
			$cancelPenaltyDesc   = isset($aCancelPenalityDesc['Text']) ? $aCancelPenalityDesc['Text'] : null;
			$aBasicPropertyInfo  = $aRoomStay['BasicPropertyInfo'];
			$aTpaExtension 		 = isset($aRoomStay['TPA_Extensions']) ? $aRoomStay['TPA_Extensions'] : [];
			$hotelName 			 = $aBasicPropertyInfo['@HotelName'];
			$areaId 			 = $aBasicPropertyInfo['@AreaID'];
			$addressLine 		 = null;
			$cityName 			 = null;
			$stateProv 			 = null;
			$countryName 		 = null;
			$aAddress 			 = $aBasicPropertyInfo['Address'];
			if($aAddress) {
				$addressLine 	 = isset($aAddress['AddressLine']) ? $aAddress['AddressLine'] : null;
				$addressLine 	 = is_array($addressLine) ? implode(',', $addressLine) : $addressLine;
				$cityName	 	 = isset($aAddress['CityName']) ? $aAddress['CityName'] : null;
				$stateProv 		 = isset($aAddress['StateProv']) ? $aAddress['StateProv'] : null;
				$countryName 	 = isset($aAddress['StateProv']) ? $aAddress['StateProv'] : null;
			}

			$aContactNumber 	 = array_get($aBasicPropertyInfo, 'ContactNumbers.ContactNumber');
			$contactNumber 		 = json_encode($aContactNumber);

			//
			$affiliateCommisionAmnt 		= 0;
			$affiliateCommisionPercentage 	= 0;
			$affiliateCommisionHotelTaxIncluded = null;
			if($aTpaExtension) {
				$_aAffliatedCommision 				= isset($aTpaExtension['AffiliateCommission']) ? $aTpaExtension['AffiliateCommission'] : [];
				$affiliateCommisionAmnt 			= isset($_aAffliatedCommision['@Amount']) ? $_aAffliatedCommision['@Amount'] : null;
				$affiliateCommisionHotelTaxIncluded = isset($_aAffliatedCommision['@HotelTaxIncluded']) ? $_aAffliatedCommision['@HotelTaxIncluded'] : null;
				$affiliateCommisionPercentage  		= isset($_aAffliatedCommision['@Percent']) ? $_aAffliatedCommision['@Percent'] : null;

			}

			//update our tables:

			$aMetaKeyValue  	 = [
				'hotel_name'	  			   => $hotelName,
				'area_name' 	 			   => $areaId,
				'address_line'  			   => $addressLine,
				'city_name' 				   => $cityName,
				'state'  					   => $stateProv,
				'country_name'  			   => $countryName,
				'hotel_contact_numbers'  	   => $contactNumber,
				'payment_referrance_id'  	   => isset($this->requestData['paymentReferanceId']) ?  $this->requestData['paymentReferanceId'] : null,
				'final_booking_uniqueid_type'  => $finalBookingIdType,
				'final_booking_unique_id' 	   => $finalBookingId, 
				'payment_status'			   => 'SUCCESS',
				'booking_status'			   => 'BOOKED',
				'cancel_penalty_info_text' 	   => $cancelPenaltyDesc,
				'affiliate_commission_amount'  				=> $affiliateCommisionAmnt,
				'affiliate_commission_percentage' 			=> $affiliateCommisionPercentage,
				'affiliate_commission_hotel_tax_included' 	=> $affiliateCommisionHotelTaxIncluded,

			];

			$aData 				 = [
				'metaKeyValue' => $aMetaKeyValue,
				'bookingId'	   => $this->requestData['transactionIdentifier'],
				'serviceApiId' => $this->requestData['serviceApiId'],

			];

			

			$oServiceMetakeyRepository = new ServiceMetaKeyRepository();
			$oServiceMetakeyRepository->updateYatraBookingAsConfirmed($aData);
			
			//communication to the user - mail, sms, push notification part:
			//


			}

		return [
			'yatraFinalBookingId' => $finalBookingId,
			'bookingId'		 	  => $this->requestData['transactionIdentifier'],
			'aError'			  => $aError,

		];






		#echo htmlentities($xmlRequest); die;
	}


	/*
	* Yatra API amount will be validated by their Provisional booking API. So we don't need to do
	* But We need to calculate the netAmounttoPay by considering our service charge + tax + promo code if any
	*
	*
	*/

	public function calculateNetAmountToPay($yatraAPIAmountBeforeTax, $yatraAPITaxAmount, $promoCode)
	{
		$aPromoCode  = [] ; //call to external function which manage promo code validation
		//dummy data:
		if($promoCode) {
			$aPromoCode =  [
				'status' 				=> 'SUCCESS',
				'discount'   			=> 10,
				'discount_type'			=> 1, //1 for percentage
				'promo_id'				=> 123
			];

		}



		$serviceChargePercentage 	  = 2;
		$serviceChargeTaxPercentage   = 18;

		$netAmountToPay 			= 0;
		$serviceCharge 				= 0;
		$serviceChargeTax 			= 0;
		$discountAmount 			= 0;



		

		if($yatraAPIAmountBeforeTax > 0) {
			if($serviceChargePercentage) {
				$serviceCharge 		= $yatraAPIAmountBeforeTax * ($serviceChargePercentage/100);
				$serviceChargeTax 	= 0;
				if($serviceChargeTaxPercentage) {
					$serviceChargeTax = $serviceCharge * ($serviceChargeTaxPercentage/100);
				}

			}
		}


		if(isset($aPromoCode['status']) && $aPromoCode['status'] == 'SUCCESS') {
			if($aPromoCode['discount_type'] == 1) {
				$discountAmount = $yatraAPIAmountBeforeTax * ($aPromoCode['discount']/100);
			}
			else{
				$discountAmount = $aPromoCode['discount'];
			}

			$aPromoCode['discount_amount'] = $discountAmount;

		}


		$netAmountToPay = $yatraAPIAmountBeforeTax + $yatraAPITaxAmount + $serviceCharge + $serviceChargeTax - $discountAmount;
		$serviceChargeTaxName = 'GST';

		return [
			'net_amount_to_pay'  => $netAmountToPay,
			'service_charge'	 => $serviceCharge,
			'service_charge_tax' => $serviceChargeTax,
			'promocode_details'  => $aPromoCode,
			'service_charge_tax_name' => $serviceChargeTaxName,
			'service_charge_tax_percentage' => $serviceChargeTaxPercentage,
			'service_charge_percentage' 	=> $serviceChargePercentage,
			'service_tax_name'				=> $serviceChargeTaxName,

		];
	}



	/*
	*
	*
	*
	*/

	public function formatBookingDetails($aRawData)
	{
		#print_r($aRawData);
		$aServiceBooked = $aRawData['service_booked'];
		$aMetaData 		= $aRawData['meta_data'];
		

		$aReturn 	    = [];

		#dd($aServiceBooked);
		$aReturn['booking_id'] 			= $aServiceBooked['id'];
		$aReturn['booking_status'] 		= ucwords(strtolower($aServiceBooked['status']));
		$aReturn['booked_at']			= formatDate('Y-m-d H:i:s', 'd-m-Y h:i A', $aServiceBooked['created_at']);
		
		$aformattedMetaData 			= $this->formatMetaKeyValues($aMetaData);

		$aReturn  = array_merge($aReturn, $aformattedMetaData);

		return $aReturn;
	}


	/**
	 * format Meta key values
	 *
	 * @param $response
	 */

	public function formatMetaKeyValues($aMetaData)
	{

		$aReturn = [];
		#echo '<pre>';
		#print_r($aMetaData[12]); die;
		foreach($aMetaData as $key => $aValue) {
			
			#print_r($aValue);
			#die('ffff');
			$metaKey = $aValue['meta_key'];
			$value 	 = $aValue['value'];

			switch($metaKey) {

				case "hotel_code":
				$hotelCode = $value;
				$oHotel    = YatraHotelDetail::find($hotelCode);
				$imageUrl  = isset($oHotel->ImagePath) ? $oHotel->ImagePath: null;
				$aReturn['hotel_code']  = $hotelCode;
				$aReturn['hotel_image'] = $imageUrl;


				case "hotel_name":
				case "no_of_rooms":
				
				case "rate_plan_code":
				case "room_type_code":
				case "address_line":
				case "area_name":
				case "city_name":
				case "state":
				case "country_name":
				case "customer_name_prefix":
				case "customer_first_name";
				case "final_booking_unique_id":
				case "hotel_image":
				$aReturn[$metaKey] = $value;
				break;

				
				case "stay_start_date":
				$aReturn['stay_start_date'] = formatDate('Y-m-d', 'd-m-Y', $value);
				break;

				case "stay_end_date":
				$aReturn['stay_end_date']   = formatDate('Y-m-d', 'd-m-Y', $value);
				break;

				case "guest_details":
				$aReturn['guest_details']   = json_decode($value, true);
				break;

				case "hotel_contact_numbers" : 
				$aContactNumber = json_decode($value, true);
				$aContactNumber = $aContactNumber ? $aContactNumber : [];
				$aPhone 		= [];
				foreach($aContactNumber as $_key => $_aPhone) {
					$_phoneTechType = $_aPhone['@PhoneTechType'];
					$_phoneNum 		= $_aPhone['@PhoneNumber'];
					$_areaCode 		= $_aPhone['@AreaCityCode'];
					
					$aPhone[] = [
						'type' 		   => $_phoneTechType == 5 ? 'mobile' : 'LandLine',
						'phone_number' => $_phoneNum,
						'code' 		   => $_areaCode,

					];


				}


				$aReturn['hotel_contact_number'] = $aPhone;
				break;

				case "payment_status":
				$aReturn['payment_status'] = $value;
				break;

				case "booking_status":
				$aReturn['booking_status'] = $value;
				break;

				case "cancel_penalty_info_text":
				$aReturn['cancel_penalty_info_text'] = $value;
				break;

				case "net_amount_to_pay":
				$aReturn['net_amount_to_pay'] = $value;
				break;

				



				default:
				//do nothing
				break;

			}
			#dd($aReturn);
			
		}

		return $aReturn;
	}


	/*
	*
	*
	*
	*
	*/

	#public function 


	/**
	 * Handle Response Error From Yatra
	 *
	 * @param $response
	 */
	public function handleResponseError ($response)
	{
		
		if (isset($response['Errors'])) {
			return $this->yatraErrorResponse ($response['Errors']);

		} else {
			return false;
		}
	}


	/**
	 * Handle Yatra Error Response
	 *
	 * @param $error
	 */
	public function yatraErrorResponse ($error)
	{
		if (isset($error['Error'])) {
			$errorResponse = $error['Error'];
			$message 	   = '';
			$shortMsg       = isset($errorResponse['@ShortText']) ? $errorResponse['@ShortText'] : '';
			$fullMessage 	= isset($errorResponse['$']) ? $errorResponse['$']: '';
			if($shortMsg){
				$message 		= ucfirst (strtolower (str_replace ('_' , ' ' , $errorResponse['@ShortText'])));
			}
			else if($fullMessage) {
				$message    = $fullMessage;
			}
			else{
				$message = 'Error response exists but not captured'.json_encode($errorResponse);
			}
				
		} else {
			$message = "Unknown response error";
		}

		return $message;
	}	
}
