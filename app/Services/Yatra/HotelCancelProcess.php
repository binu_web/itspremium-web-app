<?php
namespace App\Services\Yatra;


use App\XMLRequests\Hotels\Yatra\HotelProvisionalBookingRQ;
use App\XMLRequests\Hotels\Yatra\HotelFinalBookingRQ;
use App\XMLRequests\Hotels\Yatra\HotelProcessCancelRQ;
use App\XMLRequests\Hotels\Yatra\HotelConfirmCancelRQ;
use App\Services\Yatra\HotelBookingProcess;

use App\Services\HttpClient;
use App\Services\XmlToJson;
use Exception;
use App\Repositories\ApiRepository\UserApiRepository;
use App\Repositories\ApiRepository\ServiceMetaKeyRepository;
use App\Models\YatraLocation;
use App\Models\YatraHotelDetail;
use App\Http\Controllers\API\RazorPayWebHookController;
use App\Repositories\ApiRepository\ServiceLogRepository;
use JWTAuth;


/*
* Class to manage Yatra Hotel Booking Process
* @author Harinarayanan 
* @created on 13-07-2017
*
*/

class HotelCancelProcess
{

    private $requestData = [];

    public function __construct($requestData = [])
    {
        $this->requestData = $requestData;
        $this->oCancelRequestLog = new ServiceLogRepository();

    }

    public function processCancel()
    {


        $oHotelProcessCancelRq = new HotelProcessCancelRQ($this->requestData);
        $xmlRequest = $oHotelProcessCancelRq->requestXML();
        $aError = [];

        $oUser = JWTAuth::parseToken ()->toUser ();

        //start of section to insert into service log
        $aRequestLog = [
            'user_id' => $oUser->id,
            'service_id' => $this->requestData['serviceId'],
            'service_api_id' => $this->requestData['serviceApiId'],
            'event_type_id' => 5,
            'http_type' => 'REQUEST',
            'data_type' => 'XML',
            'data' => serialize($xmlRequest),
        ];

        $rRequestLog = $this->oCancelRequestLog->addServiceLog($aRequestLog);

        //end of section to insert into service log


        // Set up Rest Client Request
        $client = new HttpClient(['headers' => ['Content-Type' => 'application/xml']]);
        #echo $this->requestData['serviceUrl']; die;
        $client->setUrl($this->requestData['serviceUrl']);
        $client->setParams($xmlRequest);

        $response = $client->xmlPost();
        $xmlResponse = $client->getResponseXML();

        //start of section to insert into service log
        $aRequestLog = [
            'user_id' => $oUser->id,
            'service_id' => $this->requestData['serviceId'],
            'service_api_id' => $this->requestData['serviceApiId'],
            'event_type_id' => 5,
            'http_type' => 'RESPONSE',
            'data_type' => 'XML',
            'data' => serialize($xmlResponse),
        ];

        $rRequestLog = $this->oCancelRequestLog->addServiceLog($aRequestLog);

        //end of section to insert into service log
//        echo htmlentities($xmlResponse);

        // HandLe Error Response Check for Exception
        if ($response instanceof Exception) {
            throw new Exception($response->getMessage () , $response->getCode ());
        }


        $response          = array_get($response, 'Envelope.soap:Body.OTA_CancelRS');
        $response  		   = $response ? $response : [];


        // Handle Incase of Any Error In Response
        $error = $this->handleResponseError ($response);
        if ($error) {

            throw new Exception($error , 400);
        }



//        $aHotelCancel = array_get($response, 'Envelope.soap:Body.OTA_CancelRS');
//        $aHotelCancel = $aHotelCancel ? $aHotelCancel : [];

        if($response){

            $status = isset($response['@Status'])?$response['@Status'] : null;
            $CorrelationID = isset($response['@CorrelationID'])? $response['@CorrelationID'] : null;
            $cancelPolicyText = isset($response['Comment']['Text'])? $response['Comment']['Text'] : '';
            if($response['CancelInfoRS']['CancelRules']['CancelRule']['@CancelByDate']!= ''){
                $cancelDate = $response['CancelInfoRS']['CancelRules']['CancelRule']['@CancelByDate'];
            }
            else{
                $cancelDate = 'ANY DATE BEFORE 24 HOUR of CHECK-IN DATE';
            };
            $cancelType = isset($response['CancelInfoRS']['CancelRules']['CancelRule']['@Type'])? $response['CancelInfoRS']['CancelRules']['CancelRule']['@Type'] : '';
            $refundAmount = isset($response['CancelInfoRS']['CancelRules']['CancelRule']['@Amount'])? $response['CancelInfoRS']['CancelRules']['CancelRule']['@Amount'] : null;
            $currencyCode = isset($response['CancelInfoRS']['CancelRules']['CancelRule']['@CurrencyCode'])? $response['CancelInfoRS']['CancelRules']['CancelRule']['@CurrencyCode'] : '';


            $aMetaValue = [
                    'correlation_id' => $CorrelationID,
                    'cancel_policy_text' => $cancelPolicyText,
                    'cancel_date' => $cancelDate,
                    'cancel_type' => $cancelType,
                    'cancel_refund_amount' => $refundAmount,
                    'cancel_currency_code' => $currencyCode,
            ];

            $aData 				 = [
                'metaKeyValue' => $aMetaValue,
                'bookingId'	   => $this->requestData['transactionIdentifier'],
                'serviceApiId' => $this->requestData['serviceApiId'],
                'status' => $status,

            ];

            $oServiceMetakeyRepository = new ServiceMetaKeyRepository();
            $oServiceMetakeyRepository->updateYatraBookingAsCancelled($aData);

            $return =  [
                'aData'         => $aData,
                'bookingId'		=> $this->requestData['transactionIdentifier'],
                'aError'		=> $aError,
            ];

       }

        return $return;



    }


    public function confirmCancel()
    {

        $oHotelConfirmCancelRq = new HotelConfirmCancelRQ($this->requestData);
        $xmlRequest = $oHotelConfirmCancelRq->requestXML();
        $aError = [];

        $oUser = JWTAuth::parseToken ()->toUser ();
        //start of section to insert into service log
        $aRequestLog = [
            'user_id' => $oUser->id,
            'service_id' => $this->requestData['serviceId'],
            'service_api_id' => $this->requestData['serviceApiId'],
            'event_type_id' => 6,
            'http_type' => 'REQUEST',
            'data_type' => 'XML',
            'data' => serialize($xmlRequest),
        ];

        $rRequestLog = $this->oCancelRequestLog->addServiceLog($aRequestLog);

        //end of section to insert into service log
        /*  echo htmlentities($xmlRequest);
          die;*/

        // Set up Rest Client Request
        $client = new HttpClient(['headers' => ['Content-Type' => 'application/xml']]);
        #echo $this->requestData['serviceUrl']; die;
        $client->setUrl($this->requestData['serviceUrl']);
        $client->setParams($xmlRequest);

        $response = $client->xmlPost();
        $xmlResponse = $client->getResponseXML();

        //start of section to insert into service log
        $aRequestLog = [
            'user_id' => $oUser->id,
            'service_id' => $this->requestData['serviceId'],
            'service_api_id' => $this->requestData['serviceApiId'],
            'event_type_id' => 6,
            'http_type' => 'RESPONSE',
            'data_type' => 'XML',
            'data' => serialize($xmlResponse),
        ];

        $rRequestLog = $this->oCancelRequestLog->addServiceLog($aRequestLog);

        //end of section to insert into service log


//        echo htmlentities($xmlResponse);

        // HandLe Error Response Check for Exception
        if ($response instanceof Exception) {
            throw new Exception($response->getMessage () , $response->getCode ());
        }

        $response          = array_get($response, 'Envelope.soap:Body.OTA_CancelRS');
        $response  		   = $response ? $response : [];

        // Handle Incase of Any Error In Response
        $error = $this->handleResponseError ($response);
        if ($error) {

            throw new Exception($error , 400);
        }

//        $aHotelCancel = array_get($response, 'Envelope.soap:Body.OTA_CancelRS');
//        $aHotelCancel = $aHotelCancel ? $aHotelCancel : [];

        if($response){

            $status = isset($response['@Status'])?$response['@Status'] : null;
            $CorrelationID = isset($response['@CorrelationID'])? $response['@CorrelationID'] : null;
            $cancelConfirmText = isset($response['Comment']['Text'][0])? $response['Comment']['Text'][0] : '';
            $refundAmount = isset($response['CancelInfoRS']['CancelRules']['CancelRule']['@Amount'])? $response['CancelInfoRS']['CancelRules']['CancelRule']['@Amount'] : null;
            $cancelationUniquID = isset($response['CancelInfoRS']['UniqueID']['@ID'])? $response['CancelInfoRS']['UniqueID']['@ID'] : '';


            $aMetaValue = [
                'correlation_id' => $CorrelationID,
                'cancel_confirm_text' => $cancelConfirmText,
                'cancel_refund_amount' => $refundAmount,
                'cancel_unique_id' => $cancelationUniquID,
            ];


            $aData 				 = [
                'metaKeyValue' => $aMetaValue,
                'bookingId'	   => $this->requestData['transactionIdentifier'],
                'serviceApiId' => $this->requestData['serviceApiId'],
                'status' => $status,

            ];

            $oServiceMetakeyRepository = new ServiceMetaKeyRepository();
            $oServiceMetakeyRepository->updateYatraCancelAsConfirmed($aData);

            $return =  [
                'aData'         => $aData,
                'bookingId'		=> $this->requestData['transactionIdentifier'],
                'aError'		=> $aError,
            ];


        }

        return $return;



    }



    /**
     * Handle Response Error From Yatra
     *
     * @param $response
     */
    public function handleResponseError ($response)
    {

        if (isset($response['Errors'])) {
            $bookingProcess = new HotelBookingProcess();
            return $bookingProcess->yatraErrorResponse ($response['Errors']);

        } else {
            return false;
        }
    }

}