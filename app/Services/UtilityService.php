<?php
namespace App\Services;

class UtilityService
{
	/*
    * $inputFormat : such as 'd/m/Y', $outputFormat as 'Y-m-d'
    *
    */
    public static function formatDate($inputFormat, $outputFormat, $dateString)
    {
    	$formattedDate = null;
    	if($dateString) {
    		$datetime 		= \DateTime::createFromFormat($inputFormat, $dateString);
            if($datetime)
    		  $formattedDate 	= $datetime->format($outputFormat);
    	}
    	return $formattedDate;
    }


    public static function createRandomPassword()
    {
        $strLength  = 5;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for($i=0; $i< $strLength; $i++) {
            $randomString .= $characters[rand(0, $charactersLength-1)];
        }

        return $randomString;
    }


    public static function getDayOfDate($date)
    {
        $day  = date('D', strtotime($date));
        return $day;
    }

    public static function convertStringToTime($timeString)
    {
        return date("G:i", strtotime($timeString));
    }


     public static function validateEmailId($emailId)
    {
        $bValid     = false;
        if (filter_var($emailId, FILTER_VALIDATE_EMAIL)) {
           $bValid = true;
        }

        return $bValid;
    }

    public static function validateCustomerName($name)
    {
        $bValid     = false;
        if (preg_match("/^[a-zA-Z ]*$/",$name)) {
             $bValid = true;
        }

        return $bValid;
    }


    /*
    * returns the array is associated or numeric indexed
    * @return true if associative array 
    * @return false if sequential array
    */
    public static function checkIsAssocArray(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

}