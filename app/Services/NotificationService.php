<?php
/*
* Service to manage notification activities
* @author Harinarayanan
* @created on 27-07-2017
*/

namespace App\Services;

use App\Models\ServiceBooked;
use App\Models\NotificationType;
use App\Repositories\ApiRepository\DeviceRepository;
use App\Models\Notification;
use App\Services\PushNotification;
use Arcanedev\LogViewer\Entities\Log;
use Exception;


class NotificationService
{
	protected $deviceRepository;

	public function __construct ()
	{
		$this->deviceRepository = new DeviceRepository();
	}

	public function sendNotification ($aData)
	{
		$bookingId = $aData['booking_id'];
		$notificationType = isset($aData['type']) ? $aData['type'] : null; //service
		$serviceName = isset($aData['service']) ? $aData['service'] : null; //booking
		$title = isset($aData['title']) ? $aData['title'] : null;
		$desc = isset($aData['desc']) ? $aData['desc'] : null;
		$aCustomData = isset($aData['custom_data']) ? $aData['custom_data'] : null; //should be an array

		//set defaults if not passed:
		if (!$notificationType)
			$notificationType = 'service';
		if (!$serviceName)
			$serviceName = 'booking';


		$oServiceBooked = ServiceBooked::find ($bookingId);
		if (!$oServiceBooked)
			throw new Exception ('Booking Id not exists' , 404);

		$apiServiceId = isset($oServiceBooked->service_api_id) ? $oServiceBooked->service_api_id : null;
		$userId = isset($oServiceBooked->user_id) ? $oServiceBooked->user_id : null;

		$oNotificationType = NotificationType::select ('id')->where ('type' , '=' , $notificationType)->where ('name' , '=' , $serviceName)->first ();
		$notificationTypeId = isset($oNotificationType->id) ? $oNotificationType->id : null;


		$aNotification = [
			'title' => $title ,
			'text' => $desc ,
			'custom_data' => $aCustomData ,
		];

		//entry to notifications table

		$oNotification = new Notification;
		$oNotification->user_id = $userId;
		$oNotification->service_api_id = $apiServiceId;
		$oNotification->booking_id = $bookingId;
		$oNotification->notification_type_id = $notificationTypeId;
		$oNotification->notification_data = json_encode ($aNotification);
		$oNotification->save ();

		//send notification
		$tokens = $this->deviceRepository->getUserFCMTokens ($bookingId);

		if ($tokens) {
			$notify = PushNotification::sendNotification ($tokens , $title , $desc , $aCustomData);
		}
	}
}