<?php


namespace App\Services;

use Doctrine\DBAL\Exception\ServerException;
use GuzzleHttp\Client as GuzzleClient;
use Exception;
use Illuminate\Support\Facades\Log;

class HttpClient
{
	private $client;
	public  $url , $params , $headers;
	private $responseXml;


	/**
	 * HttpClient constructor. Load Guzzle Client
	 *
	 * @param array $config
	 */
	public function __construct ($config = [])
	{
		$this->client = new GuzzleClient($config);
	}

	/**
	 * @return mixed
	 */
	public function getUrl ()
	{
		return $this->url;
	}

	/**
	 * @param mixed $url
	 */
	public function setUrl ($url)
	{
		$this->url = $url;
	}


	/**
	 * @return mixed
	 */
	public function getParams ()
	{
		return $this->params;
	}

	/**
	 * @param mixed $params
	 */
	public function setParams ($params = '')
	{
		$this->params = $params;
	}


	/**
	 * Handle Get Query Method .
	 *
	 * @author Binoop V
	 */
	public function get ()
	{
		return $this->handleRequest ('GET' , ['query' => $this->getParams ()]);
	}

	/**
	 * Handle Post Query method
	 *
	 * @author Binoop V
	 */
	public function post ()
	{
		return $this->handleRequest ('POST' , ['form_params' => $this->getParams ()]);
	}


	/**
	 * Handle Put Query method
	 *
	 * @author Binoop V
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function put ()
	{
		return $this->handleRequest ('PUT' , ['query' => $this->getParams ()]);
	}


	/**
	 * XML Post Request Handling
	 */
	public function xmlPost ()
	{
		return $this->handleXMLRequest ('POST' , ['body' => $this->getParams ()] , true);
	}


	/**
	 * Handle All Request
	 *
	 * @param $request
	 * @param $params
	 *
	 * @author Binoop V
	 * @return \stdClass
	 */
	public function handleRequest ($request , $params , $isXML = false)
	{

		try {
			$response = $this->client->request ($request , $this->getUrl () , $params);


		}
		catch (\GuzzleHttp\Exception\ClientException $e) {

			$code = $e->getCode ();

			// Log Guzzle Http Failures
			guzzleHttpFailureLog ($e);

			list($message , $code) = processHttpFailure ($e);

			// Throw New Exception
			throw new Exception($message , $code);
		}
		catch (\GuzzleHttp\Exception\ServerException $e) {
			// Log Guzzle Http Failures
			guzzleHttpFailureLog ($e);

			list($message , $code) = processHttpFailure ($e);

			// Throw New Exception
			throw new Exception($message , $code);
		}

		// Handle response
		return $isXML ? $this->handleXMLResponse ($response) : $this->handleResponse ($response);

	}


	/**
	 * Handle All Request
	 *
	 * @param $request
	 * @param $params
	 *
	 * @author Binoop V
	 * @return \stdClass
	 */
	public function handleXMLRequest ($request , $params , $isXML = false)
	{

		try {
			$response = $this->client->request ($request , $this->getUrl () , $params);
		}
		catch (\GuzzleHttp\Exception\ClientException $e) {
			
			$code = $e->getCode ();

			// Log Guzzle Http Failures
			guzzleHttpFailureLog ($e);

			list($message , $code) = processHttpFailure ($e);

			// Throw New Exception
			return new Exception($message , $code);
		}
		catch (\GuzzleHttp\Exception\ServerException $e) {
			// Log Guzzle Http Failures
			#echo $this->getUrl (); die;
			guzzleHttpFailureLog ($e);

			list($message , $code) = processHttpFailure ($e);

			// Return  New Exception
			return new Exception($message , $code);
		}

		// Handle response
		return $isXML ? $this->handleXMLResponse ($response) : $this->handleResponse ($response);

	}

	/**
	 * Handle Response Obtained from the API
	 *
	 * @param $response
	 *
	 * @author Binoop V
	 */
	public function handleResponse ($response)
	{

		// Get Response Status Message
		$code = $response->getStatusCode ();
		$body = $response->getBody ();

		// Set Response Object
		$responseObj = new \stdClass();
		$responseObj->statusCode = $code;
		$responseObj->data = json_decode ($body);



		Log::info('response obj : '.serialize($responseObj));

		// Return Response Object
		return $responseObj;
	}


	/**
	 * Handle XML Response Obtained from the API
	 *
	 * @param $response
	 *
	 * @author Binoop V
	 */
	public function handleXMLResponse ($response)
	{

		// Get Response Status Message

		#dd($response);
		
		$code = $response->getStatusCode ();
		$body = $response->getBody ();

		//return $body;
		$bodyContent = $body->getContents ();
		//dd($bodyContent) ; die;
		$responseObj = XmlToJson::get ($bodyContent);
		
		$this->responseXml = $bodyContent;
		// Return Response Object
		return $responseObj;
	}


	/*
	*
	*
	*
	*
	*/

	public function getResponseXML()
	{
		return $this->responseXml;
	}
}