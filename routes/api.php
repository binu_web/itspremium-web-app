<?php

use Illuminate\Http\Request;


/**
 *  Requests not requiring Token Based Authentication
 */

// Login Api
Route::post ('authenticate' , '\App\Http\Controllers\API\AuthenticateController@authenticate')->name ('authenticate');

// Register Device
Route::post ('register-device' , '\App\Http\Controllers\API\AuthenticateController@deviceRegisteration')->name ('deviceRegisteration');

// Register User
Route::post ('user/register' , '\App\Http\Controllers\API\UserController@register')->name ('user-create');

// Verify the OTP came from device
Route::post ('user/verify' , '\App\Http\Controllers\API\UserController@verifyOtp')->name ('user-verify');

// Resend the OTP
Route::any ('user/resend-otptoken' , '\App\Http\Controllers\API\UserController@resendOtp')->name ('resend-otptoken');

// Forget password
Route::post ('user/forget-password' , '\App\Http\Controllers\API\UserController@forgetPassword')->name ('forget-password');

// Update Password
Route::post ('user/reset-password' , '\App\Http\Controllers\API\UserController@resetPassword')->name ('reset-password');

// Show user accounts
Route::get ('user/accounts' , '\App\Http\Controllers\API\UserController@accounts')->name ('accounts')->middleware ('api.auth');

Route::post('user/accounts/disconnect', '\App\Http\Controllers\API\UserController@disconnectAccount')->name('disconect-account')->middleware('api.auth');

// Show user booking details
Route::get ('user/bookings' , '\App\Http\Controllers\API\UserController@bookingHistory')->name ('bookings')->middleware ('api.auth');

// Show user booking details
Route::get ('user/booking-details' , '\App\Http\Controllers\API\UserController@bookingDetails')->name ('booking-details')->middleware ('api.auth');

// Add user user feedbacks
Route::post ('user/feedback' , '\App\Http\Controllers\API\UserController@postFeedback')->name ('feedback')->middleware ('api.auth');

// edit user profile
Route::get ('user/get-profile' , '\App\Http\Controllers\API\UserController@getProfile')->name ('get-profile')->middleware ('api.auth');

// get user notifications
Route::get ('user/get-notifications' , '\App\Http\Controllers\API\UserController@getNotifications')->name ('get-notifications')->middleware ('api.auth');

// delete notifications
Route::any ('user/delete-notifications' , '\App\Http\Controllers\API\UserController@deleteNotifications')->name ('delete-notifications')->middleware ('api.auth');

Route::get('user/get-booking-details', '\App\Http\Controllers\API\UserController@getBookingDetails')->name('get-booking-details')->middleware('api.auth');

Route::get('user/profile', '\App\Http\Controllers\API\UserController@editProfile')->name('edit-user-profile')->middleware('api.auth');
Route::post('user/profile', '\App\Http\Controllers\API\UserController@updateProfile')->name('update-user-profile')->middleware('api.auth');


Route::group(['namespace' => 'API', 'middleware' => 'api.auth'], function () {
	Route::get('promo-code/getlist', 'PromoCodeController@getAllPromoCodes');
	Route::get('promo-code/get-service-promo-codes/{service_id}', 'PromoCodeController@getServicePromoCodes');

});

Route::get('user/get-wallet-details', '\App\Http\Controllers\API\UserController@getWalletDetails')->name('get-wallet-details')->middleware('api.auth');



/**
 * Requests requiring Token Baed Authentication
 */
Route::get ("token" , '\App\Http\Controllers\API\AuthenticateController@getToken')->middleware ('api.auth');


// Cab Module
Route::group (['namespace' => 'API\Cabs' , 'prefix' => 'cab'] , function () {

	// No Token based Authentication Call
	Route::get ('webhook/{service}' , 'CabController@webhookCall');

	// Cab Service Redirect URL Actions
	Route::get ('access-token' , function () {
		return view ('api.cab.access_token_handle');
	})->name ('view.cab.access_token_handle');

	// Handle Oauth Access Token For User
	Route::get ('handle/access-token' , 'CabController@token')->name ('api.cab.access_token_handle');


	// OAuth Registeration For Cab Services
	Route::get ('authorize/{service}' , 'CabController@authorize')->middleware ('api.auth');


	// Favorite Locations
	Route::get ('location/favorites' , 'CabController@favorites')->name ('favorites')->middleware ('api.auth');

	// Set Favorite Locations
	Route::post ('location/set-favorite' , 'CabController@setFavorite')->name ('setFavorite')->middleware ('api.auth');

	//Common route to cab ride search
	Route::get ('ride-search' , '\App\Http\Controllers\API\Cabs\CabController@rideSearch')->name ('ride-search')->middleware ('api.auth');

	//get ride details - specific cab details with price information

	Route::get('ride-details', '\App\Http\Controllers\API\Cabs\CabController@getRideDetails')->middleware('api.auth');


	//Common route to cab book
	Route::get ('book' , '\App\Http\Controllers\API\Cabs\CabController@book')->name ('book')->middleware ('api.auth');

	//Common route to cab track
	Route::get ('track' , '\App\Http\Controllers\API\Cabs\CabController@track')->name ('track')->middleware ('api.auth');

	//Common Cancel Booking
	Route::get ('cancel' , '\App\Http\Controllers\API\Cabs\CabController@cancel')->name ('cancel')->middleware ('api.auth');

	//Common route to cab ridelater
	Route::any ('ride-later' , '\App\Http\Controllers\API\Cabs\CabController@rideLater')->name ('rideLater')->middleware ('api.auth');

	//
	Route::any('update-destination', '\App\Http\Controllers\API\Cabs\CabController@updateDestination')->name('updateDestination')->middleware('api.auth');

	//Common route to cab history
	Route::get ('history' , '\App\Http\Controllers\API\Cabs\CabController@history')->name ('history')->middleware ('api.auth');

	//Common route to cab history details
	Route::get ('history-details' , '\App\Http\Controllers\API\Cabs\CabController@historyDetails')->name ('historyDetails')->middleware ('api.auth');

	//Check ongoing rides
	Route::get ('check-ongoing-ride' , '\App\Http\Controllers\API\Cabs\CabController@checkOngoingRide')->name ('checkOngoingRide')->middleware ('api.auth');

	//Get cancel reasons for ola
	Route::get ('cancel-reasons' , '\App\Http\Controllers\API\Cabs\CabController@getCancelReasons')->name ('getCancelReasons')->middleware ('api.auth');

});


/**
 *  News Service
 */
Route::group (['namespace' => 'API\News' , 'prefix' => 'news'] , function () {

	//Route to display news category api
	Route::get ('news-category' , '\App\Http\Controllers\API\NewsController@category')->name ('category');

	//Route to display all news and category wise news
	Route::get ('news-list' , '\App\Http\Controllers\API\NewsController@newsList')->name ('list');

	//Route to display single news
	Route::get ('news/{id?}' , '\App\Http\Controllers\API\NewsController@news')->name ('single');

	// set news bookmark
	Route::get ('set-news-bookmark' , '\App\Http\Controllers\API\NewsController@setNewsBookmark')->name ('setNewsBookmark')->middleware ('api.auth');

	// get news bookmark
	Route::get ('news-bookmark-list' , '\App\Http\Controllers\API\NewsController@getBookNewsList')->name ('getBookNewsList')->middleware ('api.auth');

	// get news log
	Route::get ('news-log' , '\App\Http\Controllers\API\NewsController@setNewsLog')->name ('setNewsLog')->middleware ('api.auth');

});

/**
 *  CAB Services
 */
Route::group (['namespace' => 'API\Cab' , 'prefix' => 'cab'] , function () {

	//Common route to cab ride search
	Route::get ('ride-search' , '\App\Http\Controllers\API\Cabs\CabController@rideSearch')->name ('ride-search')->middleware ('api.auth');

	//Common route to cab book
	Route::any ('book' , '\App\Http\Controllers\API\Cabs\CabController@book')->name ('book')->middleware ('api.auth');

	//Common route to cab track
	Route::any ('track' , '\App\Http\Controllers\API\Cabs\CabController@track')->name ('track')->middleware ('api.auth');

	//Common route to cab cancel
	Route::any ('cancel' , '\App\Http\Controllers\API\Cabs\CabController@cancel')->name ('cancel')->middleware ('api.auth');

	//Common route to cab history
	Route::get ('history' , '\App\Http\Controllers\API\Cabs\CabController@history')->name ('history')->middleware ('api.auth');

	//Common route to cab history details
	Route::get ('history-details' , '\App\Http\Controllers\API\Cabs\CabController@historyDetails')->name ('historyDetails')->middleware ('api.auth');

	//Common route to cab ridelater
	Route::any ('ride-later' , '\App\Http\Controllers\API\Cabs\CabController@rideLater')->name ('rideLater');

});


/**
 * OLA CAB SERVICE
 */
Route::group (['namespace' => 'API\Cabs' , 'prefix' => 'cab/ola'] , function () {
	Route::any ('webhook' , 'OlaController@webhook');
});

/**
 * Uber CAB SERVICE
 */

Route::group (['namespace' => 'API\Cabs' , 'prefix' => 'cab/uber'] , function () {
	Route::any ('webhook' , 'UberController@webhook');
});


/**
 *  Notification Service
 */
Route::group (['namespace' => 'API\Notification' , 'prefix' => 'notification'] , function () {

	//Route to set notification
	Route::get ('set-notification' , '\App\Http\Controllers\API\NotificationTypeController@setNotification')->name ('setNotification');
	Route::get ('list-notification' , '\App\Http\Controllers\API\NotificationTypeController@listNotification')->name ('listNotification');

});


/**
 * Restaurants Service
 *
 */
Route::group (['namespace' => 'API\Restaurants' , 'prefix' => 'restaurant'] , function () {

	//Find the locations from the service
	Route::get ('locations' , '\App\Http\Controllers\API\Restaurants\RestaurantController@locations')->name ('restaurant-locations');

	// Find the type of restaurant
	Route::get ('category' , '\App\Http\Controllers\API\Restaurants\RestaurantController@restaurantCategory')->name ('restaurant-category');

	//Find the restaurants from the service
	Route::get ('list' , '\App\Http\Controllers\API\Restaurants\RestaurantController@restaurantSearch')->name ('restaurant-search')->middleware ('api.auth');

	//Find the restaurant details
	Route::get ('details' , '\App\Http\Controllers\API\Restaurants\RestaurantController@restaurantDetails')->name ('restaurant-details');

	//Find the restaurant daily menu details
	Route::get ('dailymenu' , '\App\Http\Controllers\API\Restaurants\RestaurantController@restaurantDailyMenu')->name ('restaurant-dailymenu');

	//Get Restarunt Reviews
	Route::get ('reviews' , '\App\Http\Controllers\API\Restaurants\RestaurantController@restaurantReview')->name ('restaurant-review');

	//Generate booking for restaurant
	Route::any ('generate-booking' , '\App\Http\Controllers\API\Restaurants\RestaurantController@restaurantGenerateBooking')->name ('generate-booking')->middleware ('api.auth');

	//Confirm booking for restaurant
	Route::any ('confirm-booking' , '\App\Http\Controllers\API\Restaurants\RestaurantController@confirmBooking')->name ('confirm-booking')->middleware ('api.auth');

	//cancel booking for restaurant
	Route::any ('cancel-booking' , '\App\Http\Controllers\API\Restaurants\RestaurantController@cancelBooking')->name ('cancel-booking')->middleware ('api.auth');

	//Get time slots of restaurant
	Route::any ('get-timeslots' , '\App\Http\Controllers\API\Restaurants\RestaurantController@getRestaurantSlots')->name ('get-timeslots')->middleware ('api.auth');

	//Get diner booking details
	Route::any ('get-booking-details' , '\App\Http\Controllers\API\Restaurants\RestaurantController@getDinerBookingDetails')->name ('get-booking-details')->middleware ('api.auth');

	//Get restaurant log history details
	Route::get ('get-log-history' , '\App\Http\Controllers\API\Restaurants\RestaurantController@getLogHistory')->name ('get-log-history')->middleware ('api.auth');

	//Get restaurant log history details
	Route::post ('set-favourite' , '\App\Http\Controllers\API\Restaurants\RestaurantController@setFavorite')->name ('set-favourite')->middleware ('api.auth');

	//Get restaurant log history details
	Route::get ('favourite-locations' , '\App\Http\Controllers\API\Restaurants\RestaurantController@getFavoriteLocations')->name ('favoriteLocations')->middleware ('api.auth');

	//Get restaurant log history details
	Route::get ('get-confirm-details' , '\App\Http\Controllers\API\Restaurants\RestaurantController@getConfirmDetails')->name ('getConfirmDetails')->middleware ('api.auth');

});


/**
 * DINEOUT WEBHOOK USER
 */
Route::group (['namespace' => 'API\Restaurants' , 'prefix' => 'restaurant/dineout'] , function () {
	Route::any ('webhook' , 'DineoutController@webhook');
});



Route::get ('soap/show' , '\App\Http\Controllers\API\SoapController@show')->name ('show');


/**
 * Hotel Web API Service
 */
Route::group (['namespace' => 'API\Hotels' , 'prefix' => 'hotel', 'middleware' => 'api.auth'] , function () {

	Route::get('search/get-last-search-params', '\App\Http\Controllers\API\Hotels\HotelController@getLastSearchParams');
	Route::get ('search/location' , '\App\Http\Controllers\API\Hotels\HotelController@searchLocation');
	Route::get ('search' , '\App\Http\Controllers\API\Hotels\HotelController@getHotels');
	Route::get ('search/location' , '\App\Http\Controllers\API\Hotels\HotelController@searchLocation');
	Route::get ('get-details' , '\App\Http\Controllers\API\Hotels\HotelController@getHotelDetails');
	Route::any('process-booking', '\App\Http\Controllers\API\Hotels\HotelController@processProvisionalBooking'); // acutlly these routes are POST methods
	Route::post('confirm-booking', '\App\Http\Controllers\API\Hotels\HotelController@processFinalBooking'); // acutlly these routes are POST methods

	Route::get('booking/get-status', '\App\Http\Controllers\API\Hotels\HotelController@getBookingStatus');
	Route::any('process-cancel', '\App\Http\Controllers\API\Hotels\HotelController@processCancel');// acutlly these routes are POST methods
	Route::any('confirm-cancel', '\App\Http\Controllers\API\Hotels\HotelController@confirmCancel');// acutlly these routes are POST methods
	Route::post('set-as-favourite-search', '\App\Http\Controllers\API\Hotels\HotelController@setAsFavouriteSearch');
	Route::get('get-search-log', '\App\Http\Controllers\API\Hotels\HotelController@getSearchLog');
	Route::get('booking/get-status', '\App\Http\Controllers\API\Hotels\HotelController@getBookingStatus');
	Route::get('booking/history', '\App\Http\Controllers\API\Hotels\HotelController@getBookingHistory');


});

Route::group(['namespace' => 'API', 'prefix' => 'razorpay'], function () {
	Route::post('receive-webhook', 'RazorPayWebHookController@receiveWebHook');
	Route::any('request-refund', 'RazorPayWebHookController@requestRefund')->name('request-refund')->middleware('api.auth');;

});


Route::group (['namespace' => 'API\Hotels' , 'prefix' => 'hotel'] , function () {
	Route::get ('search/location' , '\App\Http\Controllers\API\Hotels\HotelController@searchLocation');
	Route::post('confirm-booking', '\App\Http\Controllers\API\Hotels\HotelController@processFinalBooking'); // bcs it is coming from webhook
	

});



/*
* Flights APIs
*
*/

Route::group (['namespace' => 'API\Flights' , 'prefix' => 'flight'] , function () {
	Route::get('get-airports', '\App\Http\Controllers\API\Flights\FlightController@getAirPorts');
	Route::get('search', '\App\Http\Controllers\API\Flights\FlightController@searchFlights');
});

