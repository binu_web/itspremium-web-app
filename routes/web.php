<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});


Route::get('/test', function () {
    echo route('deviceRegisteration');
});

Route::post('/newsFilter/add_news', 'NewsFilter@add_news');

Route::post('/news/select_news_by_id', 'NewsController@SelectNewsById');

Route::resource('newsFilter', 'NewsFilter@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('pages', 'PageController');

Route::resource('users', 'UsersController');

Route::resource('admins', 'AdminController');

Route::resource('adminUserTypes', 'AdminUserTypeController');

Route::resource('adminUserLogs', 'AdminUserLogController');

Route::resource('adminLogTypes', 'AdminLogTypeController');

Route::resource('countries', 'CountryController');

Route::resource('locations', 'LocationController');

Route::resource('locationServices', 'LocationServiceController');

Route::resource('news', 'NewsController');

Route::resource('newsCategories', 'NewsCategoryController');

Route::resource('newsSources', 'NewsSourceController');

Route::resource('paymentMethodCountries', 'PaymentMethodCountryController');

Route::resource('paymentMethods', 'PaymentMethodController');

Route::resource('serviceApis', 'ServiceApiController');

Route::resource('serviceMetaDatas', 'ServiceMetaDataController');

Route::resource('serviceMetaKeys', 'ServiceMetaKeyController');

Route::resource('services', 'ServiceController');

Route::resource('serviceBookeds', 'ServiceBookedController');

Route::resource('transactions', 'TransactionController');

Route::resource('userLogs', 'UserLogController');

Route::resource('feedback', 'FeedbackController');

Route::resource('promocode', 'PromoCodeController');

Route::resource('promocode_service_map', 'PromocodeServiceMapController');

Route::resource('hotel_refund', 'HotelRefundController');

Route::resource('user_refund', 'UserRefundController');


//Route::post('store', ['as' => 'users.store', 'uses' => 'UsersController@store']);

//Route::get('store','\App\Http\Controllers\UsersController@store');
Route::resource('notification', 'NotificationController');


Route::resource('devices', 'DeviceController');
Route::get('razorpay/test', 'RazorPayTestController@testform');
Route::post('razorpay/test', 'RazorPayTestController@submitForm');

Route::any('hotel_refund/refund', ['as' => 'full_refund', 'HotelRefundController@index']);

Route::any('hotel_refund/refund/{id}', 'HotelRefundController@refund');

Route::any('hotel_refund/partial_refund/',  'HotelRefundController@partialRefund');









