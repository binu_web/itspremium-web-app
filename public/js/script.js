/**
 * Created by binu on 3/1/2017.
 */
'use strict';

var Common = Common || {};

(function (Common) {

    Common.defaults = {
        'modalTitle': "",
        'modalBody': "",
        'modalActionText': "Save Changes",
    }


    // Elements
    Common.elements = {
        // Modal Elements
        'modal': '#common-modal',
        'modalTitle': '#common-modal-title',
        'modalBody': '#common-modal-body',
        'modalAction': '#btn-common-action',
    }


    // Init Common Modal
    Common.initModal = function () {
        $(this.elements.modalTitle).html(this.defaults.modalTitle)
        $(this.elements.modalBody).html(this.defaults.modalBody);
        $(this.elements.modalAction).html(this.defaults.modalActionText);
        $(this.elements.modal).modal('show');
    }

})(Common);


