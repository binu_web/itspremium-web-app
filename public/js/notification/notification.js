'use strict';

var Notification = Notification || {};

(function (Notification) {
    Notification.elements = {
        form: '#frm-notification',
        btnSubmit: '#btnSendNotification',
        errorAlert: '#alertError',
        successAlert: '#successAlert',
    }

    // Initialize
    Notification.init = function () {
        this.bindControls();
    }

    // Bind Controls
    Notification.bindControls = function () {

        // Validate Forms
        this.validateForm();


        // Form Submission Validation
        $(this.elements.form).submit(function (e) {
            e.preventDefault();

            // Init Modal
            Notification.initModal();

        });

        // Common Modal Action Button
        $(Common.elements.modalAction).click(function (e) {
            Notification.formSubmit();
        });

    }

    // Validate Form
    Notification.validateForm = function () {
        $.validate({
            form: this.elements.form
        });
    }

    // Default Notification Modal
    Notification.initModal = function () {
        Common.defaults.modalTitle = "Confirm Action";
        Common.defaults.modalBody = "Do you wish to proceed sending the notification ? ";
        Common.defaults.modalActionText = "Send";
        Common.initModal();
    }

    // Form Submit
    Notification.formSubmit = function () {
        $.ajax({
            url: $(this.elements.form).attr('action'),
            data: $(this.elements.form).serialize(),
            type: 'POST',
            dataType: 'JSON',
            complete: function () {
                $(Common.elements.modal).modal('toggle')
            },
            success: function () {
                $(Notification.elements.successAlert).removeClass('hide')
                $(Notification.elements.form)[0].reset();
                setTimeout(function () {
                    $(Notification.elements.successAlert).addClass('hide');
                }, 2000)
            },
            error: function (data) {
                $(Notification.elements.errorAlert).html(data.responseJSON.msg).removeClass('hide');
                $(Notification.elements.form)[0].reset();

                setTimeout(function () {
                    $(Notification.elements.errorAlert).addClass('hide');
                }, 2000)
            }
        })
    }

})(Notification);


// Define Call


Notification.init();